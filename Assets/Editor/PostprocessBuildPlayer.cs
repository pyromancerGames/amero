﻿using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
using System.Collections;
using System.IO;
using System.Diagnostics;

public class PostprocessBuildPlayer
{
	
	[PostProcessBuild]
	public static void OnPostprocessBuild(BuildTarget target, string pathToBuiltProject)
	{
#if UNITY_IPHONE
		UnityEngine.Debug.Log("OnPostProcessBuild -  iOS build") ;
			// Copy files
		FileUtil.CopyFileOrDirectory ("Assets/Plugins/iOS/Podfile", pathToBuiltProject + "/Podfile");
		FileUtil.CopyFileOrDirectory ("Assets/Plugins/iOS/pods.sh", pathToBuiltProject + "/pods.sh");

		ProcessStartInfo psi = new ProcessStartInfo(); 
		psi.FileName = pathToBuiltProject + "/pods.sh";
		psi.UseShellExecute = false; 
		psi.RedirectStandardOutput = true;
		psi.Arguments = pathToBuiltProject;
		
		Process p = Process.Start(psi); 
		string strOutput = p.StandardOutput.ReadToEnd(); 
		p.WaitForExit(); 
		UnityEngine.Debug.Log(strOutput);

		updateXcodeProject(target, pathToBuiltProject);
#else
		UnityEngine.Debug.Log("OnPostProcessBuild - Warning: This is not an iOS build") ;
#endif
	}

	public static void updateXcodeProject(BuildTarget target, string pathToBuiltProject)
	{
#if UNITY_IPHONE
		if(target == BuildTarget.iOS)
		{
			string projectFile = pathToBuiltProject+"/Unity-iPhone.xcodeproj/project.pbxproj";
			string contents = File.ReadAllText(projectFile);
			
			// Replace Flags
			contents = contents.Replace("-weak-lSystem", "$(inherited)");
			File.WriteAllText(projectFile, contents);
		}
#endif
	}


}