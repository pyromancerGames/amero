﻿using UnityEngine;
using System.Collections;

public class PinballPlanckController : MonoBehaviour
{

	private HingeJoint2D joint;
	private JointMotor2D originalMotor;
	private JointAngleLimits2D originalLimits;


	void Start ()
	{
		joint = GetComponent<HingeJoint2D> ();
		originalMotor = joint.motor;
		originalLimits = joint.limits;
	}


	public void Shot ()
	{
		JointMotor2D motor = joint.motor;
		JointAngleLimits2D limits = joint.limits;
		limits.min = -45;
		motor.motorSpeed = -150;
		motor.maxMotorTorque = 10000;
		joint.motor = motor;
		joint.limits = limits;
	}

	private void SetOriginalPreferences ()
	{
		joint.motor = originalMotor;
		joint.limits = originalLimits;
	}

	void Update ()
	{
		if (joint.limitState == JointLimitState2D.LowerLimit)
			SetOriginalPreferences ();
	}
}
