﻿using UnityEngine;
using System.Collections;

public class LaserShutdown : MonoBehaviour
{

	public LaserFireController laser;
	public bool enableShutdownOnHit = true;

	void OnCollisionEnter2D (Collision2D collInfo)
	{
		if (!enableShutdownOnHit)
			return;

		if (collInfo.relativeVelocity.magnitude >= 5) {
			laser.DisableLaser ();
		}
	}
}
