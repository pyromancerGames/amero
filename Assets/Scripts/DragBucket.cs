﻿using UnityEngine;
using System.Collections;

public class DragBucket : MonoBehaviour {

	public LayerMask bucketLayer;
	public float speed = 0.016f;

	private Vector3 currentPosition;
	private Vector3  lastPositon; 
	private Vector3  deltaPositon;

	void Update() {

		if (Application.platform == RuntimePlatform.WebGLPlayer || Application.platform == RuntimePlatform.WindowsEditor) {

			if (Input.GetKeyDown(KeyCode.LeftArrow))
			{
				Vector3 position = this.transform.position;
				position.x--;
				this.transform.position = position;
			}
			if (Input.GetKeyDown(KeyCode.RightArrow))
			{
				Vector3 position = this.transform.position;
				position.x++;
				this.transform.position = position;
			}
		} else {
			if (Input.touchCount > 0) {
				Collider2D collider2d = Physics2D.OverlapCircle (Camera.main.ScreenToWorldPoint (new Vector2 (Input.GetTouch (0).position.x, Input.GetTouch (0).position.y)), 1f, bucketLayer);
				if (collider2d) {
					if (Input.GetTouch (0).phase == TouchPhase.Moved) {
						Vector2 touchDeltaPosition = Input.GetTouch (0).deltaPosition;
						transform.Translate (touchDeltaPosition.x * speed, 0, 0);
					}
				}
			}
		}


	}
}
