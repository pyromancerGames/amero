﻿using UnityEngine;
using System.Collections;

public class AutoMovement : MonoBehaviour {

	public float force = 10f;
	public float x;
	public float y;

	private Rigidbody2D body;

	void Start () {
		body = GetComponent<Rigidbody2D> ();
		body.AddForce (new Vector2 (x, y) * force);
	}

}
