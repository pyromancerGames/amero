﻿using UnityEngine;
using System.Collections;

/**
 * FAN MOVEMENT SCRIPT
 * ===================
 * 
 * Moves a hinge in fan manner. It must be applied to any GameObject that has a HingeJoint2D component
 * Uses Configured Motorspeed and Hinge Limits.
 */

public class HingeJointYoyo : MonoBehaviour {

	public bool useMotor = true;
	private HingeJoint2D hinge;
	private JointMotor2D motor;

	void Start () {
		hinge = GetComponent<HingeJoint2D> ();
		motor = hinge.motor;
	}

	void FixedUpdate () {
		// Ensure limits are back turned on.
		if (useMotor) {
			hinge.useLimits = true;
			if (hinge.limitState == JointLimitState2D.LowerLimit || hinge.limitState == JointLimitState2D.UpperLimit) {
				// Turn off limits to allow the barrel to bounce back this frame
				hinge.useLimits = false;
				motor.motorSpeed *= -1;
				hinge.motor = motor;
			}
		}
	}
}
