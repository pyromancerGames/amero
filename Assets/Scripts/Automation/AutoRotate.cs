﻿using UnityEngine;
using System.Collections;

public class AutoRotate : MonoBehaviour {

	public float roatationSpeed = 10f;
	public bool autoRotationEnabled = true;
	
	void FixedUpdate() {
		Vector3 rVector = new Vector3 (0, 0, roatationSpeed);
		transform.Rotate (rVector);
	}
}
