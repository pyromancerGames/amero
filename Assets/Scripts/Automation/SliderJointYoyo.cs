﻿using UnityEngine;
using System.Collections;

public class SliderJointYoyo : MonoBehaviour {

	private SliderJoint2D joint;
	private JointMotor2D motor;

	void Start(){
		joint = GetComponent<SliderJoint2D> ();
		motor = joint.motor;
	}

	void FixedUpdate(){
		joint.useLimits = true;
		if (joint.limitState == JointLimitState2D.LowerLimit || joint.limitState == JointLimitState2D.UpperLimit) {
			joint.useLimits = false;
			motor.motorSpeed *= -1;
			joint.motor = motor;
		}
	}
}
