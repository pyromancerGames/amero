﻿using UnityEngine;
using System.Collections;

public class FollowObject : MonoBehaviour {

	public GameObject followedObject;
	public bool followX = false;
	public bool followY = false;

	private Transform otherTransform;

	void Start () {
		otherTransform = followedObject.GetComponent<Transform> ();
	}

	void FixedUpdate () {
		float newX = 0;
		float newY = 0;

		if (followX)
			newX = otherTransform.position.x;
		else
			newX = transform.position.x;

		if (followY)
			newY = otherTransform.position.y;
		else
			newY = transform.position.y;

		transform.position = new Vector2 (newX, newY);
		
	}
}
