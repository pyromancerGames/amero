﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FireController: MonoBehaviour {

	#region Variables
	public GameObject ball;
	public Image powerFiller;
	public Transform ballSpawnPoint;
	public float baseVelocity = 6f;
	public float maxVelocity = 18f;

	private AudioSource powerChargeFx;
	public string powerChargeAudioName;
	private AudioSource fireFx;
	public string fireFxAudioName;

	public bool enablePowerFire = true;
	public bool ballFired = false;

//	public LayerMask whatIsFireButton;
	public Button fireButton;

	private bool gatherPower = false;
	private float chargeTime = 0f;

	private Animator fireAnimator;
	#endregion


	#region Events
	public delegate void FireAction();
	public static event FireAction OnBallFire;
	#endregion



	#region MonoBehaviour
	void Start () {
		// Get Roll and thud effects
		foreach (AudioSource audiofx in GetComponents<AudioSource>()){
			if (audiofx.clip.name == powerChargeAudioName)
				powerChargeFx = audiofx;

			if (audiofx.clip.name == fireFxAudioName)
				fireFx = audiofx;
		}

		// Animator
		fireAnimator = GetComponentInChildren<Animator> ();
	}

	void Update() {
		if (gatherPower && !ballFired) {
			if (powerFiller) powerFiller.fillAmount += Time.deltaTime / 2;
			if (chargeTime == 0) powerChargeFx.Play(); // Play Charging sound only when starting to charge
			chargeTime += Time.deltaTime;
		}
		
	}

	void OnEnable () {
		ContainerController.OnBallCollect += ResetBall;
		SweeperController.OnSweep += ResetBall;
	}
	
	void OnDisable () {
		ContainerController.OnBallCollect -= ResetBall;
		SweeperController.OnSweep -= ResetBall;
	}
	#endregion


	#region CustomBehaviour
	public void setBallObject(Object newBallPrefab){
		this.ball = newBallPrefab as GameObject;
	}

	void ResetBall () {
		ballFired = false;
	}

	public void AccumulatePower(){

		if (enablePowerFire) 
			gatherPower = true;
		else 
		{
			chargeTime = 1;
			if (powerFiller) powerFiller.fillAmount = 0;
		}
	}

	public void ReleaseFire() {
		gatherPower = false;

		if (!ballFired) {
			powerChargeFx.Stop(); //Stop charging sound
			fireFx.Play(); // Play fire sound
			Fire();
			fireAnimator.SetTrigger ("FireCannon");
		}

		chargeTime = 0;
		if (powerFiller) powerFiller.fillAmount = 0;
	}
	

	void Fire() {

		GameObject ballClone = Instantiate (ball, ballSpawnPoint.position, transform.rotation) as GameObject;
		Rigidbody2D ballBody = ballClone.GetComponent<Rigidbody2D> ();
		
		if (baseVelocity * chargeTime > maxVelocity) {
			ballBody.velocity = transform.right * maxVelocity;
		} else {
			ballBody.velocity = transform.right * baseVelocity * (chargeTime + 1);
		}

		// Broadcast Event
		OnBallFire ();
		ballFired = true;
	}
	#endregion
}
