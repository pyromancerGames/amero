﻿using UnityEngine;
using System.Collections;

public class DisabledOnPC : MonoBehaviour {

	// Use this for initialization
	void Start () {
		if (Application.platform == RuntimePlatform.WebGLPlayer) {
			gameObject.SetActive(false);
		}
	}
}
