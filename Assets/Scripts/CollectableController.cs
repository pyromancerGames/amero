﻿using UnityEngine;
using System.Collections;


/**
 * COLLECTABLE CONTROLLER
 * =============================
 * 
 * Assignable to any object that has a trigger collider.
 * Will turn that object into a collectable object.
 * Raises an OnCollect Event and passes collided object.
 * 
 */

public class CollectableController : MonoBehaviour
{

	public AudioClip clip;

	public delegate void CollectableAction (Collider2D other);
	public static event CollectableAction OnCollect;


	void OnTriggerEnter2D (Collider2D hittingObject)
	{

		Collider2D collectable = GetComponent<Collider2D> ();

		if (hittingObject.tag == "Ball") {
			// Plays the collect sound.
			if (clip) {
				AudioSource.PlayClipAtPoint (clip, collectable.transform.position);
			}
			
			
			if (OnCollect != null)
				OnCollect (collectable);
			
			
			// DESTROY COLLECTED OBJECT
			if (collectable.gameObject.transform.parent) {
				Destroy (collectable.gameObject.transform.parent.gameObject);
			} else {
				Destroy (collectable.gameObject);
			}
		}

	}

}
