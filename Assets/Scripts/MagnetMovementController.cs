﻿using UnityEngine;
using System.Collections;


public class MagnetMovementController : MonoBehaviour {
	
	public GameObject magnet;
	private SliderJoint2D joint;
	private JointMotor2D motor;
	private AreaEffector2D areaEfector;
	public GameController gameController;
	public LayerMask buttons;

	void Start() {
		// Joint
		joint = magnet.GetComponent<SliderJoint2D> ();
		motor = joint.motor;
		// Area efector
		areaEfector = magnet.GetComponent<AreaEffector2D> ();
		areaEfector.enabled = false;
	}

	void Update() {

		//int fingerCount = 0;
		/*
		foreach (Touch touch in Input.touches) {
			Collider2D collInfo = Physics2D.OverlapCircle (Camera.main.ScreenToWorldPoint (touch.position), 0.2f, buttons);
			if (collInfo) {
				fingerCount++;
				areaEfector.enabled = true;
			}
		}

		if(fingerCount == 0){
			areaEfector.enabled = false;
		}*/

		if (Input.GetKeyDown ("space")) {
			areaEfector.enabled = true;
		}
		if (Input.GetKeyUp ("space")) {
			areaEfector.enabled = false;
		}
		
		// Mouse input recognition - For desktop debugging purposes
		if (Input.GetMouseButtonDown (0)) {
			Vector3 mouseCurrPosition = Input.mousePosition;
			mouseCurrPosition.z = 1;
			Collider2D collInfo = Physics2D.OverlapCircle (Camera.main.ScreenToWorldPoint (mouseCurrPosition),0.2f, buttons);
			if (collInfo) {
				areaEfector.enabled = true;
			}
		}
		if (Input.GetMouseButtonUp (0)) {
			areaEfector.enabled = false;
		}
	}
	
	
	void OnTriggerEnter2D(Collider2D other){
		if (other.transform.tag == "Ball") {
			joint.useLimits = false;
			motor.motorSpeed = 100;
			joint.motor = motor;
		}
	}

	void OnTriggerStay2D(Collider2D other) {
		if (other.transform.tag == "Ball") {
			joint.useLimits = true;
			motor.motorSpeed = 100;
			joint.motor = motor;
		}
	}
	
	void OnTriggerExit2D(Collider2D other) {
		if (other.transform.tag == "Ball") {
			gameController.BallFired();
			joint.useLimits = true;
			motor.motorSpeed = -5;
			joint.motor = motor;
		}
	}
}
