﻿using UnityEngine;
using System.Collections;

public class TargetsManager : MonoBehaviour {

	public GameController gameController;
	private int totalTargets = 0;
	public bool randomTargets = false;
	public float randomTargetSpawnTime = 5f;
	public float randomTargetTimmer = 0f;
	private float randomTargetSpawnTimer;
	private BullseyeController activeTarget = null;
	private int randomChildIndex = -1;

	void Start() {
		
		totalTargets = transform.childCount;

		gameController.SetTotalTargets (totalTargets);

		// random targets
		if (randomTargets) {
			randomTargetSpawnTimer = randomTargetSpawnTime;
		}
	}

	void Update() {
		if (randomTargets) {
			CheckRandomTargets();
		}
	}

	public void UpdateTargetsStatus() {
		gameController.UpdateTargetsDestroyed ();
	}

	private void CheckRandomTargets() {
		randomTargetSpawnTimer -= Time.deltaTime;
		if (randomTargetSpawnTimer <= 0) {

			int newRandomChildIndex = GetRandomChildIndex();
			while(randomChildIndex == newRandomChildIndex) {
				newRandomChildIndex = GetRandomChildIndex();
			}
			randomChildIndex = newRandomChildIndex;
			if(activeTarget && activeTarget.isActive) {
				activeTarget.deactivate = true;
				activeTarget.Deactivate();
				activeTarget.isActive = false;
			}
			activeTarget = transform.GetChild(randomChildIndex).GetComponent<BullseyeController>();
			activeTarget.gameObject.SetActive (true);
			activeTarget.activate = true;
			randomTargetSpawnTimer = randomTargetSpawnTime;
		}
	}

	private int GetRandomChildIndex() {
		return UnityEngine.Random.Range (0, transform.childCount);
	}
}
