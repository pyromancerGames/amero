﻿using UnityEngine;
using System.Collections;

public class SweeperCollectController : MonoBehaviour {

	public delegate void SweeperAction();
	public static event SweeperAction OnSweep;
	
	void OnTriggerEnter2D(Collider2D other){

		if (other.tag == "BucketCollect") {
			other.transform.position = new Vector3(0f, other.transform.position.y, other.transform.position.z);
		} else {
			Destroy (other.gameObject);
			if (OnSweep != null)
				OnSweep ();
		}
	}
}
