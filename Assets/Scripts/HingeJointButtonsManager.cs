﻿using UnityEngine;
using System.Collections;

public enum RotationDirection {
	left, right, stopped 
}

public class HingeJointButtonsManager : MonoBehaviour {

	private HingeJoint2D hinge;
	private JointMotor2D motor;
	private RotationDirection dir = RotationDirection.stopped;

	public LayerMask buttons;

	public bool keyboardInputsEnabled = false;

	void Start () {
		hinge = GetComponent<HingeJoint2D> ();
		motor = hinge.motor;
		hinge.useLimits = true;
	}

	void Update () {

		CheckPCInputs ();

		if (dir == RotationDirection.left)
		{
			// Rotate left
			motor.motorSpeed = 30;
			hinge.motor = motor;
		}
		else if (dir == RotationDirection.right)
		{
			// Rotate right
			motor.motorSpeed = -30;
			hinge.motor = motor;
		}
		
	}

	private void CheckPCInputs() {
		if (keyboardInputsEnabled && Application.platform == RuntimePlatform.WebGLPlayer || Application.platform == RuntimePlatform.WindowsEditor) {
			if (Input.GetKeyDown(KeyCode.LeftArrow))
			{
				RotateLeft();
			}
			if (Input.GetKeyDown(KeyCode.RightArrow))
			{
				RotateRight();
			}
			if (Input.GetKeyUp (KeyCode.RightArrow)) {
				StopRotation();
			}
			if (Input.GetKeyUp (KeyCode.LeftArrow)) {
				StopRotation();
			}
		}
	}

	public void RotateLeft() {
		dir = RotationDirection.left;
	}

	public void RotateRight() {
		dir = RotationDirection.right;
	}

	public void StopRotation() {
		dir = RotationDirection.stopped;
		motor.motorSpeed = 0;
		hinge.motor = motor;
	}
}
