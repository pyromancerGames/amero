﻿using UnityEngine;
using System.Collections;

public class MusicManager : MonoBehaviour
{

	#region Variables
	public AudioClip levelMusic; //Pick an audio track to play.
	public AudioClip levelCompletedMusic; //Pick an audio track to play.
	private AudioSource audioSource;
	#endregion

	#region MonoBehaviour
	void Awake ()
	{
		StartLevelMusic ();
	}

	void OnEnable ()
	{
		InGameMenu.OnPause += Pause;
		InGameMenu.OnResume += Resume;
		InGameMenu.OnRestart += Restart;
		LevelCompletedController.OnComplete += LevelCompleted;
	}

	void OnDisable ()
	{
		InGameMenu.OnPause -= Pause;
		InGameMenu.OnResume -= Resume;
		InGameMenu.OnRestart -= Restart;
		LevelCompletedController.OnComplete -= LevelCompleted;
	}
	#endregion

	#region CustomBehaviour
	void Restart ()
	{
		audioSource.Stop ();
		audioSource.Play ();
	}

	void Pause ()
	{
		audioSource.Pause ();
	}
	void Resume ()
	{
		audioSource.UnPause ();
	}

	void LevelCompleted ()
	{
		if (audioSource) {
			audioSource.Stop ();
			if (levelCompletedMusic) {
				audioSource.clip = levelCompletedMusic; //Replaces the old audio with the new one set in the inspector. 
				audioSource.Play (); //Plays the audio. 
			}
		}
	}

	void StartLevelMusic ()
	{
		var gameMusic = GameObject.Find ("GameMusic");
		if (gameMusic) {
			audioSource = gameMusic.GetComponent<AudioSource> ();
			
			if (levelMusic == null) {
				audioSource.Stop ();
			} else if (audioSource.clip.name != levelMusic.name || !audioSource.isPlaying) {
				// If the level holds a different music to be played
				audioSource.Stop ();
				audioSource.clip = levelMusic; //Replaces the old audio with the new one set in the inspector. 
				audioSource.Play (); //Plays the audio. 
			}
			
		}
	}

	public void toggleSound ()
	{
		AudioListener.volume = 1 - AudioListener.volume;
		// Switch button sprite
		GameObject muteButton = GameObject.Find("Button MuteUnMute");
		if(muteButton == null) {
			muteButton = GameObject.Find("MuteUnMute Button");
		}

		if(muteButton != null) {
			MuteButtonManager muteBtnManager = muteButton.gameObject.GetComponent<MuteButtonManager>();
			if(muteBtnManager != null) {
				muteBtnManager.SwitchMuteButtonSpite();
			}
		}
	}

	#endregion
}
