﻿using UnityEngine;
using System.Collections;

public class TimeManager : MonoBehaviour
{

	public static void Pause ()
	{

		Time.timeScale = 0;
	}
	
	public static void UnPause ()
	{

		Time.timeScale = 1;
	}

}
