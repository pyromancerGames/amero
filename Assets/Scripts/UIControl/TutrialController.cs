﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TutrialController : MonoBehaviour
{

	#region Variables
	public GameObject tutorialPanel;
	public bool tutorialEnabled;
	public string[] wizard;
	public string[] wizardPC;
	
	private Text tutorialText;
	private int wizardStage = 0;
	#endregion

	// TODO Create a TimeManager object child of GameController use its events to stop pollfish.
	#region Events
	public delegate void TutorialAction ();
	public static event TutorialAction OnBeginTutorial;
	public static event TutorialAction OnEndTutorial;
	#endregion

	#region CustomBehaviour

	private void FinishTutorial ()
	{
		// Register that the tutorial has been finished
		PlayerPrefs.SetInt ("tutorial_level_" + Application.loadedLevelName, 1);
		
		// Close the panel
		tutorialPanel.SetActive (false);
		
		// UnPause the application
		TimeManager.UnPause ();
		if (OnEndTutorial != null)
			OnEndTutorial ();
	}
	public void WizardNext () {

		if (Application.platform == RuntimePlatform.WebGLPlayer || Application.platform == RuntimePlatform.WindowsEditor) {
			if (wizardStage < wizardPC.Length) {
				tutorialText.text = wizardPC [wizardStage];
				wizardStage += 1;
			} else {
				FinishTutorial ();
			}
		} else {
			// If there is still text to show
			if (wizardStage < wizard.Length) {
				tutorialText.text = wizard [wizardStage];
				wizardStage += 1;
			} else {
				FinishTutorial ();
			}
		}
	}

	#endregion
	
	#region MonoBehaviour
	void Start () {
		if (tutorialEnabled) {
			// If tutorial is enabled for this level get if hasnt already been completed for this user
			// 0 is default value when no pref is found
			int tutorialCompleted = PlayerPrefs.GetInt ("tutorial_level_" + Application.loadedLevelName, 0);
			
			if (tutorialCompleted == 0) {
				// Then the tutorial must be shown.
				// Pause the game, show the tutorial panel, fetch the Tutorial Text object and start the wizard
				TimeManager.Pause ();
				tutorialPanel.SetActive (true);
				tutorialText = GameObject.Find ("Tutorial Text").GetComponent<Text> () as Text;
				WizardNext ();
				if (OnBeginTutorial != null)
					OnBeginTutorial ();
			}
			
		}
	}
	#endregion
}
