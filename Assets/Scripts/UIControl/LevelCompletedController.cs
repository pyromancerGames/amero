﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LevelCompletedController : MonoBehaviour
{

	public GameObject[] starsHolders; //0=left 1=middle 2=right
	public Sprite[] stars; //0=left 1=middle 2=right
	public GameObject emotionTextHolder;
	public Sprite[] emotionText;
	public GameObject levelCompletePanel;
	public GameObject newRecordImage;
	public GameObject btnNext;

	#region Events
	public delegate void LevelActions ();
	public static event LevelActions OnComplete;
	#endregion

	/**
	 * Displays the Level Complete Panel acording the score
	 * @param float starRatio - It is the amount of stars collected divided by the total amount of stars present in the screen
	 * @param float shotsRatio - It is the minimal amount of shots fired to complete the level divided by the total shots made by the user.
	 */
	public void SumUpScore (float starRatio, float shotsRatio)
	{

		// Display the LevelCompletePanel
		levelCompletePanel.SetActive (true);

		if (OnComplete != null)
			OnComplete ();

		Time.timeScale = 0;
		Image holder;

		// Stars Earned
		int starsEarned = Mathf.FloorToInt (3 * starRatio * shotsRatio);
		if (starsEarned == 0)
			starsEarned = Mathf.FloorToInt (starsEarned + starRatio + shotsRatio);

		for (var i = 0; i < starsEarned; i++) {
			holder = starsHolders [i].GetComponent<Image> () as Image;
			holder.sprite = stars [i];
		}

		holder = emotionTextHolder.GetComponent<Image> () as Image;
		holder.sprite = emotionText [starsEarned];

		int currentLevel = Application.loadedLevel - 2;
		int currentScore = PlayerPrefs.GetInt ("level" + currentLevel.ToString ());

		if (starsEarned > currentScore) {
			PlayerPrefs.SetInt ("level" + currentLevel.ToString (), starsEarned);
			newRecordImage.SetActive (true);
		} else 
			newRecordImage.SetActive (false);
	
		// Disable next level button if not enough stars

		int totalLevels = PlayerPrefs.GetInt ("totalLevels");

		if (currentLevel == totalLevels) {
			btnNext.SetActive (false);

		} else {

			if (currentLevel < 3) {
				btnNext.SetActive (true);
			} else if (TotalizeStars (totalLevels) < Application.loadedLevel) {
				btnNext.SetActive (false);
			} else {
				btnNext.SetActive (true);
			}

		}


			
	}

	public int TotalizeStars (int levelCount)
	{
		int currentStars = 0;
		
		// Totalize Stars
		for (int level = 1; level <= levelCount; level++) {
			int levelInfo = -1;
			
			if (PlayerPrefs.HasKey ("level" + level.ToString ())) {
				levelInfo = PlayerPrefs.GetInt ("level" + level.ToString ());
			}
			
			if (levelInfo > 0)
				currentStars += levelInfo;
		}

		return currentStars;

	}

}
