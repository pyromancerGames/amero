﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class InGameMenu : MonoBehaviour
{
	#region Variables
	public GameObject pausePanel;
	private bool isPaused = false;
	#endregion

	#region Events
	public delegate void MenuAction ();
	public static event MenuAction OnPause;
	public static event MenuAction OnResume;
	public static event MenuAction OnRestart;
	public static event MenuAction OnStartNextLevel;
	public static event MenuAction OnMainMenu;
	#endregion
	
	#region CustomBehaviour
	
	public void PauseGame ()
	{
		pausePanel.SetActive (true);
		TimeManager.Pause ();
		isPaused = true;

		// Broadcast the event
		if (OnPause != null) {
			// TODO: pause or unpause pollfish via event
			OnPause ();
		}
	}

	public void ResumeGame ()
	{
		pausePanel.SetActive (false);
		TimeManager.UnPause ();
		isPaused = false;

		// Broadcast the event
		if (OnResume != null) {
			OnResume ();
		}
	}

	public void RestartLevel ()
	{
		Application.LoadLevel (Application.loadedLevel);
		TimeManager.UnPause ();
		isPaused = false;

		// Broadcast the event
		if (OnRestart != null) {
			OnRestart ();
		}
	}

	public void MainMenu ()
	{
		if (OnMainMenu != null) {
			OnMainMenu ();
		}
		isPaused = false;
		Application.LoadLevel (2);
		TimeManager.UnPause ();
	}

	public void StartNextLevel ()
	{
		if (OnStartNextLevel != null) {
			OnStartNextLevel ();
		}
		isPaused = false;
		Application.LoadLevel (Application.loadedLevel + 1);
		TimeManager.UnPause ();

	}

	public bool IsPaused ()
	{
		return isPaused;
	}
	#endregion
}
