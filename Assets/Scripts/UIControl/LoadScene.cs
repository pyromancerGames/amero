﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LoadScene : MonoBehaviour {

	// Public Attributes
	public Slider loadingBar;
	public GameObject loadingImage;

	// Private Attributes
	private AsyncOperation async;

	/**
	 * Loads a Scene Syncronously.
	 * Shows a static loading image.
	 */
	public void LoadSceneSync (int sceneIndex) {
		if (loadingImage)
			loadingImage.SetActive (true);
		Application.LoadLevel (sceneIndex);
	}

	/**
	 * Loads a Scene Syncronously.
	 * Shows a static loading image.
	 */
	public void LoadSceneSync (string sceneName) {
		if (loadingImage)
			loadingImage.SetActive (true);
		Application.LoadLevel (sceneName);
	}

	/**
	 * Loads a Scene Asyncronously.
	 * Shows a static loading image.
	 * Uses a Slidebar to show progress
	 * */
	public void LoadSceneAsync (int sceneIndex) {
		if (loadingImage)
			loadingImage.SetActive (true);
		StartCoroutine (LoadLevelWithBar (sceneIndex));
	}

	/**
	 * Loads a Scene Asyncronously.
	 * Shows a static loading image.
	 * Uses a Slidebar to show progress
	 * */
	public void LoadSceneAsync (string sceneName) {
		if (loadingImage)
			loadingImage.SetActive (true);
		StartCoroutine (LoadLevelWithBar (sceneName));
	}

	/**
	 * Create the async operation and keeps track of it, updating the loadingBar
	 * until it is finshed.
	 * It is intented to be called as a CoRoutine.
	 */
	IEnumerator LoadLevelWithBar (int sceneIndex) {
		async = Application.LoadLevelAsync (sceneIndex);
		while (!async.isDone) {
			loadingBar.value = async.progress;
			yield return null;
		}
	} 


	/**
	 * Create the async operation and keeps track of it, updating the loadingBar
	 * until it is finshed.
	 * It is intented to be called as a CoRoutine.
	 */
	IEnumerator LoadLevelWithBar (string sceneName) {
		async = Application.LoadLevelAsync (sceneName);
		while (!async.isDone) {
			loadingBar.value = async.progress;
			yield return null;
		}
	} 

}
