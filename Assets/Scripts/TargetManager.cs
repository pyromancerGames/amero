﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TargetManager : MonoBehaviour {

	public GameObject ball;
	private List<Transform> spawnPoints;
	public float spawnDelay = 2f;
	private float spawnDelayInitial;

	void Start() {
		// Spawn delay
		spawnDelayInitial = spawnDelay;

		// Spawn points
		spawnPoints = new List<Transform> ();

		Transform spawnPointsParent = transform.Find("SpawnPoints").gameObject.transform;
		foreach (Transform child in spawnPointsParent) {
			spawnPoints.Add(child);
		}
	}

	// Update is called once per frame
	void Update () {
		// Drop ball
		spawnDelay -= Time.deltaTime;
		
		if (spawnDelay <= 0) {
			SpawnBall ();
			spawnDelay = spawnDelayInitial;
		}
	}

	void SpawnBall() {

		Transform t = spawnPoints [Random.Range (0, spawnPoints.Count)];
		Instantiate (ball, t.position, t.rotation);
	}
}
