﻿using UnityEngine;
using System.Collections;

public class BullseyeController : MonoBehaviour {

	public bool isActive = false;
	bool activating = false;
	bool deactivating = false;
	CircleCollider2D circleCollider;
	TargetsManager targetsManager;
	private bool hitted;

	public bool activate = false;
	public bool deactivate = false;

	// Use this for initialization
	void Start () {
		circleCollider = gameObject.GetComponent<CircleCollider2D> ();
		circleCollider.enabled = false;
		targetsManager = transform.parent.GetComponent<TargetsManager>();
		hitted = false;
	}

	void FixedUpdate() {

		if (activating) {
			ActivateProcess ();
		} else if(deactivating) {
			DeactivateProcess();
		}

		if (activate) {
			hitted = false;
			deactivating = false;
			if (!isActive) {
				activating = true;
			}

			activate = false;
		}

	}

	public void Activate() {
		if (activate) {

			deactivating = false;
			if (!isActive) {
				activating = true;
			}
			activate = false;
		}
	}

	public void Deactivate() {
		if (deactivate) {

			activating = false;

			if (isActive) {
				deactivating = true;
			}

			deactivate = false;
		}
	}

	public void ForceDeactivate() {
		if (deactivate) {
			
			activating = false;
			
			//if (isActive) {
				deactivating = true;
			//}
			isActive = false;
			deactivate = false;
		}
	}
	
	void ActivateProcess() {

		if (!circleCollider.enabled) {
			circleCollider.enabled = true;
		}

		if (transform.rotation.eulerAngles.x > 1) {
			transform.Rotate (Vector3.right * -(Time.deltaTime * 100), Space.World);
		} else {
			isActive = true;
			activating = false;
		}
	}

	void DeactivateProcess() {
		if (transform.rotation.eulerAngles.x < 90) {
			transform.Rotate (Vector3.right * (Time.deltaTime * 100), Space.World);
		} else {
			if (circleCollider.enabled) {
				circleCollider.enabled = false;
			}
			isActive = false;
			deactivating = false;
			gameObject.SetActive (false);
		}
	}

	void OnCollisionEnter2D(Collision2D coll) {

		activate = false;
		deactivate = true;
		ForceDeactivate();
		if(!hitted){
			targetsManager.UpdateTargetsStatus();
		}
		hitted = true;
		//isActive = false;
	}
}
