﻿using UnityEngine;
using System.Collections;

public class UnitySingleton : MonoBehaviour {

	private static UnitySingleton instance; 
	
	void Awake() { 

		if (instance != null && instance != this) {
			Destroy(this.gameObject); return; 
		} else {
			instance = this; 
		} 

		DontDestroyOnLoad(this.gameObject); 
	}

}
