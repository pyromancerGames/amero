﻿using UnityEngine;
using System.Collections;

public class Utils : MonoBehaviour {

	public static bool EqualsWithTolerance(float a, float b, float epsilon){
		return Mathf.Abs (a - b) < epsilon;
	}
}
