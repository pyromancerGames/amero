﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{

//	private int score = 0;
	private int starsCollected = 0;
	private int ballsFired = 0;
	// Collect gametype
	private int ballsCollected = 0;
	private float collectedTime = 0f;
	// Target gametype
	private int totalTargets = 0;
	private int targetsDestroyed = 0;

	// PUBLIC ELEMENTS
	public Text starsText;
	public Text ballsFiredText;
	public LevelCompletedController levelCompleteController;

	public GameType gameType = GameType.BUCKET;

	public delegate void GameControllerAction ();
	public static event GameControllerAction OnLevelCompleted;

	public enum GameType
	{ 
		BUCKET,
		TARGETS,
		COLLECT,
		DESTROYBALLS
	}
	;


	// CONFIG
	// Bucket gametype
	public int totalStars;
	public int minShotsToWin;
	// Target gametype

	// Collect gametype
	public int totalBallsToCollect;
	public float timeToWin = 0f;

	//

	void Start ()
	{
		UpdateStarsText ();
	}

	void Update ()
	{
		collectedTime += Time.deltaTime;
	}

	void OnEnable ()
	{
		if (gameType == GameType.BUCKET) {
			ContainerController.OnBallCollect += LevelCompleted;
			FireController.OnBallFire += BallFired;
			SpawnBall.OnSpawn += BallFired;
		} else if (gameType == GameType.COLLECT) {
			ContainerController.OnBallCollect += CollectBall;
		} else if (gameType == GameType.TARGETS) {
			FireController.OnBallFire += BallFired;
			SpawnBall.OnSpawn += BallFired;
		} else if (gameType == GameType.DESTROYBALLS) {
			ContainerController.OnBallCollect += CollectBall;
			FireController.OnBallFire += BallFired;
			SpawnBall.OnSpawn += BallFired;
		}
		CollectableController.OnCollect += CollectItem;
	}
	
	void OnDisable ()
	{
		if (gameType == GameType.BUCKET) {
			ContainerController.OnBallCollect -= LevelCompleted;
			FireController.OnBallFire -= BallFired;
			SpawnBall.OnSpawn -= BallFired;
		} else if (gameType == GameType.COLLECT) {
			ContainerController.OnBallCollect -= CollectBall;
		} else if (gameType == GameType.TARGETS) {
			FireController.OnBallFire -= BallFired;
			SpawnBall.OnSpawn -= BallFired;
		} else if (gameType == GameType.DESTROYBALLS) {
			ContainerController.OnBallCollect -= CollectBall;
			FireController.OnBallFire -= BallFired;
			SpawnBall.OnSpawn -= BallFired;
		}
		CollectableController.OnCollect -= CollectItem;
	}

	public void BallFired ()
	{
		ballsFired += 1;
		UpdateBallsFiredText ();
	}
	
	void CollectItem (Collider2D other)
	{
		
		// UPDATE THE CORRESPONDING SCORE ITEM
		if (other.tag == "CollectableStar") {
			starsCollected += 1;
			UpdateStarsText ();
			
		}

	}

	void UpdateStarsText ()
	{
		starsText.text = starsCollected.ToString ();
	}

	void UpdateBallsFiredText ()
	{
		ballsFiredText.text = ballsFired.ToString ();
	}

	void LevelCompleted ()
	{

		if (OnLevelCompleted != null) {
			OnLevelCompleted ();
		}

		float pointsRation;
		float shotsRatio;

		if (ballsFired == 0)
			ballsFired = 1;

		if (gameType == GameType.BUCKET || gameType == GameType.DESTROYBALLS) {
			pointsRation = (float)starsCollected / (float)totalStars;
			shotsRatio = (float)minShotsToWin / (float)ballsFired;
		} else if (gameType == GameType.COLLECT ) {
			pointsRation = shotsRatio = TimeGameFinished ();
		} else {
			//Targets
			pointsRation = shotsRatio = TimeGameFinished ();
		}
		levelCompleteController.SumUpScore (pointsRation, shotsRatio);
	}

	public void CollectBall ()
	{
		ballsCollected++;
		if (ballsCollected >= totalBallsToCollect) {
			LevelCompleted ();
		}
	}

	public void SetTotalTargets (int totalTargetsVal)
	{
		totalTargets = totalTargetsVal;
	}

	public void UpdateTargetsDestroyed ()
	{
		targetsDestroyed++;
		if (targetsDestroyed >= totalTargets) {
			LevelCompleted ();
		}
	}

	float TimeGameFinished ()
	{
		if (collectedTime <= timeToWin) {
			// Perfect
			return 1f;
		} else if (collectedTime <= timeToWin * 2) {
			// Nice
			return 0.9f;
		} else if (collectedTime <= timeToWin * 3) {
			// Great
			return 0.5f;
		} else {
			// Poor
			return 0f;
		}
	}
}
