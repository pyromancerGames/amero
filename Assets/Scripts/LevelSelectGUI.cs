﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LevelSelectGUI : MonoBehaviour {

	private bool menuBIsOn = false;
	public Text stageText;
	public Text starsText;

	public GEAnim menuA;
	public GEAnim menuB;

	// Buttons
	public Button homeButton;
	public Button stagesButton;

	// Buttons graphics
	private Sprite homeButtonGraphic;

	private Vector3 homeButtonPosition;
	private Vector3 stagesButtonPosition;

	public Text startToUnlockNextLevTitle;
	public Text startToUnlockNextLev;

	// Use this for initialization
	void Start () {
		menuA.MoveIn(eGUIMove.SelfAndChildren);
		// Buttons
		homeButtonGraphic = homeButton.image.sprite;

		homeButtonPosition = homeButton.transform.position;
		stagesButtonPosition = stagesButton.transform.position;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void Awake (){
		if (Application.loadedLevel == 2) {
			GEAnimSystem.Instance.m_AutoAnimation = false;
		} else {
			GEAnimSystem.Instance.m_AutoAnimation = true;
		}

		homeButtonGraphic = homeButton.image.sprite;
	}

	public void ToggleTopLeft()
	{
		menuBIsOn = !menuBIsOn;
		if(menuBIsOn==true) {
			// Stage2 moves in
			stageText.text = "STAY TUNED";
			//starsText.color = stageText.color = Color.black;
			stagesButton.image.overrideSprite = homeButtonGraphic;
			stagesButton.transform.position = homeButtonPosition;
			menuA.MoveOut(eGUIMove.SelfAndChildren);
			menuB.MoveIn(eGUIMove.SelfAndChildren);
			startToUnlockNextLevTitle.gameObject.SetActive(false);
			startToUnlockNextLev.gameObject.SetActive(false);

		} else {
			// Stage2 moves out
			//starsText.color = stageText.color = new Color32(51, 104, 164, 255);
			stagesButton.image.overrideSprite = null;
			stagesButton.transform.position = stagesButtonPosition;
			stageText.text = "STAGE 1";
			menuB.MoveOut(eGUIMove.SelfAndChildren);
			menuA.MoveIn(eGUIMove.SelfAndChildren);
			startToUnlockNextLevTitle.gameObject.SetActive(true);
			startToUnlockNextLev.gameObject.SetActive(true);
		}
	}
}
