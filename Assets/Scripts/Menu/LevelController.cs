﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LevelController : MonoBehaviour
{

	public int levelCount = 21; //number of levels
	public GameObject[] allButtons;
	public Sprite[] starLevel;
	public Text	starCountText;

	public Text	startToUnlockNextLevTitle;
	public Text	startToUnlockNextLev;

	// Collected Stars
	int currentStars = 0;

	void  Start ()
	{
		PlayerPrefs.SetInt ("totalLevels", levelCount);
		CheckLockedLevels ();
	}
	

	//function to check for the levels locked
	void  CheckLockedLevels ()
	{
		// Loop through the levels of a particular world
		allButtons [0].SetActive (true);
		allButtons [levelCount].SetActive (false);

		int totalStars = 3 * levelCount;

		for (int level = 1; level <= levelCount; level++) {
			int levelInfo = -1;

			// Check for Key. If it does not has key then set level as locked
			// otherwise retrieve level code
			if (!PlayerPrefs.HasKey ("level" + level.ToString ())) {
				if (level == 1 || level == 2 || level == 3)
					levelInfo = 0;

				PlayerPrefs.SetInt ("level" + level.ToString (), levelInfo);
			} else {
				levelInfo = PlayerPrefs.GetInt ("level" + level.ToString ());
			}

			if (levelInfo > 0)
				currentStars += levelInfo;
			SetLevelStatus (level, levelInfo);
		}

		starCountText.text = currentStars.ToString () + "/" + totalStars.ToString ();

		if(currentStars >= (levelCount + 1)){
			startToUnlockNextLevTitle.gameObject.SetActive(false);
			startToUnlockNextLev.gameObject.SetActive(false);
		} else if (currentStars < 5){
			// level 4 = 5 stars
			// Level 21 = 22 stars
			startToUnlockNextLev.text = (5 - currentStars).ToString();
		} else {
			startToUnlockNextLev.text = "1";
		}
	}

	// Locks a level button
	private void LockButtonLevel (int levelIndex)
	{
		allButtons [levelIndex - 1].SetActive (false);
		allButtons [levelIndex - 1 + levelCount].SetActive (true);
	}
	
	// Unlocks a level button
	private void UnlockButtonLevel (int levelIndex)
	{
		allButtons [levelIndex - 1].SetActive (true);
		allButtons [levelIndex - 1 + levelCount].SetActive (false);
	}


	/**
	 *  Unlocks a given level. Set player record and unlocks the button
	 */
	public void UnlockLevel (int levelIndex)
	{
		PlayerPrefs.SetInt ("level" + levelIndex.ToString (), 0);
		UnlockButtonLevel (levelIndex);
	}


	/**
	 *  Locks a given level. Set player record and locks the button
	 */
	public void LockLevel (int levelIndex)
	{
		PlayerPrefs.SetInt ("level" + levelIndex.ToString (), -1);
		LockButtonLevel (levelIndex);
	}

	/**
	 *  Gets a Level and assigns the achieved stars to it
	 *  Set Player record, unlocks the button and assigns star level sprite.
	 */
	public void SetStars (int levelIndex, int stars)
	{
		// Record in player preferences
		PlayerPrefs.SetInt ("level" + levelIndex.ToString (), stars);
		UnlockButtonLevel (levelIndex);

		Image img = allButtons [levelIndex - 1].transform.FindChild ("Stars").GetComponent ("Image") as Image;
		img.sprite = starLevel [stars];
	}
	
	/**
	 *  Given a level, locks/unlocks it and assigns the achieved stars to it
	 * 	Status = -1 - Locks Level
	 * 	Status  0 - 3 Assigns stars;
	 */
	public void SetLevelStatus (int levelIndex, int status)
	{


		if (status < 0) {
			if (currentStars >= levelIndex + 1)
				UnlockLevel (levelIndex);
			else
				LockLevel (levelIndex);
		} else
			SetStars (levelIndex, status);

	}


}
