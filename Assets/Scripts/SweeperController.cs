﻿using UnityEngine;
using System.Collections;


/**
 * SWEEPER CONTROLLER
 * =============================
 * 
 * Assignable to any sweeper.
 * Must have a "Trigger" Collider
 * 
 * Destroys wichever object collides with that collider and fires OnSweep() event;
 */

public class SweeperController : MonoBehaviour {

	public delegate void SweeperAction();
	public static event SweeperAction OnSweep;
	
	void OnTriggerEnter2D(Collider2D other){

		Destroy (other.gameObject);

		if (OnSweep != null)
			OnSweep ();
	}
}
