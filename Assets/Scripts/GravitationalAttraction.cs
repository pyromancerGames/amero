﻿using UnityEngine;
using System.Collections;

/**	
 *  GRAVITATIONAL ATTRACTION CLASS
 *  One must asociate this class to any object that should be attracted to planets.
 *  Planets must be tagged as so ("Planet").
 *  Planets must have mass and that mass as well as the object. That mass will be determining the attraction
 *  effect to eachother. Planets and Objects subject to gravitational attraction must have World Gravity Disabled"
 */

public class GravitationalAttraction : MonoBehaviour {

	public float gravity = 10.0f;
	public bool faceMovingDirection = false;

	private float selfMass = 0;
	private GameObject[] planets;
	private GameObject[] whiteHoles;
	private Rigidbody2D body;
	
	void Start () {
		planets = GameObject.FindGameObjectsWithTag("Planet");
		whiteHoles = GameObject.FindGameObjectsWithTag("WhiteHole");

		selfMass = GetComponent<Rigidbody2D> ().mass;
		body = GetComponent<Rigidbody2D> ();
	}
	
	void FixedUpdate () {
		foreach(GameObject planet in planets) {

			// Distance is real distance in canvas, this means that to have a realistic gravitational pull, masses
			// must be irrationally small (1-10 for planets 0.005 - 0.1 for the moving objects
			float dist = Vector3.Distance(planet.transform.position, transform.position);

			Rigidbody2D planetBody = planet.GetComponent<Rigidbody2D> ();
			float planetMass = planetBody.mass;
			
			float m1 = selfMass;
			float m2 = planetMass;
			Vector3 v = planet.transform.position - transform.position;
			Vector3 g =  v.normalized * gravity;
			Vector3 f = g * ((m1*m2)/(dist*dist));
			
			body.AddForce(f);

		}

		foreach(GameObject wh in whiteHoles) {
			
			// Distance is real distance in canvas, this means that to have a realistic gravitational pull, masses
			// must be irrationally small (1-10 for planets 0.005 - 0.1 for the moving objects
			float dist = Vector3.Distance(wh.transform.position, transform.position);
			
			Rigidbody2D planetBody = wh.GetComponent<Rigidbody2D> ();
			float planetMass = planetBody.mass;
			
			float m1 = selfMass;
			float m2 = planetMass;
			Vector3 v = wh.transform.position - transform.position;
			Vector3 g =  v.normalized * gravity;
			Vector3 f = g * ((m1*m2)/(dist*dist));
			
			body.AddForce(-f);
			
		}

		if (faceMovingDirection) {
			transform.right = body.velocity;
		}
	}

	void OnCollisionStay (Collision colInfo) {
		print ("collide");
	}
}

