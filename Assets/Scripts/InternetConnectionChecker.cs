﻿using UnityEngine;
using System.Collections;
using System;

public class InternetConnectionChecker : MonoBehaviour
{
	#if UNITY_ANDROID || UNITY_IPHONE
	private const bool allowCarrierDataNetwork = true;
	private const string pingAddress = "8.8.8.8"; // Google Public DNS server
	private const float waitingTime = 2.0f;

	private bool isInternetAvailable = false;
	
	private Ping ping;
	private float pingStartTime;

	public bool callBannerCallback = false;

	public delegate void InternetCheckerAction ();
	public static event InternetCheckerAction CallBanner;

	void Awake ()
	{
		DontDestroyOnLoad (gameObject);
	}

	public void Update ()
	{

		if (ping != null) {
			bool stopCheck = true;
			if (ping.isDone) {
				InternetAvailable (true);
				ping = null;
				if (callBannerCallback) {
					if (CallBanner != null) {
						CallBanner ();
					}
					callBannerCallback = false;
				}
				return;
			} else if (Time.time - pingStartTime < waitingTime) {
				stopCheck = false;
			} else {
				InternetAvailable (false);
			}
			if (stopCheck) {
				ping = null;
			}
		}
	}

	// TODO: Implement Couroutine  chueck while Android call close banner

	public IEnumerator CheckInternet (bool bannerCallback)
	{
		InternetAvailable (false);

		if (InternetPossiblyAvailable ()) {
			float time = 0f;
			ping = new Ping (pingAddress);
			pingStartTime = Time.time;

			if (bannerCallback) {
				callBannerCallback = true;
			}

			yield return null;

		}
	}

	private bool InternetPossiblyAvailable ()
	{
		bool internetPossiblyAvailable;
		
		switch (Application.internetReachability) {
		case NetworkReachability.ReachableViaLocalAreaNetwork:
			internetPossiblyAvailable = true;
			break;
		case NetworkReachability.ReachableViaCarrierDataNetwork:
			internetPossiblyAvailable = allowCarrierDataNetwork;
			break;
		default:
			internetPossiblyAvailable = false;
			break;
		}
			
		return internetPossiblyAvailable;
	}

	// TODO: debug remove
//	void OnGUI ()
//	{	
//		GUI.Label (new Rect (10, 170, Screen.width - 20, 40), "Internet: " + isInternetAvailable);
//	}

	public bool IsInternetAvailable ()
	{
		return isInternetAvailable;
	}

	private void InternetAvailable (bool available)
	{
		isInternetAvailable = available;
	}
	#endif
}