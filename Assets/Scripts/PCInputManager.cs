﻿using UnityEngine;
using System.Collections;

public class PCInputManager : MonoBehaviour {

	public GameObject cannon;
	private FireController fireController;
	private bool pcInputsEnabled = false;

	void Start () {
		if (Application.platform == RuntimePlatform.WebGLPlayer || Application.platform == RuntimePlatform.WindowsEditor) {
			if (cannon != null) {
				fireController = cannon.GetComponent<FireController> ();
				pcInputsEnabled = true;
			}
		} else {
			gameObject.SetActive(false);
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (pcInputsEnabled) {
			if (Input.GetKeyDown ("space")) {
				fireController.AccumulatePower ();
			}

			if (Input.GetKeyUp ("space")) {
				fireController.ReleaseFire ();
			}
		}
	
	}
}
