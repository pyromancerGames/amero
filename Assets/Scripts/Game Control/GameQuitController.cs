﻿using UnityEngine;
using System.Collections;


public class GameQuitController : MonoBehaviour
{
	#region Variables
	private int quitCount = 0;
	private GUIStyle style;
	private float exitThreshold = 2;
	private float elapsedTime = 0;
	private InGameMenu inGameMenu;

	public Font customFont;
	public int fontSize = 13;
	public Color customColor;
	#endregion

	#region MonoBehaviour
	void Start ()
	{
		style = new GUIStyle ();
		style.normal.textColor = customColor;
		if (customFont != null)
			style.font = customFont;
//		style.fontSize = fontSize;
		style.fontSize = Screen.width / 20;
		style.alignment = TextAnchor.MiddleCenter;

		try {
			inGameMenu = GameObject.Find ("Game Manager").GetComponent<InGameMenu> ();
		} catch (System.Exception e) {
			inGameMenu = null;
		}

	}

	void Update ()
	{
		if (Input.GetButtonUp ("Cancel")) {
			if (Application.loadedLevel > 2) {
				if (inGameMenu.IsPaused ()) {
					inGameMenu.MainMenu ();
				} else
					inGameMenu.PauseGame ();
			} else {
				if (Application.loadedLevel == 2) {
					Application.LoadLevel (1);
				} else {
					quitCount += 1;
				}
					
			} 
		}
	}

	void FixedUpdate ()
	{
		if (elapsedTime >= exitThreshold) {
			quitCount = 0;
			elapsedTime = 0;
		}
			
		if (quitCount == 1) {
			elapsedTime += Time.deltaTime;
		}

		if (quitCount >= 2)
			Quit ();
	}
	
	void OnGUI ()
	{	
		if (quitCount == 1) 
			GUI.Label (new Rect (10, Screen.height - 80, Screen.width - 20, 40), "Press back button again to quit", style);
	}

	#endregion
	
	#region CustomBehaviour
	public void Quit ()
	{
		if (!Application.isEditor)
			Application.Quit ();
//		else
//			UnityEditor.EditorApplication.isPlaying = false;
	}
	#endregion

}
