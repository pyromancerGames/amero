﻿using UnityEngine;
using System.Collections;

public class InitialSettings : MonoBehaviour
{
	#region Variables
	public bool resetPreferences = false;
	#endregion

	#region MonoBehaviour
	void Start ()
	{
		if (resetPreferences)
			PlayerPrefs.DeleteAll ();

		// Avoid idle
		Screen.sleepTimeout = SleepTimeout.NeverSleep;

		// After loading all Settings, start application
		StartCoroutine ("StartNextScene");

	}
	#endregion

	IEnumerator StartNextScene ()
	{
		yield return new WaitForSeconds (4);
		Application.LoadLevel (1);
	}
}
