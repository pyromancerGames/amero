﻿using UnityEngine;
using System.Collections;

public class SpawnBall : MonoBehaviour {

	public Transform ballSpawnPoint;
	public GameObject ball;

	public delegate void SpawnAction();
	public static event SpawnAction OnSpawn;

	void OnEnable () {
		ContainerController.OnBallCollect += ResetBall;
		SweeperController.OnSweep += ResetBall;
	}
	
	void OnDisable () {
		ContainerController.OnBallCollect -= ResetBall;
		SweeperController.OnSweep -= ResetBall;
	}

	void ResetBall () {

		Instantiate (ball, ballSpawnPoint.position, transform.rotation);
		OnSpawn ();
	}
}
