﻿using UnityEngine;
using System.Collections;

/**
 * CONTAINER CONTROLLER
 * =============================
 * 
 * Assignable to any container.
 * Must have a "Trigger" Collider
 * 
 * Destroys wichever object collides with that collider.
 * If Collided object is Tagged as Ball then fires OnBallCallect Event
 * 
 */

public class ContainerController : MonoBehaviour {

	public delegate void CollectAction();
	public static event CollectAction OnBallCollect;

	void OnTriggerEnter2D(Collider2D other){
		if (other.tag == "Ball") {
			Destroy (other.gameObject);

			if (OnBallCollect != null)
				//Fires On Collect Event
				OnBallCollect();
		}

	}
}

