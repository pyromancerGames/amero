﻿using UnityEngine;
using System.Collections;

public class BumperController : MonoBehaviour {

	private bool hitted = false;
	
	// Update is called once per frame
	void FixedUpdate () {
		if (hitted) {
			GetComponent<SpriteRenderer> ().color = Color.white;
			hitted = false;
		}
	}

	void OnCollisionEnter2D(Collision2D coll) {
		hitted = true;
		GetComponent<SpriteRenderer> ().color = Color.red;
	}
}
