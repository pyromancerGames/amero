﻿using UnityEngine;
using System.Collections;

public class PlaySoundOnCollision : MonoBehaviour
{

	private AudioSource rollFx;
	private AudioSource thudFx;
	private Rigidbody2D body;

	public string rollAudioName;
	public string thudAudioName;
	
	public float rollSpeedThreshold;


	void Start ()
	{

		// Get Roll and thud effects
		foreach (AudioSource audiofx in GetComponents<AudioSource>()) {
			if (audiofx.clip.name == rollAudioName)
				rollFx = audiofx;

			if (audiofx.clip.name == thudAudioName)
				thudFx = audiofx;
		}
	}

	void OnCollisionEnter2D (Collision2D collInfo)
	{
		if (thudFx) {
			thudFx.volume = collInfo.relativeVelocity.magnitude - 0.8f;
			if (!thudFx.isPlaying)
				thudFx.Play ();

		}
			
	}

	void OnCollisionStay2D (Collision2D collInfo)
	{
		if (rollFx) {
			body = collInfo.rigidbody;

			rollFx.volume = body.velocity.magnitude - 0.5f;

			if (!rollFx.isPlaying) {
				rollFx.Play ();
			}                                
		}

	}

	void OnCollisionExit2D (Collision2D collInfo)
	{
		if (rollFx)
			rollFx.Stop ();
	}
}
