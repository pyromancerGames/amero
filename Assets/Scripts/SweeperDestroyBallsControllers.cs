﻿using UnityEngine;
using System.Collections;

public class SweeperDestroyBallsControllers : MonoBehaviour {

	public GameController gameController;
	
	void OnTriggerEnter2D(Collider2D other){
		if (other.tag == "BallToDestroy") {
			Destroy (other.gameObject);
			gameController.CollectBall();

		}
	}
}
