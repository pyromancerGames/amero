﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MagnetController : MonoBehaviour
{

	public float roatationSpeed;

	private GameObject magnetUp;
	private GameObject magnetLeft;

	private bool CWRightButtonIsPressed = false;
	private bool CCWRightButtonIsPressed = false;
	private bool CWLeftButtonIsPressed = false;
	private bool CCWLeftButtonIsPressed = false;


	void Start ()
	{
		magnetUp = GameObject.Find ("UpMagnet");
		magnetLeft = GameObject.Find ("LeftMagnet");
	}

	void FixedUpdate ()
	{
		if (CWLeftButtonIsPressed)
			RotateMagnet (magnetUp, -1);
		if (CCWLeftButtonIsPressed)
			RotateMagnet (magnetUp, 1);
		if (CWRightButtonIsPressed)
			RotateMagnet (magnetLeft, -1);
		if (CCWRightButtonIsPressed)
			RotateMagnet (magnetLeft, 1);
	}

	public void LeftCWButtonDown ()
	{
		CWLeftButtonIsPressed = true;
	}

	public void LeftCCWButtonDown ()
	{
		CCWLeftButtonIsPressed = true;
	}

	public void RightCWButtonDown ()
	{
		CWRightButtonIsPressed = true;
	}

	public void RightCCWButtonDown ()
	{
		CCWRightButtonIsPressed = true;
	}

	public void LeftCWButtonUp ()
	{
		CWLeftButtonIsPressed = false;
	}
	
	public void LeftCCWButtonUp ()
	{
		CCWLeftButtonIsPressed = false;
	}
	
	public void RightCWButtonUp ()
	{
		CWRightButtonIsPressed = false;
	}
	
	public void RightCCWButtonUp ()
	{
		CCWRightButtonIsPressed = false;
	}

	/**
	 * Rotates certain magnet in the given direciton
	 * 
	 * @param GameObject magnet - the instance of the magnet to rotate
	 * @param int direction - the direction to be rotated: -1 CW, 1 CCW
	 */
	void RotateMagnet (GameObject magnet, int direction)
	{
		magnet.transform.Rotate (new Vector3 (0, 0, roatationSpeed * direction));
	}
	
}
