﻿using UnityEngine;
using System.Collections;

public class ScrollTexture : MonoBehaviour {

	public float scrollSpeed = 0.5F;
	private Renderer rend;
	float pos = 0;

	void Start() {
		rend = GetComponent<Renderer>();
	}
	void Update() {
		pos += scrollSpeed;

		if (pos > 1.0f)
			pos -= 1.0f;

		rend.material.mainTextureOffset = new Vector2(pos, 0);
	}
}
 