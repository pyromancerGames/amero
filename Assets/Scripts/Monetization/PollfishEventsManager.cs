﻿using UnityEngine;
using System.Collections;

public class PollfishEventsManager : MonoBehaviour
{
	#if UNITY_ANDROID || UNITY_IPHONE
	
	private int pendingSurveys = 0;
	public bool pollfishEnabled = false;
	public Position pollfishPosition = Position.BOTTOM_RIGHT;
	public int padding = 0;
	public bool debugMode = true;
	public bool customMode = true;
	
	private string appDeveloperKey;

	private int surveysNotAvailable = 0;

	#region DEBUG REMOVE!
	private string receivedMessage = "";
	private int internalSurveysReceived = 0;
	private bool internalUserNotAvailable = false;
	private int internalSurveyOpened = 0;
	private int internalSurveyClosed = 0;
	#endregion

	private bool ispaused = false ; // pause and resume when Pollfish panel opens
	
	void Awake ()
	{
		if (Application.platform != RuntimePlatform.WindowsEditor) {
			// Tell plugin which gameobject to send messages too
			Pollfish.SetEventObjectPollfish (this.gameObject.name);
			
			Debug.Log ("UnitySendMessage to object with name: " + this.gameObject.name);

			//Makes the object target not be destroyed automatically when loading a new scene 
			DontDestroyOnLoad (gameObject);
		}
	}
	
	public void Update ()
	{
		
		if (!ispaused) { // resume
	
			Time.timeScale = 1;
			
		} else if (ispaused) { // pause
			
			Time.timeScale = 0;		
		}
	}
	
	/* 
	 * registering to listen for Pollfish events 
	 */
	public void OnEnable ()
	{
		if (Application.platform != RuntimePlatform.WindowsEditor) {
			EnablePollfishEvents ();
			EnablePollfish ();
		} else {
			pollfishEnabled = false;
		}
	}

	private void EnablePollfishEvents ()
	{
		Pollfish.surveyCompletedEvent += surveyCompleted;
		Pollfish.surveyOpenedEvent += surveyOpened;
		Pollfish.surveyClosedEvent += surveyClosed;
		Pollfish.surveyReceivedEvent += surveyReceived;
		Pollfish.surveyNotAvailableEvent += surveyNotAvailable;
		Pollfish.userNotEligibleEvent += userNotEligible;
		
		#if UNITY_ANDROID
		
		/* events for back button android */
		
		Pollfish.shouldQuitEvent += shouldQuit;
		Pollfish.shouldNotQuitEvent += shouldNotQuit;
		
		#endif
	}

	private void EnablePollfish ()
	{
		if (pollfishEnabled) {
			
			#if UNITY_ANDROID
			
			appDeveloperKey = "d385d27f-27a7-4c36-be9d-fd384bfdaceb";
			
			#elif UNITY_IPHONE
			
			appDeveloperKey = "IOS_API_KEY";
			
			#endif
			
			// Init Pollfish
			Pollfish.PollfishInitFunction ((int)pollfishPosition, padding, appDeveloperKey, debugMode, customMode);
			// Hide Pollfish
			Pollfish.HidePollfish ();
		}
	}
	
	/** 
	 * unregister from Pollfish events 
	 */
	public void OnDisable ()
	{
		if (Application.platform != RuntimePlatform.WindowsEditor) {
			Pollfish.surveyCompletedEvent -= surveyCompleted;
			Pollfish.surveyOpenedEvent -= surveyOpened;
			Pollfish.surveyClosedEvent -= surveyClosed;
			Pollfish.surveyReceivedEvent -= surveyReceived;
			Pollfish.surveyNotAvailableEvent -= surveyNotAvailable;
			Pollfish.userNotEligibleEvent -= userNotEligible;
			
			#if UNITY_ANDROID
			
			// events for back button android
			
			Pollfish.shouldQuitEvent -= shouldQuit;
			Pollfish.shouldNotQuitEvent -= shouldNotQuit;
			
			#endif
		}
	}
	
	
	#if UNITY_ANDROID
	
	public void shouldQuit ()
	{
		Debug.Log ("PollfishEventListener: shouldQuit()");
		
		Application.Quit ();
		
	}	
	public void shouldNotQuit ()
	{
		Debug.Log ("PollfishEventListener: shouldNotQuit()");
	}	
	
	#endif
	
	public void surveyCompleted (string playfulSurveyAndSurveyPrice)
	{
		pendingSurveys--;

		string[] surveyCharacteristics = playfulSurveyAndSurveyPrice.Split (',');
		
		if (surveyCharacteristics.Length >= 2) {
						
			Debug.Log ("PollfishEventListener: Survey was completed - Playful Survey: " + surveyCharacteristics [0] + " with survey Price: " + surveyCharacteristics [1]);

			//labelText = "Survey was completed - Playful Survey: " + surveyCharacteristics[0] + " with survey Price: " + surveyCharacteristics[1];	
		}
		
	}
	
	public void surveyReceived (string playfulSurveyAndSurveyPrice)
	{
		internalSurveysReceived++;
		pendingSurveys++;
		//pollfishManager.AddPendingSurveys();
		string[] surveyCharacteristics = playfulSurveyAndSurveyPrice.Split (',');
		
		if (surveyCharacteristics.Length >= 2) {
			
			Debug.Log ("PollfishEventListener: Survey was received - Playful Survey: " + surveyCharacteristics [0] + " with survey Price: " + surveyCharacteristics [1]);
			
			//labelText = "Survey was received - Playful Survey: " + surveyCharacteristics [0] + " with survey Price: " + surveyCharacteristics [1];
			
		}
	}
	
	public void surveyOpened ()
	{
		internalSurveyOpened++;
		Debug.Log ("PollfishEventListener: Survey was opened");
		
		//labelText = "Survey was opened";
		
		PausePollfish ();
	}
	
	public void surveyClosed ()
	{
		internalSurveyClosed++;
		Debug.Log ("PollfishEventListener: Survey was closed");
		
		//labelText = "Survey was closed";
		UnpausePollfish ();

	}

	public void PausePollfish ()
	{
		ispaused = true;
	}

	public void UnpausePollfish ()
	{
		ispaused = false;
	}
	
	public void surveyNotAvailable ()
	{
		surveysNotAvailable++;
	}
	
	public void userNotEligible ()
	{
		internalUserNotAvailable = true;
	}

	public bool HasPendingSurveys ()
	{
		if (pollfishEnabled) {
			if (pendingSurveys > surveysNotAvailable && !internalUserNotAvailable && internalSurveyClosed == 0) {
				return true;
			}
		}
		
		return false;
	}

	// TODO: debug remove
	/*void OnGUI () {	
		GUI.Label(new Rect(10,170,Screen.width-20,40),"pendingSurveys: " + pendingSurveys);
		GUI.Label(new Rect(10,270,Screen.width-20,40),"surveysNotAvailable: " + surveysNotAvailable);
		GUI.Label(new Rect(10,370,Screen.width-20,40),"internalUserNotAvailable: " + internalUserNotAvailable);
	}*/

	public void ShowPollfishSurvey ()
	{
		if (pollfishEnabled) {
			Pollfish.ShowPollfish ();
		}
	}
	
	#endif
}