﻿using System;
using UnityEngine;
using AppodealAds.Unity.Api;
using AppodealAds.Unity.Common;

public class AppodealManager : MonoBehaviour, IInterstitialAdListener, IBannerAdListener, IVideoAdListener
{
	
	#if UNITY_EDITOR
	string appKey = "0d3c91ec15eb9ff31c66a64f12226a79ddd0cb9c4ce26784";
	#elif UNITY_ANDROID
	string appKey = "0d3c91ec15eb9ff31c66a64f12226a79ddd0cb9c4ce26784";
	#elif UNITY_IPHONE
	string appKey = "a23f1c3c5f3a996c6f92ffdb6cb7f52ba14b26e45aab3188";
	#else
	string appKey = "unexpected_platform";
	#endif

	void Awake ()
	{
		DontDestroyOnLoad (gameObject);
	}

	void Start ()
	{
		Appodeal.setBannerCallbacks (this);
		Appodeal.setInterstitialCallbacks (this);
		Appodeal.setVideoCallbacks (this);
		Appodeal.initialize (appKey);
	}

	public void AppodealShowBanner ()
	{
		Appodeal.show (Appodeal.BANNER);
	}

	public void AppodealShowInterstitial ()
	{
		Appodeal.show (Appodeal.INTERSTITIAL);
	}

	public void AppodealShowVideo ()
	{
		Appodeal.show (Appodeal.VIDEO);
	}

	public void AppodealHideBanner ()
	{
		Appodeal.hide (Appodeal.BANNER);
	}
	
	#region Banner callback handlers
	
	public void onBannerLoaded ()
	{
		print ("banner loaded");
	}
	public void onBannerFailedToLoad ()
	{
		print ("banner failed");
	}
	public void onBannerShown ()
	{
		print ("banner opened");
	}
	public void onBannerClicked ()
	{
		print ("banner clicked");
	}
	
	#endregion
	
	#region Interstitial callback handlers
	
	public void onInterstitialLoaded ()
	{
		print ("Interstitial loaded");
	}
	public void onInterstitialFailedToLoad ()
	{
		print ("Interstitial failed");
	}
	public void onInterstitialShown ()
	{
		print ("Interstitial opened");
	}
	public void onInterstitialClosed ()
	{
		print ("Interstitial closed");
	}
	public void onInterstitialClicked ()
	{
		print ("Interstitial clicked");
	}
	
	#endregion
	
	#region Video callback handlers
	
	public void onVideoLoaded ()
	{
		print ("Video loaded");
	}
	public void onVideoFailedToLoad ()
	{
		print ("Video failed");
	}
	public void onVideoShown ()
	{
		print ("Video opened");
	}
	public void onVideoClosed ()
	{
		print ("Video closed");
	}
	public void onVideoClicked ()
	{
		print ("Video clicked");
	}
	public void onVideoFinished ()
	{
		print ("Video finished");
	}
	
	#endregion
}
