﻿using UnityEngine;
using System.Collections;

public class AdsManager : MonoBehaviour
{
	#if UNITY_ANDROID || UNITY_IPHONE
	private int lastLevel;

	// Internet connection checker
	private InternetConnectionChecker internetconnectionChecker;
	// Pollfish manager
	private PollfishEventsManager pollfishManager;
	// Appodeal manager
	private AppodealManager appodealManager;

	void Awake ()

	{
		DontDestroyOnLoad (gameObject);
	}

	void Start ()
	{
		
		// Internet Checker
		GameObject internetChecker = GameObject.Find ("InternetConnectionChecker");
		
		if (internetChecker != null) {
			internetconnectionChecker = internetChecker.GetComponent<InternetConnectionChecker> ();
		}
		
		// Get pollfish manager
		GameObject pollfishEventManagerObj = GameObject.Find ("PollfishManager");
		
		if (pollfishEventManagerObj != null) {
			pollfishManager = pollfishEventManagerObj.GetComponent<PollfishEventsManager> ();
		}
		
		// Get appodeal manager
		GameObject appodealEventManagerObj = GameObject.Find ("AppodealManager");
		
		if (appodealEventManagerObj != null) {
			appodealManager = appodealEventManagerObj.GetComponent<AppodealManager> ();
		}
	}
	
	void OnEnable ()
	{
		GameController.OnLevelCompleted += TriggerInternetChecker;
		InGameMenu.OnPause += TriggerInternetChecker;
		InGameMenu.OnStartNextLevel += CloseBanner;
		InGameMenu.OnStartNextLevel += ShowCorrespondingBanner;
		InGameMenu.OnResume += CloseBanner;
		InGameMenu.OnRestart += CloseBanner;
		InGameMenu.OnMainMenu += CloseBanner;
		InGameMenu.OnMainMenu += ShowCorrespondingBanner;
		InternetConnectionChecker.CallBanner += ShowBanner;

		// Pollfish
		InGameMenu.OnPause += PollfishPause;
		InGameMenu.OnStartNextLevel += PollfishUnPause;
		TutrialController.OnBeginTutorial += PollfishPause;
		TutrialController.OnEndTutorial += PollfishUnPause;

		InGameMenu.OnResume += PollfishUnPause;
		InGameMenu.OnRestart += PollfishUnPause;
		InGameMenu.OnMainMenu += PollfishUnPause;
	}
	
	void OnDisable ()
	{
		GameController.OnLevelCompleted -= TriggerInternetChecker;
		InGameMenu.OnPause -= TriggerInternetChecker;
		InGameMenu.OnStartNextLevel -= CloseBanner;
		InGameMenu.OnStartNextLevel -= ShowCorrespondingBanner;
		InGameMenu.OnResume -= CloseBanner;
		InGameMenu.OnMainMenu -= CloseBanner;
		InGameMenu.OnRestart -= CloseBanner;
		InGameMenu.OnMainMenu -= ShowCorrespondingBanner;
		InternetConnectionChecker.CallBanner -= ShowBanner;

		// Pollfish
		InGameMenu.OnPause -= PollfishPause;
		InGameMenu.OnStartNextLevel -= PollfishUnPause;
		TutrialController.OnBeginTutorial -= PollfishPause;
		TutrialController.OnEndTutorial -= PollfishUnPause;

		InGameMenu.OnResume -= PollfishUnPause;
		InGameMenu.OnRestart -= PollfishUnPause;
		InGameMenu.OnMainMenu -= PollfishUnPause;
	}

	private void PollfishPause ()
	{
		pollfishManager.PausePollfish ();
	}

	private void PollfishUnPause ()
	{
		pollfishManager.UnpausePollfish ();
	}

	private void CloseBanner ()
	{
		appodealManager.AppodealHideBanner ();
		internetconnectionChecker.callBannerCallback = false;
	}

	void ShowBanner ()
	{

		appodealManager.AppodealShowBanner ();
	}

	private void TriggerInternetChecker ()
	{

		StartCoroutine (internetconnectionChecker.CheckInternet (true));
	}

	private void ShowCorrespondingBanner ()
	{
		if (internetconnectionChecker.IsInternetAvailable ()) {
			if (pollfishManager != null && pollfishManager.HasPendingSurveys ()) {
				// TODO: pause or unpause pollfish via event
				pollfishManager.ShowPollfishSurvey ();
			} else {
				// Show interstitial or video from appodeal
				appodealManager.AppodealShowInterstitial ();
			}
		}
	}
	#endif
}
