﻿using UnityEngine;
using System.Collections;

public class LaserFireController : MonoBehaviour
{

	#region Variables
	public float appliedForce = 0;
	public bool ignoreTriggerColliders = true;
	public bool fireRight = true;
	public bool continuosFire = false;
	public bool scheduledFireEnable;
	public float scheduledFireTimeLapse = 5f;
	public float fireLapse = 0.05f;
	public AudioClip sustainLaserClip;


	private bool laserDisabled = false;
	private float firedLapse = 0f;
	private float fireCooldown = 5f;
	private bool shouldFire = false;
	private LineRenderer line;
	private AudioSource soundSource;
	private Vector3 fireOrientation;
	#endregion

	#region MonoBehaviour
	void Start ()
	{
		line = GetComponent<LineRenderer> ();
		line.enabled = false;
		fireCooldown = scheduledFireTimeLapse;
		soundSource = GetComponent<AudioSource> ();
	}

	void Update ()
	{
		if (laserDisabled)
			return;

		if (continuosFire) {

			// For a continuous fire we need a sustainable audio clip. Change the clip, set to loop and play.
			if (soundSource && !soundSource.isPlaying) {
				soundSource.clip = sustainLaserClip;
				soundSource.loop = true;
				soundSource.Play ();
			}

			line.enabled = true;
			Fire ();
		} else {

			if (Input.GetButtonDown ("Fire1")) {
				StopCoroutine ("FireLaser");
				StartCoroutine ("FireLaser");
			}

			if (scheduledFireEnable) {
				ScheduledFire ();
				if (shouldFire) {

					// Fire only when the line is enabled for the first time
					if (!line.enabled)
					if (soundSource)
						soundSource.Play ();

					line.enabled = true;
					Fire ();
				} else {
					line.enabled = false;
				}
			}

		}
	}
	#endregion

	#region CustomBehaviour
	IEnumerator FireLaser ()
	{

		line.enabled = true;
//		if (soundSource)
//			soundSource.Play ();

		while (Input.GetButton("Fire1")) {

			Fire ();
			yield return null;
		}
		// Stop fire sound.
		line.enabled = false;

	}

	void Fire ()
	{
		// Set up an Invisible Ray Object
		Ray2D ray = new Ray2D (transform.position, transform.right);
		
		// Check orientation
		if (fireRight) {
			fireOrientation = transform.right;
		} else {
			fireOrientation = -transform.right;
		}

		// Raycast to  know if are there obstacles ahead.
		RaycastHit2D hit = Physics2D.Raycast (transform.position, fireOrientation);
		
		// The line will always start on the ray origin
		line.SetPosition (0, ray.origin);
		
		// If there is a hit, then cast a line from the ray origin to the hit object
		if (hit.collider != null && (!hit.collider.isTrigger || !ignoreTriggerColliders)) {
			
			line.SetPosition (1, hit.point);
			if (hit.rigidbody) {
				hit.rigidbody.AddForceAtPosition (transform.right * appliedForce, hit.point);
			}
			
		} else {
			// if there is not a hit, then just render a line to infity (or 100 ahead in this case)
			line.SetPosition (1, ray.GetPoint (100));
		}

	}

	void ScheduledFire ()
	{

		fireCooldown -= Time.deltaTime;
		if (fireCooldown <= 0) {
			if (firedLapse < fireLapse) {
				shouldFire = true;
				firedLapse += Time.deltaTime;
			} else {
				shouldFire = false;
				firedLapse = 0;
				fireCooldown = scheduledFireTimeLapse;
			}
		}
	}

	public void DisableLaser ()
	{
		laserDisabled = true;
		line.enabled = false;
		soundSource.Stop ();

	}

	public void EnableLaser ()
	{
		laserDisabled = false;
//		line.enabled = true;
		soundSource.Play ();
		
	}
	#endregion
}
