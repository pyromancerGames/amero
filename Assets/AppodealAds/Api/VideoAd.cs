﻿using System;
using AppodealAds.Unity.Common;

namespace AppodealAds.Unity.Api
{
	public class VideoAd : IAdListener
	{
		private IAppodealAdsVideoClient client;
		private IVideoAdListener listener;
		
		// Creates an InsterstitialAd.
		public VideoAd(string appKey)
		{
			client = AppodealAdsClientFactory.GetAppodealAdsVideoClient(this);
			client.CreateVideoAd(appKey);
		}

		// set listener
		internal void SetListener(IVideoAdListener listener)
		{
			this.listener = listener;
		}
		
		// Loads a new interstitial request
		public void LoadAd(AODAdRequest request)
		{
			client.LoadAd(request);
		}
		
		// Determines whether the VideoAd has loaded.
		public bool IsLoaded()
		{
			return client.IsLoaded();
		}
		
		// Show the VideoAd.
		public void Show()
		{
			client.ShowVideoAd();
		}
		
		// Destroy the VideoAd.
		public void Destroy()
		{
			client.DestroyVideoAd();
		}

		public void Cache()
		{
			client.Cache();
		}

		public void SetAutoCache (bool autoCache)
		{
			client.SetAutoCache(autoCache);
		}
		
		public void SetOnLoadedTriggerBoth(bool onLoadedTriggerBoth) 
		{
		}
		
		public void DisableNetwork (string adName)
		{
			client.DisableNetwork (adName);
		}
		
		public void ShowAdWithAdName (string adName)
		{
			client.ShowAdWithAdName(adName);
		}
		
		#region IAdListener implementation
		
		// The following methods are invoked from an IAppodealAdsVideoClient. Forward
		// these calls to the developer.
		void IAdListener.FireAdLoaded()
		{
			listener.onVideoLoaded ();
		}
		
		void IAdListener.FireAdFailedToLoad(string message)
		{
			listener.onVideoFailedToLoad ();
		}
		
		void IAdListener.FireAdOpened()
		{
			listener.onVideoShown ();
		}
		
		void IAdListener.FireAdClosing()
		{
			listener.onVideoClosed();
		}
		
		void IAdListener.FireAdClosed()
		{
			listener.onVideoClosed();
		}
		
		void IAdListener.FireAdLeftApplication()
		{
			listener.onVideoClicked ();
		}
		
		void IAdListener.FireRewardUser(int amount)
		{
			listener.onVideoFinished();
		}
		
		#endregion
	}
}
