﻿using System;
using System.Runtime.InteropServices;

namespace AppodealAds.Unity.iOS
{
	// Externs used by the iOS component.
	internal class Externs
	{
		#region Common externs

		[DllImport("__Internal")]
		internal static extern void AODUInitAppodeal(string appKey);

		[DllImport("__Internal")]
		internal static extern void AODUInitAppodealWithAdTypes(string appKey, int adTypes);
		
		[DllImport("__Internal")]
		internal static extern IntPtr AODUCreateRequest();
		
		[DllImport("__Internal")]
		internal static extern void AODURelease(IntPtr obj);
		
		#endregion
		
		#region Banner externs
		
		[DllImport("__Internal")]
		internal static extern IntPtr AODUCreateBannerView(
			IntPtr bannerClient, string appKey, int positionAtTop);
		
		[DllImport("__Internal")]
		internal static extern IntPtr AODUCreateSmartBannerView(
			IntPtr bannerClient, string appKey, int positionAtTop);
		
		[DllImport("__Internal")]
		internal static extern void AODUSetBannerCallbacks(
			IntPtr bannerView,
			IOSBannerClient.AODUAdViewDidReceiveAdCallback adReceivedCallback,
			IOSBannerClient.AODUAdViewDidFailToReceiveAdWithErrorCallback adFailedCallback,
			IOSBannerClient.AODUAdViewWillPresentScreenCallback willPresentCallback,
			IOSBannerClient.AODUAdViewWillDismissScreenCallback willDismissCallback,
			IOSBannerClient.AODUAdViewDidDismissScreenCallback didDismissCallback,
			IOSBannerClient.AODUAdViewWillLeaveApplicationCallback willLeaveCallback);
		
		[DllImport("__Internal")]
		internal static extern void AODUHideBannerView(IntPtr bannerView);
		
		[DllImport("__Internal")]
		internal static extern void AODUShowBannerView(IntPtr bannerView, int adTypes);

		[DllImport("__Internal")]
		internal static extern bool AODUBannerReady(IntPtr bannerView);
		
		[DllImport("__Internal")]
		internal static extern void AODURemoveBannerView(IntPtr bannerView);
		
		[DllImport("__Internal")]
		internal static extern void AODURequestBannerAd(IntPtr bannerView, IntPtr request);

		[DllImport("__Internal")]
		internal static extern void AODUCacheBanner(IntPtr bannerView);
		
		[DllImport("__Internal")]
		internal static extern void AODUSetAutoCacheBanner(IntPtr bannerView, bool autoCache);
		
		[DllImport("__Internal")]
		internal static extern void AODUDisableNetworkBanner(IntPtr bannerView, string adName);
		
		[DllImport("__Internal")]
		internal static extern void AODUShowBannerWithAdName(IntPtr bannerView, string adName);
		
		#endregion
		
		#region Interstitial externs
		
		[DllImport("__Internal")]
		internal static extern IntPtr AODUCreateInterstitial(
			IntPtr interstitialClient, string appKey);
		
		[DllImport("__Internal")]
		internal static extern void AODUSetInterstitialCallbacks(
			IntPtr interstitial,
			IOSInterstitialClient.AODUInterstitialDidReceiveAdCallback interstitialReceivedCallback,
			IOSInterstitialClient.AODUInterstitialDidFailToReceiveAdWithErrorCallback
			interstitialFailedCallback,
			IOSInterstitialClient.AODUInterstitialWillPresentScreenCallback interstitialWillPresentCallback,
			IOSInterstitialClient.AODUInterstitialWillDismissScreenCallback interstitialWillDismissCallback,
			IOSInterstitialClient.AODUInterstitialDidDismissScreenCallback interstitialDidDismissCallback,
			IOSInterstitialClient.AODUInterstitialWillLeaveApplicationCallback
			interstitialWillLeaveCallback);
		
		[DllImport("__Internal")]
		internal static extern bool AODUInterstitialReady(IntPtr interstitial);
		
		[DllImport("__Internal")]
		internal static extern void AODUShowInterstitial(IntPtr interstitial);
		
		[DllImport("__Internal")]
		internal static extern void AODURequestInterstitial(IntPtr interstitial, IntPtr request);

		[DllImport("__Internal")]
		internal static extern void AODUCacheInterstitial(IntPtr interstitial);
		
		[DllImport("__Internal")]
		internal static extern void AODUSetAutoCacheInterstitial(IntPtr interstitial, bool autoCache);
		
		[DllImport("__Internal")]
		internal static extern void AODUDisableNetworkInterstitial(IntPtr interstitial, string adName);
		
		[DllImport("__Internal")]
		internal static extern void AODUShowInterstitialWithAdName(IntPtr interstitial, string adName);
		
		#endregion

		#region Video externs
		
		[DllImport("__Internal")]
		internal static extern IntPtr AODUCreateVideo(
			IntPtr videoClient, string appKey);
		
		[DllImport("__Internal")]
		internal static extern void AODUSetVideoCallbacks(
			IntPtr video,
			IOSVideoClient.AODUVideoAdShouldRewardUserCallback videoRewardCallback,
			IOSVideoClient.AODUVideoDidReceiveAdCallback videoReceivedCallback,
			IOSVideoClient.AODUVideoDidFailToReceiveAdWithErrorCallback
			videoFailedCallback,
			IOSVideoClient.AODUVideoWillPresentScreenCallback videoDidPresentCallback,
			IOSVideoClient.AODUVideoDidDismissScreenCallback videoDidDismissCallback,
			IOSVideoClient.AODUVideoWillLeaveApplicationCallback
			videoWillLeaveCallback);
		
		[DllImport("__Internal")]
		internal static extern bool AODUVideoReady(IntPtr video);
		
		[DllImport("__Internal")]
		internal static extern void AODUShowVideo(IntPtr video);
		
		[DllImport("__Internal")]
		internal static extern void AODURequestVideo(IntPtr video, IntPtr request);

		[DllImport("__Internal")]
		internal static extern void AODUCacheVideo(IntPtr video);

		[DllImport("__Internal")]
		internal static extern void AODUSetAutoCacheVideo(IntPtr video, bool autoCache);

		[DllImport("__Internal")]
		internal static extern void AODUDisableNetworkVideo(IntPtr video, string adName);

		[DllImport("__Internal")]
		internal static extern void AODUShowVideoWithAdName(IntPtr video, string adName);

		#endregion
	}
}