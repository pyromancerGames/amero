﻿using UnityEngine;
using System;
using AppodealAds.Unity.Api;
using AppodealAds.Unity.Common;

namespace AppodealAds.Unity.iOS
{
	public class IOSAppodealClient : IAppodealAdsClient {

		private InterstitialAd interstitial;
		private VideoAd video;
		private BannerView bannerView;

		public InterstitialAd getInterstitial() {
			if (interstitial == null) {
				interstitial = new InterstitialAd("");
				interstitial.LoadAd(createAdRequest());
			}
			return interstitial;
		}
		
		public BannerView getBanner() {
			if (bannerView == null) {
				bannerView = new BannerView("", AdPosition.BottomPortrait);
			}
			return bannerView;
		}

		public VideoAd getVideo() {
			if (video == null) {
				video = new VideoAd("");
				video.LoadAd(createAdRequest());
			}
			return video;
		}

		// Init sdk
		public void initialize(string appKey) {
			Externs.AODUInitAppodeal(appKey);
		}

		public void initialize(string appKey, int adTypes) {
			Externs.AODUInitAppodealWithAdTypes (appKey, adTypes);
		}
		
		public void setInterstitialCallbacks(IInterstitialAdListener listener) 
		{
			getInterstitial ().SetListener (listener);
		}
		
		public void setVideoCallbacks(IVideoAdListener listener)
		{
			getVideo ().SetListener (listener);
		}
		
		public void setBannerCallbacks(IBannerAdListener listener)
		{
			getBanner ().SetListener (listener);
		}
		
		public void cache(int adTypes)
		{
			switch (adTypes) 
			{
			case Appodeal.INTERSTITIAL:
				getInterstitial().Cache();
				break;
			case Appodeal.VIDEO:
				getVideo ().Cache();
				break;
			case Appodeal.BANNER:
				getBanner().Cache();
				break;
			case Appodeal.BANNER_BOTTOM:
				getBanner().Cache();
				break;
			case Appodeal.BANNER_TOP:
				getBanner().Cache();
				break;
			case Appodeal.BANNER_CENTER:
				getBanner().Cache();
				break;
			default:
				break;
			}
		}
		
		public Boolean isLoaded(int adTypes) 
		{
			switch (adTypes) 
			{
			case Appodeal.INTERSTITIAL:
				return getInterstitial().IsLoaded();
			case Appodeal.VIDEO:
				return getVideo ().IsLoaded();
			case Appodeal.BANNER:
				return getBanner().IsLoaded();
			case Appodeal.BANNER_BOTTOM:
				return getBanner().IsLoaded();
			case Appodeal.BANNER_TOP:
				return getBanner().IsLoaded();
			case Appodeal.BANNER_CENTER:
				return getBanner().IsLoaded();
			default:
				break;
			}
			return false;
		}
		
		public Boolean isPrecache(int adTypes) 
		{
			// TODO:
			return false;
		}
		
		public Boolean show(int adTypes)
		{
			switch (adTypes) {
			case Appodeal.INTERSTITIAL:
				getInterstitial().Show();
				return getInterstitial().IsLoaded();
			case Appodeal.VIDEO:
				getVideo().Show();
				return getVideo().IsLoaded();
			case Appodeal.BANNER:
				getBanner().Show(Appodeal.BANNER);
				return getBanner().IsLoaded();
			case Appodeal.BANNER_BOTTOM:
				getBanner().Show(Appodeal.BANNER_BOTTOM);
				return getBanner().IsLoaded();
			case Appodeal.BANNER_TOP:
				getBanner().Show(Appodeal.BANNER_TOP);
				return getBanner().IsLoaded();
			case Appodeal.BANNER_CENTER:
				getBanner().Show(Appodeal.BANNER_CENTER);
				return getBanner().IsLoaded();
			}
			if ((adTypes & Appodeal.INTERSTITIAL) > 0 && (adTypes & Appodeal.VIDEO) > 0) {
				if(getVideo().IsLoaded()) {
					getVideo().Show();
					return getVideo().IsLoaded();
				} else {
					getInterstitial().Show();
					return getInterstitial().IsLoaded();
				}
			}
			return false;
		}
		
		public Boolean showWithPriceFloor(int adTypes)
		{
			return show(adTypes);	
		}
		
		public void hide(int adTypes)
		{
			// TODO:
		}
		
		public void setAutoCache(int adTypes, Boolean autoCache) 
		{
			if ((adTypes & Appodeal.INTERSTITIAL) > 0) {
				getInterstitial().SetAutoCache(autoCache);
			}
			
			if ((adTypes & Appodeal.VIDEO) > 0) {
				getVideo().SetAutoCache(autoCache);
			}
			
			if ((adTypes & (Appodeal.BANNER | Appodeal.BANNER_BOTTOM | Appodeal.BANNER_TOP |
			                Appodeal.BANNER_CENTER)) > 0) {
				getBanner().SetAutoCache(autoCache);
			}	
		}
		
		public void setOnLoadedTriggerBoth(int adTypes, Boolean onLoadedTriggerBoth) 
		{
			// TODO:
		}
		
		public void disableNetwork(String network) 
		{
			getInterstitial().DisableNetwork (network);
			getVideo().DisableNetwork (network);
			getBanner().DisableNetwork (network);
		}
		
		public void disableLocationPermissionCheck() 
		{
			// TODO:
		}
		
		public void orientationChange()
		{
			// TODO:
		}

		#region private methods

		// Returns an ad request with custom ad targeting.
		private AODAdRequest createAdRequest()
		{
			return new AODAdRequest();
			
		}

		#endregion
	}
}
