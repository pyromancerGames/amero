﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MuteButtonManager : MonoBehaviour {

	public Sprite btnMuteOnSprite;
	public Sprite btnMuteOffSprite;

	private bool isOn;
	private Image btnImage;

	// Use this for initialization
	void Start () {
		btnImage = gameObject.GetComponent<Image>();
		SwitchMuteButtonSpite();
	}
	
	public void SwitchMuteButtonSpite() {
		// Switch button sprite
		if(AudioListener.volume == 1) {
			isOn = true;
		} else {
			isOn = false;
		}

		if(isOn) {
			btnImage.sprite = btnMuteOnSprite;
		} else  {
			btnImage.sprite = btnMuteOffSprite;
		}
	}
}
