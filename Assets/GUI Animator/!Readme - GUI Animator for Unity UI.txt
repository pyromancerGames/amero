- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -
GUI Animator for Unity UI
Version: 0.8.4
- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -

Quickly and easily add tween animations to Unity UI objects in just a few steps.

1) Drag and drop GEAnim to Unity UI objects.
2) Set up some parameters such as ease type, time, delay, alignment.
3) Press Play button.

This package will reduces your working time from days/hours to just minutes. It is easy to use and modify.

Features:

	- Works with Unity build-in UI system.
	- 8 Demo scenes with sample scripts.
	- Full C# source code.
	- Works with all Canvas Render Modes and UI Scale Modes.
	- Friendly inspector categorizes animations into tabs.
	- Animate position, rotation, scale, fade of UI object using tweeners.
	- Compatible with 4 popular tweeners; iTween, HOTween, DOTween and LeanTween (default is LeanTween).
	- Use preprocessor directives to switch Tweener.
	- You can tweak GEAnim script file to add support to more tweeners.
	- Ignorable time scale.
	- Add begin/end sounds to animations.
	- Supports all Unity player platforms.
	- Compatible Unity and Unity Pro 4.6.x and 5.x.x

Note:
	- Please direct any bugs/comments/suggestions to geteamdev@gmail.com

- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -

Quick Start Walkthrough.

	1. In Project tab, look for a folder names "GUI Animator/Scenes/Demo (Unity UI)"
	2. Press Control+Shift+B (for Mac user press Command+Shift+B) or use top menu select "Top Menu->Edit->Build Settings".
	3. Drag Demo00 to Demo 08 files from "GUI Animator/Scenes/Demo (Unity UI)" to "Scene In Build" section in "Build Settings" dialog.
	4. Close "Build Settings" dialog.
	5. In Project tab, double click on "GUI Animator/Scenes/Demo (Unity UI)/Demo00" file.
	6. In Game Tab, Click show display resolutions then press plus button. Add dialog will appear.
	7. In Add dialog,
			- Label text area type 960x600.
			- Type list box select Fixed Resolution.
			- Width & Height type 960 and 600.
			- Click on OK button.
	8. Make sure Game Tab use resolution at 960x600.
	9. Press Play button.

- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -
	
How to switch tweeners between DOTween, HOTween, iTween and LeanTween.

	1. Press Control+Shift+B (for Mac user press Command+Shift+B) or use top menu select "Top Menu->Edit->Build Settings".
	2. Click on "Player Settings" button.
	3. In Inspector tab, look at "Configuration->Scripting Define Symbols" text area. You can assign only one tweener for GUI Animator.
		3.1. DOTween, type DOTWEEN in the text area.
		3.2. HOTween, type HOTWEEN in the text area.
		3.3. iTween, type ITWEEN in the text area.
		3.4. LeanTween, leave it blank.
	Note: LeanTween is the default tweener for GUI Animator.

- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -

Web Player Demo:
	http://ge-team.com/pages/unity-3d/gui-animator-for-unity-ui/

Gold Experience Team Product page:
	http://ge-team.com/pages/unity-3d/

- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -
	
iTween: https://www.assetstore.unity3d.com/#/content/84 
iTween Documentation: http://itween.pixelplacement.com/documentation.php	

HOTween: https://www.assetstore.unity3d.com/#/content/3311
HOTween Documentation:  http://hotween.demigiant.com/documentation.html
	
DOTween: https://www.assetstore.unity3d.com/en/#!/content/27676
DOTween Documentation: http://dotween.demigiant.com/documentation.php

LeanTween: https://www.assetstore.unity3d.com/#/content/3595
LeanTween Documentation: http://dentedpixel.com/LeanTweenDocumentation/classes/LeanTween.html

Here are good references for what you can expect from each ease type:
Easings.net: http://easings.net
RobertPenner.com: http://www.robertpenner.com/easing/easing_demo.html

- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -

Please, feel free to send request or bug report directly to developer e-mail at geteamdev@gmail.com
We would like to hear from you.

Thank you for your support,
Gold Experience Team
