// GUI Animator for Unity UI version: 0.8.4 (Product page: http://ge-team.com/pages/unity-3d/gui-animator-for-unity-ui/)
//
// Author:			Gold Experience Team (http://www.ge-team.com/pages/)
// Products:		http://ge-team.com/pages/unity-3d/
// Support:			geteamdev@gmail.com
// Documentation:	http://ge-team.com/pages/unity-3d/gui-animator-for-unity-ui/gui-animator-for-unity-ui-documentation/
//
// Please direct any bugs/comments/suggestions to geteamdev@gmail.com

#region Namespaces

using UnityEngine;
using System.Collections;

using UnityEngine.UI;		// If you have error at this line on Unity 5.x, please make sure that you are using Unity 5.x with a valid license.

#endregion

#region Auto Animation

// collection of animation mode
public enum eAnimationMode
{
	None,
	In,
	Idle,
	Out,
	All
}

#endregion

#region Move Hierachy

// collection of object's hierarchy move type
public enum eGUIMove
{
	Self,
	Children,
	SelfAndChildren
}

#endregion

/**************
* GEAnimSystem class
* This is Singleton Pattern. It handles animation speed and auto animation of all GEAnim elements in the scene.
**************/

#region GEAnimSystem

public class GEAnimSystem : MonoBehaviour
{

	#region Variables

		// Private reference which can be accessed by this class only
		private static GEAnimSystem instance;

		// Public static reference that can be accesd from anywhere
		public static GEAnimSystem Instance
		{
			get
			{
				// Check if instance has not been set yet and set it it is not set already
				// This takes place only on the first time usage of this reference
				if(instance==null)
				{
					instance = GameObject.FindObjectOfType<GEAnimSystem>();
					DontDestroyOnLoad(instance.gameObject);
				}
				return instance;
			}
		}

		// Public property which can be accessed using the instance
		// 	Example:
		//		GEAnimSystem.Instance.m_GUISpeed = 5;
		//	Note:
		//		Changing this value anywhere in your scripts to adjust speed of all GEAnim elements.
		[Range(0.5f,10.0f)]
		public float m_GUISpeed = 1.0f;
		
		
		// Enable or Disable auto animation.
		// If this variable is true, GEAnimSystem will play all GEAnim elements automatically when the scene starts to play.
		//	Example:
		//		GEAnimSystem.Instance.m_AutoAnimation = false;
		//	Note:
		//		- It effects both on Unity Editor and target device.
		//		- If you want your whole scene plays animation automatically, this variable must be true.
		//		- Set this value to false if you want to control all GEAnim in the scene via your scripts.
		//		- In run-time you can change it only in Awake function (See Demo00 to Demo08 script files for example).
		public bool m_AutoAnimation = true;
		
		
		// Animation to run automatically in Play mode when the m_AutoAnimation flag is true.
	    //		- None:		Do not play animations.
		//		- In:		Play In and Idle animations.
		//		- Idle:		Play Idle for how long in m_IdleTime.
		//		- Out:		Play Out animation.
		//		- All:		Play In->Idle->Out animations.
		public eAnimationMode m_AnimationMode = eAnimationMode.All;
		
		// Amount of time in seconds for how long the Idle animation will play.
		// This value works with m_AutoAnimation and m_AnimationMode
	    public float m_IdleTime = 8.0f;
    
    #endregion
    
	// ######################################################################
	// MonoBehaviour Functions
	// ######################################################################
	
	#region MonoBehaviour
		
		// Awake is called when the script instance is being loaded.
		void Awake()
		{
			if(instance == null)
			{
				// Make the current instance as the singleton
				instance = this;

				// Make it persistent  
				DontDestroyOnLoad(this);
			}
			else
			{
				// If more than one singleton exists in the scene find the existing reference from the scene and destroy it
				if(this != instance)
				{
					Destroy(this.gameObject);
				}
			}
		}
		
		// Start is called on the frame when a script is enabled just before any of the Update methods is called the first time.
		void Start ()
		{
			
		}
		
		// Update is called every frame, if the MonoBehaviour is enabled.
		void Update ()
		{
			
		}
	
	#endregion
	
	// ######################################################################
	// Load Level Functions
	// These functions are used in Demo00 to Demo01 scritps
	// ######################################################################
	
	#region Load Level

		// Load Level with a delay time
		public void LoadLevel(string LevelName, float delay)
		{
			if(delay<=0)
			{
				Application.LoadLevel(LevelName);
			}
			else
			{
				StartCoroutine(LoadLevelDelay(LevelName, delay));
			}
		}
		
		// Load Level with a delay time
		IEnumerator LoadLevelDelay(string LevelName, float delay)
		{
			yield return new WaitForSeconds(delay);
			
			Application.LoadLevel(LevelName);
		}
	
	#endregion
	
	// ######################################################################
	// Move In/Out Functions
	// You can use these functions to animate GEAnim elelmets
	// ######################################################################
	
	#region Move In/Out

		// Play In-animation
		// 	Example
		//		GEAnimSystem.Instance.MoveIn(SOME_GEAnim, true);
	    public void MoveIn(Transform trans, bool EffectsOnChildren)
		{
			GEAnim pGUIAnim = trans.gameObject.GetComponent<GEAnim>();
			if(pGUIAnim!=null)
			{
				pGUIAnim.MoveIn();
			}
			
			Button pButton = trans.gameObject.GetComponent<Button>();
			if(pButton!=null)
			{
				pButton.enabled = true;
			}

			if(EffectsOnChildren==true)
			{
				foreach(Transform child in trans)
				{
					MoveIn(child, EffectsOnChildren);
				}
			}
		}

		// Play Out-animation
		// 	Example
		//		GEAnimSystem.Instance.MoveOut(SOME_GEAnim, true);
		public void MoveOut(Transform trans, bool EffectsOnChildren)
	    {
			GEAnim pGUIAnim = trans.gameObject.GetComponent<GEAnim>();
			if(pGUIAnim!=null)
			{
				pGUIAnim.MoveOut();
			}

			Button pButton = trans.gameObject.GetComponent<Button>();
			if(pButton!=null)
			{
				pButton.enabled = false;
			}

			if(EffectsOnChildren==true)
			{
				foreach(Transform child in trans)
				{
					MoveOut(child, EffectsOnChildren);
				}
			}
		}
	
	#endregion
	
	// ######################################################################
	// Enable/Disable Button Functions
	// ######################################################################
	
	#region Enable/Disable Button
		
		// Enable/disable all Unity Buttons
		public void EnableButton(bool Enable)
		{
			Button[] ButtonList = GameObject.FindObjectsOfType<Button>();
			foreach(Button pButton in ButtonList)
			{
				pButton.enabled = Enable;
			}
		}

		// Enable/disable a Button with it's children buttons
		public void EnableButton(Transform trans, bool Enable)
		{		
			Button pButton = trans.gameObject.GetComponent<Button>();
			if(pButton!=null)
			{
				pButton.enabled = Enable;
			}

			foreach(Transform child in trans)
			{
				EnableButton(child, Enable);
			}
		}
	
	#endregion
	
	// ######################################################################
	// Utilities Functions
	// ######################################################################
	
	#region UNITY_UI Utilities

		// Find parent Canvas of any GameObject
		public Canvas GetParent_Canvas(Transform trans)
		{
			Transform ParentObj = trans.parent;
			while(ParentObj!=null)
			{
				Canvas pCanvas = ParentObj.gameObject.GetComponent<Canvas>();
				if(pCanvas!=null)
				{
					return pCanvas;
				}
				ParentObj = ParentObj.parent;
			}
			
			return null;
		}
	
	#endregion

}

#endregion
