﻿// GUI Animator for Unity UI version: 0.8.4 (Product page: http://ge-team.com/pages/unity-3d/gui-animator-for-unity-ui/)
//
// Author:			Gold Experience Team (http://www.ge-team.com/pages/)
// Products:		http://ge-team.com/pages/unity-3d/
// Support:			geteamdev@gmail.com
// Documentation:	http://ge-team.com/pages/unity-3d/gui-animator-for-unity-ui/gui-animator-for-unity-ui-documentation/
//
// Please direct any bugs/comments/suggestions to geteamdev@gmail.com

#region Namespaces

using UnityEngine;
using System.Collections;

using UnityEngine.UI;		// If you have error at this line on Unity 5.x, please make sure that you are using Unity 5.x with a valid license.

// DOTween: https://www.assetstore.unity3d.com/en/#!/content/27676
// DOTween Documentation: http://dotween.demigiant.com/documentation.php

// HOTween: https://www.assetstore.unity3d.com/#/content/3311
// HOTween Documentation:  http://hotween.demigiant.com/documentation.html

// LeanTween: https://www.assetstore.unity3d.com/#/content/3595
// LeanTween Documentation: http://dentedpixel.com/LeanTweenDocumentation/classes/LeanTween.html

// iTween: https://www.assetstore.unity3d.com/#/content/84 
// iTween Documentation: http://itween.pixelplacement.com/documentation.php

#if DOTWEEN
	using DG.Tweening;
#elif HOTWEEN
	using Holoville.HOTween;
#endif

#endregion

#region EaseType

// Common ease type for iTween, HOTween, DOTween, LeanTween
// DOTween Documentation: http://dotween.demigiant.com/documentation.php/documentation.php
// HOTween EaseType: http://www.holoville.com/hotween/hotweenAPI/namespace_holoville_1_1_h_o_tween.html#ab8f6c428f087160deca07d7d402c4934
// LeanTween EaseType: http://dentedpixel.com/LeanTweenDocumentation/classes/LeanTweenType.html
// iTween EaseType: http://itween.pixelplacement.com/documentation.php

// Here are good references for what you can expect from each ease type:
// http://easings.net
// http://www.robertpenner.com/easing/easing_demo.html

// Collection of Easings
public enum eEaseType
{
	InQuad,
	OutQuad,
	InOutQuad,
	InCubic,
	OutCubic,
	InOutCubic,
	InQuart,
	OutQuart,
	InOutQuart,
	InQuint,
	OutQuint,
	InOutQuint,
	InSine,
	OutSine,
	InOutSine,
	InExpo,
	OutExpo,
	InOutExpo,
	InCirc,
	OutCirc,
	InOutCirc,
	linear,
	spring,
	InBounce,
	OutBounce,
	InOutBounce,
	InBack,
	OutBack,
	InOutBack,
	InElastic,
	OutElastic,
	InOutElastic
}

#endregion

#region Alignment

// Collection of alignments
public enum eAlignment
{
	Current,
	TopLeft,
	TopCenter,
	TopRight,
	LeftCenter,
	Center,
	RightCenter,
	BottomLeft,
	BottomCenter,
	BottomRight
}

#endregion

#region Move

// Collection of points and screen edges for In/Out animations
public enum ePosMove
{
	ParentPosition,
	LocalPosition,
	
	UpperScreenEdge,
	LeftScreenEdge,
	RightScreenEdge,
	BottomScreenEdge,
	
	UpperLeft,
	UpperCenter,
	UpperRight,
	MiddleLeft,
	MiddleCenter,
	MiddleRight,
	BottomLeft,
	BottomCenter,
	BottomRight,

	SelfPosition
}

// Sound information
[System.Serializable]
public class cSounds
{
	public AudioClip m_Begin	= null;
	public AudioClip m_End		= null;
}

// Sound information for PingPong loop animation
[System.Serializable]
public class cSoundsForPingPongAnim
{
	public AudioClip m_AudioClip		= null;
	public bool m_Loop					= false;
	[HideInInspector]
	public AudioSource m_AudioSource	= null;
}

// Move-In animation class
[System.Serializable]
public class cMoveIn
{
	public bool Enable = false;
	
	[HideInInspector]
	public bool Began = false;
	[HideInInspector]
	public bool Animating = false;
	[HideInInspector]
	public bool Done = false;
	public ePosMove MoveFrom = ePosMove.UpperScreenEdge;
	[HideInInspector]
	public Vector3 BeginPos;
	[HideInInspector]
	public Vector3 EndPos;
	//[HideInInspector]
	public Vector3 Position;
	public eEaseType EaseType = eEaseType.OutBack;
	public float Time = 1.0f;
	public float Delay;
	public cSounds Sounds;
}

// Move-Out animation class
[System.Serializable]
public class cMoveOut
{
	public bool Enable = false;
	[HideInInspector]
	public bool Began = false;
	[HideInInspector]
	public bool Animating = false;
	[HideInInspector]
	public bool Done = false;
	public ePosMove MoveTo = ePosMove.UpperScreenEdge;
	[HideInInspector]
	public Vector3 BeginPos;
	[HideInInspector]
	public Vector3 EndPos;
	//[HideInInspector]
	public Vector3 Position;
	public eEaseType EaseType = eEaseType.InBack;
	public float Time = 1.0f;
	public float Delay;
	public cSounds Sounds;
}

#endregion

#region Rotation In/Out

// Rotation-In animation class
[System.Serializable]
public class cRotationIn
{
	public bool Enable = false;
	[HideInInspector]
	public bool Began = false;
	[HideInInspector]
	public bool Animating = false;
	[HideInInspector]
	public bool Done = false;
	[HideInInspector]
	public Vector3 BeginRotation;
	[HideInInspector]
	public Vector3 EndRotation;
	//[HideInInspector]
	public Vector3 Rotation = new Vector3();
	public eEaseType EaseType = eEaseType.OutBack;
	public float Time = 1.0f;
	public float Delay;
	public cSounds Sounds;
}

// Rotation-Out animation class
[System.Serializable]
public class cRotationOut
{
	public bool Enable = false;
	[HideInInspector]
	public bool Began = false;
	[HideInInspector]
	public bool Animating = false;
	[HideInInspector]
	public bool Done = false;
	[HideInInspector]
	public Vector3 BeginRotation;
	[HideInInspector]
	public Vector3 EndRotation;
	//[HideInInspector]
	public Vector3 Rotation = new Vector3();
	public eEaseType EaseType = eEaseType.InBack;
	public float Time = 1.0f;
	public float Delay;
	public cSounds Sounds;
}

#endregion

#region Scale In/Out

// Scale-In animation class
[System.Serializable]
public class cScaleIn
{
	public bool Enable = false;
	[HideInInspector]
	public bool Began = false;
	[HideInInspector]
	public bool Animating = false;
	[HideInInspector]
	public bool Done = false;
	public Vector3 ScaleBegin = new Vector3(0,0,0);
	public eEaseType EaseType = eEaseType.OutBack;
	public float Time = 1.0f;
	public float Delay;
	public cSounds Sounds;
}

// Scale-Out animation class
[System.Serializable]
public class cScaleOut
{
	public bool Enable = false;
	[HideInInspector]
	public bool Began = false;
	[HideInInspector]
	public bool Animating = false;
	[HideInInspector]
	public bool Done = false;
	[HideInInspector]
	public Vector3 ScaleBegin = new Vector3(1,1,1);
	public Vector3 ScaleEnd = new Vector3(0,0,0);
	public eEaseType EaseType = eEaseType.InBack;
	public float Time = 1.0f;
	public float Delay;
	public cSounds Sounds;
}

#endregion

#region Fade In/Out

// Fade In/Out animation class
[System.Serializable]
public class cFade
{
	public bool Enable = false;
	[HideInInspector]
	public bool Began = false;
	[HideInInspector]
	public bool Animating = false;
	[HideInInspector]
	public bool Done = false;
	[HideInInspector]
	public float Fade = 0;
	public eEaseType EaseType = eEaseType.linear;
	public float Time = 1.0f;
	public float Delay;
	public bool FadeChildren = false;
	public cSounds Sounds;
}

#endregion

#region Scale Loop

// Scale-Loop animation class
[System.Serializable]
public class cPingPongScale
{
	public bool Enable = false;
	[HideInInspector]
	public bool Began = false;
	[HideInInspector]
	public bool Animating = false;
	[HideInInspector]
	public bool IsOverriding = false;
	[HideInInspector]
	public float IsOverridingDelayTimeCount = 0.0f;
	[HideInInspector]
	public bool Done = false;
	[HideInInspector]
	public Vector3 m_ScaleLast;
	public Vector3 Min = new Vector3(1,1,1);
	public Vector3 Max = new Vector3(1.05f,1.05f,1.05f);
	public eEaseType EaseType = eEaseType.linear;
	public float Time = 1.0f;
	public float Delay;
	public cSoundsForPingPongAnim Sound;
}

#endregion

#region Fade Loop

// Fade-Loop animation class
[System.Serializable]
public class cPingPongFade
{
	public bool Enable = false;
	[HideInInspector]
    public bool Began = false;
    [HideInInspector]
    public bool Animating = false;
    [HideInInspector]
    public bool IsOverriding = false;
    [HideInInspector]
    public float IsOverridingDelayTimeCount = 0.0f;
    [HideInInspector]
    public bool Done = false;
    [HideInInspector]
    public float m_FadeLast;
    public float Min = 0.0f;
    public float Max = 1.0f;
    public eEaseType EaseType = eEaseType.linear;
    public float Time = 1.0f;
    public float Delay;
    public bool FadeChildren = false;
    public cSoundsForPingPongAnim Sound;
}

#endregion

/**************
* GEAnim class
* This class controls animations of Move, Rotation, Scale and Fade.
**************/

public class GEAnim : MonoBehaviour
{
	
	#region Variables
	
	// Canvas

		// GEAnim works with Unity UI system so it requires Canvas to work properly.
		Canvas	m_Parent_Canvas = null;
    
	// Camera

		// Edge position from camera perspective
		[HideInInspector]
		public float m_CameraLeftEdge;
		[HideInInspector]
		public float m_CameraRightEdge;
		[HideInInspector]
		public float m_CameraTopEdge;
		[HideInInspector]
		public float m_CameraBottomEdge;

		// Three positions of Top Edge of screen
		[HideInInspector]
		public Vector3 m_CanvasWorldTopLeft;
		[HideInInspector]
		public Vector3 m_CanvasWorldTopCenter;
		[HideInInspector]
		public Vector3 m_CanvasWorldTopRight;

		// Three positions of middle of screen
		[HideInInspector]
		public Vector3 m_CanvasWorldMiddleLeft;
		[HideInInspector]
		public Vector3 m_CanvasWorldMiddleCenter;
		[HideInInspector]
		public Vector3 m_CanvasWorldMiddleRight;

		// Three positions of bottom  of screen
		[HideInInspector]
		public Vector3 m_CanvasWorldBottomLeft;
		[HideInInspector]
		public Vector3 m_CanvasWorldBottomCenter;
		[HideInInspector]
		public Vector3 m_CanvasWorldBottomRight;

    // Total bounds of this game object

		[HideInInspector]
		public Bounds m_TotalBounds;

	// Original local position, rotation, scale and fade

		[HideInInspector]
		public Vector3 m_MoveOriginal;
		[HideInInspector]
		public Quaternion m_RotationOriginal;
		[HideInInspector]
		public Vector3 m_ScaleOriginal;
		[HideInInspector]
		public float m_FadeOriginal;
		[HideInInspector]
		public float m_FadeOriginalTextOutline;
		[HideInInspector]
		public float m_FadeOriginalTextShadow;
	
	// In animations

		public cMoveIn m_MoveIn;
		public cRotationIn m_RotationIn;
		public cScaleIn m_ScaleIn;
		public cFade m_FadeIn;

	// Idle animations

		#if DOTWEEN
		Tweener m_DOTweenScaleLoop = null;
		#elif HOTWEEN
		Tweener m_HOTweenScaleLoop = null;
		#elif ITWEEN
		#else
		LTDescr m_LeanTweenScaleLoop = null;		// If you have error at this line, check out a section names "How to switch tweeners between DOTween, HOTween, iTween and LeanTween." in read me file.
		#endif
		public cPingPongScale m_ScaleLoop;	
		
		#if DOTWEEN
		Tweener m_DOTweenFadeLoop = null;
		#elif HOTWEEN
		Tweener m_HOTweenFadeLoop = null;
		#elif ITWEEN
		#else
		LTDescr m_LeanTweenFadeLoop = null;			// If you have error at this line, check out a section names "How to switch tweeners between DOTween, HOTween, iTween and LeanTween." in read me file.
		#endif
		public cPingPongFade m_FadeLoop;		

	// Out animatons
	
		public cMoveOut m_MoveOut;
		public cRotationOut m_RotationOut;
		public cScaleOut m_ScaleOut;
		public cFade m_FadeOut;
	
	// Variable to animate while animation is playing

		[HideInInspector]
		public float m_MoveVariable		= 0.0f;
		[HideInInspector]
		public float m_RotationVariable	= 0.0f;
		[HideInInspector]
		public float m_ScaleVariable	= 0.0f;
		[HideInInspector]
		public float m_FadeVariable		= 0.0f;

	// Initial flag

		bool m_InitialDone					= false;

	// Transform in Canvas

		RectTransform	m_RectTransform		= null;
    
    
	// Supported Unity UI component

		Image			m_Image							= null;		// If you have error at this line on Unity 5.x, please make sure that you are using Unity 5.x with a valid license.
		Toggle			m_Toggle						= null;		// If you have error at this line on Unity 5.x, please make sure that you are using Unity 5.x with a valid license.
		Text			m_Text							= null;		// If you have error at this line on Unity 5.x, please make sure that you are using Unity 5.x with a valid license.
		Outline			m_TextOutline					= null;		// If you have error at this line on Unity 5.x, please make sure that you are using Unity 5.x with a valid license.
		Shadow			m_TextShadow					= null;		// If you have error at this line on Unity 5.x, please make sure that you are using Unity 5.x with a valid license.
		RawImage		m_RawImage						= null;		// If you have error at this line on Unity 5.x, please make sure that you are using Unity 5.x with a valid license.
		Slider			m_Slider						= null;		// If you have error at this line on Unity 5.x, please make sure that you are using Unity 5.x with a valid license.
		CanvasRenderer	m_CanvasRenderer				= null;		// If you have error at this line on Unity 5.x, please make sure that you are using Unity 5.x with a valid license.
		RectTransform	m_ParentCanvasRectTransform		= null;		// If you have error at this line on Unity 5.x, please make sure that you are using Unity 5.x with a valid license.
	
	#endregion	
	
	// ######################################################################
	// MonoBehaviour Functions
	// ######################################################################
	
	#region MonoBehaviour

	// Awake is called when the script instance is being loaded.
	void Awake()
	{
		// DOTween: https://www.assetstore.unity3d.com/en/#!/content/27676
		// DOTween Documentation: http://dotween.demigiant.com/documentation.php
		
		// HOTween: https://www.assetstore.unity3d.com/#/content/3311
		// HOTween Documentation:  http://hotween.demigiant.com/documentation.html
		
		// LeanTween: https://www.assetstore.unity3d.com/#/content/3595
		// LeanTween Documentation: http://dentedpixel.com/LeanTweenDocumentation/classes/LeanTween.html
		
		// iTween: https://www.assetstore.unity3d.com/#/content/84 
        // iTween Documentation: http://itween.pixelplacement.com/documentation.php

		#if DOTWEEN
        #elif HOTWEEN
		#elif ITWEEN
		#else

		// LEANTWEEN INITIALIZATION
		LeanTween.init(3200); // This line is optional. Here you can specify the maximum number of tweens you will use (the default is 400).  This must be called before any use of LeanTween is made for it to be effective.

		#endif

		// Keep Original transfrom, anchoredPosition, localRotation, localScale, Fade
		m_RectTransform = (RectTransform) this.transform;
		m_MoveOriginal = m_RectTransform.anchoredPosition;
		m_RotationOriginal = m_RectTransform.localRotation;
		m_ScaleOriginal = transform.localScale;
		m_FadeOriginal = 1.0f;
		m_FadeOriginalTextOutline = 0.5f;
		m_FadeOriginalTextShadow = 0.5f;

		// Check is there UI component attached
		m_Image = this.gameObject.GetComponent<Image>();
		m_Toggle = this.gameObject.GetComponent<Toggle>();
		m_Text = this.gameObject.GetComponent<Text>();
		m_TextOutline = this.gameObject.GetComponent<Outline>();
		m_TextShadow = this.gameObject.GetComponent<Shadow>();
		m_RawImage = this.gameObject.GetComponent<RawImage>();
		m_Slider = this.gameObject.GetComponent<Slider>();
		m_CanvasRenderer = this.gameObject.GetComponent<CanvasRenderer>();
		
		// Set begin alpha
		m_FadeOriginal = GetFadeValue(this.transform);
		m_FadeOriginalTextOutline = GetFadeTextOutlineValue(this.transform);
		m_FadeOriginalTextShadow = GetFadeTextShadowValue(this.transform);
    }
    
    // Start is called on the frame when a script is enabled just before any of the Update methods is called the first time.
	void Start ()
	{
		
		// DOTween: https://www.assetstore.unity3d.com/en/#!/content/27676
		// DOTween Documentation: http://dotween.demigiant.com/documentation.php
		
		// HOTween: https://www.assetstore.unity3d.com/#/content/3311
		// HOTween Documentation:  http://hotween.demigiant.com/documentation.html
		
		// LeanTween: https://www.assetstore.unity3d.com/#/content/3595
		// LeanTween Documentation: http://dentedpixel.com/LeanTweenDocumentation/classes/LeanTween.html
		
		// iTween: https://www.assetstore.unity3d.com/#/content/84 
        // iTween Documentation: http://itween.pixelplacement.com/documentation.php

		#if DOTWEEN
		// DOTWEEN INITIALIZATION
		// Initialize DOTween (needs to be done only once).
		// If you don't initialize DOTween yourself,
		// it will be automatically initialized with default values.
		// DOTween.Init(false, true, LogBehaviour.ErrorsOnly);
        #elif HOTWEEN
		// HOTWEEN INITIALIZATION
		// Must be done only once, before the creation of your first tween
		// (you can skip this if you want, and HOTween will be initialized automatically
		// when you create your first tween - using default values)
		HOTween.Init(true, true, true);
		#endif

		// Apply animator to first update
		if(m_InitialDone==false)
		{
			m_InitialDone = true;

			// This value will be changed if GEAnimSystem.Instance.m_AutoAnimation is true
			bool ShouldCallReset = true;

			// User wants to play animations automatically
			if(GEAnimSystem.Instance.m_AutoAnimation==true)
			{
				// Auto run In or All animations
				if(GEAnimSystem.Instance.m_AnimationMode==eAnimationMode.In || GEAnimSystem.Instance.m_AnimationMode==eAnimationMode.All)
				{
					if(m_MoveIn.Enable || m_RotationIn.Enable || m_ScaleIn.Enable || m_FadeIn.Enable)
					{
						MoveIn();
					}
					else if(m_ScaleLoop.Enable || m_FadeLoop.Enable)
					{
						ShouldCallReset = false;
						StartMoveIdle();
					}
				}
				// Auto run Idle animation
				else if(GEAnimSystem.Instance.m_AnimationMode==eAnimationMode.Idle)
				{
					ShouldCallReset = false;
					StartMoveIdle();
				}
				// Auto run Out animation, init move out positon, rotation, scale and fade
				else if(GEAnimSystem.Instance.m_AnimationMode==eAnimationMode.Out)
				{
					ShouldCallReset = false;
					InitMoveOut();
				}
				// Do not run animation
				else if(GEAnimSystem.Instance.m_AnimationMode==eAnimationMode.None)
				{
					ShouldCallReset = false;
				}
			}
			
			if(ShouldCallReset==true)
			{
				// Reset m_MoveIn, m_MoveOut, m_ScaleIn, m_ScaleOut, m_FadeIn and m_FadeOut
				Reset();
			}

			// User wants to play animations automatically
			if(GEAnimSystem.Instance.m_AutoAnimation==true)
			{
				// Auto run Out animation
				if(GEAnimSystem.Instance.m_AnimationMode==eAnimationMode.Out || GEAnimSystem.Instance.m_AnimationMode==eAnimationMode.All)
				{
					float DelayTime = 1.0f;
					
					// Waits for a time then test auto run MoveOut animation
					if(GEAnimSystem.Instance.m_AnimationMode==eAnimationMode.In || GEAnimSystem.Instance.m_AnimationMode==eAnimationMode.All)
						DelayTime = GEAnimSystem.Instance.m_IdleTime;
					
					StartCoroutine(CoroutineMoveOut(DelayTime));
				}
			}
		}
    }
	
	// Update is called every frame, if the MonoBehaviour is enabled.
	void Update ()
	{
    }
    
    #endregion
    
    // ######################################################################
	// Reset Functions
	// ######################################################################
	
	#region Reset
	
	// Reset m_MoveIn, m_MoveOut, m_ScaleIn, m_ScaleOut, m_FadeIn, m_FadeOut
	public void Reset()
	{
		// Init m_MoveIn and m_MoveOut
		InitMoveIn();
		InitMoveOut();
		
		// Init m_RotaionIn
		InitRotationIn();
		
		// Init m_ScaleIn and m_ScaleOut
		InitScaleIn();
		
		// Init m_FadeIn and m_FadeOut
		InitFadeIn();
	}
	
	// Reset m_MoveIn, m_MoveOut, m_ScaleIn, m_ScaleOut, m_FadeIn, m_FadeOut of this object and all it's children
	public void ResetAllChildren()
	{
		// Init m_MoveIn and m_MoveOut
		InitMoveIn();
		InitMoveOut();
		
		// Init m_RotaionIn
		InitRotationIn();
		
		// Init m_ScaleIn and m_ScaleOut
		InitScaleIn();
		
		// Init m_FadeIn and m_FadeOut
		InitFadeIn();
		
		// make a recursive call to this GameObject's children
		foreach(Transform child in this.transform)
		{
			GEAnim pGEAnim = child.gameObject.GetComponent<GEAnim>();
			if(pGEAnim!=null)
			{
				pGEAnim.ResetAllChildren();
			}
		}
	}
	
	#endregion
	
	// ######################################################################
	// Initial Functions
	// ######################################################################
	
	#region Initial
	
	// Find camera perspective size in 3d space dimention
	void CalculateCameraArea()
	{
		// Find m_Parent_Canvas
		if(m_Parent_Canvas==null)
			m_Parent_Canvas = GEAnimSystem.Instance.GetParent_Canvas(transform);

		// Calculate Camera area
		if(m_Parent_Canvas!=null)
		{
			m_ParentCanvasRectTransform = m_Parent_Canvas.GetComponent<RectTransform>();
			
			m_CameraRightEdge = (m_ParentCanvasRectTransform.rect.width/2);
			m_CameraLeftEdge = -m_CameraRightEdge;
			m_CameraTopEdge = (m_ParentCanvasRectTransform.rect.height/2);
			m_CameraBottomEdge = -m_CameraTopEdge;

			// Top Edge positions
			m_CanvasWorldTopLeft		= m_ParentCanvasRectTransform.TransformPoint(new Vector3(m_CameraLeftEdge, m_CameraTopEdge, 0));
			m_CanvasWorldTopCenter		= m_ParentCanvasRectTransform.TransformPoint(new Vector3(0, m_CameraTopEdge, 0));
			m_CanvasWorldTopRight		= m_ParentCanvasRectTransform.TransformPoint(new Vector3(m_CameraRightEdge, m_CameraTopEdge, 0));
			
			// Middle horizontal positions
			m_CanvasWorldMiddleLeft		= m_ParentCanvasRectTransform.TransformPoint(new Vector3(m_CameraLeftEdge, 0, 0));
			m_CanvasWorldMiddleCenter	= m_ParentCanvasRectTransform.TransformPoint(new Vector3(0, 0, 0));
			m_CanvasWorldMiddleRight	= m_ParentCanvasRectTransform.TransformPoint(new Vector3(m_CameraRightEdge, 0, 0));

			// Bottom positions
			m_CanvasWorldBottomLeft		= m_ParentCanvasRectTransform.TransformPoint(new Vector3(m_CameraLeftEdge, m_CameraBottomEdge, 0));
			m_CanvasWorldBottomCenter	= m_ParentCanvasRectTransform.TransformPoint(new Vector3(0, m_CameraBottomEdge, 0));
			m_CanvasWorldBottomRight	= m_ParentCanvasRectTransform.TransformPoint(new Vector3(m_CameraRightEdge, m_CameraBottomEdge, 0));
        }
        
    }
	
	// Init Move-In animation
	void InitMoveIn()
	{
		if(m_MoveIn.Enable == true && m_MoveIn.Done == false)
		{
			// Find camera view size in 3d space dimention
			CalculateCameraArea();
			
			// Find total bounds
			CalculateTotalBounds();

			RectTransform ParentRectTransform = transform.parent.GetComponent<RectTransform>();

			// Set begin positon of UI object according of m_MoveIn.MoveFrom type
			switch(m_MoveIn.MoveFrom)
			{
			case ePosMove.ParentPosition:
				if(transform.parent!=null)
				{
					m_MoveIn.BeginPos = new Vector3(0, 0, m_RectTransform.localPosition.z);
				}
				break;

			case ePosMove.LocalPosition:
				m_MoveIn.BeginPos = new Vector3(m_MoveIn.Position.x,m_MoveIn.Position.y,m_RectTransform.localPosition.z);
				break;
				
			case ePosMove.UpperScreenEdge:
				m_MoveIn.BeginPos = ParentRectTransform.InverseTransformPoint(m_CanvasWorldTopCenter);
				m_MoveIn.BeginPos = new Vector3(m_RectTransform.localPosition.x,
				                                m_MoveIn.BeginPos.y + m_TotalBounds.size.y,
				                                m_RectTransform.localPosition.z);
                    break;
			case ePosMove.LeftScreenEdge:
				m_MoveIn.BeginPos = ParentRectTransform.InverseTransformPoint(m_CanvasWorldMiddleLeft);
				m_MoveIn.BeginPos = new Vector3(m_MoveIn.BeginPos.x - m_TotalBounds.size.x,
				                                m_RectTransform.localPosition.y,
				                                m_RectTransform.localPosition.z);
				break;

			case ePosMove.RightScreenEdge:
				m_MoveIn.BeginPos = ParentRectTransform.InverseTransformPoint(m_CanvasWorldMiddleRight);
				m_MoveIn.BeginPos = new Vector3(m_MoveIn.BeginPos.x + m_TotalBounds.size.x,
				                                m_RectTransform.localPosition.y,
				                                m_RectTransform.localPosition.z);
				break;

			case ePosMove.BottomScreenEdge:
				m_MoveIn.BeginPos = ParentRectTransform.InverseTransformPoint(m_CanvasWorldBottomCenter);
				m_MoveIn.BeginPos = new Vector3(m_RectTransform.localPosition.x,
				                                m_MoveIn.BeginPos.y - m_TotalBounds.size.y,
				                                m_RectTransform.localPosition.z);
				break;
				
			case ePosMove.UpperLeft:
				m_MoveIn.BeginPos = ParentRectTransform.InverseTransformPoint(m_CanvasWorldTopLeft);
				m_MoveIn.BeginPos = new Vector3(m_MoveIn.BeginPos.x - m_TotalBounds.size.x,
				                                m_MoveIn.BeginPos.y + m_TotalBounds.size.y,
				                                m_RectTransform.localPosition.z);

				break;
			case ePosMove.UpperCenter:
				m_MoveIn.BeginPos = ParentRectTransform.InverseTransformPoint(m_CanvasWorldTopCenter);
				m_MoveIn.BeginPos = new Vector3(m_MoveIn.BeginPos.x,
				                                m_MoveIn.BeginPos.y + m_TotalBounds.size.y,
				                                m_RectTransform.localPosition.z);
				break;
			case ePosMove.UpperRight:
				m_MoveIn.BeginPos = ParentRectTransform.InverseTransformPoint(m_CanvasWorldTopRight);
				m_MoveIn.BeginPos = new Vector3(m_MoveIn.BeginPos.x + m_TotalBounds.size.x,
				                                m_MoveIn.BeginPos.y + m_TotalBounds.size.y,
				                                m_RectTransform.localPosition.z);
				break;

			case ePosMove.MiddleLeft:
				m_MoveIn.BeginPos = ParentRectTransform.InverseTransformPoint(m_CanvasWorldMiddleLeft);
				m_MoveIn.BeginPos = new Vector3(m_MoveIn.BeginPos.x - m_TotalBounds.size.x,
				                                m_MoveIn.BeginPos.y,
				                                m_RectTransform.localPosition.z);
				break;

			case ePosMove.MiddleCenter:
				m_MoveIn.BeginPos = ParentRectTransform.InverseTransformPoint(m_CanvasWorldMiddleCenter);
				m_MoveIn.BeginPos = new Vector3(m_MoveIn.BeginPos.x,
				                                m_MoveIn.BeginPos.y,
				                                m_RectTransform.localPosition.z);
				break;

			case ePosMove.MiddleRight:
				m_MoveIn.BeginPos = ParentRectTransform.InverseTransformPoint(m_CanvasWorldMiddleRight);
				m_MoveIn.BeginPos = new Vector3(m_MoveIn.BeginPos.x + m_TotalBounds.size.x,
				                                m_MoveIn.BeginPos.y,
				                                m_RectTransform.localPosition.z);
				break;

			case ePosMove.BottomLeft:
				m_MoveIn.BeginPos = ParentRectTransform.InverseTransformPoint(m_CanvasWorldBottomLeft);
				m_MoveIn.BeginPos = new Vector3(m_MoveIn.BeginPos.x - m_TotalBounds.size.x,
				                                m_MoveIn.BeginPos.y - m_TotalBounds.size.y,
				                                m_RectTransform.localPosition.z);
                    break;

			case ePosMove.BottomCenter:
				m_MoveIn.BeginPos = ParentRectTransform.InverseTransformPoint(m_CanvasWorldBottomCenter);
				m_MoveIn.BeginPos = new Vector3(m_MoveIn.BeginPos.x,
				                                m_MoveIn.BeginPos.y - m_TotalBounds.size.y,
				                                m_RectTransform.localPosition.z);
                    break;

			case ePosMove.BottomRight:
				m_MoveIn.BeginPos = ParentRectTransform.InverseTransformPoint(m_CanvasWorldBottomRight);
				m_MoveIn.BeginPos = new Vector3(m_MoveIn.BeginPos.x + m_TotalBounds.size.x,
				                                m_MoveIn.BeginPos.y - m_TotalBounds.size.y,
				                                m_RectTransform.localPosition.z);
				break;

			case ePosMove.SelfPosition:
				m_MoveIn.BeginPos = m_MoveOriginal;
				break;
            }
            
			m_RectTransform.anchoredPosition = m_MoveIn.BeginPos;
            //m_RectTransform.localPosition = m_MoveIn.BeginPos;
			m_MoveIn.EndPos = m_MoveOriginal;
		}
	}
	
	// Init Move-Out animation
	void InitMoveOut()
	{
		if(m_MoveOut.Enable == true)
		{
			// Find camera view size in 3d space dimention
			CalculateCameraArea();
			
			// Find total bounds
			CalculateTotalBounds();
			
			RectTransform ParentRectTransform = transform.parent.GetComponent<RectTransform>();
			
			// Set end positon of UI object according of m_MoveOut.MoveTo type
			switch(m_MoveOut.MoveTo)
			{
			case ePosMove.ParentPosition:
				if(transform.parent!=null)
				{
					m_MoveOut.EndPos = new Vector3(0, 0, m_RectTransform.localPosition.z);
				}
				break;

			case ePosMove.LocalPosition:
				m_MoveOut.EndPos = new Vector3(m_MoveOut.Position.x, m_MoveOut.Position.y, m_RectTransform.localPosition.z);
				break;
				
			case ePosMove.UpperScreenEdge:
				m_MoveOut.EndPos = ParentRectTransform.InverseTransformPoint(m_CanvasWorldTopCenter);
				m_MoveOut.EndPos = new Vector3(m_RectTransform.localPosition.x,
				                               m_MoveOut.EndPos.y + m_TotalBounds.size.y,
				                                m_RectTransform.localPosition.z);
				break;

			case ePosMove.LeftScreenEdge:
				m_MoveOut.EndPos = ParentRectTransform.InverseTransformPoint(m_CanvasWorldMiddleLeft);
				m_MoveOut.EndPos = new Vector3(m_MoveOut.EndPos.x - m_TotalBounds.size.x,
				                                m_RectTransform.localPosition.y,
				                                m_RectTransform.localPosition.z);
				break;

			case ePosMove.RightScreenEdge:
				m_MoveOut.EndPos = ParentRectTransform.InverseTransformPoint(m_CanvasWorldMiddleRight);
				m_MoveOut.EndPos = new Vector3(m_MoveOut.EndPos.x + m_TotalBounds.size.x,
				                                m_RectTransform.localPosition.y,
				                                m_RectTransform.localPosition.z);
				break;

			case ePosMove.BottomScreenEdge:
				m_MoveOut.EndPos = ParentRectTransform.InverseTransformPoint(m_CanvasWorldBottomCenter);
				m_MoveOut.EndPos = new Vector3(m_RectTransform.localPosition.x,
				                                m_MoveOut.EndPos.y - m_TotalBounds.size.y,
				                                m_RectTransform.localPosition.z);
				break;
				
			case ePosMove.UpperLeft:
				m_MoveOut.EndPos = ParentRectTransform.InverseTransformPoint(m_CanvasWorldTopLeft);
				m_MoveOut.EndPos = new Vector3(m_MoveOut.EndPos.x - m_TotalBounds.size.x,
				                                 m_MoveOut.EndPos.y + m_TotalBounds.size.y,
				                                m_RectTransform.localPosition.z);
				
				break;

			case ePosMove.UpperCenter:
				m_MoveOut.EndPos = ParentRectTransform.InverseTransformPoint(m_CanvasWorldTopCenter);
				m_MoveOut.EndPos = new Vector3(m_MoveOut.EndPos.x,
				                                m_MoveOut.EndPos.y + m_TotalBounds.size.y,
				                                m_RectTransform.localPosition.z);
				break;

			case ePosMove.UpperRight:
				m_MoveOut.EndPos = ParentRectTransform.InverseTransformPoint(m_CanvasWorldTopRight);
				m_MoveOut.EndPos = new Vector3(m_MoveOut.EndPos.x + m_TotalBounds.size.x,
				                                 m_MoveOut.EndPos.y + m_TotalBounds.size.y,
				                                m_RectTransform.localPosition.z);
				break;

			case ePosMove.MiddleLeft:
				m_MoveOut.EndPos = ParentRectTransform.InverseTransformPoint(m_CanvasWorldMiddleLeft);
				m_MoveOut.EndPos = new Vector3(m_MoveOut.EndPos.x - m_TotalBounds.size.x,
				                                m_MoveOut.EndPos.y,
				                                m_RectTransform.localPosition.z);
				break;

			case ePosMove.MiddleCenter:
				m_MoveOut.EndPos = ParentRectTransform.InverseTransformPoint(m_CanvasWorldMiddleCenter);
				m_MoveOut.EndPos = new Vector3(m_MoveOut.EndPos.x,
				                                 m_MoveOut.EndPos.y,
				                                m_RectTransform.localPosition.z);
				break;

			case ePosMove.MiddleRight:
				m_MoveOut.EndPos = ParentRectTransform.InverseTransformPoint(m_CanvasWorldMiddleRight);
				m_MoveOut.EndPos = new Vector3(m_MoveOut.EndPos.x + m_TotalBounds.size.x,
				                                 m_MoveOut.EndPos.y,
				                                m_RectTransform.localPosition.z);
				break;

			case ePosMove.BottomLeft:
				m_MoveOut.EndPos = ParentRectTransform.InverseTransformPoint(m_CanvasWorldBottomLeft);
				m_MoveOut.EndPos = new Vector3(m_MoveOut.EndPos.x - m_TotalBounds.size.x,
				                                m_MoveOut.EndPos.y - m_TotalBounds.size.y,
				                                m_RectTransform.localPosition.z);
				break;

			case ePosMove.BottomCenter:
				m_MoveOut.EndPos = ParentRectTransform.InverseTransformPoint(m_CanvasWorldBottomCenter);
				m_MoveOut.EndPos = new Vector3(m_MoveOut.EndPos.x,
				                                 m_MoveOut.EndPos.y - m_TotalBounds.size.y,
				                                m_RectTransform.localPosition.z);
				break;

			case ePosMove.BottomRight:
				m_MoveOut.EndPos = ParentRectTransform.InverseTransformPoint(m_CanvasWorldBottomRight);
				m_MoveOut.EndPos = new Vector3(m_MoveOut.EndPos.x + m_TotalBounds.size.x,
				                               m_MoveOut.EndPos.y - m_TotalBounds.size.y,
				                                m_RectTransform.localPosition.z);
				break;
				
			case ePosMove.SelfPosition:
				m_MoveOut.EndPos = m_MoveOriginal;
				break;
			}
		}
	}
	
	// Init Rotation-In animation
	void InitRotationIn()
	{
		if(m_RotationIn.Enable == true)
		{
			m_RotationIn.BeginRotation = m_RotationIn.Rotation;
			m_RotationIn.EndRotation = m_RotationOriginal.eulerAngles;
		}
	}
	
	// Init Scale-In animation
	void InitScaleIn()
	{
		if(m_ScaleIn.Enable == true)
		{
			transform.localScale = m_ScaleIn.ScaleBegin;
		}
	}
	
	// Init Fade-In animation
	void InitFadeIn()
	{
		if(m_FadeIn.Enable == true)
		{
			RecursiveFade(this.transform, m_FadeIn.Fade, m_FadeIn.FadeChildren);
		}
	}
	
	#endregion
	
	// ######################################################################
	// Play In animation functions
	// ######################################################################
	
	#region Move In
	
	// Moves in this object
	public void MoveIn()
	{
		MoveIn(eGUIMove.SelfAndChildren);
	}
	
	// Moves this object in according to eGUIMove type
	public void MoveIn(eGUIMove GUIMoveType)
	{
		// GUIMoveType is Self or SelfAndChildren
		if(GUIMoveType==eGUIMove.Self || GUIMoveType==eGUIMove.SelfAndChildren)
		{
			// Check is move-in enable
			if(m_MoveIn.Enable && m_MoveIn.Began==false)
			{			
				m_MoveIn.Began = true;
				m_MoveIn.Animating = false;
				m_MoveIn.Done = false;

				#if DOTWEEN
				m_MoveVariable = 0.0f;
				// DOTween: https://www.assetstore.unity3d.com/en/#!/content/27676
				// DOTween Documentation: http://dotween.demigiant.com/documentation.php
				//DOTween.To(()=>m_MoveVariable, x=> m_MoveVariable = x, 1.0f, 1).set;
				DOTween.To(x=> m_MoveVariable = x, 0.0f, 1.0f, (float)m_MoveIn.Time/(float)GEAnimSystem.Instance.m_GUISpeed)
					.SetDelay((float)m_MoveIn.Delay/(float)GEAnimSystem.Instance.m_GUISpeed)	// Sets a delayed startup for the tween. 
					.SetEase(DOTweenEaseType(m_MoveIn.EaseType))								// Sets the ease of the tween. More info, check out http://easings.net or http://www.robertpenner.com/easing/easing_demo.html
					.OnUpdate(AnimIn_MoveUpdate)												// Sets a callback that will be fired every time the tween updates.
					.OnComplete(AnimIn_MoveComplete)											// Sets a callback that will be fired the moment the tween reaches completion, all loops included.
					.SetUpdate(UpdateType.Normal, true);										// Sets the type of update (Normal, Late or Fixed) for the tween and eventually tells it to ignore Unity's timeScale. 
                #elif HOTWEEN
				m_MoveVariable = 0.0f;
				// HOTween: https://www.assetstore.unity3d.com/#/content/3311
				// HOTween Documentation:  http://hotween.demigiant.com/documentation.html
				HOTween.To(this, (float)m_MoveIn.Time/(float)GEAnimSystem.Instance.m_GUISpeed, new TweenParms()
				           .Prop("m_MoveVariable", 1.0f, false)
				           .Delay((float)m_MoveIn.Delay/(float)GEAnimSystem.Instance.m_GUISpeed)
				           .Ease(HOTweenEaseType(m_MoveIn.EaseType))
				           .OnUpdate(AnimIn_MoveUpdate)
				           .OnComplete(AnimIn_MoveComplete)
				           .UpdateType(UpdateType.TimeScaleIndependentUpdate)
				           );
				
				m_MoveIn.Began = true;
				#elif ITWEEN
				// iTween: https://www.assetstore.unity3d.com/#/content/84 
				// iTween Documentation: http://itween.pixelplacement.com/documentation.php
				iTween.ValueTo(this.gameObject, iTween.Hash("from", 0.0f,
				                                            "to", 1.0f,
				                                            "time", (float)m_MoveIn.Time/(float)GEAnimSystem.Instance.m_GUISpeed,
				                                            "ignoretimescale", true,
				                                            "delay", (float)m_MoveIn.Delay/(float)GEAnimSystem.Instance.m_GUISpeed,
				                                            "easeType", iTweenEaseType(m_MoveIn.EaseType),
				                                            "onupdate", "AnimIn_MoveUpdateByValue",
				                                            "onupdatetarget", this.gameObject,
				                                            "oncomplete", "AnimIn_MoveComplete"));
				
				m_MoveIn.Began = true;
				#else
				// LeanTween: https://www.assetstore.unity3d.com/#/content/3595
				// LeanTween Documentation: http://dentedpixel.com/LeanTweenDocumentation/classes/LeanTween.html
				LeanTween.value(this.gameObject, AnimIn_MoveUpdateByValue, 0.0f, 1.0f, (float)m_MoveIn.Time/(float)GEAnimSystem.Instance.m_GUISpeed)
					.setDelay((float)m_MoveIn.Delay/(float)GEAnimSystem.Instance.m_GUISpeed)
						.setEase(LeanTweenEaseType(m_MoveIn.EaseType))
						.setOnComplete(AnimIn_MoveComplete)
						.setUseEstimatedTime( true );
				#endif
			}

			// Check is Rotation-in enable
			if(m_RotationIn.Enable && m_RotationIn.Began==false)
			{
				m_RotationIn.Began = true;
				m_RotationIn.Animating = false;
				m_RotationIn.Done = false;
				
				#if DOTWEEN
				m_RotationVariable = 0.0f;
				// DOTween: https://www.assetstore.unity3d.com/en/#!/content/27676
				// DOTween Documentation: http://dotween.demigiant.com/documentation.php
				DOTween.To(x=> m_RotationVariable = x, 0.0f, 1.0f, (float)m_RotationIn.Time/(float)GEAnimSystem.Instance.m_GUISpeed)
						.SetDelay((float)m_RotationIn.Delay/(float)GEAnimSystem.Instance.m_GUISpeed)	// Sets a delayed startup for the tween. 
						.SetEase(DOTweenEaseType(m_RotationIn.EaseType))				// Sets the ease of the tween. More info, check out http://easings.net or http://www.robertpenner.com/easing/easing_demo.html
						.OnUpdate(AnimIn_RotationUpdate)								// Sets a callback that will be fired every time the tween updates.
						.OnComplete(AnimIn_RotationComplete)							// Sets a callback that will be fired the moment the tween reaches completion, all loops included.
						.SetUpdate(UpdateType.Normal, true);							// Sets the type of update (Normal, Late or Fixed) for the tween and eventually tells it to ignore Unity's timeScale. 
				#elif HOTWEEN
				m_RotationVariable = 0.0f;
				// HOTween: https://www.assetstore.unity3d.com/#/content/3311
				// HOTween Documentation:  http://hotween.demigiant.com/documentation.html
				HOTween.To(this, (float)m_RotationIn.Time/(float)GEAnimSystem.Instance.m_GUISpeed, new TweenParms()
				           .Prop("m_RotationVariable", 1.0f, false)
				           .Delay((float)m_RotationIn.Delay/(float)GEAnimSystem.Instance.m_GUISpeed)
				           .Ease(HOTweenEaseType(m_RotationIn.EaseType))
				           .OnUpdate(AnimIn_RotationUpdate)
				           .OnComplete(AnimIn_RotationComplete)
				           .UpdateType(UpdateType.TimeScaleIndependentUpdate)
				           );
				
				m_RotationIn.Began = true;
				#elif ITWEEN
				// iTween: https://www.assetstore.unity3d.com/#/content/84 
				// iTween Documentation: http://itween.pixelplacement.com/documentation.php
				iTween.ValueTo(this.gameObject, iTween.Hash("from", 0,
				                                            "to", 1.0f,
				                                            "time", (float)m_RotationIn.Time/(float)GEAnimSystem.Instance.m_GUISpeed,
				                                            "ignoretimescale", true,
				                                            "delay", (float)m_RotationIn.Delay/(float)GEAnimSystem.Instance.m_GUISpeed,
				                                            "easeType", iTweenEaseType(m_RotationIn.EaseType),
				                                            "onupdate", "AnimIn_RotationUpdateByValue",
				                                            "onupdatetarget", this.gameObject,
				                                            "oncomplete", "AnimIn_RotationComplete"));
				
				m_RotationIn.Began = true;
				#else
				// LeanTween: https://www.assetstore.unity3d.com/#/content/3595
				// LeanTween Documentation: http://dentedpixel.com/LeanTweenDocumentation/classes/LeanTween.html
				LeanTween.value(this.gameObject, AnimIn_RotationUpdateByValue, 0, 1.0f, (float)m_RotationIn.Time/(float)GEAnimSystem.Instance.m_GUISpeed)
					.setDelay((float)m_RotationIn.Delay/(float)GEAnimSystem.Instance.m_GUISpeed)
						.setEase(LeanTweenEaseType(m_RotationIn.EaseType))
						.setOnComplete(AnimIn_RotationComplete)
						.setUseEstimatedTime( true );
				#endif
			}
			
			// Check is Scale-in enable
			if(m_ScaleIn.Enable && m_ScaleIn.Began==false)
			{
				m_ScaleIn.Began = true;
				m_ScaleIn.Animating = false;
				m_ScaleIn.Done = false;

				#if DOTWEEN
				m_ScaleVariable = 0.0f;
				// DOTween: https://www.assetstore.unity3d.com/en/#!/content/27676
				// DOTween Documentation: http://dotween.demigiant.com/documentation.php
				DOTween.To(x=> m_ScaleVariable = x, 0.0f, 1.0f, (float)m_ScaleIn.Time/(float)GEAnimSystem.Instance.m_GUISpeed)
					.SetDelay((float)m_ScaleIn.Delay/(float)GEAnimSystem.Instance.m_GUISpeed)	// Sets a delayed startup for the tween. 
						.SetEase(DOTweenEaseType(m_ScaleIn.EaseType))				// Sets the ease of the tween. More info, check out http://easings.net or http://www.robertpenner.com/easing/easing_demo.html
						.OnUpdate(AnimIn_ScaleUpdate)								// Sets a callback that will be fired every time the tween updates.
						.OnComplete(AnimIn_ScaleComplete)							// Sets a callback that will be fired the moment the tween reaches completion, all loops included.
						.SetUpdate(UpdateType.Normal, true);						// Sets the type of update (Normal, Late or Fixed) for the tween and eventually tells it to ignore Unity's timeScale. 
				#elif HOTWEEN
				m_ScaleVariable = 0.0f;
				// HOTween: https://www.assetstore.unity3d.com/#/content/3311
				// HOTween Documentation:  http://hotween.demigiant.com/documentation.html
				HOTween.To(this, (float)m_ScaleIn.Time/(float)GEAnimSystem.Instance.m_GUISpeed, new TweenParms()
				           .Prop("m_ScaleVariable", 1.0f, false)
				           .Delay((float)m_ScaleIn.Delay/(float)GEAnimSystem.Instance.m_GUISpeed)
				           .Ease(HOTweenEaseType(m_ScaleIn.EaseType))
				           .OnUpdate(AnimIn_ScaleUpdate)
				           .OnComplete(AnimIn_ScaleComplete)
				           .UpdateType(UpdateType.TimeScaleIndependentUpdate)
				           );
				
				m_ScaleIn.Began = true;
				#elif ITWEEN
				// iTween: https://www.assetstore.unity3d.com/#/content/84 
				// iTween Documentation: http://itween.pixelplacement.com/documentation.php
				iTween.ValueTo(this.gameObject, iTween.Hash("from", 0,
				                                            "to", 1.0f,
				                                            "time", (float)m_ScaleIn.Time/(float)GEAnimSystem.Instance.m_GUISpeed,
				                                            "ignoretimescale", true,
				                                            "delay", (float)m_ScaleIn.Delay/(float)GEAnimSystem.Instance.m_GUISpeed,
				                                            "easeType", iTweenEaseType(m_ScaleIn.EaseType),
				                                            "onupdate", "AnimIn_ScaleUpdateByValue",
				                                            "onupdatetarget", this.gameObject,
				                                            "oncomplete", "AnimIn_ScaleComplete"));
				
				m_ScaleIn.Began = true;
				#else
				// LeanTween: https://www.assetstore.unity3d.com/#/content/3595
				// LeanTween Documentation: http://dentedpixel.com/LeanTweenDocumentation/classes/LeanTween.html
				LeanTween.value(this.gameObject, AnimIn_ScaleUpdateByValue, 0, 1.0f, (float)m_ScaleIn.Time/(float)GEAnimSystem.Instance.m_GUISpeed)
					.setDelay((float)m_ScaleIn.Delay/(float)GEAnimSystem.Instance.m_GUISpeed)
						.setEase(LeanTweenEaseType(m_ScaleIn.EaseType))
						.setOnComplete(AnimIn_ScaleComplete)
						.setUseEstimatedTime( true );
				#endif
			}
			
			// Check is fade-in enable
			if(m_FadeIn.Enable && m_FadeIn.Began==false)
			{				
				m_FadeIn.Began = true;
				m_FadeIn.Animating = false;
				m_FadeIn.Done = false;

				#if DOTWEEN
				m_FadeVariable = 0.0f;
				// DOTween: https://www.assetstore.unity3d.com/en/#!/content/27676
				// DOTween Documentation: http://dotween.demigiant.com/documentation.php
				DOTween.To(x=> m_FadeVariable = x, 0.0f, 1.0f, (float)m_FadeIn.Time/(float)GEAnimSystem.Instance.m_GUISpeed)
					.SetDelay((float)m_FadeIn.Delay/(float)GEAnimSystem.Instance.m_GUISpeed)	// Sets a delayed startup for the tween. 
						.SetEase(DOTweenEaseType(m_FadeIn.EaseType))				// Sets the ease of the tween. More info, check out http://easings.net or http://www.robertpenner.com/easing/easing_demo.html
						.OnUpdate(AnimIn_FadeUpdate)								// Sets a callback that will be fired every time the tween updates.
						.OnComplete(AnimIn_FadeComplete)							// Sets a callback that will be fired the moment the tween reaches completion, all loops included.
						.SetUpdate(UpdateType.Normal, true);						// Sets the type of update (Normal, Late or Fixed) for the tween and eventually tells it to ignore Unity's timeScale. 

				#elif HOTWEEN
				m_FadeVariable = 0.0f;
				// HOTween: https://www.assetstore.unity3d.com/#/content/3311
				// HOTween Documentation:  http://hotween.demigiant.com/documentation.html
				HOTween.To(this, (float)m_FadeIn.Time/(float)GEAnimSystem.Instance.m_GUISpeed, new TweenParms()
				           .Prop("m_FadeVariable", 1.0f, false)
				           .Delay((float)m_FadeIn.Delay/(float)GEAnimSystem.Instance.m_GUISpeed)
				           .Ease(HOTweenEaseType(m_FadeIn.EaseType))
				           .OnUpdate(AnimIn_FadeUpdate)
				           .OnComplete(AnimIn_FadeComplete)
				           .UpdateType(UpdateType.TimeScaleIndependentUpdate)
				           );
				#elif ITWEEN
				// iTween: https://www.assetstore.unity3d.com/#/content/84 
				// iTween Documentation: http://itween.pixelplacement.com/documentation.php
				iTween.ValueTo(this.gameObject, iTween.Hash("from", 0,
				                                            "to", 1.0f,
				                                            "time", (float)m_FadeIn.Time/(float)GEAnimSystem.Instance.m_GUISpeed,
				                                            "ignoretimescale", true,
				                                            "delay", (float)m_FadeIn.Delay/(float)GEAnimSystem.Instance.m_GUISpeed,
				                                            "easeType", iTweenEaseType(m_FadeIn.EaseType),
				                                            "onupdate", "AnimIn_FadeUpdateByValue",
				                                            "onupdatetarget", this.gameObject,
				                                            "oncomplete", "AnimIn_FadeComplete"));
				#else
				// LeanTween: https://www.assetstore.unity3d.com/#/content/3595
				// LeanTween Documentation: http://dentedpixel.com/LeanTweenDocumentation/classes/LeanTween.html
				LeanTween.value(this.gameObject, AnimIn_FadeUpdateByValue, 0, 1.0f, (float)m_FadeIn.Time/(float)GEAnimSystem.Instance.m_GUISpeed)
					.setDelay((float)m_FadeIn.Delay/(float)GEAnimSystem.Instance.m_GUISpeed)
						.setEase(LeanTweenEaseType(m_FadeIn.EaseType))
						.setOnComplete(AnimIn_FadeComplete)
						.setUseEstimatedTime( true );
				#endif
			}

			//MoveIdle();

		}
		
		// Check if GUIMoveType is Children or SelfAndChildren then call children's Move-In
		if(GUIMoveType==eGUIMove.Children || GUIMoveType==eGUIMove.SelfAndChildren)
		{
			foreach(Transform child in this.transform)
			{
				GEAnim pGEAnim = child.gameObject.GetComponent<GEAnim>();
				if(pGEAnim!=null)
				{
					pGEAnim.MoveIn(eGUIMove.SelfAndChildren);
				}
			}
		}
	}
	
	#endregion
	
	// ######################################################################
	// Play Idle animaiton functions
	// ######################################################################
	
	#region Move Idle

	int m_MoveIdle_Attemp = 0;
	int m_MoveIdle_AttempMax = 5;
	float m_MoveIdle_AttempMax_TimeInterval = 0.5f;
	bool m_MoveIdle_StartSucceed = false;
	
	// Idle this object
	private void StartMoveIdle()
	{
		m_MoveIdle_StartSucceed = false;
		m_MoveIdle_Attemp = 0;
		MoveIdle();
	}

	private void MoveIdle()
	{
		if(m_MoveIdle_StartSucceed==true)
			return;

		if(m_MoveIn.Began==false && m_RotationIn.Began==false)
		{
			// Start scale loop
			if(m_ScaleLoop.Enable==true && m_ScaleIn.Began==false)
			{
				m_MoveIdle_StartSucceed = true;
				ScaleLoopStart(m_ScaleLoop.Delay);
			}
			
			// Start fade loop
			if(m_FadeLoop.Enable==true && m_FadeIn.Began==false)
			{
				m_MoveIdle_StartSucceed = true;
				FadeLoopStart(m_FadeLoop.Delay);
			}
		}

		if(m_MoveIdle_StartSucceed==false)
		{
			StartCoroutine(CoroutineMoveIdle(m_MoveIdle_AttempMax_TimeInterval));
		}
	}

	IEnumerator CoroutineMoveIdle(float Delay)
	{
		yield return new WaitForSeconds(Delay);

		m_MoveIdle_Attemp++;
		if(m_MoveIdle_Attemp<=m_MoveIdle_AttempMax)
		{
			MoveIdle();
		}
	}
	
	#endregion
	
	// ######################################################################
	// Play Out animation functions
	// ######################################################################
	
	#region Move Out
	
	// Moves out this object
	public void MoveOut()
	{
		MoveOut(eGUIMove.SelfAndChildren);
	}

	IEnumerator CoroutineMoveOut(float Delay)
	{
		yield return new WaitForSeconds(Delay);
		MoveOut();
	}
	
	// Moves this object out according to eGUIMove type
	public void MoveOut(eGUIMove GUIMoveType)
	{
		// GUIMoveType is Self or SelfAndChildren
		if(GUIMoveType==eGUIMove.Self || GUIMoveType==eGUIMove.SelfAndChildren)
		{
			// Stop scale loop
			if(m_ScaleLoop.Enable && m_ScaleLoop.Began==true)
			{
				m_ScaleLoop.Began = false;
				m_ScaleLoop.Done = true;

				// Stop scale loop
				StopScaleLoop();
			}
			
			// Stop fade loop
			if(m_FadeLoop.Enable && m_FadeLoop.Began==true)
			{
				m_FadeLoop.Began = false;
				m_FadeLoop.Done = true;

				// Stop fade loop
				StopFadeLoop();
			}

			// Check is move-out enable
			if(m_MoveOut.Enable && m_MoveOut.Began==false)
			{
				
				m_MoveOut.Began = true;
				m_MoveOut.Animating = false;
				m_MoveOut.Done = false;

				//m_RectTransform.anchoredPosition = m_MoveOriginal;
				m_MoveOut.BeginPos = m_RectTransform.anchoredPosition;

				#if DOTWEEN
				m_MoveVariable = 0.0f;
				// DOTween: https://www.assetstore.unity3d.com/en/#!/content/27676
				// DOTween Documentation: http://dotween.demigiant.com/documentation.php
				DOTween.To(x=> m_MoveVariable = x, 0.0f, 1.0f, (float)m_MoveOut.Time/(float)GEAnimSystem.Instance.m_GUISpeed)
					.SetDelay((float)m_MoveOut.Delay/(float)GEAnimSystem.Instance.m_GUISpeed)	// Sets a delayed startup for the tween. 
						.SetEase(DOTweenEaseType(m_MoveOut.EaseType))				// Sets the ease of the tween. More info, check out http://easings.net or http://www.robertpenner.com/easing/easing_demo.html
						.OnUpdate(AnimOut_MoveUpdate)								// Sets a callback that will be fired every time the tween updates.
						.OnComplete(AnimOut_MoveComplete)							// Sets a callback that will be fired the moment the tween reaches completion, all loops included.
						.SetUpdate(UpdateType.Normal, true);						// Sets the type of update (Normal, Late or Fixed) for the tween and eventually tells it to ignore Unity's timeScale. 
				#elif HOTWEEN
				m_MoveVariable = 0.0f;
				// HOTween: https://www.assetstore.unity3d.com/#/content/3311
				// HOTween Documentation:  http://hotween.demigiant.com/documentation.html
				HOTween.To(this, (float)m_MoveOut.Time/(float)GEAnimSystem.Instance.m_GUISpeed, new TweenParms()
				           .Prop("m_MoveVariable", 1.0f, false)
				           .Delay((float)m_MoveOut.Delay/(float)GEAnimSystem.Instance.m_GUISpeed)
				           .Ease(HOTweenEaseType(m_MoveOut.EaseType))
				           .OnUpdate(AnimOut_MoveUpdate)
				           .OnComplete(AnimOut_MoveComplete)
				           .UpdateType(UpdateType.TimeScaleIndependentUpdate)
				           );
				
				m_MoveOut.Began = true;
				#elif ITWEEN
				// iTween: https://www.assetstore.unity3d.com/#/content/84 
				// iTween Documentation: http://itween.pixelplacement.com/documentation.php
				iTween.ValueTo(this.gameObject, iTween.Hash("from", 0.0f,
				                                            "to", 1.0f,
				                                            "time", (float)m_MoveOut.Time/(float)GEAnimSystem.Instance.m_GUISpeed,
				                                            "ignoretimescale", true,
				                                            "delay", (float)m_MoveOut.Delay/(float)GEAnimSystem.Instance.m_GUISpeed,
				                                            "easeType", iTweenEaseType(m_MoveOut.EaseType),
				                                            "onupdate", "AnimOut_MoveUpdateByValue",
				                                            "onupdatetarget", this.gameObject,
				                                            "oncomplete", "AnimOut_MoveComplete"));
				
				m_MoveOut.Began = true;
				#else
				// LeanTween: https://www.assetstore.unity3d.com/#/content/3595
				// LeanTween Documentation: http://dentedpixel.com/LeanTweenDocumentation/classes/LeanTween.html
				LeanTween.value(this.gameObject, AnimOut_MoveUpdateByValue, 0.0f, 1.0f, (float)m_MoveOut.Time/(float)GEAnimSystem.Instance.m_GUISpeed)
					.setDelay((float)m_MoveOut.Delay/(float)GEAnimSystem.Instance.m_GUISpeed)
						.setEase(LeanTweenEaseType(m_MoveOut.EaseType))
						.setOnComplete(AnimOut_MoveComplete)
						.setUseEstimatedTime( true );
				#endif
			}
			
			// Check is Rotation-out enable
			if(m_RotationOut.Enable && m_RotationOut.Began==false)
			{
				
				m_RotationOut.Began = true;
				m_RotationOut.Animating = false;
				m_RotationOut.Done = false;

				m_RotationOut.BeginRotation = m_RectTransform.localRotation.eulerAngles;
				m_RotationOut.EndRotation = m_RotationOut.Rotation;
				
				#if DOTWEEN
				m_RotationVariable = 0.0f;
				// DOTween: https://www.assetstore.unity3d.com/en/#!/content/27676
				// DOTween Documentation: http://dotween.demigiant.com/documentation.php
				DOTween.To(x=> m_RotationVariable = x, 0.0f, 1.0f, (float)m_RotationOut.Time/(float)GEAnimSystem.Instance.m_GUISpeed)
					.SetDelay((float)m_RotationOut.Delay/(float)GEAnimSystem.Instance.m_GUISpeed)	// Sets a delayed startup for the tween. 
						.SetEase(DOTweenEaseType(m_RotationOut.EaseType))				// Sets the ease of the tween. More info, check out http://easings.net or http://www.robertpenner.com/easing/easing_demo.html
						.OnUpdate(AnimOut_RotationUpdate)								// Sets a callback that will be fired every time the tween updates.
						.OnComplete(AnimOut_RotationComplete)							// Sets a callback that will be fired the moment the tween reaches completion, all loops included.
						.SetUpdate(UpdateType.Normal, true);						// Sets the type of update (Normal, Late or Fixed) for the tween and eventually tells it to ignore Unity's timeScale. 

				#elif HOTWEEN
				m_RotationVariable = 0.0f;
				// HOTween: https://www.assetstore.unity3d.com/#/content/3311
				// HOTween Documentation:  http://hotween.demigiant.com/documentation.html
				HOTween.To(this, (float)m_RotationOut.Time/(float)GEAnimSystem.Instance.m_GUISpeed, new TweenParms()
				           .Prop("m_RotationVariable", 1.0f, false)
				           .Delay((float)m_RotationOut.Delay/(float)GEAnimSystem.Instance.m_GUISpeed)
				           .Ease(HOTweenEaseType(m_RotationOut.EaseType))
				           .OnUpdate(AnimOut_RotationUpdate)
				           .OnComplete(AnimOut_RotationComplete)
				           .UpdateType(UpdateType.TimeScaleIndependentUpdate)
				           );
				
				m_RotationOut.Began = true;
				#elif ITWEEN
				// iTween: https://www.assetstore.unity3d.com/#/content/84 
				// iTween Documentation: http://itween.pixelplacement.com/documentation.php
				iTween.ValueTo(this.gameObject, iTween.Hash("from", 0.0f,
				                                            "to", 1.0f,
				                                            "time", (float)m_RotationOut.Time/(float)GEAnimSystem.Instance.m_GUISpeed,
				                                            "ignoretimescale", true,
				                                            "delay", (float)m_RotationOut.Delay/(float)GEAnimSystem.Instance.m_GUISpeed,
				                                            "easeType", iTweenEaseType(m_RotationOut.EaseType),
				                                            "onupdate", "AnimOut_RotationUpdateByValue",
				                                            "onupdatetarget", this.gameObject,
				                                            "oncomplete", "AnimOut_RotationComplete"));
				
				m_RotationOut.Began = true;
				#else
				// LeanTween: https://www.assetstore.unity3d.com/#/content/3595
				// LeanTween Documentation: http://dentedpixel.com/LeanTweenDocumentation/classes/LeanTween.html
				LeanTween.value(this.gameObject, AnimOut_RotationUpdateByValue, 0.0f, 1.0f, (float)m_RotationOut.Time/(float)GEAnimSystem.Instance.m_GUISpeed)
					.setDelay((float)m_RotationOut.Delay/(float)GEAnimSystem.Instance.m_GUISpeed)
						.setEase(LeanTweenEaseType(m_RotationOut.EaseType))
						.setOnComplete(AnimOut_RotationComplete)							
						.setUseEstimatedTime( true );
				#endif
			}
			
			// Check is scale-out enable
			if(m_ScaleOut.Enable && m_ScaleOut.Began==false)
			{

				m_ScaleOut.Began = true;
				m_ScaleOut.Animating = false;
				m_ScaleOut.Done = false;

				m_ScaleOut.ScaleBegin = transform.localScale;

				#if DOTWEEN
				m_ScaleVariable = 0.0f;
				// DOTween: https://www.assetstore.unity3d.com/en/#!/content/27676
				// DOTween Documentation: http://dotween.demigiant.com/documentation.php
				DOTween.To(x=> m_ScaleVariable = x, 0.0f, 1.0f, (float)m_ScaleOut.Time/(float)GEAnimSystem.Instance.m_GUISpeed)
					.SetDelay((float)m_ScaleOut.Delay/(float)GEAnimSystem.Instance.m_GUISpeed)	// Sets a delayed startup for the tween. 
						.SetEase(DOTweenEaseType(m_ScaleOut.EaseType))				// Sets the ease of the tween. More info, check out http://easings.net or http://www.robertpenner.com/easing/easing_demo.html
						.OnUpdate(AnimOut_ScaleUpdate)								// Sets a callback that will be fired every time the tween updates.
						.OnComplete(AnimOut_ScaleComplete)							// Sets a callback that will be fired the moment the tween reaches completion, all loops included.
						.SetUpdate(UpdateType.Normal, true);						// Sets the type of update (Normal, Late or Fixed) for the tween and eventually tells it to ignore Unity's timeScale. 
				#elif HOTWEEN
				m_ScaleVariable = 0.0f;
				// HOTween: https://www.assetstore.unity3d.com/#/content/3311
				// HOTween Documentation:  http://hotween.demigiant.com/documentation.html
				HOTween.To(this, (float)m_ScaleOut.Time/(float)GEAnimSystem.Instance.m_GUISpeed, new TweenParms()
				           .Prop("m_ScaleVariable", 1.0f, false)
				           .Delay((float)m_ScaleOut.Delay/(float)GEAnimSystem.Instance.m_GUISpeed)
				           .Ease(HOTweenEaseType(m_ScaleOut.EaseType))
				           .OnUpdate(AnimOut_ScaleUpdate)
				           .OnComplete(AnimOut_ScaleComplete)
				           .UpdateType(UpdateType.TimeScaleIndependentUpdate)
				           );
				
				m_ScaleOut.Began = true;
				#elif ITWEEN
				// iTween: https://www.assetstore.unity3d.com/#/content/84 
				// iTween Documentation: http://itween.pixelplacement.com/documentation.php
				iTween.ValueTo(this.gameObject, iTween.Hash("from", 0.0f,
				                                            "to", 1.0f,
				                                            "time", (float)m_ScaleOut.Time/(float)GEAnimSystem.Instance.m_GUISpeed,
				                                            "ignoretimescale", true,
				                                            "delay", (float)m_ScaleOut.Delay/(float)GEAnimSystem.Instance.m_GUISpeed,
				                                            "easeType", iTweenEaseType(m_ScaleOut.EaseType),
				                                            "onupdate", "AnimOut_ScaleUpdateByValue",
				                                            "onupdatetarget", this.gameObject,
				                                            "oncomplete", "AnimOut_ScaleComplete"));
				
				m_ScaleOut.Began = true;
				#else
				// LeanTween: https://www.assetstore.unity3d.com/#/content/3595
				// LeanTween Documentation: http://dentedpixel.com/LeanTweenDocumentation/classes/LeanTween.html
				LeanTween.value(this.gameObject, AnimOut_ScaleUpdateByValue, 0.0f, 1.0f, (float)m_ScaleOut.Time/(float)GEAnimSystem.Instance.m_GUISpeed)
					.setDelay((float)m_ScaleOut.Delay/(float)GEAnimSystem.Instance.m_GUISpeed)
						.setEase(LeanTweenEaseType(m_ScaleOut.EaseType))
						.setOnComplete(AnimOut_ScaleComplete)							
						.setUseEstimatedTime( true );
				#endif
			}
			
			// Check is fade-out enable
			if(m_FadeOut.Enable && m_FadeOut.Began==false)
			{
				
				m_FadeOut.Began = true;
				m_FadeOut.Animating = false;
				m_FadeOut.Done = false;

				#if DOTWEEN
				m_FadeVariable = 0.0f;
				// DOTween: https://www.assetstore.unity3d.com/en/#!/content/27676
				// DOTween Documentation: http://dotween.demigiant.com/documentation.php
				DOTween.To(x=> m_FadeVariable = x, 0.0f, 1.0f, (float)m_FadeOut.Time/(float)GEAnimSystem.Instance.m_GUISpeed)
					.SetDelay((float)m_FadeOut.Delay/(float)GEAnimSystem.Instance.m_GUISpeed)	// Sets a delayed startup for the tween. 
						.SetEase(DOTweenEaseType(m_FadeOut.EaseType))				// Sets the ease of the tween. More info, check out http://easings.net or http://www.robertpenner.com/easing/easing_demo.html
						.OnUpdate(AnimOut_FadeUpdate)								// Sets a callback that will be fired every time the tween updates.
						.OnComplete(AnimOut_FadeComplete)							// Sets a callback that will be fired the moment the tween reaches completion, all loops included.
						.SetUpdate(UpdateType.Normal, true);						// Sets the type of update (Normal, Late or Fixed) for the tween and eventually tells it to ignore Unity's timeScale. 
				#elif HOTWEEN
				m_FadeVariable = 0.0f;
				// HOTween: https://www.assetstore.unity3d.com/#/content/3311
				// HOTween Documentation:  http://hotween.demigiant.com/documentation.html
				HOTween.To(this, (float)m_FadeOut.Time/(float)GEAnimSystem.Instance.m_GUISpeed, new TweenParms()
				           .Prop("m_FadeVariable", 1.0f, false)
				           .Delay(m_FadeOut.Delay/(float)GEAnimSystem.Instance.m_GUISpeed)
				           .Ease(HOTweenEaseType(m_FadeOut.EaseType))
				           .OnUpdate(AnimOut_FadeUpdate)
				           .OnComplete(AnimOut_FadeComplete)
				           .UpdateType(UpdateType.TimeScaleIndependentUpdate)
				           );
				#elif ITWEEN
				// iTween: https://www.assetstore.unity3d.com/#/content/84 
				// iTween Documentation: http://itween.pixelplacement.com/documentation.php
				iTween.ValueTo(this.gameObject, iTween.Hash("from", 0,
				                                            "to", 1.0f,
				                                            "time", (float)m_FadeOut.Time/(float)GEAnimSystem.Instance.m_GUISpeed,
				                                            "ignoretimescale", true,
				                                            "delay", (float)m_FadeOut.Delay/(float)GEAnimSystem.Instance.m_GUISpeed,
				                                            "easeType", iTweenEaseType(m_FadeOut.EaseType),
				                                            "onupdate", "AnimOut_FadeUpdateByValue",
				                                            "onupdatetarget", this.gameObject,
				                                            "oncomplete", "AnimOut_FadeComplete"));
				#else
				// LeanTween: https://www.assetstore.unity3d.com/#/content/3595
				// LeanTween Documentation: http://dentedpixel.com/LeanTweenDocumentation/classes/LeanTween.html
				LeanTween.value(this.gameObject, AnimOut_FadeUpdateByValue, 0, 1.0f, (float)m_FadeOut.Time/(float)GEAnimSystem.Instance.m_GUISpeed)
					.setDelay((float)m_FadeOut.Delay/(float)GEAnimSystem.Instance.m_GUISpeed)
						.setEase(LeanTweenEaseType(m_FadeOut.EaseType))
						.setOnComplete(AnimOut_FadeComplete)							
						.setUseEstimatedTime( true );
				#endif
			}
		}
		
		// Check if GUIMoveType is Children or SelfAndChildren then call children's Move-Out
		if(GUIMoveType==eGUIMove.Children || GUIMoveType==eGUIMove.SelfAndChildren)
		{
			foreach(Transform child in this.transform)			
			{
				GEAnim pGEAnim = child.gameObject.GetComponent<GEAnim>();
				if(pGEAnim!=null)
				{
					pGEAnim.MoveOut(eGUIMove.SelfAndChildren);
				}
			}
		}
	}
	
	#endregion
	
	// ######################################################################
	// Move In/Out functions
	// ######################################################################
	
	#region Move In/Out Animation
	
	#if DOTWEEN
	// Animate Move-In by m_MoveVariable
	// This function is used by DOTween.To()
	void AnimIn_MoveUpdate()
	{
		AnimIn_MoveUpdateByValue(m_MoveVariable);
	}
	#elif HOTWEEN
    // Animate Move-In by m_MoveVariable
	// This function is used by HOTween.To()
	void AnimIn_MoveUpdate()
	{
		AnimIn_MoveUpdateByValue(m_MoveVariable);
	}
	#endif

	// This function is used by iTween.ValueTo() and LeanTween.value()
	void AnimIn_MoveUpdateByValue(float Value)
	{
		m_RectTransform.anchoredPosition = new Vector3(m_MoveIn.BeginPos.x + ((m_MoveIn.EndPos.x-m_MoveIn.BeginPos.x) * Value),
				                                       m_MoveIn.BeginPos.y + ((m_MoveIn.EndPos.y-m_MoveIn.BeginPos.y) * Value),
		                                               m_MoveIn.BeginPos.z + ((m_MoveIn.EndPos.z-m_MoveIn.BeginPos.z) * Value));

		// Play begin sound
		if(m_MoveIn.Animating==false && m_MoveIn.Began==true && m_MoveIn.Sounds.m_Begin!=null)
		{
			m_MoveIn.Animating = true;
			PlaySoundOneShot(m_MoveIn.Sounds.m_Begin);
		}
	}

	// Move-In animate completed
	void AnimIn_MoveComplete()
	{
		// Play end sound
		if(m_MoveIn.Sounds.m_End!=null)// && m_MoveIn.Animating==true)
		{
			PlaySoundOneShot(m_MoveIn.Sounds.m_End);
		}

		m_MoveIn.Began = false;
		m_MoveIn.Animating = false;
		m_MoveIn.Done = true;
		
		m_MoveOut.Began = false;
		m_MoveOut.Animating = false;
		m_MoveOut.Done = false;
		
		// Start Idle
		StartMoveIdle();
	}

	#if DOTWEEN
	// Animate Move-Out by m_MoveVariable
	// This function is used by DOTween.To()
	void AnimOut_MoveUpdate()
	{
		AnimOut_MoveUpdateByValue(m_MoveVariable);
	}
	#elif HOTWEEN
    // Animate Move-Out by m_MoveVariable
	// This function is used by HOTween.To()
	void AnimOut_MoveUpdate()
	{
		AnimOut_MoveUpdateByValue(m_MoveVariable);
	}
	#endif

	// This function is used by iTween.ValueTo() and LeanTween.value()
	void AnimOut_MoveUpdateByValue(float Value)
	{
		if(m_RectTransform==null)
			return;
		m_RectTransform.anchoredPosition = new Vector3(m_MoveOut.BeginPos.x + ((m_MoveOut.EndPos.x-m_MoveOut.BeginPos.x) * Value),
				                                       m_MoveOut.BeginPos.y + ((m_MoveOut.EndPos.y-m_MoveOut.BeginPos.y) * Value),
		                                               m_MoveOut.BeginPos.z + ((m_MoveOut.EndPos.z-m_MoveOut.BeginPos.z) * Value));
		
		// Play begin sound
		if(m_MoveOut.Animating==false && m_MoveOut.Began==true && m_MoveOut.Sounds.m_Begin!=null)
		{
			m_MoveOut.Animating = true;
			PlaySoundOneShot(m_MoveOut.Sounds.m_Begin);
		}
	}
	
	// Move-Out animate completed
	void AnimOut_MoveComplete()
	{
		// Play end sound
		if(m_MoveOut.Sounds.m_End!=null)// && m_MoveOut.Animating==true)
		{
			PlaySoundOneShot(m_MoveOut.Sounds.m_End);
		}

		m_MoveIn.Began = false;
		m_MoveIn.Animating = false;
		m_MoveIn.Done = false;

		m_MoveOut.Began = false;
		m_MoveOut.Animating = false;
		m_MoveOut.Done = true;
	}
	
	#endregion

	
	// ######################################################################
	// Rotation In/Out functions
	// ######################################################################
	
	#region Rotation In/Out Animation
	
	#if DOTWEEN
	// Animate Rotation-In by m_RotationVariable
	// This function is used by DOTWEEN.To()
	void AnimIn_RotationUpdate()
	{
		AnimIn_RotationUpdateByValue(m_RotationVariable);
	}
	#elif HOTWEEN
	// Animate Rotation-In by m_RotationVariable
	// This function is used by HOTween.To()
	void AnimIn_RotationUpdate()
	{
		AnimIn_RotationUpdateByValue(m_RotationVariable);
	}
	#endif
	// This function is used by iTween.ValueTo() and LeanTween.value()
	void AnimIn_RotationUpdateByValue(float Value)
	{
		float AngleX = m_RotationIn.BeginRotation.x + ((m_RotationIn.EndRotation.x - m_RotationIn.BeginRotation.x)* Value);
		float AngleY = m_RotationIn.BeginRotation.y + ((m_RotationIn.EndRotation.y - m_RotationIn.BeginRotation.y)* Value);
		float AngleZ = m_RotationIn.BeginRotation.z + ((m_RotationIn.EndRotation.z - m_RotationIn.BeginRotation.z)* Value);

		m_RectTransform.localRotation = Quaternion.Euler(AngleX, AngleY, AngleZ);
		
		// Play begin sound
		if(m_RotationIn.Animating==false && m_RotationIn.Began==true && m_RotationIn.Sounds.m_Begin!=null)
		{
			m_RotationIn.Animating = true;
			PlaySoundOneShot(m_RotationIn.Sounds.m_Begin);
		}
	}
	
	// Animate Rotation-In animate completed
	void AnimIn_RotationComplete()
	{
		// Play end sound
		if(m_RotationIn.Sounds.m_End!=null)// && m_RotationIn.Animating==true)
		{
			PlaySoundOneShot(m_RotationIn.Sounds.m_End);
		}
		
		m_RotationIn.Began = false;
		m_RotationIn.Animating = false;
		m_RotationIn.Done = true;
		
		m_RotationOut.Began = false;
		m_RotationOut.Animating = false;
		m_RotationOut.Done = false;
		
		AnimIn_RotationUpdateByValue(1.0f);
		
		// Start Idle
		StartMoveIdle();
	}
	
	#if DOTWEEN
	// Animate Rotation-Out by m_RotationVariable
	// This function is used by DOTween.To()
	void AnimOut_RotationUpdate()
	{
		AnimOut_RotationUpdateByValue(m_RotationVariable);
	}
	#elif HOTWEEN
	// Animate Rotation-Out by m_RotationVariable
	// This function is used by HOTween.To()
	void AnimOut_RotationUpdate()
	{
		AnimOut_RotationUpdateByValue(m_RotationVariable);
	}
	#endif
	
	// This function is used by iTween.ValueTo() and LeanTween.value()
	void AnimOut_RotationUpdateByValue(float Value)
	{
		float AngleX = m_RotationOut.BeginRotation.x + ((m_RotationOut.EndRotation.x - m_RotationOut.BeginRotation.x)* Value);
		float AngleY = m_RotationOut.BeginRotation.y + ((m_RotationOut.EndRotation.y - m_RotationOut.BeginRotation.y)* Value);
		float AngleZ = m_RotationOut.BeginRotation.z + ((m_RotationOut.EndRotation.z - m_RotationOut.BeginRotation.z)* Value);

		m_RectTransform.localRotation = Quaternion.Euler(AngleX, AngleY, AngleZ);


		
		// Play begin sound
		if(m_RotationOut.Animating==false && m_RotationOut.Began==true && m_RotationOut.Sounds.m_Begin!=null)
		{
			m_RotationOut.Animating = true;
			PlaySoundOneShot(m_RotationOut.Sounds.m_Begin);
		}
	}
	
	// Animate Rotation-Out animate completed
	void AnimOut_RotationComplete()
	{
		// Play end sound
		if(m_RotationOut.Sounds.m_End!=null)// && m_RotationOut.Animating==true)
		{
			PlaySoundOneShot(m_RotationOut.Sounds.m_End);
		}
		
		m_RotationIn.Began = false;
		m_RotationIn.Animating = false;
		m_RotationIn.Done = false;
		
		m_RotationOut.Began = false;
		m_RotationOut.Animating = false;
		m_RotationOut.Done = true;
		
		AnimOut_RotationUpdateByValue(1.0f);
	}
	
	#endregion
	
	// ######################################################################
	// Scale In/Out functions
	// ######################################################################
	
	#region Scale In/Out Animation
	
	#if DOTWEEN
	// Animate Scale-In by m_ScaleVariable
	// Used by DOTween.To()
	void AnimIn_ScaleUpdate()
	{
		AnimIn_ScaleUpdateByValue(m_ScaleVariable);
	}
	#elif HOTWEEN
    // Animate Scale-In by m_ScaleVariable
	// This function is used by HOTween.To()
	void AnimIn_ScaleUpdate()
	{
		AnimIn_ScaleUpdateByValue(m_ScaleVariable);
	}
	#endif
	// This function is used by iTween.ValueTo() and LeanTween.value()
	void AnimIn_ScaleUpdateByValue(float Value)
	{
//		transform.localScale = new Vector3(m_ScaleIn.ScaleBegin.x + ((m_ScaleOriginal.x-m_ScaleIn.ScaleBegin.x) * Value),
//		                                   m_ScaleIn.ScaleBegin.y + ((m_ScaleOriginal.y-m_ScaleIn.ScaleBegin.y) * Value),
//		                                   m_ScaleIn.ScaleBegin.z + ((m_ScaleOriginal.z-m_ScaleIn.ScaleBegin.z) * Value));

		transform.localScale = new Vector3((m_ScaleIn.ScaleBegin.x*m_ScaleOriginal.x) + ((m_ScaleOriginal.x-(m_ScaleIn.ScaleBegin.x*m_ScaleOriginal.x)) * Value),
		                                   (m_ScaleIn.ScaleBegin.y*m_ScaleOriginal.y) + ((m_ScaleOriginal.y-(m_ScaleIn.ScaleBegin.y*m_ScaleOriginal.y)) * Value),
		                                   (m_ScaleIn.ScaleBegin.z*m_ScaleOriginal.z) + ((m_ScaleOriginal.z-(m_ScaleIn.ScaleBegin.z*m_ScaleOriginal.z)) * Value));

		// Play begin sound
		if(m_ScaleIn.Animating==false && m_ScaleIn.Began==true && m_ScaleIn.Sounds.m_Begin!=null)
		{
			m_ScaleIn.Animating = true;
			PlaySoundOneShot(m_ScaleIn.Sounds.m_Begin);
		}
	}

	// Animate Scale-In animate completed
	void AnimIn_ScaleComplete()
	{
		// Play end sound
		if(m_ScaleIn.Sounds.m_End!=null)// && m_ScaleIn.Animating==true)
		{
			PlaySoundOneShot(m_ScaleIn.Sounds.m_End);
		}

		m_ScaleIn.Began = false;
		m_ScaleIn.Animating = false;
		m_ScaleIn.Done = true;
		
		m_ScaleOut.Began = false;
		m_ScaleOut.Animating = false;
		m_ScaleOut.Done = false;

		AnimIn_ScaleUpdateByValue(1.0f);
		
		// Start Idle
		StartMoveIdle();
	}
	
	#if DOTWEEN
	// Animate Scale-Out by m_ScaleVariable
	// This function is used by DOTween.To()
	void AnimOut_ScaleUpdate()
	{
		AnimOut_ScaleUpdateByValue(m_ScaleVariable);
	}
	#elif HOTWEEN
    // Animate Scale-Out by m_ScaleVariable
	// This function is used by HOTween.To()
	void AnimOut_ScaleUpdate()
	{
		AnimOut_ScaleUpdateByValue(m_ScaleVariable);
	}
	#endif
	
	// This function is used by iTween.ValueTo() and LeanTween.value()
	void AnimOut_ScaleUpdateByValue(float Value)
	{
//		transform.localScale = new Vector3(m_ScaleOriginal.x + ((m_ScaleOut.ScaleEnd.x-m_ScaleOriginal.x) * Value),
//		                                   m_ScaleOriginal.y + ((m_ScaleOut.ScaleEnd.y-m_ScaleOriginal.y) * Value),
//		                                   m_ScaleOriginal.z + ((m_ScaleOut.ScaleEnd.z-m_ScaleOriginal.z) * Value));

		if(m_ScaleOut.ScaleBegin == m_ScaleOriginal)
		{
			transform.localScale = new Vector3(m_ScaleOut.ScaleBegin.x + ((m_ScaleOut.ScaleEnd.x-m_ScaleOut.ScaleBegin.x) * Value),
			                                   m_ScaleOut.ScaleBegin.y + ((m_ScaleOut.ScaleEnd.y-m_ScaleOut.ScaleBegin.y) * Value),
			                                   m_ScaleOut.ScaleBegin.z + ((m_ScaleOut.ScaleEnd.z-m_ScaleOut.ScaleBegin.z) * Value));
		}
		else
		{
			float x = Mathf.Lerp(m_ScaleOut.ScaleBegin.x, m_ScaleOriginal.x, Value);
			float y = Mathf.Lerp(m_ScaleOut.ScaleBegin.y, m_ScaleOriginal.y, Value);
			float z = Mathf.Lerp(m_ScaleOut.ScaleBegin.z, m_ScaleOriginal.z, Value);

			Vector3 ScaleTemp = new Vector3(x, y, z);
			transform.localScale = new Vector3(ScaleTemp.x + ((m_ScaleOut.ScaleEnd.x-ScaleTemp.x) * Value),
			                                   ScaleTemp.y + ((m_ScaleOut.ScaleEnd.y-ScaleTemp.y) * Value),
			                                   ScaleTemp.z + ((m_ScaleOut.ScaleEnd.z-ScaleTemp.z) * Value));
		}

		// Play begin sound
		if(m_ScaleOut.Animating==false && m_ScaleOut.Began==true && m_ScaleOut.Sounds.m_Begin!=null)
		{
			m_ScaleOut.Animating = true;
			PlaySoundOneShot(m_ScaleOut.Sounds.m_Begin);
		}
	}
	
	// Animate Scale-Out animate completed
	void AnimOut_ScaleComplete()
	{
		// Play end sound
		if(m_ScaleOut.Sounds.m_End!=null)// && m_ScaleOut.Animating==true)
		{
			PlaySoundOneShot(m_ScaleOut.Sounds.m_End);
		}

		m_ScaleIn.Began = false;
		m_ScaleIn.Animating = false;
		m_ScaleIn.Done = false;

		m_ScaleOut.Began = false;
		m_ScaleOut.Animating = false;
		m_ScaleOut.Done = true;

		AnimOut_ScaleUpdateByValue(1.0f);
	}
	
	#endregion
	
	// ######################################################################
	// Fade In/Out functions
	// ######################################################################
	
	#region Fade In/Out Animation

	#if DOTWEEN
	// Animate Fade-In by m_FadeVariable
	// This function is used by DOTween.To()
	void AnimIn_FadeUpdate()
	{
		AnimIn_FadeUpdateByValue(m_FadeVariable);
	}
	#elif HOTWEEN
    // Animate Fade-In by m_FadeVariable
	// This function is used by HOTween.To()
	void AnimIn_FadeUpdate()
	{
		AnimIn_FadeUpdateByValue(m_FadeVariable);
	}
	#endif

	// Used by iTween.ValueTo() and LeanTween.value()
	void AnimIn_FadeUpdateByValue(float Value)
	{
		float FadeValue = m_FadeIn.Fade + ((m_FadeOriginal-m_FadeIn.Fade)*Value);
		RecursiveFade(this.transform, FadeValue, m_FadeIn.FadeChildren);

		// Fade Text Effects
		if(m_TextOutline!=null)
		{
			float FadeOutlineValue = Value-0.75f;
			if(FadeOutlineValue<0)
				FadeOutlineValue = 0;
			if(FadeOutlineValue>0)
				FadeOutlineValue *= 4;

			float FadeOutline = m_FadeOriginalTextOutline*(FadeOutlineValue);
			FadeTextOutline(this.transform, FadeOutline);
		}		
		if(m_TextShadow!=null)
		{
			float FadeShadowValue = Value-0.75f;
			if(FadeShadowValue<0)
				FadeShadowValue = 0;
			if(FadeShadowValue>0)
				FadeShadowValue *= 4;

			float FadeShadow = m_FadeOriginalTextShadow*FadeShadowValue;
			FadeTextShadow(this.transform, FadeShadow);
		}
		
		// Play begin sound
		if(m_FadeIn.Animating==false && m_FadeIn.Began==true && m_FadeIn.Sounds.m_Begin!=null)
		{
			m_FadeIn.Animating = true;
			PlaySoundOneShot(m_FadeIn.Sounds.m_Begin);
		}
	}

	// Fade-In animate completed
	void AnimIn_FadeComplete()
	{
		// Play end sound
		if(m_FadeIn.Sounds.m_End!=null)// && m_FadeIn.Animating==true)
		{
			PlaySoundOneShot(m_FadeIn.Sounds.m_End);
		}

		m_FadeIn.Began = false;
		m_FadeIn.Animating = false;
		m_FadeIn.Done = true;

		m_FadeOut.Began = false;
		m_FadeOut.Animating = false;
		m_FadeOut.Done = false;

		AnimIn_FadeUpdateByValue(1.0f);
		
		// Start Idle
		StartMoveIdle();
	}
	
	#if DOTWEEN
	// Animate Fade-Out by m_FadeVariable
	// This function is used by DOTween.To()
	void AnimOut_FadeUpdate()
	{
		AnimOut_FadeUpdateByValue(m_FadeVariable);
	}
	#elif HOTWEEN
	// Animate Fade-Out by m_FadeVariable
	// This function is used by HOTween.To()
	void AnimOut_FadeUpdate()
	{
		AnimOut_FadeUpdateByValue(m_FadeVariable);
	}
	#endif

	// Used by iTween.ValueTo() and LeanTween.value()
	void AnimOut_FadeUpdateByValue(float Value)
	{
		float FadeValue = m_FadeOriginal + ((m_FadeOut.Fade-m_FadeOriginal)*Value);
		RecursiveFade(this.transform, FadeValue, m_FadeOut.FadeChildren);
				
		// Fade Text Effects
		if(m_TextOutline!=null)
		{
			float FadeOutlineValue = Value*3.0f;
			if(FadeOutlineValue>1)
				FadeOutlineValue = 1;
			
			float FadeOutline = m_FadeOriginalTextOutline*(1-FadeOutlineValue);
			FadeTextOutline(this.transform, FadeOutline);
		}		
		if(m_TextShadow!=null)
		{
			float FadeShadowValue = Value*3.0f;
			if(FadeShadowValue>1)
				FadeShadowValue = 1;
			
			float FadeShadow = m_FadeOriginalTextShadow*(1-FadeShadowValue);
			FadeTextShadow(this.transform, FadeShadow);
		}
		
		// Play begin sound
		if(m_FadeOut.Animating==false && m_FadeOut.Began==true && m_FadeOut.Sounds.m_Begin!=null)
		{
			m_FadeOut.Animating = true;
			PlaySoundOneShot(m_FadeOut.Sounds.m_Begin);
		}
	}

	
	// Fade-Out animate completed
	void AnimOut_FadeComplete()
	{
		// Play end sound
		if(m_FadeOut.Sounds.m_End!=null)// && m_FadeOut.Animating==true)
		{
			PlaySoundOneShot(m_FadeOut.Sounds.m_End);
		}

		m_FadeIn.Began = false;
		m_FadeOut.Animating = false;
		m_FadeIn.Done = false;

		m_FadeOut.Began = false;
		m_FadeOut.Animating = false;
		m_FadeOut.Done = true;

		AnimOut_FadeUpdateByValue(1.0f);
	}
	
	#endregion
	
	// ######################################################################
	// Scale Loop functions
	// ######################################################################
	
	#region Scale Loop Animation
	
	// Begin Scale-loop
	void ScaleLoopStart(float delay)
	{		
		m_ScaleLoop.Began = true;
		m_ScaleLoop.Animating = false;
		m_ScaleLoop.IsOverriding = false;
		m_ScaleLoop.IsOverridingDelayTimeCount = 0.0f;
		m_ScaleLoop.Done = false;
		m_ScaleLoop.m_ScaleLast = transform.localScale;

		#if DOTWEEN
		m_ScaleVariable = 0.0f;
		// DOTween: https://www.assetstore.unity3d.com/en/#!/content/27676
		// DOTween Documentation: http://dotween.demigiant.com/documentation.php
		m_DOTweenScaleLoop = DOTween.To(x=> m_ScaleVariable = x, 0.0f, 1.0f, (float)m_ScaleLoop.Time/(float)GEAnimSystem.Instance.m_GUISpeed)
				.SetDelay((float)delay/(float)GEAnimSystem.Instance.m_GUISpeed)		// Sets a delayed startup for the tween. 
				.SetEase(DOTweenEaseType(m_ScaleLoop.EaseType))			// Sets the ease of the tween. More info, check out http://easings.net or http://www.robertpenner.com/easing/easing_demo.html
				.SetLoops(-1, LoopType.Yoyo)							// Sets the looping options (Restart, Yoyo, Incremental) for the tween.
				.OnUpdate(ScaleLoopUpdate)								// Sets a callback that will be fired every time the tween updates.
				.OnComplete(ScaleLoopComplete)							// Sets a callback that will be fired the moment the tween reaches completion, all loops included.
				.SetUpdate(UpdateType.Normal, true);					// Sets the type of update (Normal, Late or Fixed) for the tween and eventually tells it to ignore Unity's timeScale. 
		#elif HOTWEEN
		m_ScaleVariable = 0.0f;
		// HOTween: https://www.assetstore.unity3d.com/#/content/3311
		// HOTween Documentation:  http://hotween.demigiant.com/documentation.html
		m_HOTweenScaleLoop = HOTween.To(this, (float)m_ScaleLoop.Time/(float)GEAnimSystem.Instance.m_GUISpeed, new TweenParms()
		           .Prop("m_ScaleVariable", 1.0f, false)
		                                .Delay((float)delay/(float)GEAnimSystem.Instance.m_GUISpeed)
		           .Ease(HOTweenEaseType(m_ScaleLoop.EaseType))
		           .Loops(-1,LoopType.Yoyo)
		           .OnUpdate(ScaleLoopUpdate)
		           .OnComplete(ScaleLoopComplete)
		           .UpdateType(UpdateType.TimeScaleIndependentUpdate)
		           );

		#elif ITWEEN
		// iTween: https://www.assetstore.unity3d.com/#/content/84 
		// iTween Documentation: http://itween.pixelplacement.com/documentation.php
		iTween.ValueTo(this.gameObject, iTween.Hash("from", 0.0f,
		                                            "to", 1.0f,
		                                            "time", ((float)m_ScaleLoop.Time)/(float)GEAnimSystem.Instance.m_GUISpeed,
		                                            "ignoretimescale", true,
		                                            "delay", (float)delay/(float)GEAnimSystem.Instance.m_GUISpeed,
		                                            "easeType", iTweenEaseType(m_ScaleLoop.EaseType),
		                                            "looptype","pingpong",
		                                            "onupdate", "ScaleLoopUpdateByValue",
		                                            "onupdatetarget", this.gameObject,
		                                            "oncomplete", "ScaleLoopComplete"
		                                            ));

		#else
		// LeanTween: https://www.assetstore.unity3d.com/#/content/3595
		// LeanTween Documentation: http://dentedpixel.com/LeanTweenDocumentation/classes/LeanTween.html
		m_LeanTweenScaleLoop = LeanTween.value(this.gameObject, ScaleLoopUpdateByValue, 0.0f, 1.0f, (float)m_ScaleLoop.Time/(float)GEAnimSystem.Instance.m_GUISpeed)
					.setDelay((float)delay/(float)GEAnimSystem.Instance.m_GUISpeed)
					.setEase(LeanTweenEaseType(m_ScaleLoop.EaseType))
					.setLoopPingPong()
					.setOnComplete(ScaleLoopComplete)
					.setUseEstimatedTime( true );
		#endif
	}
	
	#if DOTWEEN
	// Animate Scale-Loop by m_ScaleVariable
	// This function is used by DOTween.To()
	void ScaleLoopUpdate()
	{
		ScaleLoopUpdateByValue(m_ScaleVariable);
	}
	#elif HOTWEEN
	// Animate Scale-Loop by m_ScaleVariable
    // This function is used by HOTween.To()
	void ScaleLoopUpdate()
	{
		ScaleLoopUpdateByValue(m_ScaleVariable);
	}
	#endif

	// This function is used by iTween.ValueTo() and LeanTween.value()
	void ScaleLoopUpdateByValue(float Value)
	{
		// Return when scale is overriding from other class
		if(m_ScaleLoop.m_ScaleLast != transform.localScale)
		{
			StopScaleLoop();
		}
		// Does scaling
		else
		{
			// Wait a few seconds before Animate scale if it was overriding
			if(m_ScaleLoop.IsOverriding==true)
			{
				m_ScaleLoop.IsOverridingDelayTimeCount += Time.deltaTime;
				if(m_ScaleLoop.IsOverridingDelayTimeCount>2.0f)
				{
					m_ScaleLoop.IsOverridingDelayTimeCount = 0;
					m_ScaleLoop.IsOverriding = false;
				}
			}

			// Animate scale
			if(m_ScaleLoop.IsOverriding==false)
			{
				// Play sound once only when SclaeLoop start
				if(m_ScaleLoop.Animating==false)
				{
					StartScaleLoopSound();
				}

				// Update scale				
				transform.localScale = new Vector3((m_ScaleLoop.Min.x*m_ScaleOriginal.x) + (((m_ScaleLoop.Max.x*m_ScaleOriginal.x)-(m_ScaleLoop.Min.x*m_ScaleOriginal.x)) * Value),
				                                   (m_ScaleLoop.Min.y*m_ScaleOriginal.y) + (((m_ScaleLoop.Max.y*m_ScaleOriginal.y)-(m_ScaleLoop.Min.y*m_ScaleOriginal.y)) * Value),
				                                   (m_ScaleLoop.Min.z*m_ScaleOriginal.z) + (((m_ScaleLoop.Max.z*m_ScaleOriginal.z)-(m_ScaleLoop.Min.z*m_ScaleOriginal.z)) * Value));

			}
		}

		m_ScaleLoop.m_ScaleLast = transform.localScale;
	}
	
	// Scale-Loop animate completed
	void ScaleLoopComplete()
	{
	}
	
	// Stop ScaleLoop
	void StopScaleLoop()
	{
		m_ScaleLoop.Animating = false;
		m_ScaleLoop.IsOverriding = true;
		m_ScaleLoop.IsOverridingDelayTimeCount = 0.0f;

		m_ScaleLoop.Began = false;
		m_ScaleLoop.Done = true;
		
		// Stop ScaleLoop animation
		#if DOTWEEN
		if(m_DOTweenScaleLoop!=null)
		{
			m_DOTweenScaleLoop.Kill();
			m_DOTweenScaleLoop = null;
		}
		#elif HOTWEEN
		if(m_HOTweenScaleLoop!=null)
		{
			m_HOTweenScaleLoop.Kill();
			m_HOTweenScaleLoop = null;
		}
		#elif ITWEEN
		#else
		if(m_LeanTweenScaleLoop!=null)
		{
			m_LeanTweenScaleLoop.cancel();
			m_LeanTweenScaleLoop = null;
		}
		#endif
		
		// Stop ScaleLoop sound
		StopScaleLoopSound();
	}
	
	#endregion
	
	// ######################################################################
	// Fade Loop functions
	// ######################################################################
	
	#region Fade Loop Animation
	
	// Begin Fade-loop
	void FadeLoopStart(float delay)
	{		
		m_FadeLoop.Began = true;
		m_FadeLoop.Animating = false;
		m_FadeLoop.IsOverriding = false;
		m_FadeLoop.IsOverridingDelayTimeCount = 0.0f;
		m_FadeLoop.Done = false;
		m_FadeLoop.m_FadeLast = GetFadeValue(transform);
		
		#if DOTWEEN
		m_FadeVariable = 0.0f;
		// DOTween: https://www.assetstore.unity3d.com/en/#!/content/27676
		// DOTween Documentation: http://dotween.demigiant.com/documentation.php
		m_DOTweenFadeLoop = DOTween.To(x=> m_FadeVariable = x, 0.0f, 1.0f, (float)m_FadeLoop.Time/(float)GEAnimSystem.Instance.m_GUISpeed)
			.SetDelay((float)delay/(float)GEAnimSystem.Instance.m_GUISpeed)			// Sets a delayed startup for the tween. 
				.SetEase(DOTweenEaseType(m_FadeLoop.EaseType))			// Sets the ease of the tween. More info, check out http://easings.net or http://www.robertpenner.com/easing/easing_demo.html
				.SetLoops(-1, LoopType.Yoyo)							// Sets the looping options (Restart, Yoyo, Incremental) for the tween.
				.OnUpdate(FadeLoopUpdate)								// Sets a callback that will be fired every time the tween updates.
				.OnComplete(FadeLoopComplete)							// Sets a callback that will be fired the moment the tween reaches completion, all loops included.
				.SetUpdate(UpdateType.Normal, true);					// Sets the type of update (Normal, Late or Fixed) for the tween and eventually tells it to ignore Unity's timeScale. 
		#elif HOTWEEN
		m_FadeVariable = 0.0f;
		// HOTween: https://www.assetstore.unity3d.com/#/content/3311
		// HOTween Documentation:  http://hotween.demigiant.com/documentation.html
		m_HOTweenFadeLoop = HOTween.To(this, (float)m_FadeLoop.Time/(float)GEAnimSystem.Instance.m_GUISpeed, new TweenParms()
		           .Prop("m_FadeVariable", 1.0f, false)
		           .Delay((float)delay/(float)GEAnimSystem.Instance.m_GUISpeed)
		           .Ease(HOTweenEaseType(m_FadeLoop.EaseType))
		           .Loops(-1,LoopType.Yoyo)
		           .OnUpdate(FadeLoopUpdate)
		           .OnComplete(FadeLoopComplete)
		           .UpdateType(UpdateType.TimeScaleIndependentUpdate)
		           );
		#elif ITWEEN
		// iTween: https://www.assetstore.unity3d.com/#/content/84 
		// iTween Documentation: http://itween.pixelplacement.com/documentation.php
		iTween.ValueTo(this.gameObject, iTween.Hash("from", 0.0f,
		                                            "to", 1.0f,
		                                            "time", ((float)m_FadeLoop.Time)/(float)GEAnimSystem.Instance.m_GUISpeed,
		                                            "ignoretimescale", true,
		                                            "delay", (float)delay/(float)GEAnimSystem.Instance.m_GUISpeed,
		                                            "easeType", iTweenEaseType(m_FadeLoop.EaseType),
		                                            "looptype","pingpong",
		                                            "onupdate", "FadeLoopUpdateByValue",
		                                            "onupdatetarget", this.gameObject,
		                                            "oncomplete", "FadeLoopComplete"
		                                            ));
		#else
		// LeanTween: https://www.assetstore.unity3d.com/#/content/3595
		// LeanTween Documentation: http://dentedpixel.com/LeanTweenDocumentation/classes/LeanTween.html
		m_LeanTweenFadeLoop = LeanTween.value(this.gameObject, FadeLoopUpdateByValue, 0.0f, 1.0f, (float)m_FadeLoop.Time/(float)GEAnimSystem.Instance.m_GUISpeed)
				.setDelay((float)delay/(float)GEAnimSystem.Instance.m_GUISpeed)
				.setEase(LeanTweenEaseType(m_FadeLoop.EaseType))
				.setLoopPingPong()
				.setOnComplete(FadeLoopComplete)
				.setUseEstimatedTime( true );
		#endif
	}
	
	#if DOTWEEN
	// Animate Fade-Loop by m_FadeVariable
	// This function is used by DOTween.To()
	void FadeLoopUpdate()
	{
		FadeLoopUpdateByValue(m_FadeVariable);
	}
	#elif HOTWEEN
	// Animate Fade-Loop by m_FadeVariable
    // This function is used by HOTween.To()
	void FadeLoopUpdate()
	{
		FadeLoopUpdateByValue(m_FadeVariable);
	}
	#endif

	// This function is used by iTween.ValueTo() and LeanTween.value()
	void FadeLoopUpdateByValue(float Value)
	{
		float FadeValue = GetFadeValue(transform);

		// Return when fade is overriding from other class
		if(m_FadeLoop.m_FadeLast != FadeValue)
		{
			StopFadeLoop();
		}
		// Does scaling
		else
		{
			// Wait a few seconds before Animate fade if it was overriding
			if(m_FadeLoop.IsOverriding==true)
			{
				m_FadeLoop.IsOverridingDelayTimeCount += Time.deltaTime;
				if(m_FadeLoop.IsOverridingDelayTimeCount>2.0f)
				{
					m_FadeLoop.IsOverridingDelayTimeCount = 0;
					m_FadeLoop.IsOverriding = false;
				}
			}
			
			// Animate fade
			if(m_FadeLoop.IsOverriding==false)
			{
				// Play sound once only when SclaeLoop start
				if(m_FadeLoop.Animating==false)
				{
					StartFadeLoopSound();
				}

				FadeValue = m_FadeLoop.Min + ((m_FadeLoop.Max-m_FadeLoop.Min) * Value);
				RecursiveFade(this.transform, FadeValue, m_FadeLoop.FadeChildren);
				
				// Fade Text Effects
				if(m_TextOutline!=null)
				{
					float FadeOutlineValue = m_FadeOriginalTextOutline*Value;
					float FadeOutline = 0;
					if(FadeOutlineValue>m_TextOutline.effectColor.a)	// Up
					{
						FadeOutlineValue = Value-0.75f;
						if(FadeOutlineValue<0)
							FadeOutlineValue = 0;
						if(FadeOutlineValue>0)
							FadeOutlineValue *= 4;

						FadeOutline = m_FadeOriginalTextOutline*(FadeOutlineValue);
						FadeTextOutline(this.transform, FadeOutline);
					}
					if(FadeOutlineValue<m_TextOutline.effectColor.a)	// Down
					{
						FadeOutlineValue = Value*2.0f;
						if(FadeOutlineValue>1)
							FadeOutlineValue = 1;
						
						FadeOutline = m_FadeOriginalTextOutline*(1-FadeOutlineValue);
						FadeTextOutline(this.transform, FadeOutline);
					}
				}
				if(m_TextShadow!=null)
				{
					float FadeShadowValue = m_FadeOriginalTextShadow*Value;
					float FadeShadow = 0;
					if(FadeShadowValue>m_TextShadow.effectColor.a)	// Up
					{
						FadeShadowValue = Value-0.75f;
						if(FadeShadowValue<0)
							FadeShadowValue = 0;
						if(FadeShadowValue>0)
							FadeShadowValue *= 4;

						FadeShadow = m_FadeOriginalTextShadow*FadeShadowValue;
						FadeTextShadow(this.transform, FadeShadow);
					}
					if(FadeShadowValue<m_TextShadow.effectColor.a)	// Down
					{
						FadeShadowValue = Value*2.0f;
						if(FadeShadowValue>1)
							FadeShadowValue = 1;

						FadeShadow = m_FadeOriginalTextShadow*(1-FadeShadowValue);
						FadeTextShadow(this.transform, FadeShadow);
					}
				}
			}
		}
		
		m_FadeLoop.m_FadeLast = FadeValue;
	}

	// Fade-Loop animate completed
	void FadeLoopComplete()
	{
	}
	
	// Stop FadeLoop
	void StopFadeLoop()
	{
		m_FadeLoop.Animating = false;
		m_FadeLoop.IsOverriding = true;
		m_FadeLoop.IsOverridingDelayTimeCount = 0.0f;
		
		m_FadeLoop.Began = false;
		m_FadeLoop.Done = true;
		
		// Stop FadeLoop animation
		#if DOTWEEN
		if(m_DOTweenFadeLoop!=null)
		{
			m_DOTweenFadeLoop.Kill();
			m_DOTweenFadeLoop = null;
		}
		#elif HOTWEEN
		if(m_HOTweenFadeLoop!=null)
		{
			m_HOTweenFadeLoop.Kill();
			m_HOTweenFadeLoop = null;
		}
		#elif ITWEEN
		#else
		if(m_LeanTweenFadeLoop!=null)
		{
			m_LeanTweenFadeLoop.cancel();
			m_LeanTweenFadeLoop = null;
		}
		#endif

		// Stop FadeLoop sound
		StopFadeLoopSound();
	}
	
	#endregion

	// ######################################################################
	// Audio Functions
	// ######################################################################
	
	#region Audio

	// Play sound one shot
	void PlaySoundOneShot(AudioClip pAudioClip)
	{
		// Check if we have an AudioListener in the scene
		AudioListener pAudioListener = GameObject.FindObjectOfType<AudioListener>();
		if(pAudioListener!=null)
		{
			bool IsPlaySuccess = false;
			AudioSource[] pAudioSourceList = pAudioListener.gameObject.GetComponents<AudioSource>();
			if(pAudioSourceList.Length>0)
			{
				for(int i=0;i<pAudioSourceList.Length;i++)
				{
					if(pAudioSourceList[i].isPlaying==false)
					{
						IsPlaySuccess = true;
						pAudioSourceList[i].PlayOneShot(pAudioClip);
						break;
					}
				}
			}
			
			if(IsPlaySuccess==false && pAudioSourceList.Length<32)
			{
				// Add AudioSource to AudioListener game object if there is no AudioSource
				AudioSource pAudioSource = pAudioListener.gameObject.AddComponent<AudioSource>();
				pAudioSource.rolloffMode = AudioRolloffMode.Linear;
				pAudioSource.playOnAwake = false;
				pAudioSource.PlayOneShot(pAudioClip);
			}
		}
	}

	// Play sound one shot with a delay time
	IEnumerator PlaySoundOneShotWithDelay(AudioClip pAudioClip, float Delay)
	{
		yield return new WaitForSeconds(Delay);

		PlaySoundOneShot(pAudioClip);
	}

	// Play sound loop
	AudioSource PlaySoundLoop(AudioClip pAudioClip)
	{
		AudioSource pAudioSource = null;
		
		// Check if we have an AudioListener in the scene
		AudioListener pAudioListener = GameObject.FindObjectOfType<AudioListener>();
		if(pAudioListener!=null)
		{
			bool IsPlaySuccess = false;
			AudioSource[] pAudioSourceList = pAudioListener.gameObject.GetComponents<AudioSource>();
			if(pAudioSourceList.Length>0)
			{
				for(int i=0;i<pAudioSourceList.Length;i++)
				{
					if(pAudioSourceList[i].isPlaying==false)
					{
						IsPlaySuccess = true;
						pAudioSource = pAudioSourceList[i];
						pAudioSource.clip = pAudioClip;
						pAudioSource.loop = true;
						pAudioSource.Play();
						break;
					}
				}
			}
			
			if(IsPlaySuccess==false && pAudioSourceList.Length<16)
			{
				// Add AudioSource to AudioListener game object if there is no AudioSource
				pAudioSource = pAudioListener.gameObject.AddComponent<AudioSource>();
				pAudioSource.rolloffMode = AudioRolloffMode.Linear;
				pAudioSource.playOnAwake = false;
				pAudioSource.clip = pAudioClip;
				pAudioSource.loop = true;
				pAudioSource.Play();
			}
		}
		
		return pAudioSource;
	}
	
	// Start loop sound for scale loop
	void StartScaleLoopSound()
	{
		// Play sound when ScaleLoop start
		if(m_ScaleLoop.Animating==false)
		{
			if(m_ScaleLoop.Sound.m_AudioClip!=null)
			{
				m_ScaleLoop.Animating = true;
				m_ScaleLoop.Sound.m_AudioSource = null;
				
				// Should it played by PlaySoundLoop or PlaySoundOneShot
				if(m_ScaleLoop.Sound.m_Loop==true)
				{
					m_ScaleLoop.Sound.m_AudioSource = PlaySoundLoop(m_ScaleLoop.Sound.m_AudioClip);
				}
				else
				{
					PlaySoundOneShot(m_ScaleLoop.Sound.m_AudioClip);
				}				
			}
		}
	}
	
	// Stop loop sound for scale loop
	void StopScaleLoopSound()
	{
		if(m_ScaleLoop.Sound.m_AudioClip!=null && m_ScaleLoop.Sound.m_AudioSource!=null)
		{
			m_ScaleLoop.Sound.m_AudioSource.Stop();
			m_ScaleLoop.Sound.m_AudioSource = null;
		}
	}
	
	// Start loop sound for fade loop
	void StartFadeLoopSound()
	{
		// Play sound once only when SclaeLoop start
		if(m_FadeLoop.Animating==false)
		{
			if(m_FadeLoop.Sound.m_AudioClip!=null)
			{
				m_FadeLoop.Animating = true;
				m_FadeLoop.Sound.m_AudioSource = null;
				
				// Should it played by PlaySoundLoop or PlaySoundOneShot
				if(m_FadeLoop.Sound.m_Loop==true)
				{
					m_FadeLoop.Sound.m_AudioSource = PlaySoundLoop(m_FadeLoop.Sound.m_AudioClip);
				}
				else
				{
					PlaySoundOneShot(m_FadeLoop.Sound.m_AudioClip);
				}				
			}
		}
	}
	
	// Stop loop sound for fade loop
	void StopFadeLoopSound()
	{
		if(m_FadeLoop.Sound.m_AudioClip!=null && m_FadeLoop.Sound.m_AudioSource!=null)
		{
			m_FadeLoop.Sound.m_AudioSource.Stop();
			m_FadeLoop.Sound.m_AudioSource = null;
		}
	}
	
	#endregion

	// ######################################################################
	// Utilities Functions
	// ######################################################################
	
	#region Utilities
	
	// Find total bounds size
	void CalculateTotalBounds()
	{
		m_TotalBounds = new Bounds(Vector3.zero, Vector3.zero);

		if(m_Slider!=null || m_Toggle!=null || m_CanvasRenderer!=null)
		{
			RectTransform pRectTransform = this.gameObject.GetComponent<RectTransform>();
			m_TotalBounds.size = new Vector3(pRectTransform.rect.width, pRectTransform.rect.height, 0);
		}
	}
	
	// Reset position when screen resolution is changed
	public void ScreenResolutionChange()
	{
		InitMoveIn();
		
		InitMoveOut();
	}

	// Check is this object animating
	public bool IsAnimating()
	{
		if(m_MoveIn.Began == true || m_MoveOut.Began == true ||
		   m_RotationIn.Began == true || m_RotationOut.Began == true ||
		   m_ScaleIn.Began == true || m_ScaleOut.Began == true ||
		   m_FadeIn.Began == true || m_FadeOut.Began == true)
		{
			return true;
		}
		
		return false;
	}

	// Get face value of this object depends on what UI component is attached to it
	float GetFadeValue(Transform trans)
	{
		float FadeValue = 1.0f;
		
		if(m_Image!=null)
		{
			FadeValue = m_Image.color.a;
		}
		if(m_Toggle)
		{
			FadeValue = m_Toggle.GetComponentInChildren<Image>().color.a;
		}		
		if(m_Text!=null)
		{
			FadeValue = m_Text.color.a;
		}		
		if(m_RawImage!=null)
		{
			FadeValue = m_RawImage.color.a;
		}
		if(m_Slider)
		{
			FadeValue = m_Slider.colors.normalColor.a;
		}
		
		return FadeValue;
	}

	// Get outline fade value if this is Text UI
	float GetFadeTextOutlineValue(Transform trans)
	{
		float FadeValue = 1.0f;

		if(m_TextOutline!=null)
		{
			FadeValue = m_TextOutline.effectColor.a;
		}
		
		return FadeValue;
	}
	
	// Get shadow fade value if this is Text UI
	float GetFadeTextShadowValue(Transform trans)
	{
		float FadeValue = 1.0f;
		
		if(m_TextShadow!=null)
		{
			FadeValue = m_TextShadow.effectColor.a;
		}
		
		return FadeValue;
	}
	
	// Fade through it's children
	void RecursiveFade(Transform trans, float Fade, bool IsFadeChildren)
	{
		// Skip Fade if current object is enabled to fade itself.
		bool SkipFade = false;
		if(this.transform != trans)
		{
			GEAnim pGEAnim = trans.GetComponent<GEAnim>();
			if(pGEAnim!=null)
			{
				if(pGEAnim.m_FadeIn.Enable==true && pGEAnim.m_FadeIn.Began==true)
				{
					SkipFade = true;
				}
				else if(pGEAnim.m_FadeOut.Enable==true && pGEAnim.m_FadeOut.Began==true)
				{
					SkipFade = true;
				}
			}
		}

		// Skip fade for some UI component
		if(SkipFade==false)
		{
			Image pImage = trans.gameObject.GetComponent<Image>();
			if(pImage!=null)
			{
				pImage.color = new Color(pImage.color.r, pImage.color.g, pImage.color.b, Fade);
			}
			Text pText = trans.gameObject.GetComponent<Text>();
			if(pText!=null)
			{
				pText.color = new Color(pText.color.r, pText.color.g, pText.color.b, Fade);
			}
			RawImage pRawImage = trans.gameObject.GetComponent<RawImage>();
			if(pRawImage!=null)
			{
				pRawImage.color = new Color(pRawImage.color.r, pRawImage.color.g, pRawImage.color.b, Fade);
			}
		}
		
		// Fade children
		if(IsFadeChildren==true)
		{
			foreach(Transform child in trans)
			{
				RecursiveFade(child.transform, Fade, IsFadeChildren);
			}
		}
	}
	
	// Fade Outline Effect if this is Text UI
	void FadeTextOutline(Transform trans, float FadeOutline)
	{
		if(m_TextOutline!=null)
		{
			m_TextOutline.effectColor = new Color(m_TextOutline.effectColor.r, m_TextOutline.effectColor.g, m_TextOutline.effectColor.b, FadeOutline);
		}
	}
	
	// Fade Shadow Effect if this is Text UI
	void FadeTextShadow(Transform trans, float FadeShadow)
	{
		if(m_TextShadow!=null)
		{
			m_TextShadow.effectColor = new Color(m_TextShadow.effectColor.r, m_TextShadow.effectColor.g, m_TextShadow.effectColor.b, FadeShadow);
		}
	}
	
	#endregion
	
	// ######################################################################
	// EaseType Converter for DOTween/HOTween/LeanTween/iTween
	// ######################################################################
	
	#region EaseType Converter
	
	#if DOTWEEN
	// DOTween: https://www.assetstore.unity3d.com/en/#!/content/27676
	// DOTween Documentation: http://dotween.demigiant.com/documentation.php
	public Ease DOTweenEaseType(eEaseType easeType)
	{
		Ease result = Ease.Linear;
		switch (easeType)
		{
		case eEaseType.InQuad:			result = Ease.InQuad; break;
		case eEaseType.OutQuad:			result = Ease.OutQuad; break;
		case eEaseType.InOutQuad:		result = Ease.InOutQuad; break;
		case eEaseType.InCubic:			result = Ease.OutCubic; break;
		case eEaseType.OutCubic:		result = Ease.OutCubic; break;
		case eEaseType.InOutCubic:		result = Ease.InOutCubic; break;
		case eEaseType.InQuart:			result = Ease.InQuart; break;
		case eEaseType.OutQuart:		result = Ease.OutQuart; break;
		case eEaseType.InOutQuart:		result = Ease.InOutQuart; break;
		case eEaseType.InQuint:			result = Ease.InQuint; break;
		case eEaseType.OutQuint:		result = Ease.OutQuint; break;
		case eEaseType.InOutQuint:		result = Ease.InOutQuint; break;
		case eEaseType.InSine:			result = Ease.InSine; break;
		case eEaseType.OutSine:			result = Ease.OutSine; break;
		case eEaseType.InOutSine:		result = Ease.InOutSine; break;
		case eEaseType.InExpo:			result = Ease.InExpo; break;
		case eEaseType.OutExpo:			result = Ease.OutExpo; break;
		case eEaseType.InOutExpo:		result = Ease.InOutExpo; break;
		case eEaseType.InCirc:			result = Ease.InCirc; break;
		case eEaseType.OutCirc:			result = Ease.OutCirc; break;
		case eEaseType.InOutCirc:		result = Ease.InOutCirc; break;
		case eEaseType.linear:			result = Ease.Linear; break;
		case eEaseType.InBounce:		result = Ease.InBounce; break;
		case eEaseType.OutBounce:		result = Ease.OutBounce; break;
		case eEaseType.InOutBounce:		result = Ease.InOutBounce; break;
		case eEaseType.InBack:			result = Ease.InBack; break;
		case eEaseType.OutBack:			result = Ease.OutBack; break;
		case eEaseType.InOutBack:		result = Ease.InOutBack; break;
		case eEaseType.InElastic:		result = Ease.InElastic; break;
		case eEaseType.OutElastic:		result = Ease.OutElastic; break;
		case eEaseType.InOutElastic:	result = Ease.InOutElastic; break;
		default:						result = Ease.Linear; break;
		}
		return result;
	}
	#elif HOTWEEN
	// HOTween: https://www.assetstore.unity3d.com/#/content/3311
	// HOTween Documentation:  http://hotween.demigiant.com/documentation.html
	public Holoville.HOTween.EaseType HOTweenEaseType(eEaseType easeType)
	{
		Holoville.HOTween.EaseType result = Holoville.HOTween.EaseType.Linear;
		switch (easeType)
		{
		case eEaseType.InQuad:			result = Holoville.HOTween.EaseType.EaseInQuad; break;
		case eEaseType.OutQuad:			result = Holoville.HOTween.EaseType.EaseOutQuad; break;
		case eEaseType.InOutQuad:		result = Holoville.HOTween.EaseType.EaseInOutQuad; break;
		case eEaseType.InCubic:			result = Holoville.HOTween.EaseType.EaseOutCubic; break;
		case eEaseType.OutCubic:		result = Holoville.HOTween.EaseType.EaseOutCubic; break;
		case eEaseType.InOutCubic:		result = Holoville.HOTween.EaseType.EaseInOutCubic; break;
		case eEaseType.InQuart:			result = Holoville.HOTween.EaseType.EaseInQuart; break;
		case eEaseType.OutQuart:		result = Holoville.HOTween.EaseType.EaseOutQuart; break;
		case eEaseType.InOutQuart:		result = Holoville.HOTween.EaseType.EaseInOutQuart; break;
		case eEaseType.InQuint:			result = Holoville.HOTween.EaseType.EaseInQuint; break;
		case eEaseType.OutQuint:		result = Holoville.HOTween.EaseType.EaseOutQuint; break;
		case eEaseType.InOutQuint:		result = Holoville.HOTween.EaseType.EaseInOutQuint; break;
		case eEaseType.InSine:			result = Holoville.HOTween.EaseType.EaseInSine; break;
		case eEaseType.OutSine:			result = Holoville.HOTween.EaseType.EaseOutSine; break;
		case eEaseType.InOutSine:		result = Holoville.HOTween.EaseType.EaseInOutSine; break;
		case eEaseType.InExpo:			result = Holoville.HOTween.EaseType.EaseInExpo; break;
		case eEaseType.OutExpo:			result = Holoville.HOTween.EaseType.EaseOutExpo; break;
		case eEaseType.InOutExpo:		result = Holoville.HOTween.EaseType.EaseInOutExpo; break;
		case eEaseType.InCirc:			result = Holoville.HOTween.EaseType.EaseInCirc; break;
		case eEaseType.OutCirc:			result = Holoville.HOTween.EaseType.EaseOutCirc; break;
		case eEaseType.InOutCirc:		result = Holoville.HOTween.EaseType.EaseInOutCirc; break;
		case eEaseType.linear:			result = Holoville.HOTween.EaseType.Linear; break;
		case eEaseType.InBounce:		result = Holoville.HOTween.EaseType.EaseInBounce; break;
		case eEaseType.OutBounce:		result = Holoville.HOTween.EaseType.EaseOutBounce; break;
		case eEaseType.InOutBounce:		result = Holoville.HOTween.EaseType.EaseInOutBounce; break;
		case eEaseType.InBack:			result = Holoville.HOTween.EaseType.EaseInBack; break;
		case eEaseType.OutBack:			result = Holoville.HOTween.EaseType.EaseOutBack; break;
		case eEaseType.InOutBack:		result = Holoville.HOTween.EaseType.EaseInOutBack; break;
		case eEaseType.InElastic:		result = Holoville.HOTween.EaseType.EaseInElastic; break;
		case eEaseType.OutElastic:		result = Holoville.HOTween.EaseType.EaseOutElastic; break;
		case eEaseType.InOutElastic:	result = Holoville.HOTween.EaseType.EaseInOutElastic; break;
		default:						result = Holoville.HOTween.EaseType.Linear; break;
		}
		return result;
	}
	#elif ITWEEN
	// iTween: https://www.assetstore.unity3d.com/#/content/84 
	// iTween Documentation: http://itween.pixelplacement.com/documentation.php
	public string iTweenEaseType(eEaseType easeType)
	{
		string result = "linear";
		switch (easeType)
		{
		case eEaseType.InQuad:			result = "easeInQuad"; break;
		case eEaseType.OutQuad:			result = "easeOutQuad"; break;
		case eEaseType.InOutQuad:		result = "easeInOutQuad"; break;
		case eEaseType.InCubic:			result = "easeOutCubic"; break;
		case eEaseType.OutCubic:		result = "easeOutCubic"; break;
		case eEaseType.InOutCubic:		result = "easeInOutCubic"; break;
		case eEaseType.InQuart:			result = "easeInQuart"; break;
		case eEaseType.OutQuart:		result = "easeOutQuart"; break;
		case eEaseType.InOutQuart:		result = "easeInOutQuart"; break;
		case eEaseType.InQuint:			result = "easeInQuint"; break;
		case eEaseType.OutQuint:		result = "easeOutQuint"; break;
		case eEaseType.InOutQuint:		result = "easeInOutQuint"; break;
		case eEaseType.InSine:			result = "easeInSine"; break;
		case eEaseType.OutSine:			result = "easeOutSine"; break;
		case eEaseType.InOutSine:		result = "easeInOutSine"; break;
		case eEaseType.InExpo:			result = "easeInExpo"; break;
		case eEaseType.OutExpo:			result = "easeOutExpo"; break;
		case eEaseType.InOutExpo:		result = "easeInOutExpo"; break;
		case eEaseType.InCirc:			result = "easeInCirc"; break;
		case eEaseType.OutCirc:			result = "easeOutCirc"; break;
		case eEaseType.InOutCirc:		result = "easeInOutCirc"; break;
		case eEaseType.linear:			result = "linear"; break;
		case eEaseType.InBounce:		result = "easeInBounce"; break;
		case eEaseType.OutBounce:		result = "easeOutBounce"; break;
		case eEaseType.InOutBounce:		result = "easeInOutBounce"; break;
		case eEaseType.InBack:			result = "easeInBack"; break;
		case eEaseType.OutBack:			result = "easeOutBack"; break;
		case eEaseType.InOutBack:		result = "easeInOutBack"; break;
		case eEaseType.InElastic:		result = "easeInElastic"; break;
		case eEaseType.OutElastic:		result = "easeOutElastic"; break;
		case eEaseType.InOutElastic:	result = "easeInOutElastic"; break;
		default:						result = "linear"; break;
		}
		return result;
	}
	#else
	// LeanTween: https://www.assetstore.unity3d.com/#/content/3595
	// LeanTween Documentation: http://dentedpixel.com/LeanTweenDocumentation/classes/LeanTween.html
	public LeanTweenType LeanTweenEaseType(eEaseType easeType)
	{
		LeanTweenType result = LeanTweenType.linear;
		switch (easeType)
		{
		case eEaseType.InQuad:			result = LeanTweenType.easeInQuad; break;
		case eEaseType.OutQuad:			result = LeanTweenType.easeOutQuad; break;
		case eEaseType.InOutQuad:		result = LeanTweenType.easeInOutQuad; break;
		case eEaseType.InCubic:			result = LeanTweenType.easeOutCubic; break;
		case eEaseType.OutCubic:		result = LeanTweenType.easeOutCubic; break;
		case eEaseType.InOutCubic:		result = LeanTweenType.easeInOutCubic; break;
		case eEaseType.InQuart:			result = LeanTweenType.easeInQuart; break;
		case eEaseType.OutQuart:		result = LeanTweenType.easeOutQuart; break;
		case eEaseType.InOutQuart:		result = LeanTweenType.easeInOutQuart; break;
		case eEaseType.InQuint:			result = LeanTweenType.easeInQuint; break;
		case eEaseType.OutQuint:		result = LeanTweenType.easeOutQuint; break;
		case eEaseType.InOutQuint:		result = LeanTweenType.easeInOutQuint; break;
		case eEaseType.InSine:			result = LeanTweenType.easeInSine; break;
		case eEaseType.OutSine:			result = LeanTweenType.easeOutSine; break;
		case eEaseType.InOutSine:		result = LeanTweenType.easeInOutSine; break;
		case eEaseType.InExpo:			result = LeanTweenType.easeInExpo; break;
		case eEaseType.OutExpo:			result = LeanTweenType.easeOutExpo; break;
		case eEaseType.InOutExpo:		result = LeanTweenType.easeInOutExpo; break;
		case eEaseType.InCirc:			result = LeanTweenType.easeInCirc; break;
		case eEaseType.OutCirc:			result = LeanTweenType.easeOutCirc; break;
		case eEaseType.InOutCirc:		result = LeanTweenType.easeInOutCirc; break;
		case eEaseType.linear:			result = LeanTweenType.linear; break;
		case eEaseType.InBounce:		result = LeanTweenType.easeInBounce; break;
		case eEaseType.OutBounce:		result = LeanTweenType.easeOutBounce; break;
		case eEaseType.InOutBounce:		result = LeanTweenType.easeInOutBounce; break;
		case eEaseType.InBack:			result = LeanTweenType.easeInBack; break;
		case eEaseType.OutBack:			result = LeanTweenType.easeOutBack; break;
		case eEaseType.InOutBack:		result = LeanTweenType.easeInOutBack; break;
		case eEaseType.InElastic:		result = LeanTweenType.easeInElastic; break;
		case eEaseType.OutElastic:		result = LeanTweenType.easeOutElastic; break;
		case eEaseType.InOutElastic:	result = LeanTweenType.easeInOutElastic; break;
		default:						result = LeanTweenType.linear; break;
		}
		return result;
	}
	#endif

	#endregion
}