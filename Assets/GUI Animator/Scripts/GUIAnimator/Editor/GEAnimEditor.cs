﻿// GUI Animator for Unity UI version: 0.8.4 (Product page: http://ge-team.com/pages/unity-3d/gui-animator-for-unity-ui/)
//
// Author:			Gold Experience Team (http://www.ge-team.com/pages/)
// Products:		http://ge-team.com/pages/unity-3d/
// Support:			geteamdev@gmail.com
// Documentation:	http://ge-team.com/pages/unity-3d/gui-animator-for-unity-ui/gui-animator-for-unity-ui-documentation/
//
// Please direct any bugs/comments/suggestions to geteamdev@gmail.com

#region Namespaces

using UnityEngine;
using UnityEditor;
using System.Collections;

#endregion

/******************************************
* GEAnimEditor class
* Custom editor for GEAnim component
******************************************/

// http://docs.unity3d.com/ScriptReference/CustomEditor.html
// http://docs.unity3d.com/ScriptReference/CustomEditor-ctor.html
// http://unity3d.com/learn/tutorials/modules/intermediate/editor/adding-buttons-to-inspector
[CustomEditor(typeof(GEAnim))]
// http://docs.unity3d.com/ScriptReference/Editor.html
public class GEAnimEditor : Editor
{
	#region Variables

	// Current inspected GEAnim object
	GEAnim targetProp;
	
	// Collection of move types to use in this custom inspector
	enum eMoveType
	{
		ScreenEdgeOrCorner,
		ParentPosition,
		SpecificLocalPosition,
		CurrentPosition
	}
	
	#endregion	
	
	// ######################################################################
	// Editor functions
	// ######################################################################
	
	#region Editor functions

	// This function is called when the object is loaded
	void OnEnable()
	{
		// target is The object being inspected
		targetProp = (GEAnim) target;
	}

	private Rect m_rectArea_TabsSkin;
	
	// Implement this function to make a custom inspector
	public override void OnInspectorGUI ()
	{
		if(GEAnimEditorUtils.IsCalcualtedEaseGraph==false)
		{
			GEAnimEditorUtils.CalculateEaseGraph();
			GEAnimEditorUtils.IsCalcualtedEaseGraph = true;
		}
		
		//if(GEAnimEditorUtils.m_FriendlyInspector==false)
		if(GEAnimEditorUtils.GetBool("m_FriendlyInspector")==false)		
		{
			//EditorGUILayout.BeginVertical();
			EditorGUILayout.BeginVertical();
			GUILayout.Space(5);

				m_rectArea_TabsSkin = EditorGUILayout.BeginVertical();
				{
					SetBackgroundColor(DefaultBoxContent_Color);
					GUI.Box(m_rectArea_TabsSkin, GUIContent.none);
					ReleaseBackgroundColor();
	                GUILayout.Space(5);
					
					//////////////////////////////////
					/// DRAW TABS
					//////////////////////////////////
					
					EditorGUILayout.BeginHorizontal();
					{
						GUILayout.Space(15);
						
						bool FriendlyInspector = EditorGUILayout.Toggle(GEAnimEditorUtils.GetBool("m_FriendlyInspector"), GUILayout.MaxWidth(12));
						GEAnimEditorUtils.SetBool("m_FriendlyInspector", FriendlyInspector);
						//GEAnimEditorUtils.m_FriendlyInspector = EditorGUILayout.Toggle(GEAnimEditorUtils.m_FriendlyInspector, GUILayout.MaxWidth(12));
						EditorGUILayout.LabelField("Friendly Inspector", GUILayout.MaxWidth(150));

						GUILayout.Space(15);
					}
					EditorGUILayout.EndHorizontal();

					//if(GEAnimEditorUtils.m_ShowHelpBox)
					{
						// Help box
						EditorGUILayout.BeginHorizontal();
						{
							GUILayout.Space(15);
							EditorGUILayout.HelpBox("Parameters have been categorized into tabs.", MessageType.None);
							GUILayout.Space(5);
						}
						EditorGUILayout.EndHorizontal();
					}
					
					GUILayout.Space(5);
	            }
				EditorGUILayout.EndVertical();
			
			GUILayout.Space(5);
			EditorGUILayout.EndVertical();
			
			DrawDefaultInspector();
        }
		else
		{
			EditorGUILayout.BeginVertical();
			GUILayout.Space(5);
			EditorGUILayout.EndVertical();
			
			DrawTabs();
			
			EditorGUILayout.BeginVertical();
			GUILayout.Space(2);

			//switch(GEAnimEditorUtils.m_eTab)
			switch((GEAnimEditorUtils.eTab)PlayerPrefs.GetInt("m_eTab"))
			{
			case GEAnimEditorUtils.eTab.In:
				Draw_In();
				break;
			case GEAnimEditorUtils.eTab.Idle:
				Draw_Idle();
				break;
			case GEAnimEditorUtils.eTab.Out:
				Draw_Out();
				break;
			case GEAnimEditorUtils.eTab.Settings:
				Draw_Settings();
				break;
			case GEAnimEditorUtils.eTab.Help:
				Draw_Help();
				break;
			}
			EditorGUILayout.EndVertical();

			// User click on a tab
			Event evt = Event.current;		
			switch (evt.type)
			{
			case EventType.mouseUp:
				if (m_rectTab1.Contains (evt.mousePosition))
				{
					//GEAnimEditorUtils.m_eTab = GEAnimEditorUtils.eTab.In;
					PlayerPrefs.SetInt("m_eTab", (int)GEAnimEditorUtils.eTab.In);
					Repaint();
				}
				if (m_rectTab2.Contains (evt.mousePosition))
                {
					//GEAnimEditorUtils.m_eTab = GEAnimEditorUtils.eTab.Idle;
					PlayerPrefs.SetInt("m_eTab", (int)GEAnimEditorUtils.eTab.Idle);
					Repaint();
                }
                if (m_rectTab3.Contains (evt.mousePosition))
                {
					//GEAnimEditorUtils.m_eTab = GEAnimEditorUtils.eTab.Out;
					PlayerPrefs.SetInt("m_eTab", (int)GEAnimEditorUtils.eTab.Out);
					Repaint();
                }
                if (m_rectTab4.Contains (evt.mousePosition))
                {
					//GEAnimEditorUtils.m_eTab = GEAnimEditorUtils.eTab.Settings;
					PlayerPrefs.SetInt("m_eTab", (int)GEAnimEditorUtils.eTab.Settings);
					Repaint();
                }
                if (m_rectTab5.Contains (evt.mousePosition))
                {
					//GEAnimEditorUtils.m_eTab = GEAnimEditorUtils.eTab.Help;
					PlayerPrefs.SetInt("m_eTab", (int)GEAnimEditorUtils.eTab.Help);
					Repaint();
				}
				break;
			}
		}

	}

	#endregion

	#region Draw Tabs

	//////////////////////////////////
	/// This functions draws tabs for Friendly inspector skin
	//////////////////////////////////

	private void DrawTabs()
	{

		m_rectTabVerticle = EditorGUILayout.BeginVertical();

			// Keep default GUIColor
			Color DefaultGUI_Color = GUI.color;

			GUILayout.Space(20);

			int[] Tab = new int[5];
			Tab[0] = 45;
			Tab[1] = 45;
			Tab[2] = 45;
			Tab[3] = 60;
			Tab[4] = 40;
			//TabWidth=55;


			EditorGUILayout.BeginHorizontal();
				
				GUIStyle labelStyle = new GUIStyle( GUI.skin.label );
				labelStyle.alignment = TextAnchor.MiddleCenter;

				if(/*GEAnimEditorUtils.m_eTab*/ (GEAnimEditorUtils.eTab)PlayerPrefs.GetInt("m_eTab")== GEAnimEditorUtils.eTab.In)
				{
					SetBackgroundColor(TabEnabled_BGColor);
					m_rectTab1 = new Rect(m_rectTabVerticle.x, m_rectTabVerticle.y, Tab[0], m_rectTabVerticle.height);
				}
				else
				{
					SetBackgroundColor(TabDisabled_BGColor);
					m_rectTab1 = new Rect(m_rectTabVerticle.x, m_rectTabVerticle.y+1, Tab[0], m_rectTabVerticle.height-1);
				}
				GUI.Box(m_rectTab1,"");
				GUI.Label(m_rectTab1,"In", labelStyle);

				if(/*GEAnimEditorUtils.m_eTab*/ (GEAnimEditorUtils.eTab)PlayerPrefs.GetInt("m_eTab")== GEAnimEditorUtils.eTab.Idle)
				{
					SetBackgroundColor(TabEnabled_BGColor);
					m_rectTab2 = new Rect(m_rectTabVerticle.x+(Tab[0]), m_rectTabVerticle.y, Tab[1], m_rectTabVerticle.height);
				}
				else
				{
					SetBackgroundColor(TabDisabled_BGColor);
					m_rectTab2 = new Rect(m_rectTabVerticle.x+(Tab[0]), m_rectTabVerticle.y+1, Tab[1], m_rectTabVerticle.height-1);
				}
				GUI.Box(m_rectTab2, "");
				GUI.Label(m_rectTab2,"Idle", labelStyle);
				
				if(/*GEAnimEditorUtils.m_eTab*/ (GEAnimEditorUtils.eTab)PlayerPrefs.GetInt("m_eTab")== GEAnimEditorUtils.eTab.Out)
				{
					SetBackgroundColor(TabEnabled_BGColor);
					m_rectTab3 = new Rect(m_rectTabVerticle.x+(Tab[0]+Tab[1]), m_rectTabVerticle.y, Tab[2], m_rectTabVerticle.height);
				}
				else
				{
					SetBackgroundColor(TabDisabled_BGColor);
					m_rectTab3 = new Rect(m_rectTabVerticle.x+(Tab[0]+Tab[1]), m_rectTabVerticle.y+1, Tab[2], m_rectTabVerticle.height-1);
				}
				GUI.Box(m_rectTab3, "");
				GUI.Label(m_rectTab3,"Out", labelStyle);
				
				if(/*GEAnimEditorUtils.m_eTab*/ (GEAnimEditorUtils.eTab)PlayerPrefs.GetInt("m_eTab")== GEAnimEditorUtils.eTab.Settings)
				{
					SetBackgroundColor(TabEnabled_BGColor);
					m_rectTab4 = new Rect(m_rectTabVerticle.x+(Tab[0]+Tab[1]+Tab[2]), m_rectTabVerticle.y, Tab[3], m_rectTabVerticle.height);
				}
				else
				{
					SetBackgroundColor(TabDisabled_BGColor);
					m_rectTab4 = new Rect(m_rectTabVerticle.x+(Tab[0]+Tab[1]+Tab[2]), m_rectTabVerticle.y+1, Tab[3], m_rectTabVerticle.height-1);
				}
				GUI.Box(m_rectTab4, "");
				GUI.Label(m_rectTab4,"Settings", labelStyle);
				
				if(/*GEAnimEditorUtils.m_eTab*/ (GEAnimEditorUtils.eTab)PlayerPrefs.GetInt("m_eTab")== GEAnimEditorUtils.eTab.Help)
				{
					SetBackgroundColor(TabEnabled_BGColor);
					m_rectTab5 = new Rect(m_rectTabVerticle.x+(Tab[0]+Tab[1]+Tab[2]+Tab[3]), m_rectTabVerticle.y, Tab[4], m_rectTabVerticle.height);
				}
				else
				{
					SetBackgroundColor(TabDisabled_BGColor);
					m_rectTab5 = new Rect(m_rectTabVerticle.x+(Tab[0]+Tab[1]+Tab[2]+Tab[3]), m_rectTabVerticle.y+1, Tab[4], m_rectTabVerticle.height-1);
				}
				GUI.Box(m_rectTab5, "");
				GUI.Label(m_rectTab5,"Help", labelStyle);

			EditorGUILayout.EndHorizontal();

			// Return default GUIColor
			GUI.color = DefaultGUI_Color;
			ReleaseBackgroundColor();

		EditorGUILayout.EndVertical();

		//////////////////////////////////
		/// SHOW SIGN THAT USER HAS ENABLE SOME ANIMATION IN TABS
		//////////////////////////////////
		GUIStyle boxStyle = new GUIStyle( GUI.skin.box );
		boxStyle.normal.background = MakeTex( 2, 2, TabContainsData_Color);
		if(targetProp.m_MoveIn.Enable || targetProp.m_RotationIn.Enable || targetProp.m_ScaleIn.Enable || targetProp.m_FadeIn.Enable)
		{
			GUI.Box(new Rect(m_rectTab1.x+1, m_rectTab1.y+(m_rectTab1.height-2), m_rectTab1.width-2, 2), "", boxStyle);
		}
		if(targetProp.m_ScaleLoop.Enable || targetProp.m_FadeLoop.Enable)
		{
			GUI.Box(new Rect(m_rectTab2.x+1, m_rectTab2.y+(m_rectTab2.height-2), m_rectTab2.width-2, 2), "", boxStyle);
		}
		if(targetProp.m_MoveOut.Enable || targetProp.m_RotationOut.Enable || targetProp.m_ScaleOut.Enable || targetProp.m_FadeOut.Enable)
		{
			GUI.Box(new Rect(m_rectTab3.x+1, m_rectTab3.y+(m_rectTab3.height-2), m_rectTab3.width-2, 2), "", boxStyle);
		}
	}
	
	#endregion
	
	#region Make Draw Move In/Out

	private Rect m_rectArea_Move, m_rectMoveArea;
	
	private Color DefaultBoxContent_Color = new Color( 1.0f, 1.0f, 1.0f, 0.5f );

	// Draw Move In/Out area
	private void DrawMoveInOut(int InOrOut)
	{
		var centeredStyle = GUI.skin.GetStyle("Label");
		centeredStyle.alignment = TextAnchor.MiddleRight;

		m_rectArea_Move = EditorGUILayout.BeginVertical();
		{
			SetBackgroundColor(DefaultBoxContent_Color);
			GUI.Box(m_rectArea_Move, GUIContent.none);
			ReleaseBackgroundColor();
			GUILayout.Space(3);

			//////////////////////////////////
			/// DRAW ENABLE
			//////////////////////////////////

			EditorGUILayout.BeginHorizontal();
			{
				GUILayout.Space(5);
				if(InOrOut==0)
				{
					targetProp.m_MoveIn.Enable = EditorGUILayout.Toggle(targetProp.m_MoveIn.Enable, GUILayout.MaxWidth(12));
					EditorGUILayout.LabelField("Move In", GUILayout.MaxWidth(60));
				}
				else
				{
					targetProp.m_MoveOut.Enable = EditorGUILayout.Toggle(targetProp.m_MoveOut.Enable, GUILayout.MaxWidth(12));
					EditorGUILayout.LabelField("Move Out", GUILayout.MaxWidth(70));
				}
			}
			EditorGUILayout.EndHorizontal();
			
			//////////////////////////////////
			/// DRAW From or To
			//////////////////////////////////

			EditorGUILayout.BeginHorizontal();
			{

				GUILayout.Space(10);
				
				if(InOrOut==0)
				{
					GUILayout.Label("From:", GUILayout.Width(40));
				}
				else
				{
					GUILayout.Label("To:", GUILayout.Width(40));
				}

				// HOR
				EditorGUILayout.BeginVertical();
				{			
					eMoveType currentMovePos = eMoveType.ScreenEdgeOrCorner;
					if(InOrOut==0)
					{
						if(targetProp.m_MoveIn.MoveFrom==ePosMove.ParentPosition)
							currentMovePos = eMoveType.ParentPosition;
						else if(targetProp.m_MoveIn.MoveFrom==ePosMove.SelfPosition)
							currentMovePos = eMoveType.CurrentPosition;
						else if(targetProp.m_MoveIn.MoveFrom==ePosMove.LocalPosition)
							currentMovePos = eMoveType.SpecificLocalPosition;
						else 
							currentMovePos = eMoveType.ScreenEdgeOrCorner;
					}
					else
					{
						if(targetProp.m_MoveOut.MoveTo==ePosMove.ParentPosition)
							currentMovePos = eMoveType.ParentPosition;
						else if(targetProp.m_MoveOut.MoveTo==ePosMove.SelfPosition)
							currentMovePos = eMoveType.CurrentPosition;
						else if(targetProp.m_MoveOut.MoveTo==ePosMove.LocalPosition)
							currentMovePos = eMoveType.SpecificLocalPosition;
						else 
							currentMovePos = eMoveType.ScreenEdgeOrCorner;
					}
					
					currentMovePos = (eMoveType) EditorGUILayout.EnumPopup("" , currentMovePos);//, GUILayout.MaxWidth(100));
					
					EditorGUILayout.BeginHorizontal();
					{
						switch(currentMovePos)
						{
						case eMoveType.ParentPosition:
							if(InOrOut==0)
							{
								targetProp.m_MoveIn.MoveFrom = ePosMove.ParentPosition;
							}
							else
							{
								targetProp.m_MoveOut.MoveTo = ePosMove.ParentPosition;
							}
							break;
						case eMoveType.CurrentPosition:
							if(InOrOut==0)
							{
								targetProp.m_MoveIn.MoveFrom = ePosMove.SelfPosition;
							}
							else
							{
								targetProp.m_MoveOut.MoveTo = ePosMove.SelfPosition;
							}
							break;
						case eMoveType.SpecificLocalPosition:
							if(InOrOut==0)
							{
								targetProp.m_MoveIn.MoveFrom = ePosMove.LocalPosition;
								
								GUILayout.Label("X", GUILayout.MaxWidth(12));
								float MoveFromX = EditorGUILayout.FloatField(targetProp.m_MoveIn.Position.x, GUILayout.MaxWidth(38));
								GUILayout.Label("Y", GUILayout.MaxWidth(12));
								float MoveFromY = EditorGUILayout.FloatField(targetProp.m_MoveIn.Position.y, GUILayout.MaxWidth(38));
								GUILayout.Label("Z", GUILayout.MaxWidth(12));
								float MoveFromZ = EditorGUILayout.FloatField(targetProp.m_MoveIn.Position.z, GUILayout.MaxWidth(38));
								
								targetProp.m_MoveIn.Position = new Vector3(MoveFromX, MoveFromY, MoveFromZ);
							}
							else
							{
								targetProp.m_MoveOut.MoveTo = ePosMove.LocalPosition;
								
								GUILayout.Label("X", GUILayout.MaxWidth(12));
								float MoveToX = EditorGUILayout.FloatField(targetProp.m_MoveOut.Position.x, GUILayout.MaxWidth(38));
								GUILayout.Label("Y", GUILayout.MaxWidth(12));
								float MoveToY = EditorGUILayout.FloatField(targetProp.m_MoveOut.Position.y, GUILayout.MaxWidth(38));
								GUILayout.Label("Z", GUILayout.MaxWidth(12));
								float MoveToZ = EditorGUILayout.FloatField(targetProp.m_MoveOut.Position.z, GUILayout.MaxWidth(38));
								
								targetProp.m_MoveOut.Position = new Vector3(MoveToX, MoveToY, MoveToZ);
							}
							break;
						case eMoveType.ScreenEdgeOrCorner:
							SetBackgroundColor(new Color( 1.0f, 1.0f, 1.0f, 1.0f ));
							GUILayout.Box("", GUILayout.MinWidth(50), GUILayout.MinHeight(50));
							ReleaseBackgroundColor();
							m_rectMoveArea = GUILayoutUtility.GetLastRect();
							if(InOrOut==0)
							{
								if(targetProp.m_MoveIn.MoveFrom==ePosMove.ParentPosition || 
								   targetProp.m_MoveIn.MoveFrom==ePosMove.SelfPosition || 
								   targetProp.m_MoveIn.MoveFrom==ePosMove.LocalPosition)
								{
									targetProp.m_MoveIn.MoveFrom = ePosMove.MiddleCenter;
								}
								targetProp.m_MoveIn.MoveFrom = DrawScreenAnchorsAndEdges(m_rectMoveArea, targetProp.m_MoveIn.MoveFrom);
								EditorGUILayout.HelpBox("Click on an anchor or a side-bar in left image to select begin position animation.", MessageType.None, true);
							}
							else
							{
								if(targetProp.m_MoveOut.MoveTo==ePosMove.ParentPosition || 
								   targetProp.m_MoveOut.MoveTo==ePosMove.SelfPosition || 
								   targetProp.m_MoveOut.MoveTo==ePosMove.LocalPosition)
								{
									targetProp.m_MoveOut.MoveTo = ePosMove.MiddleCenter;
								}
								targetProp.m_MoveOut.MoveTo = DrawScreenAnchorsAndEdges(m_rectMoveArea, targetProp.m_MoveOut.MoveTo);
								EditorGUILayout.HelpBox("Click on an anchor or a side-bar in left image to select end position of animation.", MessageType.None, true);
							}
							break;
						}
					}
					EditorGUILayout.EndHorizontal();
					
					//if(GEAnimEditorUtils.m_ShowHelpBox)
					{
						string AnchorOrSideString = "";
						switch(currentMovePos)
						{
						case eMoveType.ParentPosition:
							if(InOrOut==0)
							{
								EditorGUILayout.HelpBox("When \"In Animation\" is played, this object will move;\r\n- From parent position.\r\n- To current position.", MessageType.None);
							}
							else
							{
								EditorGUILayout.HelpBox("When \"Out Animation\" is played, this object will move;\r\n- Form current position.\r\n- To parent position.", MessageType.None);
							}
							break;
						case eMoveType.CurrentPosition:
							if(InOrOut==0)
							{
								EditorGUILayout.HelpBox("When \"In Animation\" is played, this object will move;\r\n- From current position .\r\n- To current position.", MessageType.None);
							}
							else
							{
								EditorGUILayout.HelpBox("When \"Out Animation\" is played, this object will move;\r\n- Form current position.\r\n- To current positon.", MessageType.None);
							}
							break;
						case eMoveType.SpecificLocalPosition:
							if(InOrOut==0)
							{
								EditorGUILayout.HelpBox("When \"In Animation\" is played, this object will move;\r\n- From local "+targetProp.m_MoveIn.Position+".\r\n- To current position.", MessageType.None);
							}
							else
							{
								EditorGUILayout.HelpBox("When \"Out Animation\" is played, this object will move;\r\n- Form current position.\r\n- To local "+targetProp.m_MoveOut.Position+".", MessageType.None);
							}
							break;
						case eMoveType.ScreenEdgeOrCorner:
							ePosMove TempPosMove = targetProp.m_MoveIn.MoveFrom;
							if(InOrOut==0 && targetProp.m_MoveIn.Enable)
							{
								TempPosMove = targetProp.m_MoveIn.MoveFrom;
							}
							else if(InOrOut==1 && targetProp.m_MoveOut.Enable)
							{
								TempPosMove = targetProp.m_MoveOut.MoveTo;
							}
							switch(TempPosMove)
							{
								
							case ePosMove.UpperLeft:
								AnchorOrSideString = "Upper Left corner of screen";
								break;
							case ePosMove.UpperCenter:
								AnchorOrSideString = "Upper Center of screen";
								break;
							case ePosMove.UpperRight:
								AnchorOrSideString = "Upper Right corner of screen";
								break;
							case ePosMove.MiddleLeft:
								AnchorOrSideString = "Middle Left of screen";
								break;
							case ePosMove.MiddleCenter:
								AnchorOrSideString = "Middle Center of screen";
								break;
							case ePosMove.MiddleRight:
								AnchorOrSideString = "Middle Right of screen";
								break;
							case ePosMove.BottomLeft:
								AnchorOrSideString = "Bottom Left corner of screen";
								break;
							case ePosMove.BottomCenter:
								AnchorOrSideString = "Bottom Center of screen";
								break;
							case ePosMove.BottomRight:
								AnchorOrSideString = "Bottom Right corner of screen";
								break;
								
							case ePosMove.UpperScreenEdge:
								AnchorOrSideString = "Upper Edge of screen";
								break;
							case ePosMove.LeftScreenEdge:
								AnchorOrSideString = "Left Edge of screen";
								break;
							case ePosMove.RightScreenEdge:
								AnchorOrSideString = "Right Edge of screen";
								break;
							case ePosMove.BottomScreenEdge:
								AnchorOrSideString = "Bottom Edge of screen";
								break;
								
							default:
								AnchorOrSideString = "Unknow";
								break;
							}
							if(InOrOut==0)
							{
								EditorGUILayout.HelpBox("When \"In Animation\" is played, this object will move;\r\n- From "+AnchorOrSideString+".\r\n- To current position.", MessageType.None);
							}
							else
							{
								EditorGUILayout.HelpBox("When \"Out Animation\" is played, this object will move;\r\n- Form current position.\r\n- To "+AnchorOrSideString+" of screen.", MessageType.None);
							}
							break;
						}
						
					}
				}
				EditorGUILayout.EndVertical();
				
				GUILayout.Space(5);
			}
			// END HOR
			EditorGUILayout.EndHorizontal();
			
			EditorGUILayout.BeginHorizontal();
			{
				GUILayout.Space(10);

				GUILayout.Label("Ease:", GUILayout.Width(40));

				EditorGUILayout.BeginVertical();
				{
					if(InOrOut==0)
					{
						targetProp.m_MoveIn.EaseType = (eEaseType) EditorGUILayout.EnumPopup("" , targetProp.m_MoveIn.EaseType);//, GUILayout.MaxWidth(75));
						// Show graph
						//if(GEAnimEditorUtils.m_ShowEasingGraph)
						if(GEAnimEditorUtils.GetBool("m_ShowEasingGraph")==true)
						{
							DrawGraph(targetProp.m_MoveIn.EaseType);
						}
					}
					else
					{
						targetProp.m_MoveOut.EaseType = (eEaseType) EditorGUILayout.EnumPopup("" , targetProp.m_MoveOut.EaseType);//, GUILayout.MaxWidth(75));
						// Show graph
						//if(GEAnimEditorUtils.m_ShowEasingGraph)
						if(GEAnimEditorUtils.GetBool("m_ShowEasingGraph")==true)
						{
							DrawGraph(targetProp.m_MoveOut.EaseType);
						}
					}
					//if(GEAnimEditorUtils.m_ShowHelpBox)
					if(GEAnimEditorUtils.GetBool("m_ShowHelpBox")==true)
					{
						EditorGUILayout.HelpBox("Easing is the rate of change of animation over time.", MessageType.None);
					}
				}
				EditorGUILayout.EndVertical();

				GUILayout.Space(5);
			}
			EditorGUILayout.EndHorizontal();
			
			// Time and delay
			EditorGUILayout.BeginHorizontal();
			{
				GUILayout.Space(10);
				
				GUILayout.Label("Time:", GUILayout.Width(40));
				EditorGUILayout.BeginVertical();
				{
					EditorGUILayout.BeginHorizontal();
					{
						if(InOrOut==0)
						{
							targetProp.m_MoveIn.Time = EditorGUILayout.FloatField("" , targetProp.m_MoveIn.Time, GUILayout.MinWidth(25));
						}
						else
						{
							targetProp.m_MoveOut.Time = EditorGUILayout.FloatField("" , targetProp.m_MoveOut.Time, GUILayout.MinWidth(25));
						}
						GUILayout.Label("Delay:", GUILayout.Width(44));
						if(InOrOut==0)
						{
							targetProp.m_MoveIn.Delay = EditorGUILayout.FloatField("" , targetProp.m_MoveIn.Delay, GUILayout.MinWidth(25));
						}
						else
						{
							targetProp.m_MoveOut.Delay = EditorGUILayout.FloatField("" , targetProp.m_MoveOut.Delay, GUILayout.MinWidth(25));
						}
					}
					EditorGUILayout.EndHorizontal();
					//if(GEAnimEditorUtils.m_ShowHelpBox)
					if(GEAnimEditorUtils.GetBool("m_ShowHelpBox")==true)
					{
						EditorGUILayout.HelpBox("Time is amount (seconds) that the animation will take to complete.", MessageType.None);
						EditorGUILayout.HelpBox("Delay is amount (seconds) that the animation will wait before beginning.", MessageType.None);
					}
				}
				EditorGUILayout.EndVertical();


				GUILayout.Space(5);
			}
			EditorGUILayout.EndHorizontal();
			
			GUILayout.Space(10);	
			// Begin Sound
			EditorGUILayout.BeginHorizontal();
			{
				GUILayout.Space(10);
				
				EditorGUILayout.BeginVertical();
				{
					EditorGUILayout.BeginHorizontal();
					{
						GUILayout.Label("Begin Sound:", GUILayout.Width(80));
						if(InOrOut==0)
						{
							targetProp.m_MoveIn.Sounds.m_Begin = (AudioClip) EditorGUILayout.ObjectField( targetProp.m_MoveIn.Sounds.m_Begin, typeof(AudioClip), false);
						}
						else
						{
							targetProp.m_MoveOut.Sounds.m_Begin = (AudioClip) EditorGUILayout.ObjectField( targetProp.m_MoveOut.Sounds.m_Begin, typeof(AudioClip), false);
						}
					}
					EditorGUILayout.EndHorizontal();
					//if(GEAnimEditorUtils.m_ShowHelpBox)
					if(GEAnimEditorUtils.GetBool("m_ShowHelpBox")==true)
					{
						EditorGUILayout.BeginHorizontal();
						{
							GUILayout.Label("", GUILayout.Width(40));
							EditorGUILayout.HelpBox("AudioClip to play when animation begins.", MessageType.None);
						}
						EditorGUILayout.EndHorizontal();						
					}
				}
				GUILayout.Space(3);
				EditorGUILayout.EndVertical();

				GUILayout.Space(5);
			}
			EditorGUILayout.EndHorizontal();

			// End Sound
			EditorGUILayout.BeginHorizontal();
			{
				GUILayout.Space(10);		

				EditorGUILayout.BeginVertical();
				{
					EditorGUILayout.BeginHorizontal();
					{						
						GUILayout.Label("End Sound:", GUILayout.Width(80));
						if(InOrOut==0)
						{
							targetProp.m_MoveIn.Sounds.m_End = (AudioClip) EditorGUILayout.ObjectField( targetProp.m_MoveIn.Sounds.m_End, typeof(AudioClip), false);
						}
						else
						{
							targetProp.m_MoveOut.Sounds.m_End = (AudioClip) EditorGUILayout.ObjectField( targetProp.m_MoveOut.Sounds.m_End, typeof(AudioClip), false);
						}
					}
					EditorGUILayout.EndHorizontal();
					//if(GEAnimEditorUtils.m_ShowHelpBox)
					if(GEAnimEditorUtils.GetBool("m_ShowHelpBox")==true)
					{
						EditorGUILayout.BeginHorizontal();
						{
							GUILayout.Label("", GUILayout.Width(40));
							EditorGUILayout.HelpBox("AudioClip to play when animation has been completed.", MessageType.None);
						}
						EditorGUILayout.EndHorizontal();						
					}
				}
				GUILayout.Space(3);
				EditorGUILayout.EndVertical();

				GUILayout.Space(5);
			}
			EditorGUILayout.EndHorizontal();

			GUILayout.Space(5);
		}
		EditorGUILayout.EndVertical();

	}
	
	#endregion

	#region Draw Graph

	// Draw Easing Graph
	private void DrawGraph(eEaseType EaseType)
	{
		// Make sure we have texture2d of all graph
		if(GEAnimEditorUtils.m_Quad_In == null || 
		   GEAnimEditorUtils.m_Quad_Out == null || 
		   GEAnimEditorUtils.m_Quad_InOut == null || 
		   
		   GEAnimEditorUtils.m_Cubic_In == null || 
		   GEAnimEditorUtils.m_Cubic_Out == null || 
		   GEAnimEditorUtils.m_Cubic_InOut == null || 
		   
		   GEAnimEditorUtils.m_Quart_In == null || 
		   GEAnimEditorUtils.m_Quart_Out == null || 
		   GEAnimEditorUtils.m_Quart_InOut == null || 
		   
		   GEAnimEditorUtils.m_Quint_In == null || 
		   GEAnimEditorUtils.m_Quint_Out == null || 
		   GEAnimEditorUtils.m_Quint_InOut == null || 
		   
		   GEAnimEditorUtils.m_Sine_In == null || 
		   GEAnimEditorUtils.m_Sine_Out == null || 
		   GEAnimEditorUtils.m_Sine_InOut == null || 
		   
		   GEAnimEditorUtils.m_Expo_In == null || 
		   GEAnimEditorUtils.m_Expo_Out == null || 
		   GEAnimEditorUtils.m_Expo_InOut == null || 
		   
		   GEAnimEditorUtils.m_Circ_In == null || 
		   GEAnimEditorUtils.m_Circ_Out == null || 
		   GEAnimEditorUtils.m_Circ_InOut == null || 
		   
		   GEAnimEditorUtils.m_Linear == null || 
		   GEAnimEditorUtils.m_Spring == null || 
		   
		   GEAnimEditorUtils.m_Bounce_In == null || 
		   GEAnimEditorUtils.m_Bounce_Out == null || 
		   GEAnimEditorUtils.m_Bounce_InOut == null || 
		   
		   GEAnimEditorUtils.m_Back_In == null || 
		   GEAnimEditorUtils.m_Back_Out == null || 
		   GEAnimEditorUtils.m_Back_InOut == null || 
		   
		   GEAnimEditorUtils.m_Elastic_In == null || 
		   GEAnimEditorUtils.m_Elastic_Out == null || 
		   GEAnimEditorUtils.m_Elastic_InOut == null)
		{
			GEAnimEditorUtils.CalculateEaseGraph();
		}

		switch(EaseType)
		{
		case eEaseType.InQuad:
			if(GEAnimEditorUtils.m_Content_Quad_In!=null)
				GUILayout.Box(GEAnimEditorUtils.m_Content_Quad_In);
			break;
		case eEaseType.OutQuad:
			if(GEAnimEditorUtils.m_Content_Quad_Out!=null)
				GUILayout.Box(GEAnimEditorUtils.m_Content_Quad_Out);
			break;
		case eEaseType.InOutQuad:
			if(GEAnimEditorUtils.m_Content_Quad_InOut!=null)
				GUILayout.Box(GEAnimEditorUtils.m_Content_Quad_InOut);
			break;
		case eEaseType.InCubic:
			if(GEAnimEditorUtils.m_Content_Cubic_In!=null)
				GUILayout.Box(GEAnimEditorUtils.m_Content_Cubic_In);
			break;
		case eEaseType.OutCubic:
			if(GEAnimEditorUtils.m_Content_Cubic_Out!=null)
				GUILayout.Box(GEAnimEditorUtils.m_Content_Cubic_Out);
			break;
		case eEaseType.InOutCubic:
			if(GEAnimEditorUtils.m_Content_Cubic_InOut!=null)
				GUILayout.Box(GEAnimEditorUtils.m_Content_Cubic_InOut);
			break;
		case eEaseType.InQuart:
			if(GEAnimEditorUtils.m_Content_Quart_In!=null)
				GUILayout.Box(GEAnimEditorUtils.m_Content_Quart_In);
			break;
		case eEaseType.OutQuart:
			if(GEAnimEditorUtils.m_Content_Quart_Out!=null)
				GUILayout.Box(GEAnimEditorUtils.m_Content_Quart_Out);
			break;
		case eEaseType.InOutQuart:
			if(GEAnimEditorUtils.m_Content_Quart_InOut!=null)
				GUILayout.Box(GEAnimEditorUtils.m_Content_Quart_InOut);
			break;
		case eEaseType.InQuint:
			if(GEAnimEditorUtils.m_Content_Quint_In!=null)
				GUILayout.Box(GEAnimEditorUtils.m_Content_Quint_In);
			break;
		case eEaseType.OutQuint:
			if(GEAnimEditorUtils.m_Content_Quint_Out!=null)
				GUILayout.Box(GEAnimEditorUtils.m_Content_Quint_Out);
			break;
		case eEaseType.InOutQuint:
			if(GEAnimEditorUtils.m_Content_Quint_InOut!=null)
				GUILayout.Box(GEAnimEditorUtils.m_Content_Quint_InOut);
			break;
		case eEaseType.InSine:
			if(GEAnimEditorUtils.m_Content_Sine_In!=null)
				GUILayout.Box(GEAnimEditorUtils.m_Content_Sine_In);
			break;
		case eEaseType.OutSine:
			if(GEAnimEditorUtils.m_Content_Sine_Out!=null)
				GUILayout.Box(GEAnimEditorUtils.m_Content_Sine_Out);
			break;
		case eEaseType.InOutSine:
			if(GEAnimEditorUtils.m_Content_Sine_InOut!=null)
				GUILayout.Box(GEAnimEditorUtils.m_Content_Sine_InOut);
			break;
		case eEaseType.InExpo:
			if(GEAnimEditorUtils.m_Content_Expo_In!=null)
				GUILayout.Box(GEAnimEditorUtils.m_Content_Expo_In);
			break;
		case eEaseType.OutExpo:
			if(GEAnimEditorUtils.m_Content_Expo_Out!=null)
				GUILayout.Box(GEAnimEditorUtils.m_Content_Expo_Out);
			break;
		case eEaseType.InOutExpo:
			if(GEAnimEditorUtils.m_Content_Expo_InOut!=null)
				GUILayout.Box(GEAnimEditorUtils.m_Content_Expo_InOut);
			break;
		case eEaseType.InCirc:
			if(GEAnimEditorUtils.m_Content_Circ_In!=null)
				GUILayout.Box(GEAnimEditorUtils.m_Content_Circ_In);
			break;
		case eEaseType.OutCirc:
			if(GEAnimEditorUtils.m_Content_Circ_Out!=null)
				GUILayout.Box(GEAnimEditorUtils.m_Content_Circ_Out);
			break;
		case eEaseType.InOutCirc:
			if(GEAnimEditorUtils.m_Content_Circ_InOut!=null)
				GUILayout.Box(GEAnimEditorUtils.m_Content_Circ_InOut);
			break;
		case eEaseType.linear:
			if(GEAnimEditorUtils.m_Content_Linear!=null)
				GUILayout.Box(GEAnimEditorUtils.m_Content_Linear);
			break;
		case eEaseType.spring:
			if(GEAnimEditorUtils.m_Content_Spring!=null)
				GUILayout.Box(GEAnimEditorUtils.m_Content_Spring);
			break;
		case eEaseType.InBounce:
			if(GEAnimEditorUtils.m_Content_Bounce_In!=null)
				GUILayout.Box(GEAnimEditorUtils.m_Content_Bounce_In);
			break;
		case eEaseType.OutBounce:
			if(GEAnimEditorUtils.m_Content_Bounce_Out!=null)
				GUILayout.Box(GEAnimEditorUtils.m_Content_Bounce_Out);
			break;
		case eEaseType.InOutBounce:
			if(GEAnimEditorUtils.m_Content_Bounce_InOut!=null)
				GUILayout.Box(GEAnimEditorUtils.m_Content_Bounce_InOut);
			break;
		case eEaseType.InBack:
			if(GEAnimEditorUtils.m_Content_Back_In!=null)
				GUILayout.Box(GEAnimEditorUtils.m_Content_Back_In);
			break;
		case eEaseType.OutBack:
			if(GEAnimEditorUtils.m_Content_Back_Out!=null)
				GUILayout.Box(GEAnimEditorUtils.m_Content_Back_Out);
			break;
		case eEaseType.InOutBack:
			if(GEAnimEditorUtils.m_Content_Back_InOut!=null)
				GUILayout.Box(GEAnimEditorUtils.m_Content_Back_InOut);
			break;
		case eEaseType.InElastic:
			if(GEAnimEditorUtils.m_Content_Elastic_In!=null)
				GUILayout.Box(GEAnimEditorUtils.m_Content_Elastic_In);
			break;
		case eEaseType.OutElastic:
			if(GEAnimEditorUtils.m_Content_Elastic_Out!=null)
				GUILayout.Box(GEAnimEditorUtils.m_Content_Elastic_Out);
			break;
		case eEaseType.InOutElastic:
			if(GEAnimEditorUtils.m_Content_Elastic_InOut!=null)
				GUILayout.Box(GEAnimEditorUtils.m_Content_Elastic_InOut);
			break;
		}
	}
	
	#endregion

	#region Draw Rotate In/Out

	private Rect m_rectArea_Rotate;

	// Draw Rotate In/Out area
	private void DrawRotateInOut(int InOrOut)
	{
		m_rectArea_Rotate = EditorGUILayout.BeginVertical();
		{
			SetBackgroundColor(DefaultBoxContent_Color);
			GUI.Box(m_rectArea_Rotate, GUIContent.none);
			ReleaseBackgroundColor();
			GUILayout.Space(3);
			
			//////////////////////////////////
			/// DRAW ENABLE
			//////////////////////////////////
			
			EditorGUILayout.BeginHorizontal();
			{
				GUILayout.Space(5);
				
				if(InOrOut==0)
				{
					targetProp.m_RotationIn.Enable = EditorGUILayout.Toggle(targetProp.m_RotationIn.Enable, GUILayout.MaxWidth(12));
					EditorGUILayout.LabelField("Rotate In", GUILayout.MaxWidth(60));
				}
				else
				{
					targetProp.m_RotationOut.Enable = EditorGUILayout.Toggle(targetProp.m_RotationOut.Enable, GUILayout.MaxWidth(12));
					EditorGUILayout.LabelField("Rotate Out", GUILayout.MaxWidth(70));
				}
			}
			EditorGUILayout.EndHorizontal();
			
			//////////////////////////////////
			/// ROTATION
			//////////////////////////////////
			
			EditorGUILayout.BeginHorizontal();
			{
				GUILayout.Space(10);
				
				if(InOrOut==0)
				{
					GUILayout.Label("From:", GUILayout.Width(40));				
					EditorGUILayout.BeginVertical();
					{
						EditorGUILayout.BeginHorizontal();
						{
							GUILayout.Label("X", GUILayout.MaxWidth(12));
							float RotationX = EditorGUILayout.FloatField(targetProp.m_RotationIn.Rotation.x, GUILayout.MaxWidth(38));
							GUILayout.Label("Y", GUILayout.MaxWidth(12));
							float RotationY = EditorGUILayout.FloatField(targetProp.m_RotationIn.Rotation.y, GUILayout.MaxWidth(38));
							GUILayout.Label("Z", GUILayout.MaxWidth(12));
							float RotationZ = EditorGUILayout.FloatField(targetProp.m_RotationIn.Rotation.z, GUILayout.MaxWidth(38));
							
							targetProp.m_RotationIn.Rotation = new Vector3(RotationX, RotationY, RotationZ);
						}
						EditorGUILayout.EndHorizontal();
						//if(GEAnimEditorUtils.m_ShowHelpBox)
						if(GEAnimEditorUtils.GetBool("m_ShowHelpBox")==true)
						{
							EditorGUILayout.BeginHorizontal();
							{
								EditorGUILayout.HelpBox("Begin rotation when In animation starts.", MessageType.None);
							}
							EditorGUILayout.EndHorizontal();
						}
					}
					EditorGUILayout.EndVertical();
				}
				else
				{
					GUILayout.Label("To:", GUILayout.Width(40));				
					EditorGUILayout.BeginVertical();
					{
						EditorGUILayout.BeginHorizontal();
						{
							GUILayout.Label("X", GUILayout.MaxWidth(12));
							float RotationX = EditorGUILayout.FloatField(targetProp.m_RotationOut.Rotation.x, GUILayout.MaxWidth(38));
							GUILayout.Label("Y", GUILayout.MaxWidth(12));
							float RotationY = EditorGUILayout.FloatField(targetProp.m_RotationOut.Rotation.y, GUILayout.MaxWidth(38));
							GUILayout.Label("Z", GUILayout.MaxWidth(12));
							float RotationZ = EditorGUILayout.FloatField(targetProp.m_RotationOut.Rotation.z, GUILayout.MaxWidth(38));
							
							targetProp.m_RotationOut.Rotation = new Vector3(RotationX, RotationY, RotationZ);
						}
						EditorGUILayout.EndHorizontal();
						//if(GEAnimEditorUtils.m_ShowHelpBox)
						if(GEAnimEditorUtils.GetBool("m_ShowHelpBox")==true)
						{
							EditorGUILayout.BeginHorizontal();
							{
								EditorGUILayout.HelpBox("End rotation when Out animation is finished.", MessageType.None);
							}
							EditorGUILayout.EndHorizontal();
						}
					}
					EditorGUILayout.EndVertical();
				}
				
				GUILayout.Space(5);
			}
			EditorGUILayout.EndHorizontal();
			
			//////////////////////////////////
			/// EASE
			//////////////////////////////////
			
			EditorGUILayout.BeginHorizontal();
			{
				GUILayout.Space(10);
				
				GUILayout.Label("Ease:", GUILayout.Width(40));
				
				EditorGUILayout.BeginVertical();
				{
					if(InOrOut==0)
					{
						targetProp.m_RotationIn.EaseType = (eEaseType) EditorGUILayout.EnumPopup("" , targetProp.m_RotationIn.EaseType);//, GUILayout.MaxWidth(75));
						// Show graph
						//if(GEAnimEditorUtils.m_ShowEasingGraph)
						if(GEAnimEditorUtils.GetBool("m_ShowEasingGraph")==true)
						{
							DrawGraph(targetProp.m_RotationIn.EaseType);
						}
					}
					else
					{
						targetProp.m_RotationOut.EaseType = (eEaseType) EditorGUILayout.EnumPopup("" , targetProp.m_RotationOut.EaseType);//, GUILayout.MaxWidth(75));
						// Show graph
						//if(GEAnimEditorUtils.m_ShowEasingGraph)
						if(GEAnimEditorUtils.GetBool("m_ShowEasingGraph")==true)
						{
							DrawGraph(targetProp.m_RotationOut.EaseType);
						}
					}
					//if(GEAnimEditorUtils.m_ShowHelpBox)
					if(GEAnimEditorUtils.GetBool("m_ShowHelpBox")==true)
					{
						EditorGUILayout.HelpBox("Easing is the rate of change of animation over time.", MessageType.None);
					}
				}
				EditorGUILayout.EndVertical();
				
				GUILayout.Space(5);
			}
			EditorGUILayout.EndHorizontal();
			
			// Time and delay
			EditorGUILayout.BeginHorizontal();
			{
				GUILayout.Space(10);
				
				GUILayout.Label("Time:", GUILayout.Width(40));
				EditorGUILayout.BeginVertical();
				{
					EditorGUILayout.BeginHorizontal();
					{
						if(InOrOut==0)
						{
							targetProp.m_RotationIn.Time = EditorGUILayout.FloatField("" , targetProp.m_RotationIn.Time, GUILayout.MinWidth(25));
						}
						else
						{
							targetProp.m_RotationOut.Time = EditorGUILayout.FloatField("" , targetProp.m_RotationOut.Time, GUILayout.MinWidth(25));
						}
						GUILayout.Label("Delay:", GUILayout.Width(44));
						if(InOrOut==0)
						{
							targetProp.m_RotationOut.Delay = EditorGUILayout.FloatField("" , targetProp.m_RotationOut.Delay, GUILayout.MinWidth(25));
						}
						else
						{
							targetProp.m_RotationOut.Delay = EditorGUILayout.FloatField("" , targetProp.m_RotationOut.Delay, GUILayout.MinWidth(25));
						}
					}
					EditorGUILayout.EndHorizontal();
					//if(GEAnimEditorUtils.m_ShowHelpBox)
					if(GEAnimEditorUtils.GetBool("m_ShowHelpBox")==true)
					{
						EditorGUILayout.HelpBox("Time is amount (seconds) that the animation will take to complete.", MessageType.None);
						EditorGUILayout.HelpBox("Delay is amount (seconds) that the animation will wait before beginning.", MessageType.None);
					}
				}
				EditorGUILayout.EndVertical();
				
				
				GUILayout.Space(5);
			}
			EditorGUILayout.EndHorizontal();
			
			GUILayout.Space(10);	
			// Begin Sound
			EditorGUILayout.BeginHorizontal();
			{
				GUILayout.Space(10);	
				
				EditorGUILayout.BeginVertical();
				{
					EditorGUILayout.BeginHorizontal();
					{						
						GUILayout.Label("Begin Sound:", GUILayout.Width(80));
						if(InOrOut==0)
						{
							targetProp.m_RotationIn.Sounds.m_Begin = (AudioClip) EditorGUILayout.ObjectField( targetProp.m_RotationIn.Sounds.m_Begin, typeof(AudioClip), false);
						}
						else
						{
							targetProp.m_RotationOut.Sounds.m_Begin = (AudioClip) EditorGUILayout.ObjectField( targetProp.m_RotationOut.Sounds.m_Begin, typeof(AudioClip), false);
						}
					}
					EditorGUILayout.EndHorizontal();
					//if(GEAnimEditorUtils.m_ShowHelpBox)
					if(GEAnimEditorUtils.GetBool("m_ShowHelpBox")==true)
					{
						EditorGUILayout.BeginHorizontal();
						{
							GUILayout.Label("", GUILayout.Width(40));
							EditorGUILayout.HelpBox("AudioClip to play when animation begins.", MessageType.None);
						}
						EditorGUILayout.EndHorizontal();
					}
				}
				GUILayout.Space(3);
				EditorGUILayout.EndVertical();
				
				GUILayout.Space(5);
			}
			EditorGUILayout.EndHorizontal();
			
			// End Sound
			EditorGUILayout.BeginHorizontal();
			{
				GUILayout.Space(10);	
				
				EditorGUILayout.BeginVertical();
				{
					EditorGUILayout.BeginHorizontal();
					{						
						GUILayout.Label("End Sound:", GUILayout.Width(80));
						if(InOrOut==0)
						{
							targetProp.m_RotationIn.Sounds.m_End = (AudioClip) EditorGUILayout.ObjectField( targetProp.m_RotationIn.Sounds.m_End, typeof(AudioClip), false);
						}
						else
						{
							targetProp.m_RotationOut.Sounds.m_End = (AudioClip) EditorGUILayout.ObjectField( targetProp.m_RotationOut.Sounds.m_End, typeof(AudioClip), false);
						}
					}
					EditorGUILayout.EndHorizontal();
					//if(GEAnimEditorUtils.m_ShowHelpBox)
					if(GEAnimEditorUtils.GetBool("m_ShowHelpBox")==true)
					{
						EditorGUILayout.BeginHorizontal();
						{
							GUILayout.Label("", GUILayout.Width(40));
							EditorGUILayout.HelpBox("AudioClip to play when animation has been completed.", MessageType.None);
						}
						EditorGUILayout.EndHorizontal();
					}
				}
				GUILayout.Space(3);
				EditorGUILayout.EndVertical();
				
				GUILayout.Space(5);
			}
			EditorGUILayout.EndHorizontal();
			
			GUILayout.Space(5);
		}
		EditorGUILayout.EndVertical();
		
	}
	
	#endregion

	#region Draw Scale In/Out
	
	private Rect m_rectArea_Scale;

	// Draw Scale In/Out area
	private void DrawScaleInOut(int InOrOut)
	{
		m_rectArea_Scale = EditorGUILayout.BeginVertical();
		{
			SetBackgroundColor(DefaultBoxContent_Color);
			GUI.Box(m_rectArea_Scale, GUIContent.none);
			ReleaseBackgroundColor();
			GUILayout.Space(3);
			
			//////////////////////////////////
			/// DRAW ENABLE
			//////////////////////////////////
			
			EditorGUILayout.BeginHorizontal();
			{
				GUILayout.Space(5);
				
				if(InOrOut==0)
				{
					targetProp.m_ScaleIn.Enable = EditorGUILayout.Toggle(targetProp.m_ScaleIn.Enable, GUILayout.MaxWidth(12));
					EditorGUILayout.LabelField("Scale In", GUILayout.MaxWidth(60));
				}
				else
				{
					targetProp.m_ScaleOut.Enable = EditorGUILayout.Toggle(targetProp.m_ScaleOut.Enable, GUILayout.MaxWidth(12));
					EditorGUILayout.LabelField("Scale Out", GUILayout.MaxWidth(70));
				}
			}
			EditorGUILayout.EndHorizontal();
			
			
			
			//////////////////////////////////
			/// SCALE
			//////////////////////////////////
			
			EditorGUILayout.BeginHorizontal();
			{
				GUILayout.Space(10);
				
				if(InOrOut==0)
				{
					GUILayout.Label("From:", GUILayout.Width(40));
					EditorGUILayout.BeginVertical();
					{
						EditorGUILayout.BeginHorizontal();
						{
							GUILayout.Label("X", GUILayout.MaxWidth(12));
							float ScaleInX = EditorGUILayout.FloatField(targetProp.m_ScaleIn.ScaleBegin.x, GUILayout.MaxWidth(38));
							GUILayout.Label("Y", GUILayout.MaxWidth(12));
							float ScaleInY = EditorGUILayout.FloatField(targetProp.m_ScaleIn.ScaleBegin.y, GUILayout.MaxWidth(38));
							GUILayout.Label("Z", GUILayout.MaxWidth(12));
							float ScaleInZ = EditorGUILayout.FloatField(targetProp.m_ScaleIn.ScaleBegin.z, GUILayout.MaxWidth(38));
							
							targetProp.m_ScaleIn.ScaleBegin = new Vector3(ScaleInX, ScaleInY, ScaleInZ);
						}
						EditorGUILayout.EndHorizontal();
						//if(GEAnimEditorUtils.m_ShowHelpBox)
						if(GEAnimEditorUtils.GetBool("m_ShowHelpBox")==true)
						{
							EditorGUILayout.BeginHorizontal();
							{
								EditorGUILayout.HelpBox("Begin scale when In animation starts.", MessageType.None);
							}
							EditorGUILayout.EndHorizontal();
						}
					}
					EditorGUILayout.EndVertical();
				}
				else
				{
					GUILayout.Label("To:", GUILayout.Width(40));				
					EditorGUILayout.BeginVertical();
					{
						EditorGUILayout.BeginHorizontal();
						{
							GUILayout.Label("X", GUILayout.MaxWidth(12));
							float ScaleOutX = EditorGUILayout.FloatField(targetProp.m_ScaleOut.ScaleEnd.x, GUILayout.MaxWidth(38));
							GUILayout.Label("Y", GUILayout.MaxWidth(12));
							float ScaleOutY = EditorGUILayout.FloatField(targetProp.m_ScaleOut.ScaleEnd.y, GUILayout.MaxWidth(38));
							GUILayout.Label("Z", GUILayout.MaxWidth(12));
							float ScaleOutZ = EditorGUILayout.FloatField(targetProp.m_ScaleOut.ScaleEnd.z, GUILayout.MaxWidth(38));
							
							targetProp.m_ScaleOut.ScaleEnd = new Vector3(ScaleOutX, ScaleOutY, ScaleOutZ);
						}
						EditorGUILayout.EndHorizontal();
						//if(GEAnimEditorUtils.m_ShowHelpBox)
						if(GEAnimEditorUtils.GetBool("m_ShowHelpBox")==true)
						{
							EditorGUILayout.BeginHorizontal();
							{
								EditorGUILayout.HelpBox("End scale when Out animation is finished.", MessageType.None);
							}
							EditorGUILayout.EndHorizontal();
						}
					}
					EditorGUILayout.EndVertical();
				}
				
				GUILayout.Space(5);
			}
			EditorGUILayout.EndHorizontal();
			
			//////////////////////////////////
			/// EASE
			//////////////////////////////////
			
			EditorGUILayout.BeginHorizontal();
			{
				GUILayout.Space(10);
				
				GUILayout.Label("Ease:", GUILayout.Width(40));
				
				EditorGUILayout.BeginVertical();
				{
					if(InOrOut==0)
					{
						targetProp.m_ScaleIn.EaseType = (eEaseType) EditorGUILayout.EnumPopup("" , targetProp.m_ScaleIn.EaseType);//, GUILayout.MaxWidth(75));
						// Show graph
						//if(GEAnimEditorUtils.m_ShowEasingGraph)
						if(GEAnimEditorUtils.GetBool("m_ShowEasingGraph")==true)
						{
							DrawGraph(targetProp.m_ScaleIn.EaseType);
						}
					}
					else
					{
						targetProp.m_ScaleOut.EaseType = (eEaseType) EditorGUILayout.EnumPopup("" , targetProp.m_ScaleOut.EaseType);//, GUILayout.MaxWidth(75));
						// Show graph
						//if(GEAnimEditorUtils.m_ShowEasingGraph)
						if(GEAnimEditorUtils.GetBool("m_ShowEasingGraph")==true)
						{
							DrawGraph(targetProp.m_ScaleOut.EaseType);
						}
					}
					//if(GEAnimEditorUtils.m_ShowHelpBox)
					if(GEAnimEditorUtils.GetBool("m_ShowHelpBox")==true)
					{
						EditorGUILayout.HelpBox("Easing is the rate of change of animation over time.", MessageType.None);
					}
				}
				EditorGUILayout.EndVertical();
				
				GUILayout.Space(5);
			}
			EditorGUILayout.EndHorizontal();
			
			// Time and delay
			EditorGUILayout.BeginHorizontal();
			{
				GUILayout.Space(10);
				
				GUILayout.Label("Time:", GUILayout.Width(40));
				EditorGUILayout.BeginVertical();
				{
					EditorGUILayout.BeginHorizontal();
					{
						if(InOrOut==0)
						{
							targetProp.m_ScaleIn.Time = EditorGUILayout.FloatField("" , targetProp.m_ScaleIn.Time, GUILayout.MinWidth(25));
						}
						else
						{
							targetProp.m_ScaleOut.Time = EditorGUILayout.FloatField("" , targetProp.m_ScaleOut.Time, GUILayout.MinWidth(25));
						}
						GUILayout.Label("Delay:", GUILayout.Width(44));
						if(InOrOut==0)
						{
							targetProp.m_ScaleIn.Delay = EditorGUILayout.FloatField("" , targetProp.m_ScaleIn.Delay, GUILayout.MinWidth(25));
						}
						else
						{
							targetProp.m_ScaleOut.Delay = EditorGUILayout.FloatField("" , targetProp.m_ScaleOut.Delay, GUILayout.MinWidth(25));
						}
					}
					EditorGUILayout.EndHorizontal();
					//if(GEAnimEditorUtils.m_ShowHelpBox)
					if(GEAnimEditorUtils.GetBool("m_ShowHelpBox")==true)
					{
						EditorGUILayout.HelpBox("Time is amount (seconds) that the animation will take to complete.", MessageType.None);
						EditorGUILayout.HelpBox("Delay is amount (seconds) that the animation will wait before beginning.", MessageType.None);
					}
				}
				EditorGUILayout.EndVertical();
				
				
				GUILayout.Space(5);
			}
			EditorGUILayout.EndHorizontal();
			
			GUILayout.Space(10);	
			// Begin Sound
			EditorGUILayout.BeginHorizontal();
			{
				GUILayout.Space(10);	
				
				EditorGUILayout.BeginVertical();
				{
					EditorGUILayout.BeginHorizontal();
					{						
						GUILayout.Label("Begin Sound:", GUILayout.Width(80));
						if(InOrOut==0)
						{
							targetProp.m_ScaleIn.Sounds.m_Begin = (AudioClip) EditorGUILayout.ObjectField( targetProp.m_ScaleIn.Sounds.m_Begin, typeof(AudioClip), false);
						}
						else
						{
							targetProp.m_ScaleOut.Sounds.m_Begin = (AudioClip) EditorGUILayout.ObjectField( targetProp.m_ScaleOut.Sounds.m_Begin, typeof(AudioClip), false);
						}
					}
					EditorGUILayout.EndHorizontal();
					//if(GEAnimEditorUtils.m_ShowHelpBox)
					if(GEAnimEditorUtils.GetBool("m_ShowHelpBox")==true)
					{
						EditorGUILayout.BeginHorizontal();
						{
							GUILayout.Label("", GUILayout.Width(40));
							EditorGUILayout.HelpBox("AudioClip to play when animation begins.", MessageType.None);
						}
						EditorGUILayout.EndHorizontal();
					}
				}
				GUILayout.Space(3);
				EditorGUILayout.EndVertical();
				
				GUILayout.Space(5);
			}
			EditorGUILayout.EndHorizontal();
			
			// End Sound
			EditorGUILayout.BeginHorizontal();
			{
				GUILayout.Space(10);	
				
				EditorGUILayout.BeginVertical();
				{
					EditorGUILayout.BeginHorizontal();
					{						
						GUILayout.Label("End Sound:", GUILayout.Width(80));
						if(InOrOut==0)
						{
							targetProp.m_ScaleIn.Sounds.m_End = (AudioClip) EditorGUILayout.ObjectField( targetProp.m_ScaleIn.Sounds.m_End, typeof(AudioClip), false);
						}
						else
						{
							targetProp.m_ScaleOut.Sounds.m_End = (AudioClip) EditorGUILayout.ObjectField( targetProp.m_ScaleOut.Sounds.m_End, typeof(AudioClip), false);
						}
					}
					EditorGUILayout.EndHorizontal();
					//if(GEAnimEditorUtils.m_ShowHelpBox)
					if(GEAnimEditorUtils.GetBool("m_ShowHelpBox")==true)
					{
						EditorGUILayout.BeginHorizontal();
						{
							GUILayout.Label("", GUILayout.Width(40));
							EditorGUILayout.HelpBox("AudioClip to play when animation has been completed.", MessageType.None);
						}
						EditorGUILayout.EndHorizontal();
					}
				}
				GUILayout.Space(3);
				EditorGUILayout.EndVertical();
				
				GUILayout.Space(5);
			}
			EditorGUILayout.EndHorizontal();
			
			GUILayout.Space(5);
		}
		EditorGUILayout.EndVertical();
		
	}
	
	#endregion
	
	#region Draw Scale Loop

	private Rect m_rectArea_ScaleLoop;

	// Draw Scale loop area
	private void DrawScaleLoop()
	{
		m_rectArea_ScaleLoop = EditorGUILayout.BeginVertical();
		{
			SetBackgroundColor(DefaultBoxContent_Color);
			GUI.Box(m_rectArea_ScaleLoop, GUIContent.none);
			ReleaseBackgroundColor();
			GUILayout.Space(3);
			//m_rectArea_ScaleLoop = new Rect(m_rectTabVerticle.x, m_rectTabVerticle.y, TabWidth, m_rectTabVerticle.height);
			
			//////////////////////////////////
			/// DRAW ENABLE
			//////////////////////////////////
			
			EditorGUILayout.BeginHorizontal();
			{
				GUILayout.Space(5);
				
				targetProp.m_ScaleLoop.Enable = EditorGUILayout.Toggle(targetProp.m_ScaleLoop.Enable, GUILayout.MaxWidth(12));
				EditorGUILayout.LabelField("Scale Loop", GUILayout.MaxWidth(60));
			}
			EditorGUILayout.EndHorizontal();
			
			
			
			//////////////////////////////////
			/// MIN
			//////////////////////////////////
			
			EditorGUILayout.BeginHorizontal();
			{
				GUILayout.Space(10);
				
				GUILayout.Label("Min:", GUILayout.Width(40));
				EditorGUILayout.BeginVertical();
				{
					EditorGUILayout.BeginHorizontal();
					{
						GUILayout.Label("X", GUILayout.MaxWidth(12));
						float ScaleLoopMinX = EditorGUILayout.FloatField(targetProp.m_ScaleLoop.Min.x, GUILayout.MaxWidth(38));
						GUILayout.Label("Y", GUILayout.MaxWidth(12));
						float ScaleLoopMinY = EditorGUILayout.FloatField(targetProp.m_ScaleLoop.Min.y, GUILayout.MaxWidth(38));
						GUILayout.Label("Z", GUILayout.MaxWidth(12));
						float ScaleLoopMinZ = EditorGUILayout.FloatField(targetProp.m_ScaleLoop.Min.z, GUILayout.MaxWidth(38));
						
						targetProp.m_ScaleLoop.Min = new Vector3(ScaleLoopMinX, ScaleLoopMinY, ScaleLoopMinZ);
					}
					EditorGUILayout.EndHorizontal();
					//if(GEAnimEditorUtils.m_ShowHelpBox)
					if(GEAnimEditorUtils.GetBool("m_ShowHelpBox")==true)
					{
						EditorGUILayout.BeginHorizontal();
						{
							EditorGUILayout.HelpBox("Minimum scale when object is looping.", MessageType.None);
						}
						EditorGUILayout.EndHorizontal();
					}
				}
				EditorGUILayout.EndVertical();
				
				GUILayout.Space(5);
			}
			EditorGUILayout.EndHorizontal();

			//////////////////////////////////
			/// MAX
			//////////////////////////////////
			
			EditorGUILayout.BeginHorizontal();
			{
				GUILayout.Space(10);
				
				GUILayout.Label("Max:", GUILayout.Width(40));
				EditorGUILayout.BeginVertical();
				{
					EditorGUILayout.BeginHorizontal();
					{
						GUILayout.Label("X", GUILayout.MaxWidth(12));
						float ScaleLoopMaxX = EditorGUILayout.FloatField(targetProp.m_ScaleLoop.Max.x, GUILayout.MaxWidth(38));
						GUILayout.Label("Y", GUILayout.MaxWidth(12));
						float ScaleLoopMaxY = EditorGUILayout.FloatField(targetProp.m_ScaleLoop.Max.y, GUILayout.MaxWidth(38));
						GUILayout.Label("Z", GUILayout.MaxWidth(12));
						float ScaleLoopMaxZ = EditorGUILayout.FloatField(targetProp.m_ScaleLoop.Max.z, GUILayout.MaxWidth(38));
						
						targetProp.m_ScaleLoop.Max = new Vector3(ScaleLoopMaxX, ScaleLoopMaxY, ScaleLoopMaxZ);
					}
					EditorGUILayout.EndHorizontal();
					//if(GEAnimEditorUtils.m_ShowHelpBox)
					if(GEAnimEditorUtils.GetBool("m_ShowHelpBox")==true)
					{
						EditorGUILayout.BeginHorizontal();
						{
							EditorGUILayout.HelpBox("Maximum scale when object is looping.", MessageType.None);
						}
						EditorGUILayout.EndHorizontal();
					}
				}
				EditorGUILayout.EndVertical();
				
				GUILayout.Space(5);
			}
			EditorGUILayout.EndHorizontal();
			
			//////////////////////////////////
			/// EASE
			//////////////////////////////////
			
			EditorGUILayout.BeginHorizontal();
			{
				GUILayout.Space(10);
				
				GUILayout.Label("Ease:", GUILayout.Width(40));
				
				EditorGUILayout.BeginVertical();
				{
					targetProp.m_ScaleLoop.EaseType = (eEaseType) EditorGUILayout.EnumPopup("" , targetProp.m_ScaleLoop.EaseType);//, GUILayout.MaxWidth(75));
					// Show graph
					//if(GEAnimEditorUtils.m_ShowEasingGraph)
					if(GEAnimEditorUtils.GetBool("m_ShowEasingGraph")==true)
					{
						DrawGraph(targetProp.m_ScaleLoop.EaseType);
					}
					//if(GEAnimEditorUtils.m_ShowHelpBox)
					if(GEAnimEditorUtils.GetBool("m_ShowHelpBox")==true)
					{
						EditorGUILayout.HelpBox("Easing is the rate of change of animation over time.", MessageType.None);
					}
				}
				EditorGUILayout.EndVertical();
				
				GUILayout.Space(5);
			}
			EditorGUILayout.EndHorizontal();
			
			// Time and delay
			EditorGUILayout.BeginHorizontal();
			{
				GUILayout.Space(10);
				
				GUILayout.Label("Time:", GUILayout.Width(40));
				EditorGUILayout.BeginVertical();
				{
					EditorGUILayout.BeginHorizontal();
					{
						targetProp.m_ScaleLoop.Time = EditorGUILayout.FloatField("" , targetProp.m_ScaleLoop.Time, GUILayout.MinWidth(25));
						GUILayout.Label("Delay:", GUILayout.Width(44));
						targetProp.m_ScaleLoop.Delay = EditorGUILayout.FloatField("" , targetProp.m_ScaleLoop.Delay, GUILayout.MinWidth(25));
					}
					EditorGUILayout.EndHorizontal();
					//if(GEAnimEditorUtils.m_ShowHelpBox)
					if(GEAnimEditorUtils.GetBool("m_ShowHelpBox")==true)
					{
						EditorGUILayout.HelpBox("Time is amount (seconds) that the animation will take to complete.", MessageType.None);
						EditorGUILayout.HelpBox("Delay is amount (seconds) that the animation will wait before beginning.", MessageType.None);
					}
				}
				EditorGUILayout.EndVertical();
				
				
				GUILayout.Space(5);
			}
			EditorGUILayout.EndHorizontal();
			
			// Sound
			EditorGUILayout.BeginHorizontal();
			{
				GUILayout.Space(10);
				
				EditorGUILayout.BeginVertical();
				{
					EditorGUILayout.BeginHorizontal();
					{						
						GUILayout.Label("Sound:", GUILayout.Width(40));
						targetProp.m_ScaleLoop.Sound.m_AudioClip = (AudioClip) EditorGUILayout.ObjectField( targetProp.m_ScaleLoop.Sound.m_AudioClip, typeof(AudioClip), false);
						
						targetProp.m_ScaleLoop.Sound.m_Loop = EditorGUILayout.Toggle(targetProp.m_ScaleLoop.Sound.m_Loop, GUILayout.MaxWidth(12));
						EditorGUILayout.LabelField("Loop", GUILayout.MaxWidth(35));
					}
					EditorGUILayout.EndHorizontal();
					//if(GEAnimEditorUtils.m_ShowHelpBox)
					if(GEAnimEditorUtils.GetBool("m_ShowHelpBox")==true)
					{
						EditorGUILayout.BeginHorizontal();
						{
							GUILayout.Label("", GUILayout.Width(40));
							EditorGUILayout.HelpBox("AudioClip to play when Idle begins. If Loop is checked, the AudioCLip will be looped.", MessageType.None);
						}
						EditorGUILayout.EndHorizontal();
					}
				}
				EditorGUILayout.EndVertical();

				GUILayout.Space(5);
			}
			EditorGUILayout.EndHorizontal();
			
			GUILayout.Space(5);
		}
		EditorGUILayout.EndVertical();
	}
	
	#endregion
	
	#region Draw Fade In/Out

	private Rect m_rectArea_Fade;
	
	// Draw Fade In/Out area
	private void DrawFadeInOut(int InOrOut)
	{
		m_rectArea_Fade = EditorGUILayout.BeginVertical();
		{
			SetBackgroundColor(DefaultBoxContent_Color);
			GUI.Box(m_rectArea_Fade, GUIContent.none);
			ReleaseBackgroundColor();
			GUILayout.Space(3);
			
			//////////////////////////////////
			/// DRAW ENABLE
			//////////////////////////////////
			
			EditorGUILayout.BeginHorizontal();
			{
				GUILayout.Space(5);
				
				if(InOrOut==0)
				{
					targetProp.m_FadeIn.Enable = EditorGUILayout.Toggle(targetProp.m_FadeIn.Enable, GUILayout.MaxWidth(12));
					EditorGUILayout.LabelField("Fade In", GUILayout.MaxWidth(60));
				}
				else
				{
					targetProp.m_FadeOut.Enable = EditorGUILayout.Toggle(targetProp.m_FadeOut.Enable, GUILayout.MaxWidth(12));
					EditorGUILayout.LabelField("Fade Out", GUILayout.MaxWidth(70));
				}
			}
			EditorGUILayout.EndHorizontal();

			//////////////////////////////////
			/// Fade
			//////////////////////////////////
			
			EditorGUILayout.BeginHorizontal();
			{
				GUILayout.Space(10);
				
				EditorGUILayout.BeginVertical();
				{
					EditorGUILayout.BeginHorizontal();
					{
						EditorGUILayout.LabelField("", GUILayout.MaxWidth(40));
						if(InOrOut==0)
						{
							targetProp.m_FadeIn.FadeChildren = EditorGUILayout.Toggle(targetProp.m_FadeIn.FadeChildren, GUILayout.MaxWidth(12));
						}
						else
						{
							targetProp.m_FadeOut.FadeChildren = EditorGUILayout.Toggle(targetProp.m_FadeOut.FadeChildren, GUILayout.MaxWidth(12));
						}
						EditorGUILayout.LabelField("Effect on children", GUILayout.MaxWidth(105));
					}
					EditorGUILayout.EndHorizontal();
					//if(GEAnimEditorUtils.m_ShowHelpBox)
					if(GEAnimEditorUtils.GetBool("m_ShowHelpBox")==true)
					{
						EditorGUILayout.BeginHorizontal();
						{
							EditorGUILayout.LabelField("", GUILayout.MaxWidth(40));
							EditorGUILayout.HelpBox("Check it if you want to fade children.", MessageType.None);
						}
						EditorGUILayout.EndHorizontal();
					}
				}
				EditorGUILayout.EndVertical();
				
				GUILayout.Space(5);
			}
			EditorGUILayout.EndHorizontal();
			
			//////////////////////////////////
			/// EASE
			//////////////////////////////////
			
			EditorGUILayout.BeginHorizontal();
			{
				GUILayout.Space(10);
				
				GUILayout.Label("Ease:", GUILayout.Width(40));
				
				EditorGUILayout.BeginVertical();
				{
					if(InOrOut==0)
					{
						targetProp.m_FadeIn.EaseType = (eEaseType) EditorGUILayout.EnumPopup("" , targetProp.m_FadeIn.EaseType);//, GUILayout.MaxWidth(75));
						// Show graph
						//if(GEAnimEditorUtils.m_ShowEasingGraph)
						if(GEAnimEditorUtils.GetBool("m_ShowEasingGraph")==true)
						{
							DrawGraph(targetProp.m_FadeIn.EaseType);
						}
					}
					else
					{
						targetProp.m_FadeOut.EaseType = (eEaseType) EditorGUILayout.EnumPopup("" , targetProp.m_FadeOut.EaseType);//, GUILayout.MaxWidth(75));
						// Show graph
						//if(GEAnimEditorUtils.m_ShowEasingGraph)
						if(GEAnimEditorUtils.GetBool("m_ShowEasingGraph")==true)
						{
							DrawGraph(targetProp.m_FadeOut.EaseType);
						}
					}
					//if(GEAnimEditorUtils.m_ShowHelpBox)
					if(GEAnimEditorUtils.GetBool("m_ShowHelpBox")==true)
					{
						EditorGUILayout.HelpBox("Easing is the rate of change of animation over time.", MessageType.None);
					}
				}
				EditorGUILayout.EndVertical();
				
				GUILayout.Space(5);
			}
			EditorGUILayout.EndHorizontal();
			
			// Time and delay
			EditorGUILayout.BeginHorizontal();
			{
				GUILayout.Space(10);
				
				GUILayout.Label("Time:", GUILayout.Width(40));
				EditorGUILayout.BeginVertical();
				{
					EditorGUILayout.BeginHorizontal();
					{
						if(InOrOut==0)
						{
							targetProp.m_FadeIn.Time = EditorGUILayout.FloatField("" , targetProp.m_FadeIn.Time, GUILayout.MinWidth(25));
						}
						else
						{
							targetProp.m_FadeOut.Time = EditorGUILayout.FloatField("" , targetProp.m_FadeOut.Time, GUILayout.MinWidth(25));
						}
						GUILayout.Label("Delay:", GUILayout.Width(44));
						if(InOrOut==0)
						{
							targetProp.m_FadeIn.Delay = EditorGUILayout.FloatField("" , targetProp.m_FadeIn.Delay, GUILayout.MinWidth(25));
						}
						else
						{
							targetProp.m_FadeOut.Delay = EditorGUILayout.FloatField("" , targetProp.m_FadeOut.Delay, GUILayout.MinWidth(25));
						}
					}
					EditorGUILayout.EndHorizontal();
					//if(GEAnimEditorUtils.m_ShowHelpBox)
					if(GEAnimEditorUtils.GetBool("m_ShowHelpBox")==true)
					{
						EditorGUILayout.HelpBox("Time is amount in seconds the animation will take to complete.", MessageType.None);
						EditorGUILayout.HelpBox("Delay is amount in seconds the animation will wait before beginning.", MessageType.None);
					}
				}
				EditorGUILayout.EndVertical();
				
				
				GUILayout.Space(5);
			}
			EditorGUILayout.EndHorizontal();
			
			GUILayout.Space(10);	
			// Begin Sound
			EditorGUILayout.BeginHorizontal();
			{
				GUILayout.Space(10);	
				
				EditorGUILayout.BeginVertical();
				{
					EditorGUILayout.BeginHorizontal();
					{						
						GUILayout.Label("Begin Sound:", GUILayout.Width(80));
						if(InOrOut==0)
						{
							targetProp.m_FadeIn.Sounds.m_Begin = (AudioClip) EditorGUILayout.ObjectField( targetProp.m_FadeIn.Sounds.m_Begin, typeof(AudioClip), false);
						}
						else
						{
							targetProp.m_FadeOut.Sounds.m_Begin = (AudioClip) EditorGUILayout.ObjectField( targetProp.m_FadeOut.Sounds.m_Begin, typeof(AudioClip), false);
						}
					}
					EditorGUILayout.EndHorizontal();
					//if(GEAnimEditorUtils.m_ShowHelpBox)
					if(GEAnimEditorUtils.GetBool("m_ShowHelpBox")==true)
					{
						EditorGUILayout.BeginHorizontal();
						{
							GUILayout.Label("", GUILayout.Width(40));
							EditorGUILayout.HelpBox("AudioClip to play when animation begins.", MessageType.None);
						}
						EditorGUILayout.EndHorizontal();
					}
				}
				GUILayout.Space(3);
				EditorGUILayout.EndVertical();
				
				GUILayout.Space(5);
			}
			EditorGUILayout.EndHorizontal();
			
			// End Sound
			EditorGUILayout.BeginHorizontal();
			{
				GUILayout.Space(10);	
				
				EditorGUILayout.BeginVertical();
				{
					EditorGUILayout.BeginHorizontal();
					{
						GUILayout.Label("End Sound:", GUILayout.Width(80));
						if(InOrOut==0)
						{
							targetProp.m_FadeIn.Sounds.m_End = (AudioClip) EditorGUILayout.ObjectField( targetProp.m_FadeIn.Sounds.m_End, typeof(AudioClip), false);
						}
						else
						{
							targetProp.m_FadeOut.Sounds.m_End = (AudioClip) EditorGUILayout.ObjectField( targetProp.m_FadeOut.Sounds.m_End, typeof(AudioClip), false);
						}
					}
					EditorGUILayout.EndHorizontal();
					//if(GEAnimEditorUtils.m_ShowHelpBox)
					if(GEAnimEditorUtils.GetBool("m_ShowHelpBox")==true)
					{
						EditorGUILayout.BeginHorizontal();
						{
							GUILayout.Label("", GUILayout.Width(40));
							EditorGUILayout.HelpBox("AudioClip to play .", MessageType.None);
						}
						EditorGUILayout.EndHorizontal();
					}
				}
				GUILayout.Space(3);
				EditorGUILayout.EndVertical();
				
				GUILayout.Space(5);
			}
			EditorGUILayout.EndHorizontal();
			
			GUILayout.Space(5);
		}
		EditorGUILayout.EndVertical();
		
	}
	
	#endregion
	
	#region Draw Fade Loop

	private Rect m_rectArea_FadeLoop;

	// Draw Fade loop area
	private void DrawFadeLoop()
	{
		m_rectArea_FadeLoop = EditorGUILayout.BeginVertical();
		{
			SetBackgroundColor(DefaultBoxContent_Color);
			GUI.Box(m_rectArea_FadeLoop, GUIContent.none);
			ReleaseBackgroundColor();
			GUILayout.Space(3);
			//m_rectArea_FadeLoop = new Rect(m_rectTabVerticle.x, m_rectTabVerticle.y, TabWidth, m_rectTabVerticle.height);
			
			//////////////////////////////////
			/// DRAW ENABLE
			//////////////////////////////////
			
			EditorGUILayout.BeginHorizontal();
			{
				GUILayout.Space(5);
				
				targetProp.m_FadeLoop.Enable = EditorGUILayout.Toggle(targetProp.m_FadeLoop.Enable, GUILayout.MaxWidth(12));
				EditorGUILayout.LabelField("Fade Loop", GUILayout.MaxWidth(60));
			}
			EditorGUILayout.EndHorizontal();
			
			//////////////////////////////////
			/// EFFECT ON CHILDREN
			//////////////////////////////////
			
			EditorGUILayout.BeginHorizontal();
			{
				GUILayout.Space(10);

				EditorGUILayout.BeginVertical();
				{
					EditorGUILayout.BeginHorizontal();
					{
						EditorGUILayout.LabelField("", GUILayout.MaxWidth(40));
						targetProp.m_FadeLoop.FadeChildren = EditorGUILayout.Toggle(targetProp.m_FadeLoop.FadeChildren, GUILayout.MaxWidth(12));
						EditorGUILayout.LabelField("Effect on children", GUILayout.MaxWidth(105));
					}
					EditorGUILayout.EndHorizontal();
					//if(GEAnimEditorUtils.m_ShowHelpBox)
					if(GEAnimEditorUtils.GetBool("m_ShowHelpBox")==true)
					{
						EditorGUILayout.BeginHorizontal();
						{
							EditorGUILayout.LabelField("", GUILayout.MaxWidth(40));
							EditorGUILayout.HelpBox("Check it if you want to fade children.", MessageType.None);
						}
						EditorGUILayout.EndHorizontal();
					}
				}
				EditorGUILayout.EndVertical();
				
				GUILayout.Space(5);
			}
			EditorGUILayout.EndHorizontal();

			//////////////////////////////////
			/// ROTATION
			//////////////////////////////////
			
			EditorGUILayout.BeginHorizontal();
			{
				GUILayout.Space(10);
				
				GUILayout.Label("Min:", GUILayout.Width(40));	
				EditorGUILayout.BeginVertical();
				{
					EditorGUILayout.BeginHorizontal();
					{
						targetProp.m_FadeLoop.Min = EditorGUILayout.FloatField(targetProp.m_FadeLoop.Min, GUILayout.MaxWidth(38));
						GUILayout.Label("Max:", GUILayout.Width(40));
						targetProp.m_FadeLoop.Max = EditorGUILayout.FloatField(targetProp.m_FadeLoop.Max, GUILayout.MaxWidth(38));
					}
					EditorGUILayout.EndHorizontal();
					//if(GEAnimEditorUtils.m_ShowHelpBox)
					if(GEAnimEditorUtils.GetBool("m_ShowHelpBox")==true)
					{
						EditorGUILayout.BeginHorizontal();
						{
							EditorGUILayout.HelpBox("Minimum and Maximum fade value, amount is between 0.0 and 1.0.", MessageType.None);
						}
						EditorGUILayout.EndHorizontal();
					}
				}
				EditorGUILayout.EndVertical();
				
				GUILayout.Space(5);
			}
			EditorGUILayout.EndHorizontal();
			//////////////////////////////////
			/// EASE
			//////////////////////////////////
			
			EditorGUILayout.BeginHorizontal();
			{
				GUILayout.Space(10);
				
				GUILayout.Label("Ease:", GUILayout.Width(40));
				
				EditorGUILayout.BeginVertical();
				{
					targetProp.m_FadeLoop.EaseType = (eEaseType) EditorGUILayout.EnumPopup("" , targetProp.m_FadeLoop.EaseType);//, GUILayout.MaxWidth(75));

					// Show graph
					//if(GEAnimEditorUtils.m_ShowEasingGraph)
					if(GEAnimEditorUtils.GetBool("m_ShowEasingGraph")==true)
					{
						DrawGraph(targetProp.m_FadeLoop.EaseType);
					}
					//if(GEAnimEditorUtils.m_ShowHelpBox)
					if(GEAnimEditorUtils.GetBool("m_ShowHelpBox")==true)
					{
						EditorGUILayout.HelpBox("Easing is the rate of change of animation over time.", MessageType.None);
					}
				}
				EditorGUILayout.EndVertical();
				
				GUILayout.Space(5);
			}
			EditorGUILayout.EndHorizontal();
			
			// Time and delay
			EditorGUILayout.BeginHorizontal();
			{
				GUILayout.Space(10);
				
				GUILayout.Label("Time:", GUILayout.Width(40));
				EditorGUILayout.BeginVertical();
				{
					EditorGUILayout.BeginHorizontal();
					{
						targetProp.m_FadeLoop.Time = EditorGUILayout.FloatField("" , targetProp.m_FadeLoop.Time, GUILayout.MinWidth(25));
						GUILayout.Label("Delay:", GUILayout.Width(44));
						targetProp.m_FadeLoop.Delay = EditorGUILayout.FloatField("" , targetProp.m_FadeLoop.Delay, GUILayout.MinWidth(25));
					}
					EditorGUILayout.EndHorizontal();
					//if(GEAnimEditorUtils.m_ShowHelpBox)
					if(GEAnimEditorUtils.GetBool("m_ShowHelpBox")==true)
					{
						EditorGUILayout.HelpBox("Time is amount in seconds the animation will take to complete.", MessageType.None);
						EditorGUILayout.HelpBox("Delay is amount in seconds the animation will wait before beginning.", MessageType.None);
					}
				}
				EditorGUILayout.EndVertical();
				
				
				GUILayout.Space(5);
			}
			EditorGUILayout.EndHorizontal();
			
			// Sound
			EditorGUILayout.BeginHorizontal();
			{
				GUILayout.Space(10);

				EditorGUILayout.BeginVertical();
				{
					EditorGUILayout.BeginHorizontal();
					{						
						GUILayout.Label("Sound:", GUILayout.Width(40));
						targetProp.m_FadeLoop.Sound.m_AudioClip = (AudioClip) EditorGUILayout.ObjectField( targetProp.m_FadeLoop.Sound.m_AudioClip, typeof(AudioClip), false);
						
						targetProp.m_FadeLoop.Sound.m_Loop = EditorGUILayout.Toggle(targetProp.m_FadeLoop.Sound.m_Loop, GUILayout.MaxWidth(12));
						EditorGUILayout.LabelField("Loop", GUILayout.MaxWidth(35));
					}
					EditorGUILayout.EndHorizontal();
					//if(GEAnimEditorUtils.m_ShowHelpBox)
					if(GEAnimEditorUtils.GetBool("m_ShowHelpBox")==true)
					{
						EditorGUILayout.BeginHorizontal();
						{
							GUILayout.Label("", GUILayout.Width(40));
							EditorGUILayout.HelpBox("AudioClip to play when Idle begins. If Loop is checked, the AudioCLip will be looped.", MessageType.None);
						}
						EditorGUILayout.EndHorizontal();
					}
				}
				EditorGUILayout.EndVertical();
				
				GUILayout.Space(5);
			}
			EditorGUILayout.EndHorizontal();
			
			GUILayout.Space(5);
		}
		EditorGUILayout.EndVertical();
		
	}
	
	#endregion
	
	#region Draw Screen Anchors And Edges
	
	private Color m_DefaultBoxMoveBG_Color = new Color( 1.0f, 1.0f, 1.0f, 1.0f );
	private Color m_DefaultBoxMoveSelected_Color = new Color( 0.0f, 0.7f, 0.0f, 1.0f );
	private Color m_DefaultBoxMoveUnselected_Color = new Color( 0.7f, 0.7f, 0.7f, 1.0f );
	private Color m_DefaultEdgeMoveUnselected_Color = new Color( 0.5f, 0.5f, 0.5f, 1.0f );
	
	private Color m_DefaultProSkinBoxMoveBG_Color = new Color( 0.8f, 0.8f, 0.8f, 1.0f );
	private Color m_DefaultProSkinBoxMoveSelected_Color = new Color( 0.0f, 0.7f, 0.0f, 1.0f );
	private Color m_DefaultProSkinBoxMoveUnselected_Color = new Color( 0.5f, 0.5f, 0.5f, 1.0f );
	private Color m_DefaultEdgeSkinBoxMoveUnselected_Color = new Color( 0.2f, 0.2f, 0.2f, 1.0f);
	
	private Color m_Current_BoxMoveBG_Color = new Color( 1.0f, 1.0f, 1.0f, 1.0f );
	private Color m_Current_BoxMoveSelected_Color = new Color( 0.0f, 0.7f, 0.0f, 1.0f );
	private Color m_Current_BoxMoveUnselected_Color = new Color( 0.7f, 0.7f, 0.7f, 1.0f );
	private Color m_Current_EdgeMoveUnselected_Color = new Color( 0.5f, 0.5f, 0.5f, 1.0f );
	
	// Draw screen-points and screen-edges that can be choosed by user's click
	private ePosMove DrawScreenAnchorsAndEdges(Rect rectArea, ePosMove posMove)
	{
		// Switch colors according to Unity skin
		if(EditorGUIUtility.isProSkin)
		{
			m_Current_BoxMoveBG_Color = m_DefaultProSkinBoxMoveBG_Color;
			m_Current_BoxMoveSelected_Color = m_DefaultProSkinBoxMoveSelected_Color;
			m_Current_BoxMoveUnselected_Color = m_DefaultProSkinBoxMoveUnselected_Color;
			m_Current_EdgeMoveUnselected_Color = m_DefaultEdgeSkinBoxMoveUnselected_Color;
		}
		else
		{
			m_Current_BoxMoveBG_Color = m_DefaultBoxMoveBG_Color;
			m_Current_BoxMoveSelected_Color = m_DefaultBoxMoveSelected_Color;
			m_Current_BoxMoveUnselected_Color = m_DefaultBoxMoveUnselected_Color;
			m_Current_EdgeMoveUnselected_Color = m_DefaultEdgeMoveUnselected_Color;
		}

		DrawTexture(rectArea, m_Current_BoxMoveBG_Color);

		// Pivot
		int PlotSize = 10;
		
		float Left = rectArea.x+2;
		float Top = rectArea.y+2;
		float Center = rectArea.x+((rectArea.width/2)-(PlotSize/2));
		float Right = (rectArea.x+rectArea.width)-(PlotSize+2);
		float Middle = rectArea.y+((rectArea.height/2)-(PlotSize/2));
		float Bottom = (rectArea.y+rectArea.height)-(PlotSize+2);
		
		Rect rectUpperLeft = new Rect(Left, Top, PlotSize,PlotSize);
		Rect rectUpperCenter = new Rect(Center, Top, PlotSize,PlotSize);
		Rect rectUpperRight = new Rect(Right, Top, PlotSize,PlotSize);
		Rect rectMiddleLeft = new Rect(Left, Middle, PlotSize,PlotSize);
		Rect rectMiddleCenter = new Rect(Center, Middle, PlotSize,PlotSize);
		Rect rectMiddleRight = new Rect(Right, Middle, PlotSize,PlotSize);
		Rect rectBottomLeft = new Rect(Left, Bottom, PlotSize,PlotSize);
		Rect rectBottomCenter = new Rect(Center, Bottom, PlotSize,PlotSize);
        Rect rectBottomRight = new Rect(Right, Bottom, PlotSize,PlotSize);

        if(posMove==ePosMove.UpperLeft)
			DrawTexture(rectUpperLeft, m_Current_BoxMoveSelected_Color);
		else
			DrawTexture(rectUpperLeft, m_Current_BoxMoveUnselected_Color);
		
		if(posMove==ePosMove.UpperCenter)
			DrawTexture(rectUpperCenter, m_Current_BoxMoveSelected_Color);
		else
			DrawTexture(rectUpperCenter, m_Current_BoxMoveUnselected_Color);		
		
		if(posMove==ePosMove.UpperRight)
			DrawTexture(rectUpperRight, m_Current_BoxMoveSelected_Color);
		else
			DrawTexture(rectUpperRight, m_Current_BoxMoveUnselected_Color);		
		
		if(posMove==ePosMove.MiddleLeft)
			DrawTexture(rectMiddleLeft, m_Current_BoxMoveSelected_Color);
		else
			DrawTexture(rectMiddleLeft, m_Current_BoxMoveUnselected_Color);		
		
		if(posMove==ePosMove.MiddleCenter)
			DrawTexture(rectMiddleCenter, m_Current_BoxMoveSelected_Color);
		else
			DrawTexture(rectMiddleCenter, m_Current_BoxMoveUnselected_Color);		
		
		if(posMove==ePosMove.MiddleRight)
			DrawTexture(rectMiddleRight, m_Current_BoxMoveSelected_Color);
		else
			DrawTexture(rectMiddleRight, m_Current_BoxMoveUnselected_Color);
		
		if(posMove==ePosMove.BottomLeft)
			DrawTexture(rectBottomLeft, m_Current_BoxMoveSelected_Color);
		else
			DrawTexture(rectBottomLeft, m_Current_BoxMoveUnselected_Color);		
		
		if(posMove==ePosMove.BottomCenter)
			DrawTexture(rectBottomCenter, m_Current_BoxMoveSelected_Color);
		else
			DrawTexture(rectBottomCenter, m_Current_BoxMoveUnselected_Color);		
		
		if(posMove==ePosMove.BottomRight)
			DrawTexture(rectBottomRight, m_Current_BoxMoveSelected_Color);
		else
			DrawTexture(rectBottomRight, m_Current_BoxMoveUnselected_Color);

		float EdgeThick = 5;

		// Draw Top edge
		Rect rectEdgeTopA = new Rect(rectUpperLeft.x+PlotSize, rectUpperLeft.y+(EdgeThick), rectUpperCenter.x-(rectUpperLeft.x+PlotSize), EdgeThick);
		Rect rectEdgeTopB = new Rect(rectUpperCenter.x+PlotSize, rectUpperCenter.y+(EdgeThick), rectUpperRight.x-(rectUpperCenter.x+PlotSize), EdgeThick);
		//Rect rectEdgeTop = new Rect(rectUpperLeft.x+PlotSize, rectUpperLeft.y+(PlotSize), rectUpperRight.x-(rectUpperLeft.x+PlotSize), PlotSize/4);
		
		if(posMove==ePosMove.UpperScreenEdge)
		{
			DrawTexture(rectEdgeTopA, m_Current_BoxMoveSelected_Color);
			DrawTexture(rectEdgeTopB, m_Current_BoxMoveSelected_Color);
			//DrawTexture(rectEdgeTop, m_Current_BoxMoveSelected_Color);
		}
		else
		{
			DrawTexture(rectEdgeTopA, m_Current_EdgeMoveUnselected_Color);
			DrawTexture(rectEdgeTopB, m_Current_EdgeMoveUnselected_Color);
			//DrawTexture(rectEdgeTop, m_Current_BoxMoveUnselected_Color);
		}
		
		// Draw Left edge
		Rect rectEdgeLeftA = new Rect(rectUpperLeft.x+(EdgeThick), rectUpperLeft.y+PlotSize, EdgeThick, rectMiddleLeft.y-(rectUpperLeft.y+PlotSize));
		Rect rectEdgeLeftB = new Rect(rectMiddleLeft.x+(EdgeThick), rectMiddleLeft.y+PlotSize, EdgeThick, rectBottomLeft.y-(rectMiddleLeft.y+PlotSize));
		
		if(posMove==ePosMove.LeftScreenEdge)
		{
			DrawTexture(rectEdgeLeftA, m_Current_BoxMoveSelected_Color);
			DrawTexture(rectEdgeLeftB, m_Current_BoxMoveSelected_Color);
        }
        else
		{
			DrawTexture(rectEdgeLeftA, m_Current_EdgeMoveUnselected_Color);
			DrawTexture(rectEdgeLeftB, m_Current_EdgeMoveUnselected_Color);
        }
		
		// Draw Right edge
		Rect rectEdgeRightA = new Rect(rectUpperRight.x, rectUpperRight.y+PlotSize, EdgeThick, rectMiddleRight.y-(rectUpperRight.y+PlotSize));
		Rect rectEdgeRightB = new Rect(rectMiddleRight.x, rectMiddleRight.y+PlotSize, EdgeThick, rectBottomRight.y-(rectMiddleRight.y+PlotSize));
		
		if(posMove==ePosMove.RightScreenEdge)
		{
			DrawTexture(rectEdgeRightA, m_Current_BoxMoveSelected_Color);
			DrawTexture(rectEdgeRightB, m_Current_BoxMoveSelected_Color); 
        }
        else
		{
			DrawTexture(rectEdgeRightA, m_Current_EdgeMoveUnselected_Color);
			DrawTexture(rectEdgeRightB, m_Current_EdgeMoveUnselected_Color);
		}
		
		// Draw Bottom edge
		Rect rectEdgeBottomA = new Rect(rectBottomLeft.x+PlotSize, rectBottomLeft.y, rectBottomCenter.x-(rectBottomLeft.x+PlotSize), EdgeThick);
		Rect rectEdgeBottomB = new Rect(rectBottomCenter.x+PlotSize, rectBottomCenter.y, rectBottomRight.x-(rectBottomCenter.x+PlotSize), EdgeThick);
		
		if(posMove==ePosMove.BottomScreenEdge)
		{
			DrawTexture(rectEdgeBottomA, m_Current_BoxMoveSelected_Color);
			DrawTexture(rectEdgeBottomB, m_Current_BoxMoveSelected_Color);
		}
		else
		{
			DrawTexture(rectEdgeBottomA, m_Current_EdgeMoveUnselected_Color);
			DrawTexture(rectEdgeBottomB, m_Current_EdgeMoveUnselected_Color); 
        }
        
        //////////////////////////////////
		/// CLICK ON A DOT
		//////////////////////////////////

		Event evt = Event.current;		
		switch (evt.type)
		{
		case EventType.mouseUp:

			// User select a screen point
			if (rectUpperLeft.Contains (evt.mousePosition))
			{
				posMove = ePosMove.UpperLeft;
				Repaint();
			}
			if (rectUpperCenter.Contains (evt.mousePosition))
			{
				posMove = ePosMove.UpperCenter;
				Repaint();
			}
			if (rectUpperRight.Contains (evt.mousePosition))
			{
				posMove = ePosMove.UpperRight;
				Repaint();
			}
			if (rectMiddleLeft.Contains (evt.mousePosition))
			{
				posMove = ePosMove.MiddleLeft;
				Repaint();
			}
			if (rectMiddleCenter.Contains (evt.mousePosition))
			{
				posMove = ePosMove.MiddleCenter;
				Repaint();
			}
			if (rectMiddleRight.Contains (evt.mousePosition))
			{
				posMove = ePosMove.MiddleRight;
				Repaint();
			}
			if (rectBottomLeft.Contains (evt.mousePosition))
			{
				posMove = ePosMove.BottomLeft;
				Repaint();
			}
			if (rectBottomCenter.Contains (evt.mousePosition))
			{
				posMove = ePosMove.BottomCenter;
				Repaint();
			}
			if (rectBottomRight.Contains (evt.mousePosition))
			{
				posMove = ePosMove.BottomRight;
				Repaint();
			}
			
			// User select an screen edge
			if (rectEdgeTopA.Contains (evt.mousePosition) || rectEdgeTopB.Contains (evt.mousePosition))
			//if (rectEdgeTop.Contains (evt.mousePosition))
			{
				posMove = ePosMove.UpperScreenEdge;
				Repaint();
			}
			if (rectEdgeLeftA.Contains (evt.mousePosition) || rectEdgeLeftB.Contains (evt.mousePosition))
			{
				posMove = ePosMove.LeftScreenEdge;
				Repaint();
			}
			if (rectEdgeRightA.Contains (evt.mousePosition) || rectEdgeRightB.Contains (evt.mousePosition))
			{
				posMove = ePosMove.RightScreenEdge;
				Repaint();
			}
			if (rectEdgeBottomA.Contains (evt.mousePosition) || rectEdgeBottomB.Contains (evt.mousePosition))
			{
				posMove = ePosMove.BottomScreenEdge;
				Repaint();
            }

			break;
		}

		return posMove;
	}
	
	#endregion
	
	#region Draw Tabs
	
	// Draw In tab
	private void Draw_In()
	{
		DrawMoveInOut(0);
		DrawRotateInOut(0);
		DrawScaleInOut(0);
		DrawFadeInOut(0);
	}
	
	// Draw Idle tab
	private void Draw_Idle()
	{
		DrawScaleLoop();
		DrawFadeLoop();
	}
	
	// Draw Out tab
	private void Draw_Out()
	{
		DrawMoveInOut(1);
		DrawRotateInOut(1);
		DrawScaleInOut(1);
		DrawFadeInOut(1);
	}
	
	// Draw Settings tab
	private Rect m_rectArea_Settings;
	private void Draw_Settings()
	{
		m_rectArea_Settings = EditorGUILayout.BeginVertical();
		{
			SetBackgroundColor(DefaultBoxContent_Color);
			GUI.Box(m_rectArea_Settings, GUIContent.none);
			ReleaseBackgroundColor();
			GUILayout.Space(15);
			//m_rectArea_Settings = new Rect(m_rectTabVerticle.x, m_rectTabVerticle.y, TabWidth, m_rectTabVerticle.height);
			
			//////////////////////////////////
			/// DRAW TABS
			//////////////////////////////////
			
			EditorGUILayout.BeginHorizontal();
			{
				GUILayout.Space(15);
				
				bool FriendlyInspector = EditorGUILayout.Toggle(GEAnimEditorUtils.GetBool("m_FriendlyInspector"), GUILayout.MaxWidth(12));
				GEAnimEditorUtils.SetBool("m_FriendlyInspector", FriendlyInspector);
				//GEAnimEditorUtils.m_FriendlyInspector = EditorGUILayout.Toggle(GEAnimEditorUtils.m_FriendlyInspector, GUILayout.MaxWidth(12));
				EditorGUILayout.LabelField("Friendly Inspector", GUILayout.MaxWidth(150));
			}
			EditorGUILayout.EndHorizontal();
			
			// Help box
			//if(GEAnimEditorUtils.m_ShowHelpBox)
			if(GEAnimEditorUtils.GetBool("m_ShowHelpBox")==true)
			{
				EditorGUILayout.BeginHorizontal();
				{
					GUILayout.Space(15);
					EditorGUILayout.HelpBox("Parameters have been categorized into tabs.", MessageType.None);
					GUILayout.Space(5);
				}
				EditorGUILayout.EndHorizontal();
			}

			GUILayout.Space(5);
			
			//////////////////////////////////
			/// DRAW TIPS
			//////////////////////////////////
			
			EditorGUILayout.BeginHorizontal();
			{
				GUILayout.Space(15);
				
				//GEAnimEditorUtils.m_ShowHelpBox = EditorGUILayout.Toggle(GEAnimEditorUtils.m_ShowHelpBox, GUILayout.MaxWidth(12));		
				bool FriendlyInspector = EditorGUILayout.Toggle(GEAnimEditorUtils.GetBool("m_ShowHelpBox"), GUILayout.MaxWidth(12));
				GEAnimEditorUtils.SetBool("m_ShowHelpBox", FriendlyInspector);

				EditorGUILayout.LabelField("Show Help Box", GUILayout.MaxWidth(130));
            }
			EditorGUILayout.EndHorizontal();

			// Help box
			//if(GEAnimEditorUtils.m_ShowHelpBox)
			if(GEAnimEditorUtils.GetBool("m_ShowHelpBox")==true)
			{
				EditorGUILayout.BeginHorizontal();
				{
					GUILayout.Space(15);
					EditorGUILayout.HelpBox("Show Help Box beneath parameters.", MessageType.None);
					GUILayout.Space(5);
				}
				EditorGUILayout.EndHorizontal();
			}
			
			GUILayout.Space(5);
			
			//////////////////////////////////
			/// DRAW EASE GRAPH
			//////////////////////////////////
			
			EditorGUILayout.BeginHorizontal();
			{
				GUILayout.Space(15);
				
				//GEAnimEditorUtils.m_ShowEasingGraph = EditorGUILayout.Toggle(GEAnimEditorUtils.m_ShowEasingGraph, GUILayout.MaxWidth(12));		
				bool FriendlyInspector = EditorGUILayout.Toggle(GEAnimEditorUtils.GetBool("m_ShowEasingGraph"), GUILayout.MaxWidth(12));
				GEAnimEditorUtils.SetBool("m_ShowEasingGraph", FriendlyInspector);

				EditorGUILayout.LabelField("Show Easing Graph", GUILayout.MaxWidth(130));
            }
			EditorGUILayout.EndHorizontal();
			
			// Help box
			//if(GEAnimEditorUtils.m_ShowHelpBox)
			if(GEAnimEditorUtils.GetBool("m_ShowHelpBox")==true)
			{
				EditorGUILayout.BeginHorizontal();
				{
					GUILayout.Space(15);
					EditorGUILayout.HelpBox("Show graph that helps you choose the right easing function.", MessageType.None);
					GUILayout.Space(5);
				}
				EditorGUILayout.EndHorizontal();
			}

			GUILayout.Space(15);
		}
		EditorGUILayout.EndVertical();
	}
	
	// Draw Help tab
	private Rect m_rectArea_Help1, m_rectArea_Help2;
	private void Draw_Help()
	{
		m_rectArea_Help1 = EditorGUILayout.BeginVertical();
		{
			SetBackgroundColor(DefaultBoxContent_Color);
			GUI.Box(m_rectArea_Help1, GUIContent.none);
			ReleaseBackgroundColor();
			GUILayout.Space(15);
			
			//////////////////////////////////
			/// DRAW Version label
			//////////////////////////////////
			
			EditorGUILayout.BeginHorizontal();
			{
				GUILayout.Space(15);
				
				var centeredStyle = GUI.skin.GetStyle("Label");
				centeredStyle.alignment = TextAnchor.MiddleCenter;
				GUILayout.Label("GUI Animator for Unity UI 0.8.4");
				
				GUILayout.Space(15);
			}
			EditorGUILayout.EndHorizontal();

			GUILayout.Space(5);
			
			//////////////////////////////////
			/// DRAW Changelogs button
			//////////////////////////////////
			
			EditorGUILayout.BeginHorizontal();
			{
				GUILayout.Space(15);
				
				if(GUILayout.Button("Change logs"))
				{
					Help.BrowseURL("http://ge-team.com/pages/unity-3d/gui-animator-for-unity-ui/gui-animator-for-unity-ui-change-logs/");
				}
				
				GUILayout.Space(15);
			}
			EditorGUILayout.EndHorizontal();
			
			GUILayout.Space(5);
			
			//////////////////////////////////
			/// DRAW Documentation button
			//////////////////////////////////
			
			EditorGUILayout.BeginHorizontal();
			{
				GUILayout.Space(15);

				if(GUILayout.Button("Online Documentation"))
				{
					Help.BrowseURL("http://ge-team.com/pages/unity-3d/gui-animator-for-unity-ui/gui-animator-for-unity-ui-documentation/");
				}

				GUILayout.Space(15);
			}
			EditorGUILayout.EndHorizontal();
			
			GUILayout.Space(5);
			
			//////////////////////////////////
			/// DRAW Easings.net button
			//////////////////////////////////
			
			EditorGUILayout.BeginHorizontal();
			{
				GUILayout.Space(15);
				
				if(GUILayout.Button("Easings.net")) //"Easings.net"
				{
					Help.BrowseURL("http://easings.net");
				}
				
				if(GUILayout.Button("Easing Demo")) //"RobertPenner.com"
				{
					Help.BrowseURL("http://robertpenner.com/easing/easing_demo.html");
				}
				
				GUILayout.Space(15);
			}
			EditorGUILayout.EndHorizontal();
			
			GUILayout.Space(15);
		}
		EditorGUILayout.EndVertical();
		
		m_rectArea_Help2 = EditorGUILayout.BeginVertical();
		{
			SetBackgroundColor(DefaultBoxContent_Color);
			GUI.Box(m_rectArea_Help2, GUIContent.none);
			ReleaseBackgroundColor();
			GUILayout.Space(15);

			//////////////////////////////////
			/// DRAW Support button
			//////////////////////////////////
			
			EditorGUILayout.BeginHorizontal();
			{
				GUILayout.Space(15);
				
				if(GUILayout.Button("Support"))
				{
					Help.BrowseURL("mailto:geteamdev@gmail.com");
				}
				
				GUILayout.Space(15);
			}
			EditorGUILayout.EndHorizontal();
			
			GUILayout.Space(5);
			
			//////////////////////////////////
			/// DRAW Changelogs button
			//////////////////////////////////
			
			EditorGUILayout.BeginHorizontal();
			{
				GUILayout.Space(15);
				
				if(GUILayout.Button("Gold Experience Team"))
				{
					Help.BrowseURL("http://ge-team.com/");
				}
				
				GUILayout.Space(15);
			}
			EditorGUILayout.EndHorizontal();
			
			GUILayout.Space(15);
		}
		EditorGUILayout.EndVertical();
	}
	
	#endregion

	#region Utilities

	// Return a filled Texture2D
	private Texture2D MakeTex( int width, int height, Color col )
	{
		Color[] pix = new Color[width * height];
		for( int i = 0; i < pix.Length; ++i )
		{
			pix[ i ] = col;
		}
		Texture2D result = new Texture2D( width, height );
		result.SetPixels( pix );
		result.Apply();
		return result;
	}
	
	// Draw a rect on Texture2D
	private void DrawTexture(Rect rect, Color colorFill)
	{		
		Texture2D tex = new Texture2D(1, 1, TextureFormat.RGBA32, false);
		tex.SetPixel(0, 0, colorFill);
		tex.Apply();

		GUI.DrawTexture(rect, tex, ScaleMode.StretchToFill);
	}
	
	private Rect m_rectTabVerticle, m_rectTab1, m_rectTab2, m_rectTab3, m_rectTab4, m_rectTab5;
	//private float TabWidth;
	
	private Color TabEnabled_BGColor = new Color(1.0f,1.0f,1.0f,1.0f);
	private Color TabDisabled_BGColor = new Color(0.5f,0.5f,0.5f,0.4f);
	private Color TabContainsData_Color = new Color( 0.0f, 0.5f, 0.0f, 1.0f );
	
	private Color m_DefaultBackgroundColor = GUI.backgroundColor;

	// Set background color
	private void SetBackgroundColor(Color newColor)
	{
		GUI.backgroundColor = newColor;
	}

	// Release background color
	private void ReleaseBackgroundColor()
	{
		GUI.backgroundColor = m_DefaultBackgroundColor;
	}
	
	#endregion
}
