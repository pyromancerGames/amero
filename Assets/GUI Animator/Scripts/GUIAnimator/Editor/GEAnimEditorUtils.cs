﻿// GUI Animator for Unity UI version: 0.8.4 (Product page: http://ge-team.com/pages/unity-3d/gui-animator-for-unity-ui/)
//
// Author:			Gold Experience Team (http://www.ge-team.com/pages/)
// Products:		http://ge-team.com/pages/unity-3d/
// Support:			geteamdev@gmail.com
// Documentation:	http://ge-team.com/pages/unity-3d/gui-animator-for-unity-ui/gui-animator-for-unity-ui-documentation/
//
// Please direct any bugs/comments/suggestions to geteamdev@gmail.com

#region Namespaces

using UnityEngine;
using UnityEditor;
using System.Collections;

#endregion

/******************************************
* GEAnimEditorUtils class
* Collection of utilities functions
******************************************/

public static class GEAnimEditorUtils
{
	#region Variables

	public enum eTab
	{
		In,
		Idle,
		Out,
		Settings,
		Help
	}

	//public static eTab m_eTab = eTab.In;
	//public static bool m_FriendlyInspector = true;
	//public static bool m_ShowHelpBox = false;
	//public static bool m_ShowEasingGraph = true;

	public static bool m_AutoTestOnPlay = true;

	// GRAPH
	public static Texture2D m_Quad_In = null;
	public static Texture2D m_Quad_Out = null;
	public static Texture2D m_Quad_InOut = null;

	public static Texture2D m_Cubic_In = null;
	public static Texture2D m_Cubic_Out = null;
	public static Texture2D m_Cubic_InOut = null;
	
	public static Texture2D m_Quart_In = null;
	public static Texture2D m_Quart_Out = null;
	public static Texture2D m_Quart_InOut = null;
	
	public static Texture2D m_Quint_In = null;
	public static Texture2D m_Quint_Out = null;
	public static Texture2D m_Quint_InOut = null;
	
	public static Texture2D m_Sine_In = null;
	public static Texture2D m_Sine_Out = null;
	public static Texture2D m_Sine_InOut = null;
	
	public static Texture2D m_Expo_In = null;
	public static Texture2D m_Expo_Out = null;
	public static Texture2D m_Expo_InOut = null;
	
	public static Texture2D m_Circ_In = null;
	public static Texture2D m_Circ_Out = null;
	public static Texture2D m_Circ_InOut = null;
	
	public static Texture2D m_Linear = null;
	public static Texture2D m_Spring = null;
	
	public static Texture2D m_Bounce_In = null;
	public static Texture2D m_Bounce_Out = null;
	public static Texture2D m_Bounce_InOut = null;
	
	public static Texture2D m_Back_In = null;
	public static Texture2D m_Back_Out = null;
	public static Texture2D m_Back_InOut = null;
	
	public static Texture2D m_Elastic_In = null;
	public static Texture2D m_Elastic_Out = null;
	public static Texture2D m_Elastic_InOut = null;
	
	public static bool IsCalcualtedEaseGraph = false;

	public static GUIContent m_Content_Quad_In = new GUIContent();
	public static GUIContent m_Content_Quad_Out = new GUIContent();
	public static GUIContent m_Content_Quad_InOut = new GUIContent();
	
	public static GUIContent m_Content_Cubic_In = new GUIContent();
	public static GUIContent m_Content_Cubic_Out = new GUIContent();
	public static GUIContent m_Content_Cubic_InOut = new GUIContent();
	
	public static GUIContent m_Content_Quart_In = new GUIContent();
	public static GUIContent m_Content_Quart_Out = new GUIContent();
	public static GUIContent m_Content_Quart_InOut = new GUIContent();
	
	public static GUIContent m_Content_Quint_In = new GUIContent();
	public static GUIContent m_Content_Quint_Out = new GUIContent();
	public static GUIContent m_Content_Quint_InOut = new GUIContent();
	
	public static GUIContent m_Content_Sine_In = new GUIContent();
	public static GUIContent m_Content_Sine_Out = new GUIContent();
	public static GUIContent m_Content_Sine_InOut = new GUIContent();
	
	public static GUIContent m_Content_Expo_In = new GUIContent();
	public static GUIContent m_Content_Expo_Out = new GUIContent();
	public static GUIContent m_Content_Expo_InOut = new GUIContent();
	
	public static GUIContent m_Content_Circ_In = new GUIContent();
	public static GUIContent m_Content_Circ_Out = new GUIContent();
	public static GUIContent m_Content_Circ_InOut = new GUIContent();
	
	public static GUIContent m_Content_Linear = new GUIContent();
	public static GUIContent m_Content_Spring = new GUIContent();
	
	public static GUIContent m_Content_Bounce_In = new GUIContent();
	public static GUIContent m_Content_Bounce_Out = new GUIContent();
	public static GUIContent m_Content_Bounce_InOut = new GUIContent();
	
	public static GUIContent m_Content_Back_In = new GUIContent();
	public static GUIContent m_Content_Back_Out = new GUIContent();
	public static GUIContent m_Content_Back_InOut = new GUIContent();
	
	public static GUIContent m_Content_Elastic_In = new GUIContent();
    public static GUIContent m_Content_Elastic_Out = new GUIContent();
    public static GUIContent m_Content_Elastic_InOut = new GUIContent();

	private static string m_StringGraphTooltip = "To see how each Easing works, click Help tab then click Easings.net or Easing Demo button.";
	
	#endregion	
	
	#region Utilities functions

	public static Rect FillBG(Texture2D pTexture2D, int Width, int Height, Color BG, Color Border, Color Line, Color Graph)
	{
		int marginVer = 15;
		int marginHor = 5;
		for(int y=0;y<Height;y++)
		{
			for(int x=0;x<Width;x++)
			{
				// Border
				if(x==0 || x==Width-1 || y==0 || y==Height-1)
				{
					pTexture2D.SetPixel(x, y, Border);
				}
				// Line
				else if((x>marginHor && x<Width-marginHor) && (y==marginVer || y==Height-marginVer))
				{
					pTexture2D.SetPixel(x, y, Line);
                }
                // BG
                else
                {
					pTexture2D.SetPixel(x, y, BG);
                }
            }
		}
		return new Rect(marginHor, marginVer, Width-(marginHor*2), Height-(marginVer*2));
    }

	public static void CalculateEaseGraph()
	{
		CalculateEaseGraph(174, 60, new Color(0.95f,0.95f,0.95f,1), new Color(0.6f,0.6f,0.6f,1), new Color(0.7f,0.7f,0.7f,1), new Color(0,0,0.5f,1));
	}
    
    public static void CalculateEaseGraph(int Width, int Height, Color BG, Color Border, Color Line, Color Graph)
	{
		if(m_Quad_In == null)
		{
			m_Quad_In = new Texture2D(Width, Height, TextureFormat.RGBA32, false);

			// Fill BG
			Rect rect = FillBG(m_Quad_In, Width, Height, BG, Border, Line, Graph);
			
			// DRAW GRAPH
			for(int value = 0; value<=rect.width; value++)
			{
				float easeValue = easeInQuad(0, rect.height, ((float)value)/rect.width);
				m_Quad_In.SetPixel(((int)rect.x)+value, ((int)rect.y)+(int) easeValue, Graph);

				//m_Quad_In.SetPixel(((int)rect.x)+value, ((int)rect.y)+(int) easeValue + 1, Graph);
				//m_Quad_In.SetPixel(((int)rect.x)+value + 1, ((int)rect.y)+(int) easeValue, Graph);
				
				//Debug.Log(value+" easeValue="+easeValue);
			}
			m_Quad_In.Apply();
			m_Content_Quad_In.image = m_Quad_In;
			m_Content_Quad_In.text = "";
			m_Content_Quad_In.tooltip = m_StringGraphTooltip;
		}

		if(m_Quad_Out == null)
		{
			m_Quad_Out = new Texture2D(Width, Height, TextureFormat.RGBA32, false);
			
			// Fill BG
			Rect rect = FillBG(m_Quad_Out, Width, Height, BG, Border, Line, Graph);
			
			// DRAW GRAPH
			for(int value = 0; value<=rect.width; value++)
			{
				float easeValue = easeOutQuad(0, rect.height, ((float)value)/rect.width);
				m_Quad_Out.SetPixel(((int)rect.x)+value, ((int)rect.y)+(int) easeValue, Graph);
				m_Quad_Out.SetPixel(((int)rect.x)+value, ((int)rect.y)+(int) easeValue + 1, Graph);
				m_Quad_Out.SetPixel(((int)rect.x)+value + 1, ((int)rect.y)+(int) easeValue, Graph);
                //Debug.Log(value+" easeValue="+easeValue);
            }
			m_Quad_Out.Apply();
			m_Content_Quad_Out.image = m_Quad_Out;
			m_Content_Quad_Out.text = "";
			m_Content_Quad_Out.tooltip = m_StringGraphTooltip;
		}
		
		if(m_Quad_InOut == null)
		{
			m_Quad_InOut = new Texture2D(Width, Height, TextureFormat.RGBA32, false);
			
			// Fill BG
			Rect rect = FillBG(m_Quad_InOut, Width, Height, BG, Border, Line, Graph);
			
			// DRAW GRAPH
			for(int value = 0; value<=rect.width; value++)
			{
				float easeValue = easeInOutQuad(0, rect.height, ((float)value)/rect.width);
				m_Quad_InOut.SetPixel(((int)rect.x)+value, ((int)rect.y)+(int) easeValue, Graph);
				m_Quad_InOut.SetPixel(((int)rect.x)+value, ((int)rect.y)+(int) easeValue + 1, Graph);
				m_Quad_InOut.SetPixel(((int)rect.x)+value + 1, ((int)rect.y)+(int) easeValue, Graph);
				//Debug.Log(value+" easeValue="+easeValue);
            }
			m_Quad_InOut.Apply();
			m_Content_Quad_InOut.image = m_Quad_InOut;
			m_Content_Quad_InOut.text = "";
			m_Content_Quad_InOut.tooltip = m_StringGraphTooltip;
        }

		
		if(m_Cubic_In == null)
		{
			m_Cubic_In = new Texture2D(Width, Height, TextureFormat.RGBA32, false);
			
			// Fill BG
			Rect rect = FillBG(m_Cubic_In, Width, Height, BG, Border, Line, Graph);
			
			// DRAW GRAPH
			for(int value = 0; value<=rect.width; value++)
			{
				float easeValue = easeInCubic(0, rect.height, ((float)value)/rect.width);
				m_Cubic_In.SetPixel(((int)rect.x)+value, ((int)rect.y)+(int) easeValue, Graph);
				m_Cubic_In.SetPixel(((int)rect.x)+value, ((int)rect.y)+(int) easeValue + 1, Graph);
				m_Cubic_In.SetPixel(((int)rect.x)+value + 1, ((int)rect.y)+(int) easeValue, Graph);
				//Debug.Log(value+" easeValue="+easeValue);
			}
			m_Cubic_In.Apply();
			m_Content_Cubic_In.image = m_Cubic_In;
			m_Content_Cubic_In.text = "";
			m_Content_Cubic_In.tooltip = m_StringGraphTooltip;
		}
		
		if(m_Cubic_Out == null)
		{
			m_Cubic_Out = new Texture2D(Width, Height, TextureFormat.RGBA32, false);
			
			// Fill BG
			Rect rect = FillBG(m_Cubic_Out, Width, Height, BG, Border, Line, Graph);
			
			// DRAW GRAPH
			for(int value = 0; value<=rect.width; value++)
			{
				float easeValue = easeOutCubic(0, rect.height, ((float)value)/rect.width);
				m_Cubic_Out.SetPixel(((int)rect.x)+value, ((int)rect.y)+(int) easeValue, Graph);
				m_Cubic_Out.SetPixel(((int)rect.x)+value, ((int)rect.y)+(int) easeValue + 1, Graph);
				m_Cubic_Out.SetPixel(((int)rect.x)+value + 1, ((int)rect.y)+(int) easeValue, Graph);
				//Debug.Log(value+" easeValue="+easeValue);
			}
			m_Cubic_Out.Apply();
			m_Content_Cubic_Out.image = m_Cubic_Out;
			m_Content_Cubic_Out.text = "";
			m_Content_Cubic_Out.tooltip = m_StringGraphTooltip;
		}
		
		if(m_Cubic_InOut == null)
		{
			m_Cubic_InOut = new Texture2D(Width, Height, TextureFormat.RGBA32, false);
			
			// Fill BG
			Rect rect = FillBG(m_Cubic_InOut, Width, Height, BG, Border, Line, Graph);
			
			// DRAW GRAPH
			for(int value = 0; value<=rect.width; value++)
			{
				float easeValue = easeInOutCubic(0, rect.height, ((float)value)/rect.width);
				m_Cubic_InOut.SetPixel(((int)rect.x)+value, ((int)rect.y)+(int) easeValue, Graph);
				m_Cubic_InOut.SetPixel(((int)rect.x)+value, ((int)rect.y)+(int) easeValue + 1, Graph);
				m_Cubic_InOut.SetPixel(((int)rect.x)+value + 1, ((int)rect.y)+(int) easeValue, Graph);
				//Debug.Log(value+" easeValue="+easeValue);
			}
			m_Cubic_InOut.Apply();
			m_Content_Cubic_InOut.image = m_Cubic_InOut;
			m_Content_Cubic_InOut.text = "";
			m_Content_Cubic_InOut.tooltip = m_StringGraphTooltip;
		}
		
		
		if(m_Quart_In == null)
		{
			m_Quart_In = new Texture2D(Width, Height, TextureFormat.RGBA32, false);
			
			// Fill BG
			Rect rect = FillBG(m_Quart_In, Width, Height, BG, Border, Line, Graph);
			
			// DRAW GRAPH
			for(int value = 0; value<=rect.width; value++)
			{
				float easeValue = easeInQuart(0, rect.height, ((float)value)/rect.width);
				m_Quart_In.SetPixel(((int)rect.x)+value, ((int)rect.y)+(int) easeValue, Graph);
				m_Quart_In.SetPixel(((int)rect.x)+value, ((int)rect.y)+(int) easeValue + 1, Graph);
				m_Quart_In.SetPixel(((int)rect.x)+value + 1, ((int)rect.y)+(int) easeValue, Graph);
				//Debug.Log(value+" easeValue="+easeValue);
			}
			m_Quart_In.Apply();
			m_Content_Quart_In.image = m_Quart_In;
			m_Content_Quart_In.text = "";
			m_Content_Quart_In.tooltip = m_StringGraphTooltip;
		}
		
		if(m_Quart_Out == null)
		{
			m_Quart_Out = new Texture2D(Width, Height, TextureFormat.RGBA32, false);
			
			// Fill BG
			Rect rect = FillBG(m_Quart_Out, Width, Height, BG, Border, Line, Graph);
			
			// DRAW GRAPH
			for(int value = 0; value<=rect.width; value++)
			{
				float easeValue = easeOutQuart(0, rect.height, ((float)value)/rect.width);
				m_Quart_Out.SetPixel(((int)rect.x)+value, ((int)rect.y)+(int) easeValue, Graph);
				m_Quart_Out.SetPixel(((int)rect.x)+value, ((int)rect.y)+(int) easeValue + 1, Graph);
				m_Quart_Out.SetPixel(((int)rect.x)+value + 1, ((int)rect.y)+(int) easeValue, Graph);
				//Debug.Log(value+" easeValue="+easeValue);
			}
			m_Quart_Out.Apply();
			m_Content_Quart_Out.image = m_Quart_Out;
			m_Content_Quart_Out.text = "";
			m_Content_Quart_Out.tooltip = m_StringGraphTooltip;
		}
		
		if(m_Quart_InOut == null)
		{
			m_Quart_InOut = new Texture2D(Width, Height, TextureFormat.RGBA32, false);
			
			// Fill BG
			Rect rect = FillBG(m_Quart_InOut, Width, Height, BG, Border, Line, Graph);
			
			// DRAW GRAPH
			for(int value = 0; value<=rect.width; value++)
			{
				float easeValue = easeInOutQuart(0, rect.height, ((float)value)/rect.width);
				m_Quart_InOut.SetPixel(((int)rect.x)+value, ((int)rect.y)+(int) easeValue, Graph);
				m_Quart_InOut.SetPixel(((int)rect.x)+value, ((int)rect.y)+(int) easeValue + 1, Graph);
				m_Quart_InOut.SetPixel(((int)rect.x)+value + 1, ((int)rect.y)+(int) easeValue, Graph);
				//Debug.Log(value+" easeValue="+easeValue);
			}
			m_Quart_InOut.Apply();
			m_Content_Quart_InOut.image = m_Quart_InOut;
			m_Content_Quart_InOut.text = "";
			m_Content_Quart_InOut.tooltip = m_StringGraphTooltip;
		}
		
		if(m_Quint_In == null)
		{
			m_Quint_In = new Texture2D(Width, Height, TextureFormat.RGBA32, false);
			
			// Fill BG
			Rect rect = FillBG(m_Quint_In, Width, Height, BG, Border, Line, Graph);
			
			// DRAW GRAPH
			for(int value = 0; value<=rect.width; value++)
			{
				float easeValue = easeInQuint(0, rect.height, ((float)value)/rect.width);
				m_Quint_In.SetPixel(((int)rect.x)+value, ((int)rect.y)+(int) easeValue, Graph);
				m_Quint_In.SetPixel(((int)rect.x)+value, ((int)rect.y)+(int) easeValue + 1, Graph);
				m_Quint_In.SetPixel(((int)rect.x)+value + 1, ((int)rect.y)+(int) easeValue, Graph);
				//Debug.Log(value+" easeValue="+easeValue);
			}
			m_Quint_In.Apply();
			m_Content_Quint_In.image = m_Quint_In;
			m_Content_Quint_In.text = "";
			m_Content_Quint_In.tooltip = m_StringGraphTooltip;
		}
		
		if(m_Quint_Out == null)
		{
			m_Quint_Out = new Texture2D(Width, Height, TextureFormat.RGBA32, false);
			
			// Fill BG
			Rect rect = FillBG(m_Quint_Out, Width, Height, BG, Border, Line, Graph);
			
			// DRAW GRAPH
			for(int value = 0; value<=rect.width; value++)
			{
				float easeValue = easeOutQuint(0, rect.height, ((float)value)/rect.width);
				m_Quint_Out.SetPixel(((int)rect.x)+value, ((int)rect.y)+(int) easeValue, Graph);
				m_Quint_Out.SetPixel(((int)rect.x)+value, ((int)rect.y)+(int) easeValue + 1, Graph);
				m_Quint_Out.SetPixel(((int)rect.x)+value + 1, ((int)rect.y)+(int) easeValue, Graph);
				//Debug.Log(value+" easeValue="+easeValue);
			}
			m_Quint_Out.Apply();
			m_Content_Quint_Out.image = m_Quint_Out;
			m_Content_Quint_Out.text = "";
			m_Content_Quint_Out.tooltip = m_StringGraphTooltip;
		}
		
		if(m_Quint_InOut == null)
		{
			m_Quint_InOut = new Texture2D(Width, Height, TextureFormat.RGBA32, false);
			
			// Fill BG
			Rect rect = FillBG(m_Quint_InOut, Width, Height, BG, Border, Line, Graph);
			
			// DRAW GRAPH
			for(int value = 0; value<=rect.width; value++)
			{
				float easeValue = easeInOutQuint(0, rect.height, ((float)value)/rect.width);
				m_Quint_InOut.SetPixel(((int)rect.x)+value, ((int)rect.y)+(int) easeValue, Graph);
				m_Quint_InOut.SetPixel(((int)rect.x)+value, ((int)rect.y)+(int) easeValue + 1, Graph);
				m_Quint_InOut.SetPixel(((int)rect.x)+value + 1, ((int)rect.y)+(int) easeValue, Graph);
				//Debug.Log(value+" easeValue="+easeValue);
			}
			m_Quint_InOut.Apply();
			m_Content_Quint_InOut.image = m_Quint_InOut;
			m_Content_Quint_InOut.text = "";
			m_Content_Quint_InOut.tooltip = m_StringGraphTooltip;
		}
		
		if(m_Sine_In == null)
		{
			m_Sine_In = new Texture2D(Width, Height, TextureFormat.RGBA32, false);
			
			// Fill BG
			Rect rect = FillBG(m_Sine_In, Width, Height, BG, Border, Line, Graph);
			
			// DRAW GRAPH
			for(int value = 0; value<=rect.width; value++)
			{
				float easeValue = easeInSine(0, rect.height, ((float)value)/rect.width);
				m_Sine_In.SetPixel(((int)rect.x)+value, ((int)rect.y)+(int) easeValue, Graph);
				m_Sine_In.SetPixel(((int)rect.x)+value, ((int)rect.y)+(int) easeValue + 1, Graph);
				m_Sine_In.SetPixel(((int)rect.x)+value + 1, ((int)rect.y)+(int) easeValue, Graph);
				//Debug.Log(value+" easeValue="+easeValue);
			}
			m_Sine_In.Apply();
			m_Content_Sine_In.image = m_Sine_In;
			m_Content_Sine_In.text = "";
			m_Content_Sine_In.tooltip = m_StringGraphTooltip;
		}
		
		if(m_Sine_Out == null)
		{
			m_Sine_Out = new Texture2D(Width, Height, TextureFormat.RGBA32, false);
			
			// Fill BG
			Rect rect = FillBG(m_Sine_Out, Width, Height, BG, Border, Line, Graph);
			
			// DRAW GRAPH
			for(int value = 0; value<=rect.width; value++)
			{
				float easeValue = easeOutSine(0, rect.height, ((float)value)/rect.width);
				m_Sine_Out.SetPixel(((int)rect.x)+value, ((int)rect.y)+(int) easeValue, Graph);
				m_Sine_Out.SetPixel(((int)rect.x)+value, ((int)rect.y)+(int) easeValue + 1, Graph);
				m_Sine_Out.SetPixel(((int)rect.x)+value + 1, ((int)rect.y)+(int) easeValue, Graph);
				//Debug.Log(value+" easeValue="+easeValue);
			}
			m_Sine_Out.Apply();
			m_Content_Sine_Out.image = m_Sine_Out;
			m_Content_Sine_Out.text = "";
			m_Content_Sine_Out.tooltip = m_StringGraphTooltip;
		}
		
		if(m_Sine_InOut == null)
		{
			m_Sine_InOut = new Texture2D(Width, Height, TextureFormat.RGBA32, false);
			
			// Fill BG
			Rect rect = FillBG(m_Sine_InOut, Width, Height, BG, Border, Line, Graph);
			
			// DRAW GRAPH
			for(int value = 0; value<=rect.width; value++)
			{
				float easeValue = easeInOutSine(0, rect.height, ((float)value)/rect.width);
				m_Sine_InOut.SetPixel(((int)rect.x)+value, ((int)rect.y)+(int) easeValue, Graph);
				m_Sine_InOut.SetPixel(((int)rect.x)+value, ((int)rect.y)+(int) easeValue + 1, Graph);
				m_Sine_InOut.SetPixel(((int)rect.x)+value + 1, ((int)rect.y)+(int) easeValue, Graph);
				//Debug.Log(value+" easeValue="+easeValue);
			}
			m_Sine_InOut.Apply();
			m_Content_Sine_InOut.image = m_Sine_InOut;
			m_Content_Sine_InOut.text = "";
			m_Content_Sine_InOut.tooltip = m_StringGraphTooltip;
		}
		
		if(m_Expo_In == null)
		{
			m_Expo_In = new Texture2D(Width, Height, TextureFormat.RGBA32, false);
			
			// Fill BG
			Rect rect = FillBG(m_Expo_In, Width, Height, BG, Border, Line, Graph);
			
			// DRAW GRAPH
			for(int value = 0; value<=rect.width; value++)
			{
				float easeValue = easeInExpo(0, rect.height, ((float)value)/rect.width);
				m_Expo_In.SetPixel(((int)rect.x)+value, ((int)rect.y)+(int) easeValue, Graph);
				m_Expo_In.SetPixel(((int)rect.x)+value, ((int)rect.y)+(int) easeValue + 1, Graph);
				m_Expo_In.SetPixel(((int)rect.x)+value + 1, ((int)rect.y)+(int) easeValue, Graph);
				//Debug.Log(value+" easeValue="+easeValue);
			}
			m_Expo_In.Apply();
			m_Content_Expo_In.image = m_Expo_In;
			m_Content_Expo_In.text = "";
			m_Content_Expo_In.tooltip = m_StringGraphTooltip;
		}
		
		if(m_Expo_Out == null)
		{
			m_Expo_Out = new Texture2D(Width, Height, TextureFormat.RGBA32, false);
			
			// Fill BG
			Rect rect = FillBG(m_Expo_Out, Width, Height, BG, Border, Line, Graph);
			
			// DRAW GRAPH
			for(int value = 0; value<=rect.width; value++)
			{
				float easeValue = easeOutExpo(0, rect.height, ((float)value)/rect.width);
				m_Expo_Out.SetPixel(((int)rect.x)+value, ((int)rect.y)+(int) easeValue, Graph);
				m_Expo_Out.SetPixel(((int)rect.x)+value, ((int)rect.y)+(int) easeValue + 1, Graph);
				m_Expo_Out.SetPixel(((int)rect.x)+value + 1, ((int)rect.y)+(int) easeValue, Graph);
				//Debug.Log(value+" easeValue="+easeValue);
			}
			m_Expo_Out.Apply();
			m_Content_Expo_Out.image = m_Expo_Out;
			m_Content_Expo_Out.text = "";
			m_Content_Expo_Out.tooltip = m_StringGraphTooltip;
		}
		
		if(m_Expo_InOut == null)
		{
			m_Expo_InOut = new Texture2D(Width, Height, TextureFormat.RGBA32, false);
			
			// Fill BG
			Rect rect = FillBG(m_Expo_InOut, Width, Height, BG, Border, Line, Graph);
			
			// DRAW GRAPH
			for(int value = 0; value<=rect.width; value++)
			{
				float easeValue = easeInOutExpo(0, rect.height, ((float)value)/rect.width);
				m_Expo_InOut.SetPixel(((int)rect.x)+value, ((int)rect.y)+(int) easeValue, Graph);
				m_Expo_InOut.SetPixel(((int)rect.x)+value, ((int)rect.y)+(int) easeValue + 1, Graph);
				m_Expo_InOut.SetPixel(((int)rect.x)+value + 1, ((int)rect.y)+(int) easeValue, Graph);
				//Debug.Log(value+" easeValue="+easeValue);
			}
			m_Expo_InOut.Apply();
			m_Content_Expo_InOut.image = m_Expo_InOut;
			m_Content_Expo_InOut.text = "";
			m_Content_Expo_InOut.tooltip = m_StringGraphTooltip;
		}
		
		if(m_Circ_In == null)
		{
			m_Circ_In = new Texture2D(Width, Height, TextureFormat.RGBA32, false);
			
			// Fill BG
			Rect rect = FillBG(m_Circ_In, Width, Height, BG, Border, Line, Graph);
			
			// DRAW GRAPH
			for(int value = 0; value<=rect.width; value++)
			{
				float easeValue = easeInCirc(0, rect.height, ((float)value)/rect.width);
				m_Circ_In.SetPixel(((int)rect.x)+value, ((int)rect.y)+(int) easeValue, Graph);
				m_Circ_In.SetPixel(((int)rect.x)+value, ((int)rect.y)+(int) easeValue + 1, Graph);
				m_Circ_In.SetPixel(((int)rect.x)+value + 1, ((int)rect.y)+(int) easeValue, Graph);
				//Debug.Log(value+" easeValue="+easeValue);
			}
			m_Circ_In.Apply();
			m_Content_Circ_In.image = m_Circ_In;
			m_Content_Circ_In.text = "";
			m_Content_Circ_In.tooltip = m_StringGraphTooltip;
		}
		
		if(m_Circ_Out == null)
		{
			m_Circ_Out = new Texture2D(Width, Height, TextureFormat.RGBA32, false);
			
			// Fill BG
			Rect rect = FillBG(m_Circ_Out, Width, Height, BG, Border, Line, Graph);
			
			// DRAW GRAPH
			for(int value = 0; value<=rect.width; value++)
			{
				float easeValue = easeOutCirc(0, rect.height, ((float)value)/rect.width);
				m_Circ_Out.SetPixel(((int)rect.x)+value, ((int)rect.y)+(int) easeValue, Graph);
				m_Circ_Out.SetPixel(((int)rect.x)+value, ((int)rect.y)+(int) easeValue + 1, Graph);
				m_Circ_Out.SetPixel(((int)rect.x)+value + 1, ((int)rect.y)+(int) easeValue, Graph);
				//Debug.Log(value+" easeValue="+easeValue);
			}
			m_Circ_Out.Apply();
			m_Content_Circ_Out.image = m_Circ_Out;
			m_Content_Circ_Out.text = "";
			m_Content_Circ_Out.tooltip = m_StringGraphTooltip;
		}
		
		if(m_Circ_InOut == null)
		{
			m_Circ_InOut = new Texture2D(Width, Height, TextureFormat.RGBA32, false);
			
			// Fill BG
			Rect rect = FillBG(m_Circ_InOut, Width, Height, BG, Border, Line, Graph);
			
			// DRAW GRAPH
			for(int value = 0; value<=rect.width; value++)
			{
				float easeValue = easeInOutCirc(0, rect.height, ((float)value)/rect.width);
				m_Circ_InOut.SetPixel(((int)rect.x)+value, ((int)rect.y)+(int) easeValue, Graph);
				m_Circ_InOut.SetPixel(((int)rect.x)+value, ((int)rect.y)+(int) easeValue + 1, Graph);
				m_Circ_InOut.SetPixel(((int)rect.x)+value + 1, ((int)rect.y)+(int) easeValue, Graph);
				//Debug.Log(value+" easeValue="+easeValue);
			}
			m_Circ_InOut.Apply();
			m_Content_Circ_InOut.image = m_Circ_InOut;
			m_Content_Circ_InOut.text = "";
			m_Content_Circ_InOut.tooltip = m_StringGraphTooltip;
		}
		
		
		if(m_Linear == null)
		{
			m_Linear = new Texture2D(Width, Height, TextureFormat.RGBA32, false);
			
			// Fill BG
			Rect rect = FillBG(m_Linear, Width, Height, BG, Border, Line, Graph);
			
			// DRAW GRAPH
			for(int value = 0; value<=rect.width; value++)
			{
				float easeValue = easeLinear(0, rect.height, ((float)value)/rect.width);
				m_Linear.SetPixel(((int)rect.x)+value, ((int)rect.y)+(int) easeValue, Graph);
				m_Linear.SetPixel(((int)rect.x)+value, ((int)rect.y)+(int) easeValue + 1, Graph);
				m_Linear.SetPixel(((int)rect.x)+value + 1, ((int)rect.y)+(int) easeValue, Graph);
				//Debug.Log(value+" easeValue="+easeValue);
			}
			m_Linear.Apply();
			m_Content_Linear.image = m_Linear;
			m_Content_Linear.text = "";
			m_Content_Linear.tooltip = m_StringGraphTooltip;
		}
		
		if(m_Spring == null)
		{
			m_Spring = new Texture2D(Width, Height, TextureFormat.RGBA32, false);
			
			// Fill BG
			Rect rect = FillBG(m_Spring, Width, Height, BG, Border, Line, Graph);
			
			// DRAW GRAPH
			for(int value = 0; value<=rect.width; value++)
			{
				float easeValue = easeSpring(0, rect.height, ((float)value)/rect.width);
				m_Spring.SetPixel(((int)rect.x)+value, ((int)rect.y)+(int) easeValue, Graph);
				m_Spring.SetPixel(((int)rect.x)+value, ((int)rect.y)+(int) easeValue + 1, Graph);
				m_Spring.SetPixel(((int)rect.x)+value + 1, ((int)rect.y)+(int) easeValue, Graph);
				//Debug.Log(value+" easeValue="+easeValue);
			}
			m_Spring.Apply();
			m_Content_Spring.image = m_Spring;
			m_Content_Spring.text = "";
			m_Content_Spring.tooltip = m_StringGraphTooltip;
		}
		
		if(m_Bounce_In == null)
		{
			m_Bounce_In = new Texture2D(Width, Height, TextureFormat.RGBA32, false);
			
			// Fill BG
			Rect rect = FillBG(m_Bounce_In, Width, Height, BG, Border, Line, Graph);
			
			// DRAW GRAPH
			for(int value = 0; value<=rect.width; value++)
			{
				float easeValue = easeInBounce(0, rect.height, ((float)value)/rect.width);
				m_Bounce_In.SetPixel(((int)rect.x)+value, ((int)rect.y)+(int) easeValue, Graph);
				m_Bounce_In.SetPixel(((int)rect.x)+value, ((int)rect.y)+(int) easeValue + 1, Graph);
				m_Bounce_In.SetPixel(((int)rect.x)+value + 1, ((int)rect.y)+(int) easeValue, Graph);
				//Debug.Log(value+" easeValue="+easeValue);
			}
			m_Bounce_In.Apply();
			m_Content_Bounce_In.image = m_Bounce_In;
			m_Content_Bounce_In.text = "";
			m_Content_Bounce_In.tooltip = m_StringGraphTooltip;
		}
		
		if(m_Bounce_Out == null)
		{
			m_Bounce_Out = new Texture2D(Width, Height, TextureFormat.RGBA32, false);
			
			// Fill BG
			Rect rect = FillBG(m_Bounce_Out, Width, Height, BG, Border, Line, Graph);
			
			// DRAW GRAPH
			for(int value = 0; value<=rect.width; value++)
			{
				float easeValue = easeOutBounce(0, rect.height, ((float)value)/rect.width);
				m_Bounce_Out.SetPixel(((int)rect.x)+value, ((int)rect.y)+(int) easeValue, Graph);
				m_Bounce_Out.SetPixel(((int)rect.x)+value, ((int)rect.y)+(int) easeValue + 1, Graph);
				m_Bounce_Out.SetPixel(((int)rect.x)+value + 1, ((int)rect.y)+(int) easeValue, Graph);
				//Debug.Log(value+" easeValue="+easeValue);
			}
			m_Bounce_Out.Apply();
			m_Content_Bounce_Out.image = m_Bounce_Out;
			m_Content_Bounce_Out.text = "";
			m_Content_Bounce_Out.tooltip = m_StringGraphTooltip;
		}
		
		if(m_Bounce_InOut == null)
		{
			m_Bounce_InOut = new Texture2D(Width, Height, TextureFormat.RGBA32, false);
			
			// Fill BG
			Rect rect = FillBG(m_Bounce_InOut, Width, Height, BG, Border, Line, Graph);
			
			// DRAW GRAPH
			for(int value = 0; value<=rect.width; value++)
			{
				float easeValue = easeInOutBounce(0, rect.height, ((float)value)/rect.width);
				m_Bounce_InOut.SetPixel(((int)rect.x)+value, ((int)rect.y)+(int) easeValue, Graph);
				m_Bounce_InOut.SetPixel(((int)rect.x)+value, ((int)rect.y)+(int) easeValue + 1, Graph);
				m_Bounce_InOut.SetPixel(((int)rect.x)+value + 1, ((int)rect.y)+(int) easeValue, Graph);
				//Debug.Log(value+" easeValue="+easeValue);
			}
			m_Bounce_InOut.Apply();
			m_Content_Bounce_InOut.image = m_Bounce_InOut;
			m_Content_Bounce_InOut.text = "";
			m_Content_Bounce_InOut.tooltip = m_StringGraphTooltip;
		}
		
		if(m_Back_In == null)
		{
			m_Back_In = new Texture2D(Width, Height, TextureFormat.RGBA32, false);
			
			// Fill BG
			Rect rect = FillBG(m_Back_In, Width, Height, BG, Border, Line, Graph);
			
			// DRAW GRAPH
			for(int value = 0; value<=rect.width; value++)
			{
				float easeValue = easeInBack(0, rect.height, ((float)value)/rect.width);
                m_Back_In.SetPixel(((int)rect.x)+value, ((int)rect.y)+(int) easeValue, Graph);
				m_Back_In.SetPixel(((int)rect.x)+value, ((int)rect.y)+(int) easeValue + 1, Graph);
				m_Back_In.SetPixel(((int)rect.x)+value + 1, ((int)rect.y)+(int) easeValue, Graph);
                //Debug.Log(value+" easeValue="+easeValue);
            }
            m_Back_In.Apply();
            m_Content_Back_In.image = m_Back_In;
            m_Content_Back_In.text = "";
			m_Content_Back_In.tooltip = m_StringGraphTooltip;
        }
        
        if(m_Back_Out == null)
        {
            m_Back_Out = new Texture2D(Width, Height, TextureFormat.RGBA32, false);
            
            // Fill BG
            Rect rect = FillBG(m_Back_Out, Width, Height, BG, Border, Line, Graph);
            
            // DRAW GRAPH
            for(int value = 0; value<=rect.width; value++)
            {
                float easeValue = easeOutBack(0, rect.height, ((float)value)/rect.width);
                m_Back_Out.SetPixel(((int)rect.x)+value, ((int)rect.y)+(int) easeValue, Graph);
				m_Back_Out.SetPixel(((int)rect.x)+value, ((int)rect.y)+(int) easeValue + 1, Graph);
				m_Back_Out.SetPixel(((int)rect.x)+value + 1, ((int)rect.y)+(int) easeValue, Graph);
                //Debug.Log(value+" easeValue="+easeValue);
            }
            m_Back_Out.Apply();
			m_Content_Back_Out.image = m_Back_Out;
            m_Content_Back_Out.text = "";
			m_Content_Back_Out.tooltip = m_StringGraphTooltip;
        }
        
        if(m_Back_InOut == null)
        {
            m_Back_InOut = new Texture2D(Width, Height, TextureFormat.RGBA32, false);
            
            // Fill BG
            Rect rect = FillBG(m_Back_InOut, Width, Height, BG, Border, Line, Graph);
            
            // DRAW GRAPH
            for(int value = 0; value<=rect.width; value++)
            {
                float easeValue = easeInOutBack(0, rect.height, ((float)value)/rect.width);
                m_Back_InOut.SetPixel(((int)rect.x)+value, ((int)rect.y)+(int) easeValue, Graph);
				m_Back_InOut.SetPixel(((int)rect.x)+value, ((int)rect.y)+(int) easeValue + 1, Graph);
				m_Back_InOut.SetPixel(((int)rect.x)+value + 1, ((int)rect.y)+(int) easeValue, Graph);
                //Debug.Log(value+" easeValue="+easeValue);
            }
            m_Back_InOut.Apply();
			m_Content_Back_InOut.image = m_Back_InOut;
            m_Content_Back_InOut.text = "";
			m_Content_Back_InOut.tooltip = m_StringGraphTooltip;
        }
        
        if(m_Elastic_In == null)
        {
            m_Elastic_In = new Texture2D(Width, Height, TextureFormat.RGBA32, false);
            
            // Fill BG
            Rect rect = FillBG(m_Elastic_In, Width, Height, BG, Border, Line, Graph);
            
            // DRAW GRAPH
            for(int value = 0; value<=rect.width; value++)
            {
                float easeValue = easeInElastic(0, rect.height, ((float)value)/rect.width);
                m_Elastic_In.SetPixel(((int)rect.x)+value, ((int)rect.y)+(int) easeValue, Graph);
				m_Elastic_In.SetPixel(((int)rect.x)+value, ((int)rect.y)+(int) easeValue + 1, Graph);
				m_Elastic_In.SetPixel(((int)rect.x)+value + 1, ((int)rect.y)+(int) easeValue, Graph);
                //Debug.Log(value+" easeValue="+easeValue);
            }
            m_Elastic_In.Apply();
			m_Content_Elastic_In.image = m_Elastic_In;
            m_Content_Elastic_In.text = "";
			m_Content_Elastic_In.tooltip = m_StringGraphTooltip;
        }
        
        if(m_Elastic_Out == null)
        {
            m_Elastic_Out = new Texture2D(Width, Height, TextureFormat.RGBA32, false);
            
            // Fill BG
            Rect rect = FillBG(m_Elastic_Out, Width, Height, BG, Border, Line, Graph);
            
            // DRAW GRAPH
            for(int value = 0; value<=rect.width; value++)
            {
                float easeValue = easeOutElastic(0, rect.height, ((float)value)/rect.width);
                m_Elastic_Out.SetPixel(((int)rect.x)+value, ((int)rect.y)+(int) easeValue, Graph);
				m_Elastic_Out.SetPixel(((int)rect.x)+value, ((int)rect.y)+(int) easeValue + 1, Graph);
				m_Elastic_Out.SetPixel(((int)rect.x)+value + 1, ((int)rect.y)+(int) easeValue, Graph);
                //Debug.Log(value+" easeValue="+easeValue);
            }
            m_Elastic_Out.Apply();
			m_Content_Elastic_Out.image = m_Elastic_Out;
            m_Content_Elastic_Out.text = "";
			m_Content_Elastic_Out.tooltip = m_StringGraphTooltip;
        }
        
        if(m_Elastic_InOut == null)
        {
            m_Elastic_InOut = new Texture2D(Width, Height, TextureFormat.RGBA32, false);
            
            // Fill BG
            Rect rect = FillBG(m_Elastic_InOut, Width, Height, BG, Border, Line, Graph);
            
            // DRAW GRAPH
            for(int value = 0; value<=rect.width; value++)
            {
                float easeValue = easeInOutElastic(0, rect.height, ((float)value)/rect.width);
                m_Elastic_InOut.SetPixel(((int)rect.x)+value, ((int)rect.y)+(int) easeValue, Graph);
				m_Elastic_InOut.SetPixel(((int)rect.x)+value, ((int)rect.y)+(int) easeValue + 1, Graph);
				m_Elastic_InOut.SetPixel(((int)rect.x)+value + 1, ((int)rect.y)+(int) easeValue, Graph);
                //Debug.Log(value+" easeValue="+easeValue);
            }
            m_Elastic_InOut.Apply();
			m_Content_Elastic_InOut.image = m_Elastic_InOut;
            m_Content_Elastic_InOut.text = "";
			m_Content_Elastic_InOut.tooltip = m_StringGraphTooltip;
        }

    }
    
    // QUAD
    
    private static float easeInQuad(float start, float end, float value)
    {
        end -= start;

        return end * value * value + start;
    }
    
    private static float easeOutQuad(float start, float end, float value)
	{
        end -= start;

		return -end * value * (value - 2) + start;
	}
	
	private static float easeInOutQuad(float start, float end, float value){
		value /= .5f;
		end -= start;
		if (value < 1) return end * 0.5f * value * value + start;
		value--;
		return -end * 0.5f * (value * (value - 2) - 1) + start;
	}
	
	// CUBIC
    
	private static float easeInCubic(float start, float end, float value)
	{
		end -= start;

		return end * value * value * value + start;
	}
	
	private static float easeOutCubic(float start, float end, float value)
	{
		value--;
		end -= start;

		return end * (value * value * value + 1) + start;
	}
	
	private static float easeInOutCubic(float start, float end, float value)
	{
		value /= .5f;
		end -= start;
		if (value < 1) 
			return end * 0.5f * value * value * value + start;
        value -= 2;

        return end * 0.5f * (value * value * value + 2) + start;
	}
	
	// QUART
	
	private static float easeInQuart(float start, float end, float value)
	{
		end -= start;

		return end * value * value * value * value + start;
	}
	
	private static float easeOutQuart(float start, float end, float value)
	{
		value--;
		end -= start;
		return -end * (value * value * value * value - 1) + start;
	}
	
	private static float easeInOutQuart(float start, float end, float value)
	{
		value /= .5f;
		end -= start;
		if (value < 1) 
			return end * 0.5f * value * value * value * value + start;
		value -= 2;

        return -end * 0.5f * (value * value * value * value - 2) + start;
	}
	
	// QUINT
	
	private static float easeInQuint(float start, float end, float value)
	{
		end -= start;

		return end * value * value * value * value * value + start;
	}
	
	private static float easeOutQuint(float start, float end, float value)
	{
		value--;
		end -= start;

		return end * (value * value * value * value * value + 1) + start;
	}
	
	private static float easeInOutQuint(float start, float end, float value)
	{
		value /= .5f;
		end -= start;
		if (value < 1) 
			return end * 0.5f * value * value * value * value * value + start;
		value -= 2;

        return end * 0.5f * (value * value * value * value * value + 2) + start;
	}
	
	// SINE
	
	private static float easeInSine(float start, float end, float value)
	{
		end -= start;

		return -end * Mathf.Cos(value * (Mathf.PI * 0.5f)) + end + start;
	}
	
	private static float easeOutSine(float start, float end, float value)
	{
		end -= start;

		return end * Mathf.Sin(value * (Mathf.PI * 0.5f)) + start;
	}
	
	private static float easeInOutSine(float start, float end, float value)
	{
		end -= start;

		return -end * 0.5f * (Mathf.Cos(Mathf.PI * value) - 1) + start;
	}
	
	// EXPO
	
	private static float easeInExpo(float start, float end, float value)
	{
		end -= start;

		return end * Mathf.Pow(2, 10 * (value - 1)) + start;
	}
	
	private static float easeOutExpo(float start, float end, float value){
		end -= start;
		return end * (-Mathf.Pow(2, -10 * value ) + 1) + start;
	}
	
	private static float easeInOutExpo(float start, float end, float value)
	{
		value /= .5f;
		end -= start;
		if (value < 1) 
			return end * 0.5f * Mathf.Pow(2, 10 * (value - 1)) + start;
		value--;

        return end * 0.5f * (-Mathf.Pow(2, -10 * value) + 2) + start;
	}
	
	// CIRC
	
	private static float easeInCirc(float start, float end, float value)
	{
		end -= start;

		return -end * (Mathf.Sqrt(1 - value * value) - 1) + start;
	}
	
	private static float easeOutCirc(float start, float end, float value){
		value--;
		end -= start;
		return end * Mathf.Sqrt(1 - value * value) + start;
	}
	
	private static float easeInOutCirc(float start, float end, float value)
	{
		value /= .5f;
		end -= start;
		if (value < 1) 
			return -end * 0.5f * (Mathf.Sqrt(1 - value * value) - 1) + start;
		value -= 2;

        return end * 0.5f * (Mathf.Sqrt(1 - value * value) + 1) + start;
	}
	
	// LINEAR
	
	private static float easeLinear(float start, float end, float value)
	{
		return Mathf.Lerp(start, end, value);
	}
	
	// SPRING
	
	private static float easeSpring(float start, float end, float value)
	{
		value = Mathf.Clamp01(value);
		value = (Mathf.Sin(value * Mathf.PI * (0.2f + 2.5f * value * value * value)) * Mathf.Pow(1f - value, 2.2f) + value) * (1f + (1.2f * (1f - value)));

		return start + (end - start) * value;
	}
	
	// BOUNCE

	private static float easeInBounce(float start, float end, float value)
	{
		end -= start;
		float d = 1f;

		return end - easeOutBounce(0, end, d-value) + start;
	}

	private static float easeOutBounce(float start, float end, float value)
	{
		value /= 1f;
		end -= start;
		if (value < (1 / 2.75f))
		{
			return end * (7.5625f * value * value) + start;
		}
		else if (value < (2 / 2.75f))
		{
			value -= (1.5f / 2.75f);
			return end * (7.5625f * (value) * value + .75f) + start;
		}
		else if (value < (2.5 / 2.75))
		{
			value -= (2.25f / 2.75f);
			return end * (7.5625f * (value) * value + .9375f) + start;
		}
		else
		{
			value -= (2.625f / 2.75f);
			return end * (7.5625f * (value) * value + .984375f) + start;
		}
	}

	private static float easeInOutBounce(float start, float end, float value)
	{
		end -= start;
		float d = 1f;
        if (value < d* 0.5f) 
			return easeInBounce(0, end, value*2) * 0.5f + start;
        else 
			return easeOutBounce(0, end, value*2-d) * 0.5f + end*0.5f + start;
	}
	
	// BACK
	
	private static float easeInBack(float start, float end, float value)
	{
		end -= start;
		value /= 1;
		float s = 1.70158f;

		return end * (value) * value * ((s + 1) * value - s) + start;
	}
	
	private static float easeOutBack(float start, float end, float value){
		float s = 1.70158f;
		end -= start;
		value = (value) - 1;

		return end * ((value) * value * ((s + 1) * value + s) + 1) + start;
	}
	
	private static float easeInOutBack(float start, float end, float value)
	{
		float s = 1.70158f;
		end -= start;
		value /= .5f;
		if ((value) < 1)
		{
			s *= (1.525f);
			return end * 0.5f * (value * value * (((s) + 1) * value - s)) + start;
		}
        value -= 2;
        s *= (1.525f);

        return end * 0.5f * ((value) * value * (((s) + 1) * value + s) + 2) + start;
	}
	
	// ELASTIC

	private static float easeInElastic(float start, float end, float value)
	{
		end -= start;
		
		float d = 1f;
		float p = d * .3f;
		float s = 0;
		float a = 0;
		
		if (value == 0) return start;
		
		if ((value /= d) == 1) return start + end;
		
		if (a == 0f || a < Mathf.Abs(end))
		{
			a = end;
			s = p / 4;
		}
		else
		{
			s = p / (2 * Mathf.PI) * Mathf.Asin(end / a);
		}
		
		return -(a * Mathf.Pow(2, 10 * (value-=1)) * Mathf.Sin((value * d - s) * (2 * Mathf.PI) / p)) + start;
	}

	private static float easeOutElastic(float start, float end, float value)
	{
		//Thank you to rafael.marteleto for fixing this as a port over from Pedro's UnityTween
		end -= start;
		
		float d = 1f;
		float p = d * .3f;
		float s = 0;
		float a = 0;
		
		if (value == 0) return start;
		
		if ((value /= d) == 1) return start + end;
		
		if (a == 0f || a < Mathf.Abs(end))
		{
			a = end;
			s = p * 0.25f;
		}
		else
		{
			s = p / (2 * Mathf.PI) * Mathf.Asin(end / a);
		}
		
		return (a * Mathf.Pow(2, -10 * value) * Mathf.Sin((value * d - s) * (2 * Mathf.PI) / p) + end + start);
	}

	private static float easeInOutElastic(float start, float end, float value)
	{
		end -= start;
		
		float d = 1f;
		float p = d * .3f;
		float s = 0;
		float a = 0;
		
		if (value == 0) return start;
		
		if ((value /= d*0.5f) == 2) return start + end;
		
		if (a == 0f || a < Mathf.Abs(end))
		{
            a = end;
            s = p / 4;
        }
		else
		{
            s = p / (2 * Mathf.PI) * Mathf.Asin(end / a);
        }
        
        if (value < 1)
			return -0.5f * (a * Mathf.Pow(2, 10 * (value-=1)) * Mathf.Sin((value * d - s) * (2 * Mathf.PI) / p)) + start;

        return a * Mathf.Pow(2, -10 * (value-=1)) * Mathf.Sin((value * d - s) * (2 * Mathf.PI) / p) * 0.5f + end + start;
    }

	public static void SetBool(string name, bool booleanValue) 
	{
		PlayerPrefs.SetInt(name, booleanValue ? 1 : 0);
	}
	
	public static bool GetBool(string name)  
	{
		return PlayerPrefs.GetInt(name) == 1 ? true : false;
	}

	#endregion

}
