﻿// GUI Animator for Unity UI version: 0.8.4 (Product page: http://ge-team.com/pages/unity-3d/gui-animator-for-unity-ui/)
//
// Author:			Gold Experience Team (http://www.ge-team.com/pages/)
// Products:		http://ge-team.com/pages/unity-3d/
// Support:			geteamdev@gmail.com
// Documentation:	http://ge-team.com/pages/unity-3d/gui-animator-for-unity-ui/gui-animator-for-unity-ui-documentation/
//
// Please direct any bugs/comments/suggestions to geteamdev@gmail.com

#region Namespaces

using UnityEngine;
using UnityEditor;
using System.Collections;

#endregion

/******************************************
* GEAnimSystemEditor class
* Custom editor for GEAnimSystem component
******************************************/

// http://docs.unity3d.com/ScriptReference/CustomEditor.html
// http://docs.unity3d.com/ScriptReference/CustomEditor-ctor.html
// http://unity3d.com/learn/tutorials/modules/intermediate/editor/adding-buttons-to-inspector
[CustomEditor(typeof(GEAnimSystem))]
// http://docs.unity3d.com/ScriptReference/Editor.html
public class GEAnimSystemEditor : Editor
{	
	#region Variables
	
		private Rect m_rectArea_Move;

	#endregion

	// ######################################################################
	// Editor functions
	// ######################################################################
	
	#region Editor functions

		// This function is called when the object is loaded
		void OnEnable()
		{
		}

		// Implement this function to make a custom inspector
		public override void OnInspectorGUI ()
		{
			// Begin a vertical control group
			// All controls rendered inside this element will be placed vertically below each other. The group must be closed with a call to EndVertical
			// http://docs.unity3d.com/ScriptReference/GUILayout.BeginVertical.html
			GUILayout.BeginVertical();
			{
				GUILayout.Space(5);
				
				// Draw the built-in inspector
				// http://docs.unity3d.com/ScriptReference/Editor.DrawDefaultInspector.html
				DrawDefaultInspector();
			
				// Insert a space in the current layout group
				GUILayout.Space(5);

				// Begin a vertical control group			
				// All controls rendered inside this element will be placed vertically below each other. The group must be closed with a call to EndVertical
				// http://docs.unity3d.com/ScriptReference/GUILayout.BeginVertical.html
				m_rectArea_Move = EditorGUILayout.BeginVertical();
				{
					// Insert a space in the current layout group
					GUILayout.Space(2);

					// Make a help box with a message to the user
					// http://docs.unity3d.com/ScriptReference/EditorGUI.HelpBox.html
					EditorGUILayout.HelpBox("-----------\r\n  GUI Speed\r\n-----------\r\n\r\n" +
					                        "    GUI Speed is amount between 0.1 to 10. This is global value that will multiply all Time and Delay of all GEAnim elements in the scene. You can adjust it anytime by using Inspector or scripts.\r\n\r\n" +
					                        "    To change this value in run-time via script.\r\n\r\n" +
					                        "       GEAnimSystem.Instance.m_GUISpeed = 10;\r\n\r\n" +
					                        "----------------\r\n  Auto Animation\r\n----------------\r\n\r\n" +
					                        "    This flag will effects both on Play mode (in Unity Editor) and run-time (on target device).\r\n\r\n" +
					                        "    If you want your whole scene plays animation automatically, this variable must be true.\r\n\r\n" +
					                        "    Set this value to false if you want to control all GEAnim in the scene via your scripts.\r\n\r\n" +
					                        "    In run-time you can change it once in Awake function (See Demo00 to Demo08 script files for example).\r\n\r\n" +
					                        "       GEAnimSystem.Instance.m_AutoAnimation = false;\r\n\r\n" +
					                        "-----------------\r\n  Animation Mode\r\n-----------------\r\n\r\n" +
					                        "    Animation to run automatically in Play mode when the \"Auto Animation\" flag is true.\r\n\r\n" +
					                        "    - None: Do not play animations.\r\n" +
					                        "    - In: Play In and Idle animations.\r\n" +
					                        "    - Idle: Play Idle as long as value in \"Idle Time\".\r\n" +
					                        "    - Out: Play Out animation.\r\n" +
					                        "    - All: Play In->Idle->Out animations.\r\n\r\n" +
					                        "-----------\r\n  Idle Time\r\n-----------\r\n\r\n" +
					                        "    Idle Time is amount of time in seconds for how long the Idle animation will play. This value works with \"Auto Animation\" and \"Animation Mode\"\r\n", MessageType.None);
				
					// Insert a space in the current layout group
					GUILayout.Space(2);
				}
				// Close a group started with BeginVertical
				// http://docs.unity3d.com/ScriptReference/GUILayout.EndVertical.html
				EditorGUILayout.EndVertical();

				// http://docs.unity3d.com/ScriptReference/GUILayout.Button.html
				if(GUILayout.Button("Online Documentation"))
				{
					// http://docs.unity3d.com/ScriptReference/Help.BrowseURL.html
					Help.BrowseURL("http://ge-team.com/pages/unity-3d/gui-animator-for-unity-ui/gui-animator-for-unity-ui-documentation/");
				}
			
				// Insert a space in the current layout group
				GUILayout.Space(10);
			}
			// Close a group started with BeginVertical
			// http://docs.unity3d.com/ScriptReference/GUILayout.EndVertical.html
			GUILayout.EndVertical();

		}
	
	#endregion
}
