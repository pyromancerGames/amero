#import "PollfishUnityBridge.h"
#import <Pollfish/Pollfish.h>

extern void UnitySendMessage(const char * className,const char * methodName, const char * param);

@implementation PollfishUnityBridge

NSString *gameObj; // the main game object in Unity that will listen for the events


//Loads before application's didFinishLaunching method is called (i.e. when this plugin
//is added to the Objective C runtime)
+ (void)load
{
    // Register for Pollfish notifications to inform Unity
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(surveyCompleted:)
                                                 name:@"PollfishSurveyCompleted" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(surveyOpened:)
                                                 name:@"PollfishOpened" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(surveyClosed:)
                                                 name:@"PollfishClosed" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(surveyReceived:)
                                                 name:@"PollfishSurveyReceived" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(surveyNotAvailable:)
                                                 name:@"PollfishSurveyNotAvailable" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userNotEligible:)
                                                 name:@"PollfishUserNotEligible" object:nil];
    
}


//iOS-to-Unity, methods get called when the notification triggers

+ (void)surveyCompleted:(NSNotification *)notification
{
    NSString *playfulSurveyStr = (NSString *)[[notification userInfo] valueForKey:@"playfulSurvey"];
  
    int surveyPrice = [[[notification userInfo] valueForKey:@"surveyPrice"] intValue];

    const char *playfulSurveyAndSurveyPrice = [[NSString stringWithFormat:@"%@,%d",playfulSurveyStr,surveyPrice] UTF8String];

    UnitySendMessage([gameObj UTF8String],"surveyCompleted",playfulSurveyAndSurveyPrice);
}

+ (void)surveyOpened:(NSNotification *)notification
{
    UnitySendMessage([gameObj UTF8String],"surveyOpened","");
}

+ (void)surveyClosed:(NSNotification *)notification
{
    UnitySendMessage([gameObj UTF8String],"surveyClosed","");
}

+ (void)surveyReceived:(NSNotification *)notification
{
    NSString *playfulSurveyStr = (NSString *)[[notification userInfo] valueForKey:@"playfulSurvey"];
   
    int surveyPrice = [[[notification userInfo] valueForKey:@"surveyPrice"] intValue];
    
    const char *playfulSurveyAndSurveyPrice = [[NSString stringWithFormat:@"%@,%d",playfulSurveyStr,surveyPrice] UTF8String];

    UnitySendMessage([gameObj UTF8String],"surveyReceived",playfulSurveyAndSurveyPrice);
}

+ (void)userNotEligible:(NSNotification *)notification
{
    UnitySendMessage([gameObj UTF8String],"userNotEligible","");
}

+ (void)surveyNotAvailable:(NSNotification *)notification
{
    UnitySendMessage([gameObj UTF8String],"surveyNotAvailable","");
}
@end

//Unity-to-iOS, will get called when Unity method is called.
extern "C"
{
    void PollfishInit(int position, int padding, const char *apiKey, bool debugMode, bool customMode) {

        [Pollfish initAtPosition:position
                     withPadding: padding
                 andDeveloperKey: [NSString stringWithUTF8String: apiKey ? apiKey : ""]
                   andDebuggable: debugMode
                   andCustomMode: customMode];
    }
    
    void PollfishInitWithRequestUUID(int position, int padding, const char *apiKey, bool debugMode, bool customMode, const char *request_uuid) {
        
        [Pollfish initAtPosition:position
                     withPadding: padding
                 andDeveloperKey: [NSString stringWithUTF8String: apiKey ? apiKey : ""]
                   andDebuggable: debugMode
                   andCustomMode: customMode
                   andRequestUUID: [NSString stringWithUTF8String:request_uuid? request_uuid : ""]];
    }
    
    void  ShowPollfishFunction(){
        [Pollfish show];
    }
    
    void  HidePollfishFunction(){
        [Pollfish hide];
        
    }
    
    void SetEventObjectNamePollfish(const char * gameObjName) {

        gameObj =[[NSString stringWithUTF8String: gameObjName ? gameObjName : ""] copy];
    }
    
    void SetAttributeDictionaryPollfish(const char * attributes)
    {
        NSString *attris = [NSString stringWithUTF8String:attributes];
        NSArray *attributesArray = [attris componentsSeparatedByString:@"\n"];
            
        NSMutableDictionary *oAttributes = [[NSMutableDictionary alloc] init];
        
        for (int i=0; i < [attributesArray count]; i++) {
            
            NSString *keyValuePair = [attributesArray objectAtIndex:i];
            NSRange range = [keyValuePair rangeOfString:@"="];
            
            if (range.location != NSNotFound) {
                
                NSString *key = [keyValuePair substringToIndex:range.location];
                NSString *value = [keyValuePair substringFromIndex:range.location+1];
                
                [oAttributes setObject:value forKey:key];
            }
        }
  
        [Pollfish setAttributeDictionary:oAttributes];
    
    }
}
