#!/bin/sh
export LC_ALL="en_US.UTF-8"

for arg in $*
do
	cd $arg && pod install
done