﻿using UnityEngine;
using System;
using System.Runtime.InteropServices;
using System.Collections.Generic;

public enum Position
{
	TOP_LEFT=0,
	BOTTOM_LEFT,
	TOP_RIGHT,
	BOTTOM_RIGHT,
	MIDDLE_LEFT,
	MIDDLE_RIGHT 
}

#if UNITY_ANDROID || UNITY_IPHONE

public class Pollfish
{
	#if UNITY_ANDROID 

	private static AndroidJavaClass mPollfishAssistantClass;

	static Pollfish()
	{
		if( Application.platform != RuntimePlatform.Android )
			return;
		
		Debug.Log ("Pollfish - Pollfish()");
	
		mPollfishAssistantClass = new AndroidJavaClass ("com.pollfish_unity.main.PollfishPlugin");
	}

	#elif UNITY_IPHONE

	#region API

	// Pollfish iOS Bridge functions

	[DllImport ("__Internal")]
	private static extern void PollfishInit(int position, int padding, string developerKey, bool debuggable, bool customMode);

	[DllImport ("__Internal")]
	private static extern void PollfishInitWithRequestUUID(int position, int padding, string developerKey, bool debuggable, bool customMode, string request_uuid);

	[DllImport ("__Internal")]
	private static extern void ShowPollfishFunction();

	[DllImport ("__Internal")]
	private static extern void HidePollfishFunction();

	[DllImport ("__Internal")]
	private static extern void SetEventObjectNamePollfish(string gameObjName);

	[DllImport ("__Internal")]
	private static extern void SetAttributeDictionaryPollfish(string attrDict);



	#endregion

	#endif

	// Pollfish init
	public static void PollfishInitFunction(int position, int padding, string apiKeyKey, bool debugMode, bool customMode){

		#if UNITY_ANDROID
	
		if(!customMode)
			mPollfishAssistantClass.CallStatic("init",apiKeyKey,position, padding); 
		else
			mPollfishAssistantClass.CallStatic("customInit",apiKeyKey,position, padding); 

		#elif UNITY_IPHONE

			PollfishInit(position,padding,apiKeyKey,debugMode,customMode);

		#endif
	}

	// Pollfish init with request uuid for s2s postback call
	public static void PollfishInitFunction(int position, int padding, string apiKeyKey, bool debugMode, bool customMode, string request_uuid){
		
		#if UNITY_ANDROID
		
		if(!customMode)
			mPollfishAssistantClass.CallStatic("init",apiKeyKey,position, padding,request_uuid); 
		else
			mPollfishAssistantClass.CallStatic("customInit",apiKeyKey,position, padding,request_uuid); 
		
		#elif UNITY_IPHONE
		
		PollfishInitWithRequestUUID(position,padding,apiKeyKey,debugMode,customMode,request_uuid);
		
		#endif
	}

	// Specify object to receive Unity messages
	public static void SetEventObjectPollfish(string gameObjNmae)
	{
		#if UNITY_ANDROID
		
		mPollfishAssistantClass.CallStatic("setEventObjectPollfish", gameObjNmae);
		
		#elif UNITY_IPHONE

		SetEventObjectNamePollfish(gameObjNmae);

		#endif
	}
	
	// Manually show Pollfish.
	public static void ShowPollfish()
	{
		#if UNITY_ANDROID

		mPollfishAssistantClass.CallStatic("show");

		#elif UNITY_IPHONE
				
		ShowPollfishFunction ();

		#endif
	}
	
	
	// Manually hide Pollfish.
	public static void HidePollfish()
	{
		#if UNITY_ANDROID
		
		mPollfishAssistantClass.CallStatic("hide");

		#elif UNITY_IPHONE

		HidePollfishFunction();

		#endif
	}


	
	// Set attributes Pollfish.
	public static void SetAttributesPollfish(Dictionary<string,string> attrDict)
	{

		Debug.Log ("Pollfish - SetAttributesPollfish");
		

		#if UNITY_ANDROID

		using(AndroidJavaObject obj_HashMap = new AndroidJavaObject("java.util.HashMap"))
		{
			// Call 'put' via the JNI instead of using helper classes to avoid:
			//  "JNI: Init'd AndroidJavaObject with null ptr!"
			IntPtr method_Put = AndroidJNIHelper.GetMethodID(obj_HashMap.GetRawClass(), "put",
			                                                 "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;");
			
			object[] args = new object[2];

			foreach(KeyValuePair<string, string> kvp in attrDict)
			{
				using(AndroidJavaObject k = new AndroidJavaObject("java.lang.String", kvp.Key))
				{
					using(AndroidJavaObject v = new AndroidJavaObject("java.lang.String", kvp.Value))
					{
						args[0] = k;
						args[1] = v;

						AndroidJNI.CallObjectMethod(obj_HashMap.GetRawObject(),
						                            method_Put, AndroidJNIHelper.CreateJNIArgArray(args));
					}
				}
			}

			mPollfishAssistantClass.CallStatic("setAttributes", obj_HashMap);

			}
			
			#elif UNITY_IPHONE

			string attributesString = "";

			foreach(KeyValuePair<string, string> kvp in attrDict)
			{
				attributesString += kvp.Key + "=" + kvp.Value + "\n";
			}

			SetAttributeDictionaryPollfish(attributesString);
			
			#endif


	}


	// decide if you should quit your app on back button event (Pollfish is not open)
	public static void ShouldQuit()
	{	
		#if UNITY_ANDROID

		mPollfishAssistantClass.CallStatic<bool> ("shouldQuit");

		#endif
	}


	#region Events

	public static event Action surveyOpenedEvent;
	public static event Action surveyClosedEvent;
	public static event Action <string> surveyReceivedEvent;
	public static event Action <string> surveyCompletedEvent;
	public static event Action surveyNotAvailableEvent;
	public static event Action userNotEligibleEvent;

	#if UNITY_ANDROID

	/* events for back button android */

	public static event Action shouldQuitEvent;
	public static event Action shouldNotQuitEvent;

	public void shouldQuit()
	{							
		if (shouldQuitEvent != null)
			shouldQuitEvent ();
	}

	public void shouldNotQuit()
	{
		if (shouldNotQuitEvent != null)
			shouldNotQuitEvent ();
	}

	#endif

	
	public void surveyCompleted(string playfulSurveyAndSurveyPrice)
	{
		if (surveyCompletedEvent != null)
			surveyCompletedEvent (playfulSurveyAndSurveyPrice);
	}

	public void surveyReceived(string playfulSurveyAndSurveyPrice)
	{
		
		if (surveyReceivedEvent != null)
			surveyReceivedEvent (playfulSurveyAndSurveyPrice);
	}
	
	public void surveyOpened()
	{
		if (surveyOpenedEvent != null)
			surveyOpenedEvent();
	}
	
	public void surveyClosed()
	{
		if (surveyClosedEvent != null)
			surveyClosedEvent();
	}

	public void surveyNotAvailable()
	{	
		if (surveyNotAvailableEvent != null)
			surveyNotAvailableEvent();
	}

	public void userNotEligible()
	{
		if (userNotEligibleEvent != null)
			userNotEligibleEvent();
	}

	#endregion

}

#endif
	

